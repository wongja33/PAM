!> \file
!> \brief Get width of diagnostic sections
!!
!! @author K. von Salzen
!
!-------------------------------------------------------------------
subroutine diagsdw(fphis,fdphi0s)
  !
  use sdparm, only : aextf,aintf,isaext,isaint,kext,r0,ylarge
  use compar, only : isdnum
  !
  implicit none
  !
  real, intent(out) :: fphis !< Dry particle size at the lower boundary of the size sections
  real, intent(out) :: fdphi0s !< Diagnostic section width
  !
  !     internal work variables
  !
  real :: fphie !<
  real :: fphit !<
  real :: rads !<
  real :: rade !<
  integer :: kx !<
  !
  integer, parameter :: imeth=2 !<
  !-------------------------------------------------------------------
  !
  !
  if (imeth == 1) then
    !
    !       * width of diagnostic sections. the size range covered
    !       * by the diagnostic size distribution spans the range from
    !       * the smallest to the largest simulated particle size for
    !       * internally and externally mixed aerosol.
    !
    fphis= ylarge
    fphie=-ylarge
    if (isaint > 0) then
      fphis=log(aintf%radb/r0)
      fphie=fphis+isaint*aintf%dpstar
    end if
    if (isaext > 0) then
      do kx=1,kext
        fphit=log(aextf%tp(kx)%radb/r0)
        fphis=min(fphit,fphis)
        fphie=max(fphit+real( &
                   aextf%tp(kx)%isec)*aextf%tp(kx)%dpstar,fphie)
      end do
    end if
  else
    !
    !       * fixed, specified range.
    !
    rads=0.008e-06
    rade=0.4e-06
    fphis=log(rads/r0)
    fphie=log(rade/r0)
  end if
  fdphi0s=(fphie-fphis)/real(isdnum)
  !
end subroutine diagsdw
