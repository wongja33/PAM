!> \file
!> \brief Integration of aerosol size distribution for diagnostic purposes.
!>       A filter is available for removing unresolved/unrealistic features
!>       of the size distribution.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine intsd(fvolo,fphis0,fdphi0,pnum,pmas,pn0,phi0, &
                       psi,phis0,dphi0,wetrc,dryrc, &
                       isdiag,nfilt,ilga,leva,isec)
  !
  use sdparm, only : r0,ycnst,yna,ytiny
  use sdcode, only : sdintb
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in) :: isdiag !< Number of diagnostic sections
  integer, intent(in) :: nfilt !< Filter for removing unresolved/unrealistic features of the size distribution
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: pnum !< Aerosol number concentration \f$[1/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pmas !< Aerosol (dry) mass concentration \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$, mode size)
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$, width)
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(isec)              :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isdiag)  :: fphis0 !< Filtered particle sizes corresponding to section boundaries.
  real, intent(in), dimension(ilga,leva,isdiag)  :: fdphi0 !< Filtered width of diagnostic sections.
  real, intent(out), dimension(ilga,leva,isdiag) :: fvolo !< Filtered volume size distribution
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,isdiag) :: fmom3 !<
  integer :: is !<
  integer :: isc !<
  integer :: isd !<
  integer :: ist !<
  integer :: isx !<
  integer :: il !<
  integer :: l !<
  real :: alfgr !<
  real :: fgr !<
  real :: fph1 !<
  real :: fph2 !<
  real :: fphis1 !<
  real :: frac !<
  real :: fphie1 !<
  real :: fdphi1 !<
  real :: phi0t !<
  real :: rmom3 !<
  !
  !-----------------------------------------------------------------------
  !     * integrate pla size distribution over given sub-sections.
  !
  rmom3=3.
  fmom3=0.
  if (isec > 0) then
    do is=1,isec
      do isd=1,isdiag
        do l=1,leva
          do il=1,ilga
            fph1=phis0(il,l,is)
            fph2=phis0(il,l,is)+dphi0(il,l,is)
            if (pnum(il,l,is) > ytiny .and. pmas(il,l,is) > ytiny &
                .and. abs(psi(il,l,is)-yna) > ytiny) then
              fgr=wetrc(il,l,is)/dryrc(is)
              alfgr=log(fgr)
              fph1=fph1+alfgr
              fph2=fph2+alfgr
            else
              alfgr=0.
            end if
            if (    (fphis0(il,l,isd) >= fph1 &
                .and. fphis0(il,l,isd) <= fph2) &
                .or. ( (fphis0(il,l,isd)+fdphi0(il,l,isd)) >= fph1 &
                .and. (fphis0(il,l,isd)+fdphi0(il,l,isd)) <= fph2) ) then
              !
              !           * for first part of the section.
              !
              fphis1=max(fphis0(il,l,isd),fph1)
              fphie1=min(fphis0(il,l,isd)+fdphi0(il,l,isd),fph2)
              fdphi1=max(fphie1-fphis1,0.)
              phi0t=phi0(il,l,is)+alfgr
              if (abs(psi(il,l,is)-yna) > ytiny) then
                fmom3(il,l,isd)=fmom3(il,l,isd)+ycnst*pn0(il,l,is) &
                      *sdintb(phi0t,psi(il,l,is),rmom3,fphis1,fdphi1)
              end if
            end if
          end do
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * filter the volume size distribution. the number of sub-sections
  !     * over the which the filter is applied is 2*nfilt+1.
  !
  frac=1./real(2*nfilt+1)
  fvolo=fmom3
  do isx=1,isdiag
    do isc=1,nfilt
      ist=isx+isc
      if (ist <= isdiag) then
        fvolo(:,:,isx)=fvolo(:,:,isx)+fmom3(:,:,ist)
      end if
    end do
    do isc=1,nfilt
      ist=isx-isc
      if (ist >= 1) then
        fvolo(:,:,isx)=fvolo(:,:,isx)+fmom3(:,:,ist)
      end if
    end do
    fvolo(:,:,isx)=fvolo(:,:,isx)*frac
  end do
  !
end subroutine intsd
