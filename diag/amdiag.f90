!>    \file
!>    \brief Aerosol mass concentrations
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
!
subroutine amdiag(acas,acoa,acbc,acss,acmd,pemas,pimas,pifrc, &
                        rhoa,ilga,leva)
  !-----------------------------------------------------------------------
  !
  use sdparm, only : iexbc,iexmd,iexoc,iexso4,iexss, &
                         iinbc,iinmd,iinoc,iinso4,iinss, &
                         isaext,isaint, &
                         isextbc,isextmd,isextoc,isextso4,isextss, &
                         isintbc,isintmd,isintoc,isintso4,isintss, &
                         kextbc,kextmd,kextoc,kextso4,kextss, &
                         kintbc,kintmd,kintoc,kintso4,kintss, kint
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva) :: acas !< Ammonium sulphate mass concentration \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva) :: acoa !< Organic aerosol mass concentration \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva) :: acbc !< Black carbon mass concentration \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva) :: acss !< Sea salt mass concentration \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva) :: acmd !< Mineral dust mass concentration \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  integer :: is !<
  !
  !     calculate ammonium sulphate mass concentration
  !
  acas=0.
  if (kextso4 > 0) then
    do is=1,isextso4
      acas=acas+pemas(:,:,iexso4(is))
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      acas=acas+pimas(:,:,iinso4(is))*pifrc(:,:,iinso4(is),kintso4)
    end do
  end if
  acas=acas*rhoa
  !
  !     calculate organic aerosol mass concentration
  !
  acoa=0.
  if (kextoc > 0) then
    do is=1,isextoc
      acoa=acoa+pemas(:,:,iexoc(is))
    end do
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      acoa=acoa+pimas(:,:,iinoc(is))*pifrc(:,:,iinoc(is),kintoc)
    end do
  end if
  acoa=acoa*rhoa
  !
  !     calculate black carbon mass concentration.
  !
  acbc=0.
  if (kextbc > 0) then
    do is=1,isextbc
      acbc=acbc+pemas(:,:,iexbc(is))
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      acbc=acbc+pimas(:,:,iinbc(is))*pifrc(:,:,iinbc(is),kintbc)
    end do
  end if
  acbc=acbc*rhoa
  !
  !     calculate sea salt mass concentration.
  !
  acss=0.
  if (kextss > 0) then
    do is=1,isextss
      acss=acss+pemas(:,:,iexss(is))
    end do
  end if
  if (kintss > 0) then
    do is=1,isintss
      acss=acss+pimas(:,:,iinss(is))*pifrc(:,:,iinss(is),kintss)
    end do
  end if
  acss=acss*rhoa
  !
  !     calculate mineral dust mass concentration.
  !
  acmd=0.
  if (kextmd > 0) then
    do is=1,isextmd
      acmd=acmd+pemas(:,:,iexmd(is))
    end do
  end if
  if (kintmd > 0) then
    do is=1,isintmd
      acmd=acmd+pimas(:,:,iinmd(is))*pifrc(:,:,iinmd(is),kintmd)
    end do
  end if
  acmd=acmd*rhoa
  !
end subroutine amdiag
