!> \file
!> \brief Diagnostic aerosol parameters internal to PAM.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pamdi(cnt,cn20,cn50,cn100,cn200,cntw,cn20w,cn50w, &
                       cn100w,cn200w,cc02,cc04,cc08,cc16,acas,acoa, &
                       acbc,acss,acmd,sups,ccn,ccne,rcri,vccn,vcne, &
                       pemas,penum,pen0,pephi0,pepsi,pephis0, &
                       pedphi0,pecdnc,peddn,pimas,pinum,pin0, &
                       piphi0,pipsi,piphis0,pidphi0,picdnc,piddn, &
                       pifrc,pirci,ceddnis,ceddnsl,cenuio,cemolw, &
                       cekappa,ceepsm,cewetrb,ciddnis,ciddnsl,cinuio, &
                       cimolw,cikappa,ciepsm,ciwetrb,ta,rhoa,szclf, &
                       szmlwc,svf,vitrm,zcthr,svref,ilga,leva)
  !
  use sdparm, only : iexkap,iinkap,isaext,isaint,isaintb, &
                         kext,kint, &
                         pedryrb,pedryrc,pidryr,pidryrb,pidryrc, &
                         sextf,ysmall
  use scparm, only : isextb,isintb
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga) :: vccn !<
  real, intent(out), dimension(ilga) :: vcne !<
  real, intent(out), dimension(ilga,leva)   :: cnt !<
  real, intent(out), dimension(ilga,leva)   :: cn20 !<
  real, intent(out), dimension(ilga,leva)   :: cn50 !<
  real, intent(out), dimension(ilga,leva)   :: cn100 !<
  real, intent(out), dimension(ilga,leva)   :: cn200 !<
  real, intent(out), dimension(ilga,leva)   :: cntw !<
  real, intent(out), dimension(ilga,leva)   :: cn20w !<
  real, intent(out), dimension(ilga,leva)   :: cn50w !<
  real, intent(out), dimension(ilga,leva)   :: cn100w !<
  real, intent(out), dimension(ilga,leva)   :: cn200w !<
  real, intent(out), dimension(ilga,leva) :: acas !<
  real, intent(out), dimension(ilga,leva) :: acoa !<
  real, intent(out), dimension(ilga,leva) :: acbc !<
  real, intent(out), dimension(ilga,leva) :: acss !<
  real, intent(out), dimension(ilga,leva) :: acmd !<
  real, intent(out), dimension(ilga,leva) :: sups !<
  real, intent(out), dimension(ilga,leva) :: ccn !<
  real, intent(out), dimension(ilga,leva) :: rcri !<
  real, intent(out), dimension(ilga,leva) :: cc02 !<
  real, intent(out), dimension(ilga,leva) :: cc04 !<
  real, intent(out), dimension(ilga,leva) :: cc08 !<
  real, intent(out), dimension(ilga,leva) :: cc16 !<
  real, intent(out), dimension(ilga,leva) :: ccne !<
  !
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnis !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnsl !<
  real, intent(in), dimension(ilga,leva,isextb) :: cenuio !<
  real, intent(in), dimension(ilga,leva,isextb) :: cemolw !<
  real, intent(in), dimension(ilga,leva,isextb) :: cekappa !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceepsm !<
  real, intent(in), dimension(ilga,leva,isextb) :: cewetrb !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnis !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnsl !<
  real, intent(in), dimension(ilga,leva,isintb) :: cinuio !<
  real, intent(in), dimension(ilga,leva,isintb) :: cimolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: cikappa !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciepsm !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(in), dimension(ilga,leva) :: ta !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva) :: szclf !<
  real, intent(in), dimension(ilga,leva) :: szmlwc !<
  real, intent(in), dimension(ilga,leva) :: svf !<
  real, intent(in), dimension(ilga,leva) :: vitrm !<
  real, intent(in), dimension(ilga,leva,kext) :: pecdnc !<
  real, intent(in), dimension(ilga,leva) :: picdnc !<
  real, intent(in), dimension(ilga,leva) :: pirci !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephis0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pedphi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !<
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  !
  logical, dimension(ilga,leva) :: kcldf !<
  real, dimension(ilga,leva) :: rads !<
  real, dimension(ilga,leva) :: awgt !<
  real, dimension(ilga,leva) :: sv !<
  real, dimension(ilga,leva) :: rhta !<
  integer, allocatable, dimension(:) :: peinds !<
  integer, allocatable, dimension(:) :: peinde !<
  real, allocatable, dimension(:,:,:,:) :: peradc !<
  real, allocatable, dimension(:,:,:) :: piradc !<
  real, allocatable, dimension(:,:,:) :: cecdnc !<
  real, allocatable, dimension(:,:,:) :: cercit !<
  real, allocatable, dimension(:,:)   :: cicdnc !<
  real, allocatable, dimension(:,:)   :: circit !<
  real, allocatable, dimension(:,:,:) :: cerc !<
  real, allocatable, dimension(:,:,:) :: cesc !<
  real, allocatable, dimension(:,:,:) :: pewetrc !<
  real, allocatable, dimension(:,:,:) :: circ !<
  real, allocatable, dimension(:,:,:) :: cisc !<
  real, allocatable, dimension(:,:,:) :: piwetrc !<
  real, allocatable, dimension(:,:)   :: emcdnc !<
  integer :: is !<
  integer :: kx !<
  integer :: kxm !<
  integer :: nc !<
  integer :: l !<
  integer :: il !<
  real :: rmax !<
  real :: zcthr !<
  real :: svref !<
  !
  integer, parameter :: nct=5 ! number of diagnosed cn categories
  real, dimension(nct) :: radc0 ! droplet radius threshold
  data radc0 / 0., 10.e-09, 25.e-09, 50.e-09, 100.e-09 /
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for pla section size parameter arrays.
  !
  allocate(emcdnc(ilga,leva))
  if (isaext > 0) then
    allocate(peinds(kext))
    allocate(peinde(kext))
    allocate(peradc(ilga,leva,kext,nct))
    allocate(pewetrc(ilga,leva,isaext))
  end if
  if (kext > 0) then
    allocate(cecdnc(ilga,leva,kext))
    allocate(cercit(ilga,leva,kext))
  end if
  if (isextb > 0) then
    allocate(cerc(ilga,leva,isextb))
    allocate(cesc(ilga,leva,isextb))
  end if
  if (isaint > 0) then
    allocate(piradc(ilga,leva,nct))
    allocate(piwetrc(ilga,leva,isaint))
  end if
  if (kint > 0) then
    allocate(cicdnc(ilga,leva))
    allocate(circit(ilga,leva))
  end if
  if (isintb > 0) then
    allocate(circ(ilga,leva,isintb))
    allocate(cisc(ilga,leva,isintb))
  end if
  !
  !-----------------------------------------------------------------------
  !     * for externally mixed aerosol, determine the size section
  !     * range for each individual type.
  !
  if (isaext > 0) then
    is=1
    kx=sextf%isaer(is)%ityp
    peinds(kx)=is
    do is=2,isaext
      kx=sextf%isaer(is)%ityp
      kxm=sextf%isaer(is-1)%ityp
      if (kx/=kxm) then
        peinds(kx)=is
        peinde(kxm)=is-1
      end if
    end do
    is=isaext
    kx=sextf%isaer(is)%ityp
    peinde(kx)=is
  end if
  !
  !-----------------------------------------------------------------------
  !     * aerosol number concentration for dry droplet size thresholds.
  !
  if (isaext > 0) then
    do nc=1,nct
      peradc(:,:,:,nc)=radc0(nc)
    end do
  end if
  if (isaint > 0) then
    do nc=1,nct
      piradc(:,:,nc)=radc0(nc)
    end do
  end if
  call cndiag (cnt,cn20,cn50,cn100,cn200,peradc,piradc, &
                   pen0,pephi0,pepsi,pephis0,pedphi0, &
                   pin0,piphi0,pipsi,piphis0,pidphi0, &
                   peinds,peinde,nct,ilga,leva)
  cnt   =cnt   *rhoa*1.e-06
  cn20  =cn20  *rhoa*1.e-06
  cn50  =cn50  *rhoa*1.e-06
  cn100 =cn100 *rhoa*1.e-06
  cn200 =cn200 *rhoa*1.e-06
  !
  !     * aerosol number concentration for wet droplet size thresholds
  !     * at 60% relative humidity.
  !
  rhta=0.6
  call sdaprop (pewetrc,piwetrc,pen0,pephi0,pepsi,peddn, &
                    pin0,piphi0,pipsi,pifrc,piddn,ta,rhta, &
                    ilga,leva)
  if (isaext > 0) then
    do nc=1,nct
      do kx=1,kext
        rads(:,:)=pedryrb(peinds(kx))*pewetrc(:,:,peinds(kx)) &
                     /pedryrc(peinds(kx))
        where (radc0(nc) < rads(:,:) )
          peradc(:,:,kx,nc)=pedryrb(peinds(kx))
        else where (radc0(nc) < pewetrc(:,:,peinds(kx)) )
          peradc(:,:,kx,nc)=radc0(nc)*pedryrc(peinds(kx)) &
                               /pewetrc(:,:,peinds(kx))
        end where
        do is=peinds(kx),peinde(kx)-1
          where (radc0(nc) >= pewetrc(:,:,is) &
              .and. radc0(nc)  < pewetrc(:,:,is+1) )
            awgt(:,:)=(radc0(nc)-pewetrc(:,:,is)) &
                         /(pewetrc(:,:,is+1)-pewetrc(:,:,is))
            peradc(:,:,kx,nc)=(1.-awgt(:,:))*pedryrc(is) &
                                 +awgt(:,:)*pedryrc(is+1)
          end where
        end do
        rads(:,:)=pedryrb(peinde(kx))*pewetrc(:,:,peinde(kx)) &
                     /pedryrc(peinde(kx))
        where (radc0(nc) >= rads(:,:) )
          peradc(:,:,kx,nc)=pedryrb(peinde(kx))
        else where (radc0(nc) >= pewetrc(:,:,peinde(kx)) )
          peradc(:,:,kx,nc)=radc0(nc)*pedryrc(peinde(kx)) &
                               /pewetrc(:,:,peinde(kx))
        end where
      end do
    end do
  end if
  if (isaint > 0) then
    do nc=1,nct
      rads(:,:)=pidryrb(1)*piwetrc(:,:,1)/pidryrc(1)
      where (radc0(nc) < rads(:,:) )
        piradc(:,:,nc)=pidryrb(1)
      else where (radc0(nc) < piwetrc(:,:,1) )
        piradc(:,:,nc)=radc0(nc)*pidryrc(1)/piwetrc(:,:,1)
      end where
      do is=1,isaint-1
        where (radc0(nc) >= piwetrc(:,:,is) &
            .and. radc0(nc)  < piwetrc(:,:,is+1) )
          awgt(:,:)=(radc0(nc)-piwetrc(:,:,is)) &
                       /(piwetrc(:,:,is+1)-piwetrc(:,:,is))
          piradc(:,:,nc)=(1.-awgt(:,:))*pidryrc(is) &
                                +awgt(:,:)*pidryrc(is+1)
        end where
      end do
      rads(:,:)=pidryrb(isaintb)*piwetrc(:,:,isaint) &
                   /pidryrc(isaint)
      where (radc0(nc) >= rads(:,:) )
        piradc(:,:,nc)=pidryrb(isaintb)
      else where (radc0(nc) >= piwetrc(:,:,isaint) )
        piradc(:,:,nc)=radc0(nc)*pidryrc(isaint) &
                          /piwetrc(:,:,isaint)
      end where
    end do
  end if
  call cndiag (cntw,cn20w,cn50w,cn100w,cn200w,peradc,piradc, &
                   pen0,pephi0,pepsi,pephis0,pedphi0, &
                   pin0,piphi0,pipsi,piphis0,pidphi0, &
                   peinds,peinde,nct,ilga,leva)
  cntw  =cntw  *rhoa*1.e-06
  cn20w =cn20w *rhoa*1.e-06
  cn50w =cn50w *rhoa*1.e-06
  cn100w=cn100w*rhoa*1.e-06
  cn200w=cn200w*rhoa*1.e-06
  !
  !-----------------------------------------------------------------------
  !     * aerosol mass concentrations.
  !
  call amdiag(acas,acoa,acbc,acss,acmd,pemas,pimas,pifrc, &
                  rhoa,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * cloud indicator flag.
  !
  where ( (1.-szclf) < zcthr .and. szmlwc > ysmall)
    kcldf=.true.
  else where
    kcldf=.false.
  end where
  !
  !     * diagnose supersaturation.
  !
  where (kcldf)
    sups=svf
  else where
    sups=0.
  end where
  !
  !     * diagnose critical radius and cloud droplet number.
  !
  vccn=0.
  ccn=0.
  rcri=0.
  if (kext > 0) then
    do kx=1,kext
      where (kcldf)
        ccn=ccn+pecdnc(:,:,kx)*rhoa*1.e-06
      end where
      do l=1,leva
        do il=1,ilga
          if (kcldf(il,l) ) then
            vccn(il)=vccn(il)+vitrm(il,l)*pecdnc(il,l,kx)*1.e-06
          end if
        end do
      end do
    end do
  end if
  if (kint > 0) then
    rmax=pidryr(isaint)%vr
    where (kcldf .and. pirci < rmax)
      rcri=rcri+pirci*1.e+06
    end where
    where (kcldf)
      ccn=ccn+picdnc*rhoa*1.e-06
    end where
    do l=1,leva
      do il=1,ilga
        if (kcldf(il,l) ) then
          vccn(il)=vccn(il)+vitrm(il,l)*picdnc(il,l)*1.e-06
        end if
      end do
    end do
  end if

  !
  !-----------------------------------------------------------------------
  !     * calculate ccn concentrations for 0.2% supersaturation.
  !
  sv=svref
  call ccnpar(cerc,cesc,circ,cisc,ta,ceddnsl,ceddnis, &
                  cenuio,cekappa,cemolw,ciddnsl,ciddnis,cinuio, &
                  cikappa,cimolw,ciepsm,sv,ilga,leva, &
                  iexkap,iinkap)
  call cncdnca(cecdnc,cercit,cicdnc,circit,cewetrb,cerc,cesc, &
                   ciwetrb,circ,cisc,pen0,pephi0,pepsi,pephis0, &
                   pedphi0,pin0,piphi0,pipsi,piphis0,pidphi0,sv, &
                   ilga,leva)
  !
  !     * save results.
  !
  cc02=0.
  if (kext > 0) then
    do kx=1,kext
      cc02=cc02+cecdnc(:,:,kx)*rhoa*1.e-06
    end do
  end if
  if (kint > 0) then
    cc02=cc02+cicdnc*rhoa*1.e-06
  end if
  !
  !     * calculate ccn concentrations for 0.4% supersaturation.
  !
  sv=0.004
  call ccnpar(cerc,cesc,circ,cisc,ta,ceddnsl,ceddnis, &
                  cenuio,cekappa,cemolw,ciddnsl,ciddnis,cinuio, &
                  cikappa,cimolw,ciepsm,sv,ilga,leva, &
                  iexkap,iinkap)
  call cncdnca(cecdnc,cercit,cicdnc,circit,cewetrb,cerc,cesc, &
                   ciwetrb,circ,cisc,pen0,pephi0,pepsi,pephis0, &
                   pedphi0,pin0,piphi0,pipsi,piphis0,pidphi0,sv, &
                   ilga,leva)
  !
  !     * save results.
  !
  cc04=0.
  if (kext > 0) then
    do kx=1,kext
      cc04=cc04+cecdnc(:,:,kx)*rhoa*1.e-06
    end do
  end if
  if (kint > 0) then
    cc04=cc04+cicdnc*rhoa*1.e-06
  end if
  !
  !     * calculate ccn concentrations for 0.8% supersaturation.
  !
  sv=0.008
  call ccnpar(cerc,cesc,circ,cisc,ta,ceddnsl,ceddnis, &
                  cenuio,cekappa,cemolw,ciddnsl,ciddnis,cinuio, &
                  cikappa,cimolw,ciepsm,sv,ilga,leva, &
                  iexkap,iinkap)
  call cncdnca(cecdnc,cercit,cicdnc,circit,cewetrb,cerc,cesc, &
                   ciwetrb,circ,cisc,pen0,pephi0,pepsi,pephis0, &
                   pedphi0,pin0,piphi0,pipsi,piphis0,pidphi0,sv, &
                   ilga,leva)
  !
  !     * save results.
  !
  cc08=0.
  if (kext > 0) then
    do kx=1,kext
      cc08=cc08+cecdnc(:,:,kx)*rhoa*1.e-06
    end do
  end if
  if (kint > 0) then
    cc08=cc08+cicdnc*rhoa*1.e-06
  end if
  !
  !     * calculate ccn concentrations for 1.6% supersaturation.
  !
  sv=0.016
  call ccnpar(cerc,cesc,circ,cisc,ta,ceddnsl,ceddnis, &
                  cenuio,cekappa,cemolw,ciddnsl,ciddnis,cinuio, &
                  cikappa,cimolw,ciepsm,sv,ilga,leva, &
                  iexkap,iinkap)
  call cncdnca(cecdnc,cercit,cicdnc,circit,cewetrb,cerc,cesc, &
                   ciwetrb,circ,cisc,pen0,pephi0,pepsi,pephis0, &
                   pedphi0,pin0,piphi0,pipsi,piphis0,pidphi0,sv, &
                   ilga,leva)
  !
  !     * save results.
  !
  cc16=0.
  if (kext > 0) then
    do kx=1,kext
      cc16=cc16+cecdnc(:,:,kx)*rhoa*1.e-06
    end do
  end if
  if (kint > 0) then
    cc16=cc16+cicdnc*rhoa*1.e-06
  end if
  !
  !-----------------------------------------------------------------------
  !     * cloud droplet number based on semi-empirical relationship
  !     * (old method).
  !
  call cdncem(emcdnc,pemas,pepsi,pimas,pipsi,pifrc, &
                  rhoa,ilga,leva)
  vcne=0.
  where (kcldf)
    ccne=emcdnc*rhoa*1.e-06
  else where
    ccne=0.
  end where
  do l=1,leva
    do il=1,ilga
      if (kcldf(il,l) ) then
        vcne(il)=vcne(il)+vitrm(il,l)*emcdnc(il,l)*1.e-06
      end if
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocation of arrays.
  !
  deallocate(emcdnc)
  if (isaext > 0) then
    deallocate(peinds)
    deallocate(peinde)
    deallocate(peradc)
    deallocate(pewetrc)
  end if
  if (kext > 0) then
    deallocate(cecdnc)
    deallocate(cercit)
  end if
  if (isextb > 0) then
    deallocate(cerc)
    deallocate(cesc)
  end if
  if (isaint > 0) then
    deallocate(piradc)
    deallocate(piwetrc)
  end if
  if (kint > 0) then
    deallocate(cicdnc)
    deallocate(circit)
  end if
  if (isintb > 0) then
    deallocate(circ)
    deallocate(cisc)
  end if
  !
end subroutine pamdi
