!> \file
!> \brief Diagnostic aerosol parameters for radiation and other model
!>       components external to PAM
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pamd(trac,ta,rha,pha,ssld,ress,vess,mdld,remd,vemd, &
                      bcld,rebc,vebc,ocld,reoc,veoc,amld,ream,veam, &
                      fr1,fr2,so4ldi,reso4i,veso4i,fr1so4i,fr2so4i, &
                      bcldi,rebci,vebci,fr1bci,fr2bci,ocldi, &
                      reoci,veoci,fr1oci,fr2oci,pm2p5,pm10,pm2p5d,pm10d, &
                      dpa,clda,rmua,tnss,tnmd,tnia,tmss,tmmd,tmoc, &
                      tmbc,tmsp,snss,snmd,snia,smss,smmd,smoc,smbc, &
                      smsp,siwh,sewh,sdnu,sdma,sdac,sdco,sssn,smdn, &
                      sian,sssm,smdm,sewm,ssum,socm,sbcm,siwm,sdvl, &
                      leva,ilga,irfrc)
  !
  use sdparm, only : aextf,aintf,ibcs,iinbc,iinoc,iinso4,iocs, &
                         isaext,isaint, &
                         isintbc,isintoc,isintso4,iso4s,itbc,itoc,itso4, &
                         kext,kextbc,kextmd,kextoc,kextss, &
                         kint,kintbc,kintoc,kintso4, &
                         ntract,pedphis,pedryrc,pephiss,pidphis,pidryrc,piphiss, &
                         sextf,yna
  use sdphys, only : grav,rgas
  use compar, only : isdiag,isdnum,kxtra1,kxtra2
  !
  implicit none
  !
  !     * pam input variables.
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga) :: clda !< cloud fraction
  real, intent(in), dimension(ilga) :: rmua !< Solar zenith angle
  real, intent(in), dimension(ilga,leva) :: ta !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rha !< !< Relative humidity for clear sky conditions
  real, intent(in), dimension(ilga,leva) :: dpa !< Difference in air pressure between bottom and top of grid cell \f$[Pa]\f$
  real, intent(in), dimension(ilga,leva) :: pha !< !< Pressure \f$[Pa]\f$
  real, intent(in), dimension(ilga,leva,ntract) :: trac !< Tracer array
  !
  !     * pam output variables.
  !
  real, intent(out), dimension(ilga,leva) :: ssld !< Sea salt aerosol mass loading for each layer
  real, intent(out), dimension(ilga,leva) :: ress !< Sea salt effective radius
  real, intent(out), dimension(ilga,leva) :: vess !< Sea salt effective variance
  real, intent(out), dimension(ilga,leva) :: mdld !< Dust aerosol mass loading for each layer
  real, intent(out), dimension(ilga,leva) :: remd !< Dust effective radius
  real, intent(out), dimension(ilga,leva) :: vemd !< Dust effective variance
  real, intent(out), dimension(ilga,leva) :: amld !< Sulfate aerosol mass loading for each layer
  real, intent(out), dimension(ilga,leva) :: ream !< Sulfate effective radius
  real, intent(out), dimension(ilga,leva) :: veam !< Sulfate carbon effective variance
  real, intent(out), dimension(ilga,leva) :: bcld !< Black carbon aerosol mass loading for each layer
  real, intent(out), dimension(ilga,leva) :: rebc !< Black carbon effective radius
  real, intent(out), dimension(ilga,leva) :: vebc !< Black carbon effective variance
  real, intent(out), dimension(ilga,leva) :: ocld !< Organic carbon aerosol mass loading for each layer
  real, intent(out), dimension(ilga,leva) :: reoc !< Organic carbon effective radius
  real, intent(out), dimension(ilga,leva) :: veoc !< Organic carbon effective variance
  real, intent(out), dimension(ilga,leva) :: fr1 !< Mass fraction 1
  real, intent(out), dimension(ilga,leva) :: fr2 !< Mass fraction 2
  real, intent(out), dimension(ilga,leva) :: so4ldi !< Load of sulphate in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: reso4i !< Effective radius of sulphate in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: veso4i !< Effective variance of sulphate in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: fr1so4i !< Mass fraction 1 for internally mixed sulphate
  real, intent(out), dimension(ilga,leva) :: fr2so4i !< Mass fraction 2 for internally mixed sulphate
  real, intent(out), dimension(ilga,leva) :: bcldi !< Load of black carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: rebci !< Effective radius of black carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: vebci !< Effective variance of black carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: fr1bci !< Mass fraction 1 for internally mixed black carbon
  real, intent(out), dimension(ilga,leva) :: fr2bci !< Mass fraction 2 for internally mixed black carbon
  real, intent(out), dimension(ilga,leva) :: ocldi !< Load of organic carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: reoci !< Effective radius of organic carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: veoci !< Effective variance of organic carbon in internally mixed aerosol
  real, intent(out), dimension(ilga,leva) :: fr1oci !< Mass fraction 1 for internally mixed organic carbon
  real, intent(out), dimension(ilga,leva) :: fr2oci !< Mass fraction 2 for internally mixed organic carbon
  real, intent(out), dimension(ilga,leva,kext+kint) :: pm2p5 !< PM2.5
  real, intent(out), dimension(ilga,leva,kext+kint) :: pm10 !< PM10
  real, intent(out), dimension(ilga,leva,kext+kint) :: pm2p5d !< PM2.5 dry
  real, intent(out), dimension(ilga,leva,kext+kint) :: pm10d !< PM10 dry
  real, intent(out), dimension(ilga,leva,isdnum) :: sdnu !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sdma !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sdac !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sdco !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sssn !<
  real, intent(out), dimension(ilga,leva,isdnum) :: smdn !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sian !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sssm !<
  real, intent(out), dimension(ilga,leva,isdnum) :: smdm !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sewm !<
  real, intent(out), dimension(ilga,leva,isdnum) :: ssum !<
  real, intent(out), dimension(ilga,leva,isdnum) :: socm !<
  real, intent(out), dimension(ilga,leva,isdnum) :: sbcm !<
  real, intent(out), dimension(ilga,leva,isdnum) :: siwm !<
  real, intent(out), dimension(ilga)        :: tnmd !<
  real, intent(out), dimension(ilga)        :: tnia !<
  real, intent(out), dimension(ilga)        :: tmss !<
  real, intent(out), dimension(ilga)        :: tnss !<
  real, intent(out), dimension(ilga)        :: tmmd !<
  real, intent(out), dimension(ilga)        :: tmoc !<
  real, intent(out), dimension(ilga)        :: tmbc !<
  real, intent(out), dimension(ilga)        :: tmsp !<
  real, intent(out), dimension(ilga,isdnum) :: snss !<
  real, intent(out), dimension(ilga,isdnum) :: snmd !<
  real, intent(out), dimension(ilga,isdnum) :: snia !<
  real, intent(out), dimension(ilga,isdnum) :: smss !<
  real, intent(out), dimension(ilga,isdnum) :: smmd !<
  real, intent(out), dimension(ilga,isdnum) :: smoc !<
  real, intent(out), dimension(ilga,isdnum) :: smbc !<
  real, intent(out), dimension(ilga,isdnum) :: smsp !<
  real, intent(out), dimension(ilga,isdnum) :: siwh !<
  real, intent(out), dimension(ilga,isdnum) :: sewh !<
  real, intent(out), dimension(ilga,isdiag) :: sdvl !<
  !
  !     * work arrays.
  !
  real, allocatable, dimension(:,:,:) :: pedphi0 !<
  real, allocatable, dimension(:,:,:) :: pephis0 !<
  real, allocatable, dimension(:,:,:) :: peddn !<
  real, allocatable, dimension(:,:,:) :: pewetrc !<
  real, allocatable, dimension(:,:,:) :: pidphi0 !<
  real, allocatable, dimension(:,:,:) :: piphis0 !<
  real, allocatable, dimension(:,:,:) :: piddn !<
  real, allocatable, dimension(:,:,:) :: piwetrc !<
  real, allocatable, dimension(:,:,:) :: pen0 !<
  real, allocatable, dimension(:,:,:) :: pephi0 !<
  real, allocatable, dimension(:,:,:) :: pepsi !<
  real, allocatable, dimension(:,:,:) :: penum !<
  real, allocatable, dimension(:,:,:) :: pemas !<
  real, allocatable, dimension(:,:,:) :: pin0 !<
  real, allocatable, dimension(:,:,:) :: piphi0 !<
  real, allocatable, dimension(:,:,:) :: pipsi !<
  real, allocatable, dimension(:,:,:) :: pinum !<
  real, allocatable, dimension(:,:,:) :: pimas !<
  real, allocatable, dimension(:,:,:) :: pidm !<
  real, allocatable, dimension(:,:,:) :: pidn !<
  real, allocatable, dimension(:,:,:) :: tiddn !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tiwetrc !<
  real, allocatable, dimension(:,:,:) :: tinum !<
  real, allocatable, dimension(:,:,:) :: timas !<
  real, allocatable, dimension(:,:,:,:) :: pefrc !<
  real, allocatable, dimension(:,:,:,:) :: pifrc !<
  real, allocatable, dimension(:,:,:,:) :: pidf !<
  real, allocatable, dimension(:,:,:,:) :: tifrc !<
  real, allocatable, dimension(:,:,:) :: pere !<
  real, allocatable, dimension(:,:,:) :: peve !<
  real, allocatable, dimension(:,:,:) :: peload !<
  real, allocatable, dimension(:,:)   :: pire !<
  real, allocatable, dimension(:,:)   :: pive !<
  real, allocatable, dimension(:,:)   :: piload !<
  real, allocatable, dimension(:,:) :: vitrm !<
  real, allocatable, dimension(:,:) :: dirsmn !<
  real, allocatable, dimension(:,:) :: dirsn !<
  real, allocatable, dimension(:,:) :: dirsms !<
  real, allocatable, dimension(:,:)     :: rhoa !<
  real, allocatable, dimension(:,:)     :: rhat !<
  real, allocatable, dimension(:,:)     :: vvol !<
  real, allocatable, dimension(:,:,:,:) :: fnumo !<
  real, allocatable, dimension(:,:,:,:) :: fmaso !<
  real, allocatable, dimension(:,:,:,:)   :: fmass !<
  real, allocatable, dimension(:,:,:,:)   :: fmassd !<
  real, allocatable, dimension(:)   :: radc !<
  character(len=70) :: cname !<
  integer :: is !<
  integer :: kx !<
  integer :: ist !<
  integer :: imlin !<
  integer :: isd !<
  integer :: l !<
  integer :: il !<
  integer :: irfrc !<
  integer :: ierr !<
  integer :: ispm !<
  integer :: ikf !<
  integer :: kxx !<
  real :: xrtmp !<
  real :: rads !<
  real :: rade !<
  real :: drx !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for pla section size parameter arrays.
  !
  allocate(vitrm(ilga,leva))
  allocate(dirsmn(ilga,leva))
  allocate(dirsn(ilga,leva))
  allocate(dirsms(ilga,leva))
  allocate(rhoa(ilga,leva))
  allocate(rhat(ilga,leva))
  if (isaext > 0) then
    allocate(pephis0(ilga,leva,isaext))
    allocate(pedphi0(ilga,leva,isaext))
    allocate(peddn  (ilga,leva,isaext))
    allocate(pen0   (ilga,leva,isaext))
    allocate(pephi0 (ilga,leva,isaext))
    allocate(pepsi  (ilga,leva,isaext))
    allocate(pewetrc(ilga,leva,isaext))
    allocate(penum  (ilga,leva,isaext))
    allocate(pemas  (ilga,leva,isaext))
    allocate(pefrc  (ilga,leva,isaext,kext))
    allocate(pere   (ilga,leva,kext))
    allocate(peve   (ilga,leva,kext))
    allocate(peload (ilga,leva,kext))
  end if
  if (isaint > 0) then
    allocate(piphis0(ilga,leva,isaint))
    allocate(pidphi0(ilga,leva,isaint))
    allocate(piddn  (ilga,leva,isaint))
    allocate(pin0   (ilga,leva,isaint))
    allocate(piphi0 (ilga,leva,isaint))
    allocate(pipsi  (ilga,leva,isaint))
    allocate(piwetrc(ilga,leva,isaint))
    allocate(pinum  (ilga,leva,isaint))
    allocate(pimas  (ilga,leva,isaint))
    allocate(pifrc  (ilga,leva,isaint,kint))
    allocate(pire   (ilga,leva))
    allocate(pive   (ilga,leva))
    allocate(piload (ilga,leva))
    allocate(pidm   (ilga,leva,isaint))
    allocate(pidn   (ilga,leva,isaint))
    allocate(pidf   (ilga,leva,isaint,kint))
    allocate(tiddn  (ilga,leva,isaint))
    allocate(tin0   (ilga,leva,isaint))
    allocate(tiphi0 (ilga,leva,isaint))
    allocate(tipsi  (ilga,leva,isaint))
    allocate(tiwetrc(ilga,leva,isaint))
    allocate(tinum  (ilga,leva,isaint))
    allocate(timas  (ilga,leva,isaint))
    allocate(tifrc  (ilga,leva,isaint,kint))
  end if
  !
  !-----------------------------------------------------------------------
  !     * aerosol size information.
  !
  do is=1,isaext
    pedphi0(:,:,is)=pedphis(is)
    pephis0(:,:,is)=pephiss(is)
  end do
  do is=1,isaint
    pidphi0(:,:,is)=pidphis(is)
    piphis0(:,:,is)=piphiss(is)
  end do
  !
  !     * dry particle densities for externally mixed aerosol types.
  !
  do is=1,isaext
    kx=sextf%isaer(is)%ityp
    peddn  (:,:,is)=aextf%tp(kx)%dens
  end do
  !
  !     * initialize dry particle densities for internally mixed
  !     * aerosol types. the densities are later calculated as a
  !     * function of aerosol mass.
  !
  do is=1,isaint
    piddn  (:,:,is)=yna
  end do
  !
  !     * meteorological :: variables for pla calculations.
  !
  rhoa=pha/(rgas*ta)
  !
  !-----------------------------------------------------------------------
  !     extract pla aerosol number and mass concentrations from basic
  !     tracer arrays in the gcm. calculate total mass and mass fractions
  !     for internally mixed types of aerosol.
  !
  call trc2nm (trac,penum,pemas,pinum,pimas,pifrc,ilga,leva)
  !
  !
  !     set aerosol species mass fraction for externally mixed aerosol to 1
  !
  if (isaext > 0) then
    pefrc=0.
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      pefrc(:,:,is,kx)=1.
    end do
  end if


  !
  !-----------------------------------------------------------------------
  !     update density (internal mixture) and pla parameters.
  !
  call nm2par (pen0,pephi0,pepsi,pephis0,pedphi0,pin0,piphi0, &
                   pipsi,piphis0,pidphi0,piddn,penum,pemas,peddn, &
                   pinum,pimas,pifrc,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * aerosol radiative properties for externally mixed aerosol species.
  !
  if (kext > 0) then
    do is=1,isaext
      pewetrc(:,:,is)=pedryrc(is)
    end do
    call radpare(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                     pewetrc,peddn,pere,peve,peload, &
                     ilga,leva)
  end if
  ssld=0.
  ress=0.
  vess=0.
  if (kextss > 0) then
    ssld=peload(:,:,kextss)
    !! !!
    ! id  oaerrad
    !        ssld=0.
    ress=pere(:,:,kextss)
    vess=peve(:,:,kextss)
  end if
  mdld=0.
  remd=0.
  vemd=0.
  if (kextmd > 0) then
    mdld=peload(:,:,kextmd)
    !! !!
    ! id  oaerrad
    !        mdld=0.
    remd=pere(:,:,kextmd)
    vemd=peve(:,:,kextmd)
  end if
  bcld=0.
  rebc=0.
  vebc=0.
  if (kextbc > 0) then
    bcld=peload(:,:,kextbc)
    !! !!
    ! id  oaerrad
    !        bcld=0.
    rebc=pere(:,:,kextbc)
    vebc=peve(:,:,kextbc)
  end if
  ocld=0.
  reoc=0.
  veoc=0.
  if (kextoc > 0) then
    ocld=peload(:,:,kextoc)
    !! !!
    ! id  oaerrad
    !        ocld=0.
    reoc=pere(:,:,kextoc)
    veoc=peve(:,:,kextoc)
  end if
  !
  !-----------------------------------------------------------------------
  !     * aerosol radiative properties for internally mixed aerosol species.
  !
  fr1=1.
  fr2=0.
  if (kint > 0) then
    do is=1,isaint
      piwetrc(:,:,is)=pidryrc(is)
    end do
    call radpari(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
                     piwetrc,piddn,pire,pive,piload, &
                     pifrc,fr1,fr2,ilga,leva)
  end if
  amld=0.
  ream=0.
  veam=0.
  if (kint > 0) then
    amld=piload
    !! !!
    ! id  oaerrad
    !        amld=0.
    ream=pire
    veam=pive
  end if
  !
  !-----------------------------------------------------------------------
  !     * size distributions for internally mixed aerosol with
  !     * components removed (for calculation of radiative forcings).
  !
  so4ldi=0.
  reso4i=0.
  veso4i=0.
  fr1so4i=0.
  fr2so4i=0.
  ocldi=0.
  reoci=0.
  veoci=0.
  fr1oci=0.
  fr2oci=0.
  bcldi=0.
  rebci=0.
  vebci=0.
  fr1bci=0.
  fr2bci=0.
  if (irfrc == 1) then
    if (kintso4 > 0) then
      call extac (pidn,pidm,pidf,dirsmn,dirsn,dirsms, &
                      pimas,pinum,pin0,piphi0,pipsi,piddn,pifrc, &
                      iinso4,itso4,kintso4,isintso4,iso4s,ilga,leva, &
                      ierr)
      tinum=max(pinum+pidn,0.)
      timas=max(pimas+pidm,0.)
      tifrc=max(pifrc+pidf,0.)
      call nm2pari(tin0,tiphi0,tipsi,piphis0,pidphi0,tiddn, &
                       tinum,timas,tifrc,ilga,leva)
      do is=1,isaint
        tiwetrc(:,:,is)=pidryrc(is)
      end do
      call radpari(tinum,timas,tin0,tiphi0,tipsi,piphis0,pidphi0, &
                       tiwetrc,tiddn,reso4i,veso4i,so4ldi, &
                       tifrc,fr1so4i,fr2so4i,ilga,leva)
    end if
    if (kintoc > 0) then
      call extac (pidn,pidm,pidf,dirsmn,dirsn,dirsms, &
                      pimas,pinum,pin0,piphi0,pipsi,piddn,pifrc, &
                      iinoc,itoc,kintoc,isintoc,iocs,ilga,leva, &
                      ierr)
      tinum=max(pinum+pidn,0.)
      timas=max(pimas+pidm,0.)
      tifrc=max(pifrc+pidf,0.)
      call nm2pari(tin0,tiphi0,tipsi,piphis0,pidphi0,tiddn, &
                       tinum,timas,tifrc,ilga,leva)
      do is=1,isaint
        tiwetrc(:,:,is)=pidryrc(is)
      end do
      call radpari(tinum,timas,tin0,tiphi0,tipsi,piphis0,pidphi0, &
                       tiwetrc,tiddn,reoci,veoci,ocldi, &
                       tifrc,fr1oci,fr2oci,ilga,leva)
    end if
    if (kintbc > 0) then
      call extac (pidn,pidm,pidf,dirsmn,dirsn,dirsms, &
                      pimas,pinum,pin0,piphi0,pipsi,piddn,pifrc, &
                      iinbc,itbc,kintbc,isintbc,ibcs,ilga,leva, &
                      ierr)
      tinum=max(pinum+pidn,0.)
      timas=max(pimas+pidm,0.)
      tifrc=max(pifrc+pidf,0.)
      call nm2pari(tin0,tiphi0,tipsi,piphis0,pidphi0,tiddn, &
                       tinum,timas,tifrc,ilga,leva)
      do is=1,isaint
        tiwetrc(:,:,is)=pidryrc(is)
      end do
      call radpari(tinum,timas,tin0,tiphi0,tipsi,piphis0,pidphi0, &
                       tiwetrc,tiddn,rebci,vebci,bcldi, &
                       tifrc,fr1bci,fr2bci,ilga,leva)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !       * pm2.5 and pm10
  !
  if (kxtra1) then
    if (isaint > 0 .or. isaext > 0) then
      rhat=0.5
      call sdaprop (pewetrc,piwetrc,pen0,pephi0,pepsi,peddn, &
                        pin0,piphi0,pipsi,pifrc,piddn,ta,rhat, &
                        ilga,leva)
    end if
    ispm=2
    allocate(radc(ispm))
    radc(1)=0.5*2.5e-06
    radc(2)=0.5*10.e-06
    pm2p5=0.
    pm10=0.
    pm2p5d=0.
    pm10d=0.
    if (isaint > 0) then
      allocate(fmass(ilga,leva,kint,ispm))
      allocate(fmassd(ilga,leva,kint,ispm))
      call intsdpm(fmass,fmassd,pinum,pimas,pin0,piphi0,pipsi, &
                       piphis0,pidphi0,pifrc,piddn,piwetrc,pidryrc,radc, &
                       rhoa,ispm,ilga,leva,isaint,kint)
      pm2p5(:,:,1:kint)=pm2p5(:,:,1:kint)+fmass(:,:,1:kint,1)
      pm10(:,:,1:kint)=pm10(:,:,1:kint)+fmass(:,:,1:kint,2)
      pm2p5d(:,:,1:kint)=pm2p5d(:,:,1:kint)+fmassd(:,:,1:kint,1)
      pm10d(:,:,1:kint)=pm10d(:,:,1:kint)+fmassd(:,:,1:kint,2)
      deallocate(fmass)
      deallocate(fmassd)
    end if
    if (isaext > 0) then
      allocate(fmass(ilga,leva,kext,ispm))
      allocate(fmassd(ilga,leva,kext,ispm))
      call intsdpm(fmass,fmassd,penum,pemas,pen0,pephi0,pepsi, &
                       pephis0,pedphi0,pefrc,peddn,pewetrc,pedryrc,radc, &
                       rhoa,ispm,ilga,leva,isaext,kext)
      pm2p5(:,:,kint+1:kint+kext)= &
                                       pm2p5(:,:,kint+1:kint+kext)+fmass(:,:,1:kext,1)
      pm10(:,:,kint+1:kint+kext)= &
                                      pm10(:,:,kint+1:kint+kext)+fmass(:,:,1:kext,2)
      pm2p5d(:,:,kint+1:kint+kext)= &
                                        pm2p5d(:,:,kint+1:kint+kext)+fmassd(:,:,1:kext,1)
      pm10d(:,:,kint+1:kint+kext)= &
                                       pm10d(:,:,kint+1:kint+kext)+fmassd(:,:,1:kext,2)
      deallocate(fmass)
      deallocate(fmassd)
    end if
    deallocate(radc)
  end if
  !
  !-----------------------------------------------------------------------
  !       * diagnose and filter size distribution.
  !
  vitrm=dpa/grav
  if (kxtra2) then
    if (isaint > 0 .or. isaext > 0 .and. isdnum > 1) then

      call sdaprop (pewetrc,piwetrc,pen0,pephi0,pepsi,peddn, &
                        pin0,piphi0,pipsi,pifrc,piddn,ta,rha, &
                        ilga,leva)
      !
      !         * arrays for total aerosol number size distribution and
      !         * aerosol mass for each separate aerosol species. aerosol
      !         * water is also provided. the following indices are used
      !         * for aerosol mass:
      !         * 1 - 1st externally mixed species
      !         * 2 - 2nd externally mixed species
      !         * ...
      !         * kext - last externally mixed species
      !         * kext+1 - aerosol water for externally mixed aerosol
      !         * kext+2 - 1st internally mixed species
      !         * ...
      !         * kext+kint+1 - last internally mixed species
      !         * kext+kint+2 - aerosol water for internally mixed aerosol
      !
      allocate (fnumo(ilga,leva,isdnum,kext+1))
      allocate (fmaso(ilga,leva,isdnum,kext+kint+2))
      !
      call sdfilt(fnumo,fmaso,pen0,pephi0,pepsi,peddn,pedphi0, &
                      pephis0,pewetrc,penum,pemas,pin0,piphi0,pipsi, &
                      piddn,pidphi0,piphis0,pifrc,piwetrc,pinum, &
                      pimas,ilga,leva)
      !
      !         * convert units so that concentrations will be provided
      !         * instead of mass mixing ratios.
      !
      do kx=1,kext+1
        do is=1,isdnum
          fnumo(:,:,is,kx)=fnumo(:,:,is,kx)*rhoa(:,:)
        end do
      end do
      do kx=1,kext+kint+2
        do is=1,isdnum
          fmaso(:,:,is,kx)=fmaso(:,:,is,kx)*rhoa(:,:)
        end do
      end do
      !
      !         * aerosol number for externally mixed types of aerosol.
      !
      if (kext > 0) then
        tnss=0.
        tnmd=0.
        do kx=1,kext
          cname=aextf%tp(kx)%name
          if (cname(1:7) == 'SEASALT'   ) then
            do isd=1,isdnum
              sssn(:,:,isd)=fnumo(:,:,isd,kx)
              snss(:,isd)=fnumo(:,leva,isd,kx)
              tnss(:)=tnss(:)+fnumo(:,leva,isd,kx)
            end do
          end if
          if (cname(1:7) == 'MINDUST'   ) then
            do isd=1,isdnum
              smdn(:,:,isd)=fnumo(:,:,isd,kx)
              snmd(:,isd)=fnumo(:,leva,isd,kx)
              tnmd(:)=tnmd(:)+fnumo(:,leva,isd,kx)
            end do
          end if
        end do
      end if
      !
      !         * aerosol number for internally mixed types of aerosol.
      !
      if (kint > 0) then
        tnia=0.
        do isd=1,isdnum
          sian(:,:,isd)=fnumo(:,:,isd,kext+1)
          snia(:,isd)=fnumo(:,leva,isd,kext+1)
          tnia(:)=tnia(:)+fnumo(:,leva,isd,kext+1)
        end do
      end if
      !
      !         * aerosol mass for externally mixed types of aerosol.
      !
      sssm=0.
      smss=0.
      tmss=0.
      smdm=0.
      smmd=0.
      tmmd=0.
      ssum=0.
      smsp=0.
      tmsp=0.
      socm=0.
      smoc=0.
      tmoc=0.
      sbcm=0.
      smbc=0.
      tmbc=0.
      if (kext > 0) then
        do kx=1,kext
          cname=aextf%tp(kx)%name
          if (cname(1:7) == 'SEASALT'   ) then
            do isd=1,isdnum
              sssm(:,:,isd)=fmaso(:,:,isd,kx)
              smss(:,isd)=fmaso(:,leva,isd,kx)
              tmss(:)=tmss(:)+fmaso(:,leva,isd,kx)
            end do
          end if
          if (cname(1:7) == 'MINDUST'   ) then
            do isd=1,isdnum
              smdm(:,:,isd)=fmaso(:,:,isd,kx)
              smmd(:,isd)=fmaso(:,leva,isd,kx)
              tmmd(:)=tmmd(:)+fmaso(:,leva,isd,kx)
            end do
          end if
          if (cname(1:7) == '(NH4)2SO4' ) then
            do isd=1,isdnum
              ssum(:,:,isd)=fmaso(:,:,isd,kx)
              smsp(:,isd)=fmaso(:,leva,isd,kx)
              tmsp(:)=tmsp(:)+fmaso(:,leva,isd,kx)
            end do
          end if
          if (cname(1:7) == 'ORGCARB'   ) then
            do isd=1,isdnum
              socm(:,:,isd)=fmaso(:,:,isd,kx)
              smoc(:,isd)=fmaso(:,leva,isd,kx)
              tmoc(:)=tmoc(:)+fmaso(:,leva,isd,kx)
            end do
          end if
          if (cname(1:7) == 'BLCCARB'   ) then
            do isd=1,isdnum
              sbcm(:,:,isd)=fmaso(:,:,isd,kx)
              smbc(:,isd)=fmaso(:,leva,isd,kx)
              tmbc(:)=tmbc(:)+fmaso(:,leva,isd,kx)
            end do
          end if
        end do
      end if
      !
      !         * aerosol mass for internally mixed types of aerosol.
      !
      if (kint > 0) then
        do kx=1,kint
          ikf=kext+kx+1
          cname=aintf%tp(kx)%name
          if (cname(1:9) == 'SEASALT'   ) then
            do isd=1,isdnum
              sssm(:,:,isd)=sssm(:,:,isd)+fmaso(:,:,isd,ikf)
              smss(:,isd)=smss(:,isd)+fmaso(:,leva,isd,ikf)
              tmss(:)=tmss(:)+fmaso(:,leva,isd,ikf)
            end do
          end if
          if (cname(1:9) == 'MINDUST'   ) then
            do isd=1,isdnum
              smdm(:,:,isd)=smdm(:,:,isd)+fmaso(:,:,isd,ikf)
              smmd(:,isd)=smmd(:,isd)+fmaso(:,leva,isd,ikf)
              tmmd(:)=tmmd(:)+fmaso(:,leva,isd,ikf)
            end do
          end if
          if (cname(1:9) == '(NH4)2SO4'   ) then
            do isd=1,isdnum
              ssum(:,:,isd)=ssum(:,:,isd)+fmaso(:,:,isd,ikf)
              smsp(:,isd)=smsp(:,isd)+fmaso(:,leva,isd,ikf)
              tmsp(:)=tmsp(:)+fmaso(:,leva,isd,ikf)
            end do
          end if
          if (cname(1:7) == 'ORGCARB'   ) then
            do isd=1,isdnum
              socm(:,:,isd)=socm(:,:,isd)+fmaso(:,:,isd,ikf)
              smoc(:,isd)=smoc(:,isd)+fmaso(:,leva,isd,ikf)
              tmoc(:)=tmoc(:)+fmaso(:,leva,isd,ikf)
            end do
          end if
          if (cname(1:7) == 'BLCCARB'   ) then
            do isd=1,isdnum
              sbcm(:,:,isd)=sbcm(:,:,isd)+fmaso(:,:,isd,ikf)
              smbc(:,isd)=smbc(:,isd)+fmaso(:,leva,isd,ikf)
              tmbc(:)=tmbc(:)+fmaso(:,leva,isd,ikf)
            end do
          end if
        end do
      end if
      !
      !         * aerosol water content.
      !
      if (kext > 0) then
        do isd=1,isdnum
          sewm(:,:,isd)=fmaso(:,:,isd,kext+1)
          sewh(:,isd)=fmaso(:,leva,isd,kext+1)
        end do
      end if
      if (kint > 0) then
        ikf=kext+kint+2
        do isd=1,isdnum
          siwm(:,:,isd)=fmaso(:,:,isd,ikf)
          siwh(:,isd)=fmaso(:,leva,isd,ikf)
        end do
      end if
      !
      do isd=1,isdnum
        !
        !           * aerosol number and dry mass for all types of aerosol.
        !
        sdnu(:,:,isd)=sum(fnumo(:,:,isd,1:kext),dim=3) &
                         +fnumo(:,:,isd,kext+1)
        sdma(:,:,isd)=sum(fmaso(:,:,isd,1:kext),dim=3) &
                         +sum(fmaso(:,:,isd,kext+2:kext+kint+1),dim=3)
        !
        !           * total dry aerosol mass for externally mixed types.
        !
        do kx=1,kext
          sdco(:,:,isd)=fmaso(:,:,isd,kx)
        end do
        !
        !           * total dry aerosol mass for internally mixed types.
        !
        do kxx=1,kint
          kx=kxx+kext+1
          sdac(:,:,isd)=fmaso(:,:,isd,kx)
        end do
      end do
      !
      deallocate (fnumo)
      deallocate (fmaso)
      !
      !         * vertically integrated volume size distribution.
      !         * aeronet size bins are used.
      !
      rads=0.05e-06
      rade=15.e-06
      drx=0.5*(log10(rade)-log(rads))/(real(isdiag)-1.)
      rads=10.**(log10(rads)-drx)
      rade=10.**(log10(rade)+drx)
      !
      allocate (vvol(ilga,isdiag))
      !
      call vintsd (vvol,pen0,pephi0,pepsi,pedphi0,pephis0, &
                       pewetrc,penum,pemas,pin0,piphi0,pipsi, &
                       pidphi0,piphis0,piwetrc,pinum,pimas,vitrm, &
                       rads,rade,isdiag,ilga,leva)
      !
      !         * save volume size distribution for daytime,
      !         * clear conditions.
      !
      sdvl=0.
      do il=1,ilga
        if (rmua(il) > 0.001 .and. clda(il) < 0.5) then
          sdvl(il,:)=vvol(il,:)
        end if
      end do
      !
      deallocate (vvol)
      !
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocation of pla arrays.
  !
  deallocate(vitrm)
  deallocate(dirsmn)
  deallocate(dirsn)
  deallocate(dirsms)
  deallocate(rhoa)
  deallocate(rhat)
  if (isaext > 0) then
    deallocate(pephis0)
    deallocate(pedphi0)
    deallocate(peddn)
    deallocate(pen0)
    deallocate(pephi0)
    deallocate(pepsi)
    deallocate(pewetrc)
    deallocate(penum)
    deallocate(pemas)
    deallocate(pefrc)
    deallocate(pere)
    deallocate(peve)
    deallocate(peload)
  end if
  if (isaint > 0) then
    deallocate(piphis0)
    deallocate(pidphi0)
    deallocate(piddn)
    deallocate(pin0)
    deallocate(piphi0)
    deallocate(pipsi)
    deallocate(piwetrc)
    deallocate(pinum)
    deallocate(pimas)
    deallocate(pifrc)
    deallocate(pire)
    deallocate(pive)
    deallocate(piload)
    deallocate(pidm)
    deallocate(pidn)
    deallocate(pidf)
    deallocate(tiddn)
    deallocate(tin0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tiwetrc)
    deallocate(tinum)
    deallocate(timas)
    deallocate(tifrc)
  end if
  !
end subroutine pamd
