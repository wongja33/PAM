!> \file
!> \brief Selection of appropriate look-up-table for given koehler terms
!>       for droplet growth calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnrat(ratp0,irds,ak,bk,ilga,leva,isec)
  !
  use cnparm, only : idef,irdp,ratp,ylarge,yna,ysmall
  !
  implicit none
  !
  logical, parameter :: kio=.false. !<
  !      logical, parameter :: kio=.true.
  integer, parameter :: iof=50 !<
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(ilga,leva) :: ak !<  A-term in Koehler equation (for curvature effect)
  real, intent(in), dimension(ilga,leva,isec) :: bk !<  B-term in Koehler equation (for solute effect)
  integer, intent(out), dimension(ilga,leva,isec) :: irds !<
  real, intent(out), dimension(ilga,leva,isec) :: ratp0 !< Koehler parameter ratio
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: rat0 !<
  real, allocatable, dimension(:,:,:) :: rdiff !<
  real, allocatable, dimension(:,:,:) :: rdifft !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: irat !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(rat0  (ilga,leva,isec))
  allocate(rdiff (ilga,leva,isec))
  allocate(rdifft(ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * koehler parameter ratio to determine the appropriate data table.
  !     * the approach is to select the table that produces the best
  !     * agreement for the parameter ratio.
  !
  rat0=yna
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (abs(bk(il,l,is)-yna) > ysmall) then
          rat0(il,l,is)=bk(il,l,is)/(2.*ak(il,l))
        end if
      end do
    end do
  end do
  rdiff=ylarge
  ratp0=yna
  irds=idef
  do irat=1,irdp
    do is=1,isec
      do l=1,leva
        do il=1,ilga
          if (abs(rat0(il,l,is)-yna) > ysmall) then
            rdifft(il,l,is)=abs(rat0(il,l,is)-ratp(irat))
            if (rdifft(il,l,is) < rdiff(il,l,is) ) then
              rdiff(il,l,is)=rdifft(il,l,is)
              irds(il,l,is)=irat
              ratp0(il,l,is)=ratp(irat)
            end if
          end if
        end do
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * write out points.
  !
  if (kio) then
    do is=1,isec
      do l=1,leva
        do il=1,ilga
          write(iof, &
            '(A30,I3,1X,I3,1X,I3,I3,E11.4,1X,E11.4,1X,E11.4,1X,E11.4)') &
                 'IL,L,IS,IRDS,RAT0,RATP0,AK,BK=',il,l,is, &
                 irds(il,l,is),rat0(il,l,is),ratp0(il,l,is), &
                 ak(il,l),bk(il,l,is)
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(rat0)
  deallocate(rdiff)
  deallocate(rdifft)
  !
end subroutine cnrat
