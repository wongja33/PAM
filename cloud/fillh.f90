!> \file
!> \brief Fill holes in vertical profile by linear interpolation and
!>       extrapolation.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine fillh(vavm,zha,varef,ilga,leva)
  !
  use sdparm, only : ina,yna,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in) :: varef !<
  real, intent(in), dimension(ilga,leva) :: zha !<
  real, intent(inout), dimension(ilga,leva) :: vavm !<
  !
  !     internal work variables
  !
  integer :: il !<
  integer :: l !<
  integer :: lx !<
  integer :: levph !<
  integer :: levpl !<
  integer :: levpl0 !<
  real :: wgt !<
  real :: dzt !<
  !
  !-----------------------------------------------------------------------
  !
  do il=1,ilga
    levph=ina
    do l=leva,1,-1
      if (abs(vavm(il,l)-yna) > ytiny .and. levph == ina) then
        levph=l
      end if
    end do
    if (abs(vavm(il,leva)-yna) <= ytiny) then
      if (levph /= ina) then
        vavm(il,levph+1:leva)=vavm(il,levph)
      else
        vavm(il,:)=varef
      end if
    end if
    levpl=levph
    levpl0=levpl
    levph=ina
    if (levpl0 /= ina) then
      do l=levpl0-1,1,-1
        if (abs(vavm(il,l)-yna) > ytiny .and. levph == ina) then
          levph=l
        end if
        if (levph /= ina) then
          dzt=zha(il,levph)-zha(il,levpl)
          do lx=levpl-1,levph+1,-1
            wgt=(zha(il,levph)-zha(il,lx))/dzt
            vavm(il,lx)=wgt*vavm(il,levpl)+(1.-wgt)*vavm(il,levph)
          end do
          levpl=levph
          levph=ina
        end if
      end do
      vavm(il,1:levpl)=vavm(il,levpl)
    end if
  end do
  !
end subroutine fillh
