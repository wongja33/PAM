!> \file
!> \brief Numerical integration of droplet growth equation based on scaled
!>       version of droplet growth equation.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnigge(wetrn,rc,sc,wetr,bk,tscl,sv,ak, &
                        svi,ratp0,irds,dryr,dt,ilga,leva,isec, &
                        imod1,imod2,imod3)
  !
  use cnparm, only : yna,ysmall
  !
  implicit none
  !
  logical, parameter :: kov=.false. !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(ilga,leva) :: dt !<
  real, intent(in), dimension(ilga,leva) :: sv !<
  real, intent(in), dimension(ilga,leva) :: svi !<
  real, intent(in), dimension(isec) :: dryr !<
  integer, intent(in), dimension(ilga,leva,isec) :: irds !<
  real, intent(in), dimension(ilga,leva,isec) :: tscl !<
  real, intent(in), dimension(ilga,leva,isec) :: ratp0 !<
  real, intent(inout), dimension(ilga,leva) :: ak !<
  real, intent(inout), dimension(ilga,leva,isec) :: bk !<
  real, intent(inout), dimension(ilga,leva,isec) :: wetr !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod1 !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod2 !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod3 !<
  real, intent(out), dimension(ilga,leva,isec) :: wetrn !<
  real, intent(out), dimension(ilga,leva,isec) :: rc !<
  real, intent(out), dimension(ilga,leva,isec) :: sc !<
  real, dimension(ilga,leva) :: akt !<
  real, dimension(ilga,leva) :: sve !<
  real, dimension(ilga,leva,isec) :: wetrt !<
  real, dimension(ilga,leva,isec) :: xp !<
  real, dimension(ilga,leva,isec) :: xpn !<
  real, dimension(ilga,leva,isec) :: du !<
  real, dimension(ilga,leva,isec) :: bkt !<
  real, dimension(ilga,leva,isec) :: bsp !<
  real, dimension(ilga,leva,isec) :: svt !<
  real, allocatable, dimension(:,:,:,:) :: xps !<
  real, allocatable, dimension(:,:,:,:) :: tps !<
  integer, dimension(ilga,leva,isec) :: irs !<
  integer, dimension(ilga,leva,isec) :: irgs !<
  integer, dimension(ilga,leva,isec) :: ibs !<
  logical, dimension(ilga,leva,isec) :: kop !<
  logical, dimension(ilga,leva,isec) :: kgr !<
  integer, dimension(leva,isec) :: irsmax !<
  integer :: is !<
  integer :: il !<
  integer :: l !<
  integer :: irstmax !<
  real, parameter :: weigh = 0.0 ! 0.1
  !
  !-----------------------------------------------------------------------
  !     * effective supersaturation value for droplet growth from
  !     * weighting of value for current and previous model time step.
  !
  sve=weigh*svi+(1.-weigh)*sv
  !
  !     * define growth regime and calculate key parameters.
  !
  call cnextrp(wetrt,svt,akt,bkt,xp,rc,sc,kgr,bsp,irs,irsmax, &
                   wetr,sve,ak,bk,ratp0,irds,irgs,ibs,ilga,leva, &
                   isec,irstmax,imod1,imod2,imod3)
  !
  !       * allocate shared arrays that will contain the scaled particle
  !       * sizes and growth times after interpolation.
  !
  allocate(xps(ilga,leva,isec,irstmax))
  allocate(tps(ilga,leva,isec,irstmax))
  xps=yna
  tps=yna
  !
  !     * extract characteristic growth curve.
  !
  call cnextr(bsp,xp,xps,tps,kop,irs,irsmax,irds,irgs,ibs, &
                  ilga,leva,isec,irstmax)
  !
  !-----------------------------------------------------------------------
  !     * adjust initial particle size and supersaturation, if initial
  !     * values are outside the considered domain.
  !
  if (kov) then
    wetr=wetrt
    ak=akt
    bk=bkt
  end if
  !
  !     * normalized time interval for paricle growth (du).
  !
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        du(il,l,is)=abs(svt(il,l,is))*dt(il,l)/tscl(il,l,is)
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * wet particle size.
  !
  call cnrcal(xpn,xp,du,xps,tps,kop,kgr,irs,irsmax,ilga, &
                  leva,isec,irstmax)
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (abs(bk(il,l,is)-yna) > ysmall) then
          wetrn(il,l,is)=sqrt(2.*xpn(il,l,is))
        else
          wetrn(il,l,is)=dryr(is)
        end if
      end do
    end do
  end do
  !
end subroutine cnigge
