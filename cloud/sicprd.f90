!> \file
!> \brief In-cloud production of sulphate for stratiform clouds.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sicprd (pedndt,pedmdt,pidndt,pidmdt,pidfdt, &
                         dersn,dersm,dirsmn,dirsn,dirsms, &
                         texsvi,texsvip,texsvia,timass,timassp, &
                         timassa,tiden,teicrit,tiicrit,pesmas, &
                         pemas,penum,pen0,pephi0,pepsi,peddn, &
                         pismas,pimas,pinum,pin0,piphi0,pipsi, &
                         piddn,pifrc,dt,ilga,leva,ierr)
  !
  use sdparm, only : aextf,aintf, &
                         iexso4,iinso4,ina,isaext,isaint,isextso4,isintso4,iso4s,itso4, &
                         kext,kextso4,kint,kintso4, &
                         pedphis,pephiss,pidphis,piphiss,ylarge
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, parameter :: imodc=2 !<
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(out) :: ierr !<
  real, intent(out), dimension(ilga,leva) :: dersn !<
  real, intent(out), dimension(ilga,leva) :: dersm !<
  real, intent(out), dimension(ilga,leva) :: dirsmn !<
  real, intent(out), dimension(ilga,leva) :: dirsn !<
  real, intent(out), dimension(ilga,leva) :: dirsms !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !<
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !<
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !<
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pemas !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !<
  real, intent(in), dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in), dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: piddn !<
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !<
  real, intent(in), dimension(ilga,leva) :: pesmas !<
  real, intent(in), dimension(ilga,leva) :: pismas !<
  real, intent(in), dimension(ilga,leva) :: timass !<
  real, intent(in), dimension(ilga,leva) :: timassp !<
  real, intent(in), dimension(ilga,leva) :: tiden !<
  real, intent(in), dimension(ilga,leva) :: texsvi !<
  real, intent(in), dimension(ilga,leva) :: texsvip !<
  real, intent(in), dimension(ilga,leva) :: texsvia !<
  real, intent(in), dimension(ilga,leva) :: timassa !<
  integer, intent(in), dimension(ilga,leva,kext) :: teicrit !<
  integer, intent(in), dimension(ilga,leva) :: tiicrit !<
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:,:) :: timn !<
  real, allocatable, dimension(:,:,:,:) :: tifrcn !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: teddn !<
  real, allocatable, dimension(:,:,:) :: tefrc !<
  real, allocatable, dimension(:,:,:) :: tecgr1 !<
  real, allocatable, dimension(:,:,:) :: tecgr2 !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tedp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tedp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tedm3s !<
  real, allocatable, dimension(:,:,:) :: tin0 !<
  real, allocatable, dimension(:,:,:) :: tiphi0 !<
  real, allocatable, dimension(:,:,:) :: tipsi !<
  real, allocatable, dimension(:,:,:) :: tiddn !<
  real, allocatable, dimension(:,:,:) :: tifrcs !<
  real, allocatable, dimension(:,:,:) :: ticgr1 !<
  real, allocatable, dimension(:,:,:) :: ticgr2 !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2l !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3l !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2r !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3r !<
  real(r8), allocatable, dimension(:,:,:) :: tidp0s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp3s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp4s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp5s !<
  real(r8), allocatable, dimension(:,:,:) :: tidp6s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm1s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm2s !<
  real(r8), allocatable, dimension(:,:,:) :: tidm3s !<
  real(r8), allocatable, dimension(:,:,:) :: temt !<
  real(r8), allocatable, dimension(:,:,:) :: timst !<
  real(r8), allocatable, dimension(:,:,:,:) :: timnt !<
  real, allocatable, dimension(:,:,:) :: ten !<
  real, allocatable, dimension(:,:,:) :: tent !<
  real, allocatable, dimension(:,:,:) :: tem !<
  real, allocatable, dimension(:,:,:) :: tedryr !<
  real, allocatable, dimension(:,:,:) :: tedphis !<
  real, allocatable, dimension(:,:,:) :: tephiss !<
  real, allocatable, dimension(:,:,:) :: tin !<
  real, allocatable, dimension(:,:,:) :: tint !<
  real, allocatable, dimension(:,:,:) :: tim !<
  real, allocatable, dimension(:,:,:) :: timt !<
  real, allocatable, dimension(:,:,:) :: tims !<
  real, allocatable, dimension(:,:,:) :: tirsmn !<
  real, allocatable, dimension(:,:,:) :: titm1 !<
  real, allocatable, dimension(:,:,:) :: titm2 !<
  real, allocatable, dimension(:,:,:) :: tidryr !<
  real, allocatable, dimension(:,:,:) :: tidphis !<
  real, allocatable, dimension(:,:,:) :: tiphiss !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  real, allocatable, dimension(:,:) :: term3 !<
  real, allocatable, dimension(:,:) :: term4 !<
  integer, allocatable, dimension(:,:,:) :: tei0l !<
  integer, allocatable, dimension(:,:,:) :: tei0r !<
  integer, allocatable, dimension(:,:,:) :: tii0l !<
  integer, allocatable, dimension(:,:,:) :: tii0r !<
  integer :: il !<
  integer :: is !<
  integer :: it !<
  integer :: l !<
  integer :: isl !<
  real :: cfn !<
  real :: cfm !<
  real :: term5 !<
  real :: term6 !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  if (isaext > 0) then
    pedndt=0.
    pedmdt=0.
  end if
  if (isaint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=0.
  end if
  ierr=0
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for local fields.
  !
  if (isextso4 > 0) then
    allocate(tecgr1 (ilga,leva,isextso4))
    allocate(tecgr2 (ilga,leva,isextso4))
    allocate(ten0   (ilga,leva,isextso4))
    allocate(tephi0 (ilga,leva,isextso4))
    allocate(tepsi  (ilga,leva,isextso4))
    allocate(teddn  (ilga,leva,isextso4))
    allocate(tem    (ilga,leva,isextso4))
    allocate(temt   (ilga,leva,isextso4))
    allocate(ten    (ilga,leva,isextso4))
    allocate(tent   (ilga,leva,isextso4))
    allocate(tei0l  (ilga,leva,isextso4))
    allocate(tei0r  (ilga,leva,isextso4))
    allocate(tedp0l (ilga,leva,isextso4))
    allocate(tedp1l (ilga,leva,isextso4))
    allocate(tedp2l (ilga,leva,isextso4))
    allocate(tedp3l (ilga,leva,isextso4))
    allocate(tedp4l (ilga,leva,isextso4))
    allocate(tedp5l (ilga,leva,isextso4))
    allocate(tedp6l (ilga,leva,isextso4))
    allocate(tedm1l (ilga,leva,isextso4))
    allocate(tedm2l (ilga,leva,isextso4))
    allocate(tedm3l (ilga,leva,isextso4))
    allocate(tedp0r (ilga,leva,isextso4))
    allocate(tedp1r (ilga,leva,isextso4))
    allocate(tedp2r (ilga,leva,isextso4))
    allocate(tedp3r (ilga,leva,isextso4))
    allocate(tedp4r (ilga,leva,isextso4))
    allocate(tedp5r (ilga,leva,isextso4))
    allocate(tedp6r (ilga,leva,isextso4))
    allocate(tedm1r (ilga,leva,isextso4))
    allocate(tedm2r (ilga,leva,isextso4))
    allocate(tedm3r (ilga,leva,isextso4))
    allocate(tedp0s (ilga,leva,isextso4))
    allocate(tedp1s (ilga,leva,isextso4))
    allocate(tedp2s (ilga,leva,isextso4))
    allocate(tedp3s (ilga,leva,isextso4))
    allocate(tedp4s (ilga,leva,isextso4))
    allocate(tedp5s (ilga,leva,isextso4))
    allocate(tedp6s (ilga,leva,isextso4))
    allocate(tedm1s (ilga,leva,isextso4))
    allocate(tedm2s (ilga,leva,isextso4))
    allocate(tedm3s (ilga,leva,isextso4))
    allocate(tefrc  (ilga,leva,isextso4))
    allocate(tedphis(ilga,leva,isextso4))
    allocate(tephiss(ilga,leva,isextso4))
    allocate(tedryr (ilga,leva,isextso4+1))
  end if
  if (isintso4 > 0) then
    allocate(ticgr1 (ilga,leva,isintso4))
    allocate(ticgr2 (ilga,leva,isintso4))
    allocate(timn   (ilga,leva,isintso4,iso4s))
    allocate(timnt  (ilga,leva,isintso4,iso4s))
    allocate(tifrcn (ilga,leva,isintso4,iso4s))
    allocate(tifrcs (ilga,leva,isintso4))
    allocate(tin0   (ilga,leva,isintso4))
    allocate(tiphi0 (ilga,leva,isintso4))
    allocate(tipsi  (ilga,leva,isintso4))
    allocate(tiddn  (ilga,leva,isintso4))
    allocate(tim    (ilga,leva,isintso4))
    allocate(timt   (ilga,leva,isintso4))
    allocate(tin    (ilga,leva,isintso4))
    allocate(tint   (ilga,leva,isintso4))
    allocate(tims   (ilga,leva,isintso4))
    allocate(timst  (ilga,leva,isintso4))
    allocate(tii0l  (ilga,leva,isintso4))
    allocate(tii0r  (ilga,leva,isintso4))
    allocate(tidp0l (ilga,leva,isintso4))
    allocate(tidp1l (ilga,leva,isintso4))
    allocate(tidp2l (ilga,leva,isintso4))
    allocate(tidp3l (ilga,leva,isintso4))
    allocate(tidp4l (ilga,leva,isintso4))
    allocate(tidp5l (ilga,leva,isintso4))
    allocate(tidp6l (ilga,leva,isintso4))
    allocate(tidm1l (ilga,leva,isintso4))
    allocate(tidm2l (ilga,leva,isintso4))
    allocate(tidm3l (ilga,leva,isintso4))
    allocate(tidp0r (ilga,leva,isintso4))
    allocate(tidp1r (ilga,leva,isintso4))
    allocate(tidp2r (ilga,leva,isintso4))
    allocate(tidp3r (ilga,leva,isintso4))
    allocate(tidp4r (ilga,leva,isintso4))
    allocate(tidp5r (ilga,leva,isintso4))
    allocate(tidp6r (ilga,leva,isintso4))
    allocate(tidm1r (ilga,leva,isintso4))
    allocate(tidm2r (ilga,leva,isintso4))
    allocate(tidm3r (ilga,leva,isintso4))
    allocate(tidp0s (ilga,leva,isintso4))
    allocate(tidp1s (ilga,leva,isintso4))
    allocate(tidp2s (ilga,leva,isintso4))
    allocate(tidp3s (ilga,leva,isintso4))
    allocate(tidp4s (ilga,leva,isintso4))
    allocate(tidp5s (ilga,leva,isintso4))
    allocate(tidp6s (ilga,leva,isintso4))
    allocate(tidm1s (ilga,leva,isintso4))
    allocate(tidm2s (ilga,leva,isintso4))
    allocate(tidm3s (ilga,leva,isintso4))
    allocate(tirsmn (ilga,leva,iso4s))
    allocate(titm1  (ilga,leva,iso4s))
    allocate(titm2  (ilga,leva,iso4s))
    allocate(tidphis(ilga,leva,isintso4))
    allocate(tiphiss(ilga,leva,isintso4))
    allocate(tidryr (ilga,leva,isintso4+1))
  end if
  allocate(term1(ilga,leva))
  allocate(term2(ilga,leva))
  allocate(term3(ilga,leva))
  allocate(term4(ilga,leva))
  !
  !-----------------------------------------------------------------------
  !     * temporary aerosol fields for sulphate aerosol for dry
  !     * and wet particle sizes for externally mixed aerosol type.
  !
  if (kextso4 > 0) then
    do is=1,isextso4
      ten    (:,:,is)=penum  (:,:,iexso4(is))
      tem    (:,:,is)=pemas  (:,:,iexso4(is))
      ten0   (:,:,is)=pen0   (:,:,iexso4(is))
      tephi0 (:,:,is)=pephi0 (:,:,iexso4(is))
      tepsi  (:,:,is)=pepsi  (:,:,iexso4(is))
      teddn  (:,:,is)=peddn  (:,:,iexso4(is))
      tedphis(:,:,is)=pedphis(iexso4(is))
      tephiss(:,:,is)=pephiss(iexso4(is))
      tedryr (:,:,is)=aextf%tp(kextso4)%dryr(is)%vl
    end do
    tedryr(:,:,isextso4+1)=aextf%tp(kextso4)%dryr(isextso4)%vr
  end if
  !
  !     * temporary aerosol fields for sulphate aerosol for dry
  !     * and wet particle sizes for internally mixed aerosol type.
  !
  if (kintso4 > 0) then
    do is=1,isintso4
      tin    (:,:,is)=pinum  (:,:,iinso4(is))
      tim    (:,:,is)=pimas  (:,:,iinso4(is))
      tims   (:,:,is)=pimas  (:,:,iinso4(is)) &
                         *pifrc  (:,:,iinso4(is),kintso4)
      tifrcs (:,:,is)=pifrc  (:,:,iinso4(is),kintso4)
      do it=1,iso4s
        timn(:,:,is,it)=pimas  (:,:,iinso4(is)) &
                           *pifrc  (:,:,iinso4(is),itso4(it))
        tifrcn(:,:,is,it)=pifrc(:,:,iinso4(is),itso4(it))
      end do
      tin0   (:,:,is)=pin0   (:,:,iinso4(is))
      tiphi0 (:,:,is)=piphi0 (:,:,iinso4(is))
      tipsi  (:,:,is)=pipsi  (:,:,iinso4(is))
      tiddn  (:,:,is)=piddn  (:,:,iinso4(is))
      tidphis(:,:,is)=pidphis(iinso4(is))
      tiphiss(:,:,is)=piphiss(iinso4(is))
      tidryr (:,:,is)=aintf%dryr(iinso4(is))%vl
    end do
    tidryr(:,:,isintso4+1)=aintf%dryr(aintf%tp(kintso4)%isce)%vr
  end if
  !
  !-----------------------------------------------------------------------
  !     * growth factor from r(t)=r(t=0)*c, with c=cgr1+cgr2*r(t=0),
  !     * for dry particle radius r and time t. it is assumed that
  !     * the relative increase in dry particle radius for each section
  !     * equals (vt/v)**(1/3), where v is the initial total volume for
  !     * (nh4)2so4 aerosol and vt is the volume after in-cloud production.
  !     * in other words, in-cloud chemical production of sulphate aerosol
  !     * is assumed to be proportional to the initial volume of
  !     * aerosol in the droplets (volume-controlled growth).
  !
  term4=1./3.
  if (isextso4 > 0) then
    !
    !       * ratio of new aerosol volume concentration over original
    !       * concentration for activated particles and growth factor
    !       * for fully activated size sections.
    !
    where (2+abs(exponent(pesmas) - exponent(texsvi)) &
        < maxexponent(pesmas) .and. texsvi/=0. )
      term1=1.+pesmas/texsvi
      term1=min(max(term1,1.),ylarge)
    else where
      term1=1.
    end where
    term2=term1**term4
    !
    !       * the same as above for partially activated size section.
    !       * compute the ratio of volume change per current volume in
    !       * partly activated section (term1) using the change in volume
    !       * in that section (term5).
    !
    term3=term1
    term1=1.
    do l=1,leva
      do il=1,ilga
        isl=teicrit(il,l,kextso4)
        if (isl /= ina) then
          term5=(term3(il,l)-1.)*texsvip(il,l)
          term6=texsvia(il,l)
          if (2+abs(exponent(term5) - exponent(term6)) &
              < maxexponent(term5) .and. term6/=0. ) then
            term1(il,l)=1.+term5/term6
          end if
        end if
      end do
    end do
    term1=min(max(term1,1.),term3)
    term3=term1**term4
    !
    !       * save results for growth factor.
    !
    tecgr1=1.
    tecgr2=0.
    do l=1,leva
      do il=1,ilga
        isl=teicrit(il,l,kextso4)
        if (isl /= ina) then
          tecgr1(il,l,isl)=max(term3(il,l),1.)
          do is=isl+1,isextso4
            tecgr1(il,l,is)=max(term2(il,l),1.)
          end do
        end if
      end do
    end do
  end if
  if (isintso4 > 0) then
    !
    !       * ratio of new aerosol volume concentration over original
    !       * concentration for activated particles and growth factor
    !       * for fully activated size sections.
    !
    term1=pismas/aintf%tp(kintso4)%dens
    where (2+abs(exponent(term1*tiden) - exponent(timass)) &
        < maxexponent(term1*tiden) .and. timass/=0. )
      term1=1.+(term1*tiden)/timass
      term1=min(max(term1,1.),ylarge)
    else where
      term1=1.
    end where
    term2=term1**term4
    !
    !       * the same as above for partially activated size section.
    !       * compute the ratio of volume change per current volume in
    !       * partly activated section (term6) using the change in volume
    !       * in that section (term5).
    !
    term3=term1
    term1=1.
    do l=1,leva
      do il=1,ilga
        isl=tiicrit(il,l)
        if (isl /= ina) then
          term5=(term3(il,l)-1.)*timassp(il,l)
          term6=timassa(il,l)
          if (2+abs(exponent(term5) - exponent(term6)) &
              < maxexponent(term5) .and. term6/=0. ) then
            term1(il,l)=1.+term5/term6
          end if
        end if
      end do
    end do
    term1=min(max(term1,1.),term3)
    term3=term1**term4
    !
    !       * save results for growth factor.
    !
    ticgr1=1.
    ticgr2=0.
    do l=1,leva
      do il=1,ilga
        isl=tiicrit(il,l)
        if (isl /= ina) then
          ticgr1(il,l,isl)=max(term3(il,l),1.)
          do is=isl+1,isintso4
            ticgr1(il,l,is)=max(term2(il,l),1.)
          end do
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * particle growth due to condensation/evaporation. the following
  !     * calculations provide the updated size distributions for
  !     * particle number, sulphur aerosol mass, and mass of other
  !     * aerosol species after the condensation step.
  !
  dersn=0.
  dersm=0.
  if (isextso4 > 0) then
    call pgrpar (tedp0l,tedp1l,tedp2l,tedp3l,tedp4l,tedp5l,tedp6l, &
                     tedm1l,tedm2l,tedm3l,tedp0r,tedp1r,tedp2r,tedp3r, &
                     tedp4r,tedp5r,tedp6r,tedm1r,tedm2r,tedm3r,tedp0s, &
                     tedp1s,tedp2s,tedp3s,tedp4s,tedp5s,tedp6s,tedm1s, &
                     tedm2s,tedm3s,tei0l,tei0r,tecgr1,tecgr2,ten0, &
                     tedryr,tephiss,tedphis,tephi0,tepsi,ilga,leva, &
                     isextso4,ierr,imodc)
    call pgrwth0(tent,dersn,tei0l,tei0r,tedp0l,tedp0r,tedp0s, &
                     ten0,ilga,leva,isextso4)
    tefrc(:,:,:)=1.
    call pgrwthc(temt,dersm,tefrc,tei0l,tei0r, &
                     tedp3l,tedp4l,tedp5l,tedp6l,tedp3r,tedp4r,tedp5r, &
                     tedp6r,tedp3s,tedp4s,tedp5s,tedp6s, &
                     tecgr1,tecgr2,ten0,teddn,ilga,leva,isextso4)
  end if
  dirsn=0.
  dirsms=0.
  dirsmn=0.
  if (isintso4 > 0) then
    call pgrpar (tidp0l,tidp1l,tidp2l,tidp3l,tidp4l,tidp5l,tidp6l, &
                     tidm1l,tidm2l,tidm3l,tidp0r,tidp1r,tidp2r,tidp3r, &
                     tidp4r,tidp5r,tidp6r,tidm1r,tidm2r,tidm3r,tidp0s, &
                     tidp1s,tidp2s,tidp3s,tidp4s,tidp5s,tidp6s,tidm1s, &
                     tidm2s,tidm3s,tii0l,tii0r,ticgr1,ticgr2,tin0, &
                     tidryr,tiphiss,tidphis,tiphi0,tipsi,ilga,leva, &
                     isintso4,ierr,imodc)
    call pgrwth0(tint,dirsn,tii0l,tii0r,tidp0l,tidp0r,tidp0s, &
                     tin0,ilga,leva,isintso4)
    call pgrwthc(timst,dirsms,tifrcs,tii0l,tii0r, &
                     tidp3l,tidp4l,tidp5l,tidp6l,tidp3r,tidp4r,tidp5r, &
                     tidp6r,tidp3s,tidp4s,tidp5s,tidp6s, &
                     ticgr1,ticgr2,tin0,tiddn,ilga,leva,isintso4)
    if (iso4s > 0) then
      tirsmn=0.
      call pgrwthn(timnt,tirsmn,tifrcn,tii0l,tii0r,tidp3l,tidp3r, &
                       tidp3s,tin0,tiddn,ilga,leva,iso4s,isintso4)
      dirsmn=sum(tirsmn,dim=3)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * number and mass fixers. this corrects for any truncation
  !     * errors that may have occurred previously. the approach is
  !     * to scale the total number and mass concentrations so that the
  !     * final total mass will be consistent with the corresponding
  !     * physico-chemical production and loss rates.
  !
  term1=0.
  term2=0.
  term3=0.
  term4=0.
  if (isextso4 > 0) then
    tent=max(tent,0.)
    temt=max(temt,0._r8)
    term1=term1+sum(temt,dim=3)+dersm
    term2=term2+sum(tent,dim=3)+dersn
    term3=term3+pesmas+sum(tem,dim=3)
    term4=term4+sum(ten ,dim=3)
  end if
  if (isintso4 > 0) then
    tint=max(tint,0.)
    timst=max(timst,0._r8)
    term1=term1+sum(timst,dim=3)+dirsms
    term2=term2+sum(tint ,dim=3)+dirsn
    term3=term3+pismas+sum(tims,dim=3)
    term4=term4+sum(tin  ,dim=3)
    if (iso4s > 0) then
      titm1=sum(timnt,dim=3)+tirsmn
      titm2=sum(timn ,dim=3)
    end if
  end if
  !
  do l=1,leva
    do il=1,ilga
      !
      !       * correct aerosol number concentration.
      !
      if (2+abs(exponent(term4(il,l)) - exponent(term2(il,l))) &
          < maxexponent(term4(il,l)) .and. term2(il,l)/=0. ) then
        cfn=term4(il,l)/term2(il,l)
        if (isextso4 > 0) tent(il,l,:)=tent(il,l,:)*cfn
        if (isintso4 > 0) tint(il,l,:)=tint(il,l,:)*cfn
      end if
      !
      !       * correct sulphur aerosol mass concentration.
      !
      if (2+abs(exponent(term3(il,l)) - exponent(term1(il,l))) &
          < maxexponent(term3(il,l)) .and. term1(il,l)/=0. ) then
        cfm=term3(il,l)/term1(il,l)
        if (isextso4 > 0) temt (il,l,:)=temt (il,l,:)*cfm
        if (isintso4 > 0) timst(il,l,:)=timst(il,l,:)*cfm
      end if
    end do
  end do
  !
  !     * correct non-sulphate aerosol mass concentration.
  !
  if (isintso4 > 0) then
    do it=1,iso4s
      do l=1,leva
        do il=1,ilga
          if (2+abs(exponent(titm2(il,l,it)) - exponent(titm1(il,l,it))) &
              < maxexponent(titm2(il,l,it)) .and. titm1(il,l,it)/=0. ) then
            cfm=titm2(il,l,it)/titm1(il,l,it)
            timnt(il,l,:,it)=timnt(il,l,:,it)*cfm
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * total aerosol mass for internally mixed aerosol species.
  !
  if (kintso4 > 0) then
    timt=timst
    do it=1,iso4s
      timt(:,:,:)=timt(:,:,:)+timnt(:,:,:,it)
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * tracer tendencies.
  !
  if (kextso4 > 0) then
    do is=1,isextso4
      pedndt(:,:,iexso4(is))=(tent(:,:,is)-ten(:,:,is))/dt
      pedmdt(:,:,iexso4(is))=(temt(:,:,is)-tem(:,:,is))/dt
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      pidndt(:,:,iinso4(is))=(tint(:,:,is)-tin(:,:,is))/dt
      pidmdt(:,:,iinso4(is))=(timt(:,:,is)-tim(:,:,is))/dt
      where (2+abs(exponent(timst(:,:,is)) - exponent(timt(:,:,is))) &
          < maxexponent(timst(:,:,is)) .and. timt(:,:,is)/=0. )
        pidfdt(:,:,iinso4(is),kintso4)= &
                                            (timst(:,:,is)/timt(:,:,is)-tifrcs(:,:,is))/dt
      else where
        pidfdt(:,:,iinso4(is),kintso4)=-tifrcs(:,:,is)/dt
      end where
      do it=1,iso4s
        where (2+abs(exponent(timnt(:,:,is,it)) - exponent(timt(:,:,is))) &
            < maxexponent(timnt(:,:,is,it)) .and. timt(:,:,is)/=0. )
          pidfdt(:,:,iinso4(is),itso4(it))= &
                                                (timnt(:,:,is,it)/timt(:,:,is)-tifrcn(:,:,is,it))/dt
        else where
          pidfdt(:,:,iinso4(is),itso4(it))=-tifrcn(:,:,is,it)/dt
        end where
      end do
    end do
  end if
  !
  !     * deallocation.
  !
  if (isextso4 > 0) then
    deallocate(tecgr1)
    deallocate(tecgr2)
    deallocate(ten0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(teddn)
    deallocate(tem)
    deallocate(temt)
    deallocate(ten)
    deallocate(tent)
    deallocate(tei0l)
    deallocate(tei0r)
    deallocate(tedp0l)
    deallocate(tedp1l)
    deallocate(tedp2l)
    deallocate(tedp3l)
    deallocate(tedp4l)
    deallocate(tedp5l)
    deallocate(tedp6l)
    deallocate(tedm1l)
    deallocate(tedm2l)
    deallocate(tedm3l)
    deallocate(tedp0r)
    deallocate(tedp1r)
    deallocate(tedp2r)
    deallocate(tedp3r)
    deallocate(tedp4r)
    deallocate(tedp5r)
    deallocate(tedp6r)
    deallocate(tedm1r)
    deallocate(tedm2r)
    deallocate(tedm3r)
    deallocate(tedp0s)
    deallocate(tedp1s)
    deallocate(tedp2s)
    deallocate(tedp3s)
    deallocate(tedp4s)
    deallocate(tedp5s)
    deallocate(tedp6s)
    deallocate(tedm1s)
    deallocate(tedm2s)
    deallocate(tedm3s)
    deallocate(tefrc)
    deallocate(tedphis)
    deallocate(tephiss)
    deallocate(tedryr)
  end if
  if (isintso4 > 0) then
    deallocate(ticgr1)
    deallocate(ticgr2)
    deallocate(timn)
    deallocate(timnt)
    deallocate(tifrcn)
    deallocate(tifrcs)
    deallocate(tin0)
    deallocate(tiphi0)
    deallocate(tipsi)
    deallocate(tiddn)
    deallocate(tim)
    deallocate(timt)
    deallocate(tin)
    deallocate(tint)
    deallocate(tims)
    deallocate(timst)
    deallocate(tii0l)
    deallocate(tii0r)
    deallocate(tidp0l)
    deallocate(tidp1l)
    deallocate(tidp2l)
    deallocate(tidp3l)
    deallocate(tidp4l)
    deallocate(tidp5l)
    deallocate(tidp6l)
    deallocate(tidm1l)
    deallocate(tidm2l)
    deallocate(tidm3l)
    deallocate(tidp0r)
    deallocate(tidp1r)
    deallocate(tidp2r)
    deallocate(tidp3r)
    deallocate(tidp4r)
    deallocate(tidp5r)
    deallocate(tidp6r)
    deallocate(tidm1r)
    deallocate(tidm2r)
    deallocate(tidm3r)
    deallocate(tidp0s)
    deallocate(tidp1s)
    deallocate(tidp2s)
    deallocate(tidp3s)
    deallocate(tidp4s)
    deallocate(tidp5s)
    deallocate(tidp6s)
    deallocate(tidm1s)
    deallocate(tidm2s)
    deallocate(tidm3s)
    deallocate(tirsmn)
    deallocate(titm1)
    deallocate(titm2)
    deallocate(tidphis)
    deallocate(tiphiss)
    deallocate(tidryr)
  end if
  deallocate(term1)
  deallocate(term2)
  deallocate(term3)
  deallocate(term4)
  !
end subroutine sicprd
