!> \file
!> \brief Extracts characteristic non-dimensionalized paricle growth curves
!>       and limits for given initial particle size, supersaturation, and
!>       Koehler parameters.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnextrp (wetrt,svt,akt,bkt,xp,rc,sc,kgr,bsp,irs,irsmax, &
                          wetr,sv,ak,bk,ratp0,irds,irgs,ibs,ilga,leva, &
                          isec,irstmax,imod1,imod2,imod3)
  !
  use cnparm, only : bscp,bsmaxn,bsmaxp,bsminn,bsminp, &
                         ibtmax,idbsb,idef,idpl,idpu, &
                         rbsb,rgb,rgpn,rpmin, &
                         sqrt2,sqrt2p3, &
                         xpderl,xpderu,xpmin,xppntl,xppntu,yna,ysmall
  !
  implicit none
  !
  !      logical, parameter :: kbs=.false.
  logical, parameter :: kbs=.true. !<
  real, parameter :: ytol=1.e-01 !<
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(ilga,leva) :: sv !<
  real, intent(in), dimension(ilga,leva) :: ak !<
  integer, intent(in), dimension(ilga,leva,isec) :: irds !<
  real, intent(in), dimension(ilga,leva,isec) :: wetr !<
  real, intent(in), dimension(ilga,leva,isec) :: bk !<
  real, intent(in), dimension(ilga,leva,isec) :: ratp0 !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod1 !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod2 !<
  integer, intent(inout), dimension(ilga,leva,isec) :: imod3 !<
  real, intent(out), dimension(ilga,leva,isec) :: wetrt !<
  real, intent(out), dimension(ilga,leva,isec) :: xp !<
  real, intent(out), dimension(ilga,leva,isec) :: bkt !<
  real, intent(out), dimension(ilga,leva,isec) :: bsp !<
  real, intent(out), dimension(ilga,leva,isec) :: svt !<
  real, intent(out), dimension(ilga,leva,isec) :: sc !<
  real, intent(out), dimension(ilga,leva,isec) :: rc !<
  integer, intent(out), dimension(ilga,leva,isec) :: irs !<
  integer, intent(out), dimension(ilga,leva,isec) :: irgs !<
  integer, intent(out), dimension(ilga,leva,isec) :: ibs !<
  logical, intent(out), dimension(ilga,leva,isec) :: kgr !<
  real, intent(out), dimension(ilga,leva) :: akt !<
  integer, intent(out), dimension(leva,isec) :: irsmax !<
  logical, allocatable, dimension(:) :: kok !<
  integer, allocatable, dimension(:) :: ibst !<
  integer, allocatable, dimension(:) :: irdsl !<
  integer, allocatable, dimension(:) :: irgl !<
  integer, allocatable, dimension(:) :: ibtl !<
  integer, allocatable, dimension(:,:,:,:) :: irgt !<
  real, allocatable, dimension(:) :: bspl !<
  real, allocatable, dimension(:,:,:) :: xc !<
  !
  integer :: i !<
  integer :: l !<
  integer :: ix !<
  integer :: il !<
  integer :: is !<
  integer :: irdst !<
  integer :: irg !<
  integer :: ib !<
  integer :: id !<
  integer :: ibstt !<
  integer :: idl !<
  integer :: idu !<
  integer :: irstmax !<
  integer :: irgst !<
  integer :: irstt !<
  real :: asp !<
  real :: xpl !<
  real :: xpu !<
  !
  !-----------------------------------------------------------------------
  !
  !     * allocate temporary arrays.
  !
  allocate(kok   (ilga))
  allocate(ibst  (ilga))
  allocate(irdsl (ilga))
  allocate(irgl  (ilga))
  allocate(bspl  (ilga))
  allocate(ibtl  (ilga))
  allocate(xc    (ilga,leva,isec))
  allocate(irgt  (ilga,leva,isec,2))
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  akt=yna
  bkt=yna
  xp=yna
  bsp=yna
  xc=0.
  wetrt=wetr
  irgt=idef
  irgs=idef
  ibs=idef
  do is=1,isec
    where (sv < -ysmall .or. sv > ysmall)
      svt(:,:,is)=sv(:,:)
    else where
      svt(:,:,is)=ysmall
    end where
  end do
  !
  !-----------------------------------------------------------------------
  !     * essential parameters and preliminary determination of growth
  !     * regime.
  !
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        irdst=irds(il,l,is)
        if (irdst /= idef) then
          !
          !         * scaled b-term in koehler equation.
          !
          if (kbs) then
            bsp(il,l,is)=bk(il,l,is)/(sqrt2p3*svt(il,l,is))
            asp=bsp(il,l,is)/ratp0(il,l,is)
          else
            asp=ak(il,l)/(sqrt2*svt(il,l,is))
            bsp(il,l,is)=ratp0(il,l,is)*asp
          end if
          !
          !         * overwrite original results for ak and bk by approximate
          !         * results.
          !
          akt(il,l)=asp*sqrt2*svt(il,l,is)
          bkt(il,l,is)=2.*ratp0(il,l,is)*akt(il,l)
          !
          !         * scaled particle size.
          !
          xp(il,l,is)=.5*wetrt(il,l,is)**2
          !
          !         * restrict particle sizes to given domain. the program
          !         * will abort if the wet particle size is smaller than
          !         * the smallest tabulated size for the droplet growth
          !         * calculations. replace input data table (provided via
          !         * module cnevdt) in that case.
          !
          if (xp(il,l,is) < xpmin(irdst) ) then
            wetrt(il,l,is)=rpmin(irdst)
            xp(il,l,is)=xpmin(irdst)
            imod1(il,l,is)=1
          end if
          !
          !         * restrict supersaturation to given domain.
          !
          if (bsp(il,l,is) >= 0. ) then
            if (bsp(il,l,is) < bsminp(irdst) ) then
              svt(il,l,is)=svt(il,l,is)*bsp(il,l,is)/bsminp(irdst)
              bsp(il,l,is)=bsminp(irdst)
              imod2(il,l,is)=1
            end if
            if (bsp(il,l,is) > bsmaxp(irdst) ) then
              svt(il,l,is)=svt(il,l,is)*bsp(il,l,is)/bsmaxp(irdst)
              bsp(il,l,is)=bsmaxp(irdst)
              imod2(il,l,is)=2
            end if
          else
            if (bsp(il,l,is) < bsminn(irdst) ) then
              svt(il,l,is)=svt(il,l,is)*bsp(il,l,is)/bsminn(irdst)
              bsp(il,l,is)=bsminn(irdst)
              imod2(il,l,is)=3
            end if
            if (bsp(il,l,is) > bsmaxn(irdst) ) then
              svt(il,l,is)=svt(il,l,is)*bsp(il,l,is)/bsmaxn(irdst)
              bsp(il,l,is)=bsmaxn(irdst)
              imod2(il,l,is)=4
            end if
          end if
          !
          !         * critical size for each growth curve.
          !
          xc(il,l,is)=3.*ratp0(il,l,is)
          !
          !         * determine 2 possible growth regimes.
          !
          if (svt(il,l,is) > 0. ) then
            irgt(il,l,is,1)=2
            if (xp(il,l,is) > xc(il,l,is) &
                .and. bsp(il,l,is) > bscp(irdst)                ) then
              irgt(il,l,is,2)=3
            else
              irgt(il,l,is,2)=1
            end if
          else
            irgt(il,l,is,1)=5
            if (xp(il,l,is) <= xc(il,l,is)                      ) then
              irgt(il,l,is,2)=4
            else
              irgt(il,l,is,2)=5
            end if
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * critical radius and supersaturation for each growth curve
  !     * for diagnostic purposes.
  !
  rc=sqrt(2.*xc)
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        irdst=irds(il,l,is)
        if (irdst /= idef) then
          sc(il,l,is)=akt(il,l)/rc(il,l,is)-bkt(il,l,is)/rc(il,l,is)**3
        else
          sc(il,l,is)=yna
          rc(il,l,is)=yna
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * finally determine growth regime.
  !
  do ix=1,2
    do is=1,isec
      do l=1,leva
        irdsl(:)=irds(:,l,is)
        irgl(:)=irgt(:,l,is,ix)
        bspl(:)=bsp(:,l,is)
        !
        !       * check whether bs-parameter is within the range of the
        !       * tabulated data.
        !
        kok=.false.
        do il=1,ilga
          irdst=irdsl(il)
          if (irdst /= idef) then
            irg=irgl(il)
            if (rgb(irg)%flg(irdst) ) then
              if (bspl(il) >= rgb(irg)%bsbmin(irdst) &
                  .and. bspl(il) <= rgb(irg)%bsbmax(irdst) ) then
                kok(il)=.true.
              end if
            end if
          end if
        end do

        !
        !       * determine index for bs-parameter for corresponding
        !       * tabulated data
        !
        do il=1,ilga
          if (kok(il) ) then
            ibtl(il)=rgb(irgl(il))%ibt
          end if
        end do
        ibst=idef
        do ib=1,ibtmax
          do il=1,ilga
            if (kok(il) ) then
              irdst=irdsl(il)
              irg=irgl(il)
              if (ib <= ibtl(il) ) then
                id=idbsb(irg,irdst,ib)
                if (bspl(il) >= rbsb(id) ) then
                  ibst(il)=ib
                end if
              end if
            end if
          end do
        end do
        !
        !       * lower and upper values of the xp-parameter in the tabulated
        !       * data for given bs-parameter. check if xp falls into that range
        !       * and save indices for the tabulated data for that point if that
        !       * is the case. otherwise, no change in particle size is
        !       * anticipated because the particle size is already in equilibrium.
        !
        do il=1,ilga
          if (kok(il) ) then
            irdst=irdsl(il)
            irg=irgl(il)
            ibstt=ibst(il)
            idl=idpl(irg,irdst,ibstt)
            idu=idpu(irg,irdst,ibstt)
            xpl=xppntl(idl)+bspl(il)*xpderl(idl)
            xpu=xppntu(idu)+bspl(il)*xpderu(idu)
            if (     (xp(il,l,is) >= xpl .and. xp(il,l,is) <= xpu) &
                .or. (xp(il,l,is) > xpu &
                .and. rgpn(irg)%kopen(ibstt,irdst))        ) then
              ibs(il,l,is)=ibstt
              irgs(il,l,is)=irg
              imod3(il,l,is)=0
            else
              imod3(il,l,is)=2
            end if
          end if
        end do
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * interpolation of tabulated data.
  !
  irs(:,:,:)=0
  irsmax=0
  irstmax=0
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        irgst=irgs(il,l,is)
        if (irgst /= idef) then
          !
          !           * determine number of points in tabulated data for the given
          !           * slice of the data.
          !
          irdst=irds(il,l,is)
          ibstt=ibs(il,l,is)
          irstt=rgpn(irgst)%ipnts(ibstt,irdst)
          irs(il,l,is)=irstt
          irsmax(l,is)=max(irsmax(l,is),irstt)
          irstmax=max(irstmax,irstt)
          !
          !           * determine if growth is positive or negative. positive
          !           * (negative) growth means that the normalized time
          !           * increases (decreases) with increasing size.
          !
          if (irgst == 2 .or. irgst == 5) then
            kgr(il,l,is)=.false.
          else
            kgr(il,l,is)=.true.
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate temporary arrays.
  !
  deallocate(kok)
  deallocate(ibst)
  deallocate(irdsl)
  deallocate(irgl)
  deallocate(bspl)
  deallocate(ibtl)
  deallocate(xc)
  deallocate(irgt)
  !
end subroutine cnextrp
