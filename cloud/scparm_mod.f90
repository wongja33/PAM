!> \file
!> \brief Arrays for characterisation of properties of internally and
!>       externally mixed aerosols. These arrays use a different (i.e.
!>       generally higher) number of sections than the basic PLA arrays.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module scparm
  !
  use mbnds, only : bnds
  !
  implicit none
  !
  integer, parameter :: isescb=3 !<
  !      integer, parameter :: isescb=1
  integer, parameter :: isesc=2*isescb ! total number of sub-sections
  integer :: isext !<
  integer :: isint !<
  integer :: isextb !<
  integer :: isintb !<
  data isext  / 0 /
  data isint  / 0 /
  data isextb / 0 /
  data isintb / 0 /
  !
  integer, allocatable,  dimension(:) :: cekx !<
  integer, allocatable,  dimension(:) :: ceis !<
  integer, allocatable,  dimension(:) :: ceisvl !<
  integer, allocatable,  dimension(:) :: ceisvr !<
  integer, allocatable,  dimension(:) :: ciis !<
  integer, allocatable,  dimension(:) :: ciisvl !<
  integer, allocatable,  dimension(:) :: ciisvr !<
  real, allocatable, dimension(:) :: cidryrc !<
  real, allocatable, dimension(:) :: cidryrb !<
  real, allocatable, dimension(:) :: cedryrc !<
  real, allocatable, dimension(:) :: cedryrb !<
  type(bnds), allocatable, dimension(:) :: ciphi,cidryr, &
                                               cephi,cedryr
  !
end module scparm
