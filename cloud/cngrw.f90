!> \file
!> \brief Wet particle size from application of Koehler theory and
!>       condensational growth/evaporation based on scaled version
!>       of the growth equation.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cngrw(sv,qrv,rs,qrc,temp,cewmass,cerc,cesc,cewetrb, &
                       cetscl,ciwmass,circ,cisc,ciwetrb,citscl,lmsk, &
                       qr,drdt,hlst,dhdt,zh,rhoa,pres,ceddnsl, &
                       ceddnis,cenuio,cekappa,cemolw,cen0,cepsi,cemom2, &
                       cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
                       ciddnsl,ciddnis,cinuio,cikappa,cimolw,ciepsm, &
                       cin0,cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                       cimom7,cimom8,dt,ilga,leva,cemod1,cemod2, &
                       cemod3,cimod1,cimod2,cimod3,convf,iexkap,iinkap)
  !
  use scparm, only : cedryrb,cidryrb,isext,isextb,isint,isintb
  use cnparm, only : ina,yna,ypi,ysmall
  use sdphys, only : alphc,alpht,cpres,knorm,ksatf,ppa,ppb,rgasm,rhoh2o, &
                         rl,rw1,rw2,rw3,wa,wh2o
  use fpdef,  only : kdbl,r8
  !
  implicit none
  !
  logical, parameter :: kadiab=.false. ! switch to improve convergence
  logical, parameter :: kcalc=.true. ! switch for diagnostic mode
  real, parameter :: svspec=0.0032 !<
  !      integer, parameter :: itmax=4
  integer, parameter :: itmax=7 !<
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  logical, intent(in) , dimension(ilga,leva) :: lmsk !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva) :: pres !<
  real, intent(in), dimension(ilga,leva) :: hlst !<
  real, intent(in), dimension(ilga,leva) :: dhdt !<
  real, intent(in), dimension(ilga,leva) :: drdt !<
  real, intent(in), dimension(ilga,leva) :: qr !<
  real, intent(in), dimension(ilga,leva) :: zh !<
  real, intent(in), dimension(ilga,leva) :: dt !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnsl !<
  real, intent(in), dimension(ilga,leva,isextb) :: ceddnis !<
  real, intent(in), dimension(ilga,leva,isextb) :: cenuio !<
  real, intent(in), dimension(ilga,leva,isextb) :: cekappa !<
  real, intent(in), dimension(ilga,leva,isextb) :: cemolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnsl !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciddnis !<
  real, intent(in), dimension(ilga,leva,isintb) :: cinuio !<
  real, intent(in), dimension(ilga,leva,isintb) :: cikappa !<
  real, intent(in), dimension(ilga,leva,isintb) :: cimolw !<
  real, intent(in), dimension(ilga,leva,isintb) :: ciepsm !<
  real, intent(in), dimension(ilga,leva,isext) :: cen0 !<
  real, intent(in), dimension(ilga,leva,isext) :: cepsi !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom2 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom3 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom4 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom5 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom6 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom7 !<
  real(r8), intent(in), dimension(ilga,leva,isext) :: cemom8 !<
  real, intent(in), dimension(ilga,leva,isint) :: cin0 !<
  real, intent(in), dimension(ilga,leva,isint) :: cipsi !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom2 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom3 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom4 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom5 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom6 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom7 !<
  real(r8), intent(in), dimension(ilga,leva,isint) :: cimom8 !<
  real, intent(inout), dimension(ilga,leva,isextb) :: cewetrb !<
  real, intent(inout), dimension(ilga,leva,isintb) :: ciwetrb !<
  real, intent(inout), dimension(ilga,leva) :: temp !<
  real, intent(inout), dimension(ilga,leva) :: sv !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod1 !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod2 !<
  integer, intent(out), dimension(ilga,leva,isextb) :: cemod3 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod1 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod2 !<
  integer, intent(out), dimension(ilga,leva,isintb) :: cimod3 !<
  real, intent(out), dimension(ilga,leva) :: rs !<
  real, intent(out), dimension(ilga,leva) :: qrv !<
  real, intent(out), dimension(ilga,leva) :: qrc !<
  real, intent(out), dimension(ilga,leva) :: convf !<
  real, intent(out), dimension(ilga,leva,isextb) :: cerc !<
  real, intent(out), dimension(ilga,leva,isextb) :: cesc !<
  real, intent(out), dimension(ilga,leva,isextb) :: cetscl !<
  real, intent(out), dimension(ilga,leva,isintb) :: circ !<
  real, intent(out), dimension(ilga,leva,isintb) :: cisc !<
  real, intent(out), dimension(ilga,leva,isintb) :: citscl !<
  real, intent(out), dimension(ilga,leva,isext) :: cewmass !<
  real, intent(out), dimension(ilga,leva,isint) :: ciwmass !<
  !
  !     internal work variables
  !
  integer, allocatable, dimension(:,:,:) :: ciird !<
  integer, allocatable, dimension(:,:,:) :: ceird !<
  real, allocatable, dimension(:,:,:) :: ciwetrn !<
  real, allocatable, dimension(:,:,:) :: cibk !<
  real, allocatable, dimension(:,:,:) :: cidvp !<
  real, allocatable, dimension(:,:,:) :: citkp !<
  real, allocatable, dimension(:,:,:) :: cirat !<
  real, allocatable, dimension(:,:,:) :: ciwetri !<
  real, allocatable, dimension(:,:,:) :: cewetrn !<
  real, allocatable, dimension(:,:,:) :: cebk !<
  real, allocatable, dimension(:,:,:) :: cedvp !<
  real, allocatable, dimension(:,:,:) :: cetkp !<
  real, allocatable, dimension(:,:,:) :: cerat !<
  real, allocatable, dimension(:,:,:) :: cewetri !<
  real, allocatable, dimension(:,:) :: sfctw !<
  real, allocatable, dimension(:,:) :: ak !<
  real, allocatable, dimension(:,:) :: dv !<
  real, allocatable, dimension(:,:) :: dvx !<
  real, allocatable, dimension(:,:) :: tk !<
  real, allocatable, dimension(:,:) :: tkx !<
  real, allocatable, dimension(:,:) :: tmp1 !<
  real, allocatable, dimension(:,:) :: tmp2 !<
  real, allocatable, dimension(:,:) :: tmp3 !<
  real, allocatable, dimension(:,:) :: esw !<
  real, allocatable, dimension(:,:) :: svi !<
  real, allocatable, dimension(:,:) :: fresl !<
  real, allocatable, dimension(:,:) :: fresu !<
  real, allocatable, dimension(:,:) :: fresn !<
  real, allocatable, dimension(:,:) :: fresnt !<
  real, allocatable, dimension(:,:) :: svt !<
  real, allocatable, dimension(:,:) :: svl !<
  real, allocatable, dimension(:,:) :: svu !<
  real, allocatable, dimension(:,:) :: svn !<
  real, allocatable, dimension(:,:) :: svtt !<
  real, allocatable, dimension(:,:) :: qrci !<
  real, allocatable, dimension(:,:) :: qrcl !<
  real, allocatable, dimension(:,:) :: qrcu !<
  real, allocatable, dimension(:,:) :: qrcn !<
  real, allocatable, dimension(:,:) :: qrct !<
  real, allocatable, dimension(:,:) :: frac !<
  real, allocatable, dimension(:,:) :: tempi !<
  real, allocatable, dimension(:,:) :: rsn !<
  real, allocatable, dimension(:,:) :: tempn !<
  real, allocatable, dimension(:,:) :: qrvn !<
  real, allocatable, dimension(:,:) :: qrhyg !<
  real, allocatable, dimension(:,:) :: hlsthyg !<
  integer, allocatable, dimension(:,:) :: imodn !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: iit !<
  integer :: iinkap !<
  integer :: iexkap !<
  real :: sclf !<
  real :: arat !<
  real :: coru !<
  real :: amsl !<
  real :: ans !<
  !
  !-----------------------------------------------------------------------
  !
  if (itmax < 1) call xit('CNGRW',-1)
  !
  !     * allocate temporary arrays.
  !
  if (isextb > 0) then
    allocate(cewetrn(ilga,leva,isextb))
    allocate(cewetri(ilga,leva,isextb))
    allocate(cebk   (ilga,leva,isextb))
    allocate(cedvp  (ilga,leva,isextb))
    allocate(cetkp  (ilga,leva,isextb))
    allocate(cerat  (ilga,leva,isextb))
    allocate(ceird  (ilga,leva,isextb))
  end if
  if (isintb > 0) then
    allocate(ciwetrn(ilga,leva,isintb))
    allocate(ciwetri(ilga,leva,isintb))
    allocate(cibk   (ilga,leva,isintb))
    allocate(cidvp  (ilga,leva,isintb))
    allocate(citkp  (ilga,leva,isintb))
    allocate(cirat  (ilga,leva,isintb))
    allocate(ciird  (ilga,leva,isintb))
  end if
  allocate(ak     (ilga,leva))
  allocate(sfctw  (ilga,leva))
  allocate(dv     (ilga,leva))
  allocate(dvx    (ilga,leva))
  allocate(tk     (ilga,leva))
  allocate(tkx    (ilga,leva))
  allocate(tmp1   (ilga,leva))
  allocate(tmp2   (ilga,leva))
  allocate(tmp3   (ilga,leva))
  allocate(esw    (ilga,leva))
  allocate(svi    (ilga,leva))
  allocate(svn    (ilga,leva))
  allocate(rsn    (ilga,leva))
  allocate(tempn  (ilga,leva))
  allocate(qrvn   (ilga,leva))
  allocate(qrhyg  (ilga,leva))
  allocate(hlsthyg(ilga,leva))
  allocate(fresn  (ilga,leva))
  allocate(fresl  (ilga,leva))
  allocate(fresu  (ilga,leva))
  allocate(fresnt (ilga,leva))
  allocate(svtt   (ilga,leva))
  allocate(svt    (ilga,leva))
  allocate(svl    (ilga,leva))
  allocate(svu    (ilga,leva))
  allocate(tempi  (ilga,leva))
  allocate(qrci   (ilga,leva))
  allocate(qrcu   (ilga,leva))
  allocate(qrcl   (ilga,leva))
  allocate(qrcn   (ilga,leva))
  allocate(qrct   (ilga,leva))
  allocate(frac   (ilga,leva))
  allocate(imodn  (ilga,leva))
  !
  !-----------------------------------------------------------------------
  !     * surface tension of water.
  !
  sfctw=0.0761-1.55e-04*(temp-273.)
  !
  !     * a-term in koehler equation (for curvature effect).
  !
  ak=2.*wh2o*sfctw/(rgasm*temp*rhoh2o)
  !
  !     * b-term in koehler equation (for solute effect).
  !
  if (isextb > 0) then
    cebk=yna
    if (iexkap /= 1) then
      coru=1.
      do is=1,isextb
        do l=1,leva
          do il=1,ilga
            !
            !           * number of moles of solute.
            !
            amsl=(4.*ypi/3.)*ceddnsl(il,l,is)*cedryrb(is)**3
            ans=amsl*coru*cenuio(il,l,is)/cemolw(il,l,is)
            !
            !           * b-term in koehler equation (for solute effect).
            !
            cebk(il,l,is)=0.75*ans*wh2o/(ypi*rhoh2o)
          end do
        end do
      end do
    else
      do is=1,isextb
        do l=1,leva
          do il=1,ilga
            !
            !           * b-term in koehler equation (for solute effect).
            !
            cebk(il,l,is)=cekappa(il,l,is)*cedryrb(is)**3
          end do
        end do
      end do
    end if
  end if
  if (isintb > 0) then
    cibk=yna
    if (iinkap /= 1) then
      do is=1,isintb
        do l=1,leva
          do il=1,ilga
            if (abs(ciepsm(il,l,is)-yna) > ysmall &
                .and. ciepsm(il,l,is) > ysmall) then
              arat=(1.-ciepsm(il,l,is))/ciepsm(il,l,is)
              if (arat > ysmall) then
                coru=1./(1.+(ciddnsl(il,l,is)/ciddnis(il,l,is))*arat)
              else
                coru=1.
              end if
              !
              !             * number of moles of solute.
              !
              amsl=(4.*ypi/3.)*ciddnsl(il,l,is)*cidryrb(is)**3
              ans=amsl*coru*cinuio(il,l,is)/cimolw(il,l,is)
              !
              !             * b-term in koehler equation (for solute effect).
              !
              cibk(il,l,is)=0.75*ans*wh2o/(ypi*rhoh2o)
            end if
          end do
        end do
      end do
    else
      do is=1,isintb
        do l=1,leva
          do il=1,ilga
            if (abs(cikappa(il,l,is)-yna) > ysmall &
                .and. cikappa(il,l,is) > ysmall &
                .and. abs(ciepsm(il,l,is) -yna) > ysmall &
                .and. ciepsm(il,l,is) > ysmall) then
              arat=(1.-ciepsm(il,l,is))/ciepsm(il,l,is)
              if (arat > ysmall) then
                coru=1./(1.+(ciddnsl(il,l,is)/ciddnis(il,l,is))*arat)
              else
                coru=1.
              end if
              !
              !             * b-term in koehler equation (for solute effect).
              !
              cibk(il,l,is)=cikappa(il,l,is)*coru*cidryrb(is)**3
            end if
          end do
        end do
      end do
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * associate appropriate look-up data tables.
  !
  if (isextb > 0) then
    call cnrat(cerat,ceird,ak,cebk,ilga,leva,isextb)
  end if
  if (isintb > 0) then
    call cnrat(cirat,ciird,ak,cibk,ilga,leva,isintb)
  end if
  !
  !-----------------------------------------------------------------------
  !     * diffusivity of water vapour in air (in m**2/s).
  !
  tmp1=(temp/273.)**1.94
  dv=(0.211e-04/(pres*1.e-05))*tmp1
  tmp3=2.*ypi*wh2o/(rgasm*temp)
  tmp1=sqrt(tmp3)
  dvx=(dv/alphc)*tmp1
  !
  !     * thermal conductivity of air (j/m/s/k).
  !
  tk=(23.84+0.071*temp)*1.e-03
  tkx=(tk/(alpht*rhoa*cpres))*tmp1*sqrt(wa/wh2o)
  !
  !     * saturation vapour pressure over water.
  !
  if (ksatf) then
    if (kdbl) then
      tmp3=rw1+rw2/temp
      tmp1=exp(tmp3)
      tmp3=rw3
      tmp2=temp**tmp3
    else
      tmp3=rw1+rw2/temp
      tmp1=exp(tmp3)
      tmp3=rw3
      tmp2=temp**tmp3
    end if
    esw=1.e+02*tmp1*tmp2
  else
    do l=1,leva
      do il=1,ilga
        esw(il,l)=1.e+02*exp(ppa-ppb/temp(il,l))
      end do
    end do
  end if
  !
  !     * modified diffusivity, thermal conductivity, and growth time
  !     * scale parameter.
  !
  if (isextb > 0) then
    call cntscl(cetscl,cedvp,cetkp,cewetrb,dvx,tkx,esw,temp,dv,tk, &
                    ilga,leva,isextb)
  end if
  if (isintb > 0) then
    call cntscl(citscl,cidvp,citkp,ciwetrb,dvx,tkx,esw,temp,dv,tk, &
                    ilga,leva,isintb)
  end if
  !
  !-----------------------------------------------------------------------
  !       * calculate supersaturation and particle sizes.
  !
  if (kcalc) then
    !
    !       * calculate initial amount of condensed water.
    !
    call scwatm(qrc,cewmass,ciwmass,cen0,cepsi,cewetrb,cemom2, &
                    cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
                    cin0,cipsi,ciwetrb,cimom2,cimom3,cimom4,cimom5, &
                    cimom6,cimom7,cimom8,ilga,leva)
    !
    !       * save intitial values.
    !
    svi=sv
    qrci=qrc
    tempi=temp
    if (isextb > 0) cewetri=cewetrb
    if (isintb > 0) ciwetri=ciwetrb
    !
    !       * minimum possible amount of condensed water at zero
    !       * supersaturation to account for hygroscopic growth.
    !
    svt=0.
    call cnigwm(fresn,svn,svt,qrct,qrv,rs,temp,cerc,cesc, &
                    cewmass,cewetrn,cewetrb,ciwmass,ciwetrn, &
                    ciwetrb,circ,cisc,cebk,cetscl,cerat,ceird, &
                    cen0,cepsi,cemom2,cemom3,cemom4,cemom5,cemom6, &
                    cemom7,cemom8,cibk,citscl,cirat,ciird,cin0, &
                    cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                    cimom7,cimom8,hlst,dhdt,qr,drdt,zh,pres,ak, &
                    svi,dt,ilga,leva,cemod1,cemod2,cemod3,cimod1, &
                    cimod2,cimod3)
    !
    !       * adjust total water mixing ratio and static energy to
    !       * account for minimum cloud water amount. the initial
    !       * total water mixing ratio only accounts for water vapour
    !       * load, which is lower and may be insufficient for formation
    !       * of a cloud.
    !
    qrhyg=qr+qrct
    hlsthyg=hlst
    if ( .not.knorm) then
      hlsthyg=hlsthyg-rl*qrct
    end if
    !
    !       * implied supersaturation for given cloud water content.
    !
    !        call cnsbnd(svt,qrvn,rsn,tempn,qrc,hlsthyg,dhdt,qrhyg,
    !     1              drdt,zh,pres,dt,ilga,leva)
    !
    !       * estimated range of supersaturation (from svl to svu).
    !       * and initial guess for supersaturation (svt).
    !
    !        where ( svt < -ysmall)
    !          svl=-0.01
    !          svu=svl*0.1
    !        else where ( svt > ysmall)
    !          svu=svt
    !          svl=svt*0.1
    !        else where
    !          svu=0.01
    !          svl=-0.01
    !        end where
    !
    !       * select initial supersaturation as best guess for new value
    !       * and determine range of values by scaling best guess.
    !
    svt=max(svi,1.e-04)
    sclf=10.
    !        sclf=3.
    svl=svt/sclf
    svu=svt*sclf
    !
    !       * ensure plausible range.
    !
    where ( .not. (svt-svl > ysmall .and. svu-svt > ysmall) )
      frac=0.5
      svt=svl*frac+(1.-frac)*svu
    end where
    where (abs(svt) < ysmall)
      svt=ysmall
    end where
    !
    !       * residual for initially highest supersaturation.
    !
    if (isextb > 0) cewetrb=cewetri
    if (isintb > 0) ciwetrb=ciwetri
    call cnigwm(fresu,svn,svu,qrcu,qrv,rs,temp,cerc,cesc, &
                    cewmass,cewetrn,cewetrb,ciwmass,ciwetrn, &
                    ciwetrb,circ,cisc,cebk,cetscl,cerat,ceird, &
                    cen0,cepsi,cemom2,cemom3,cemom4,cemom5,cemom6, &
                    cemom7,cemom8,cibk,citscl,cirat,ciird,cin0, &
                    cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                    cimom7,cimom8,hlsthyg,dhdt,qrhyg,drdt,zh,pres,ak, &
                    svi,dt,ilga,leva,cemod1,cemod2,cemod3,cimod1, &
                    cimod2,cimod3)
    !
    !       * residual for initially lowest supersaturation.
    !
    if (isextb > 0) cewetrb=cewetri
    if (isintb > 0) ciwetrb=ciwetri
    call cnigwm(fresl,svn,svl,qrcl,qrv,rs,temp,cerc,cesc, &
                    cewmass,cewetrn,cewetrb,ciwmass,ciwetrn, &
                    ciwetrb,circ,cisc,cebk,cetscl,cerat,ceird, &
                    cen0,cepsi,cemom2,cemom3,cemom4,cemom5,cemom6, &
                    cemom7,cemom8,cibk,citscl,cirat,ciird,cin0, &
                    cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                    cimom7,cimom8,hlsthyg,dhdt,qrhyg,drdt,zh,pres,ak, &
                    svi,dt,ilga,leva,cemod1,cemod2,cemod3,cimod1, &
                    cimod2,cimod3)
    !
    !       * bi-section of residual based on the previous 2 results
    !       * to obtain improved estimate of supersaturation (root finding).
    !       * based on weighted mean so that the improved estimate is
    !       * closer to the supersaturation that produces the smallest
    !       * residual. allow new estimate to exceed the initial range
    !       * if the root is outside that range. this will be the case if
    !       * the amount of condensed water for the lower value of the
    !       * supersaturation exceeds the adiabatic value or if the
    !       * residual does not switch sign.
    !
    where (kadiab .and. qrcl >= qrci .and. svl > ysmall)
      svt=(svl/svu)*svl
    else where (fresu*fresl > ysmall)
      where (fresl > ysmall)
        svt=(svu/svl)*svu
      else where (fresl < -ysmall)
        svt=(svl/svu)*svl
      end where
    end where
    where (abs(svt) < ysmall)
      svt=ysmall
    end where
    !
    !       * iterations to further improve estimate of supersaturation and
    !       * corresponding condensate distribution. the minimization of the
    !       * residual is equivalent to increasing the accuracy of the
    !       * water budget.
    !
    do iit=1,itmax
      !
      !         * residual from temporal evolution of condensate distribution.
      !
      if (isextb > 0) cewetrb=cewetri
      if (isintb > 0) ciwetrb=ciwetri
      call cnigwm(fresn,svn,svt,qrcn,qrv,rs,temp,cerc,cesc, &
                      cewmass,cewetrn,cewetrb,ciwmass,ciwetrn, &
                      ciwetrb,circ,cisc,cebk,cetscl,cerat,ceird, &
                      cen0,cepsi,cemom2,cemom3,cemom4,cemom5,cemom6, &
                      cemom7,cemom8,cibk,citscl,cirat,ciird,cin0, &
                      cipsi,cimom2,cimom3,cimom4,cimom5,cimom6, &
                      cimom7,cimom8,hlsthyg,dhdt,qrhyg,drdt,zh,pres,ak, &
                      svi,dt,ilga,leva,cemod1,cemod2,cemod3,cimod1, &
                      cimod2,cimod3)
      imodn=0
      do is=1,isextb
        where (cemod2(:,:,is)==2) imodn(:,:)=1
      end do
      do is=1,isintb
        where (cimod2(:,:,is)==2) imodn(:,:)=1
      end do
      !
      !         * establish correct order of the three supersaturation points
      !         * so that svl < svt < svu.
      !
      where (svt < svl)
        svtt=svl
        fresnt=fresl
        qrct=qrcl
        svl=svt
        fresl=fresn
        qrcl=qrcn
        svt=svtt
        fresn=fresnt
        qrcn=qrct
      else where (svt > svu)
        svtt=svu
        fresnt=fresu
        qrct=qrcu
        svu=svt
        fresu=fresn
        qrcu=qrcn
        svt=svtt
        fresn=fresnt
        qrcn=qrct
      end where
      !
      !         * update local bounds of the supersaturation interval by
      !         * replacing one of the bounds from previous calculations
      !         * with the current result.
      !
      where (fresn*fresl < -ysmall)
        svu=svt
        fresu=fresn
        qrcu=qrcn
      else where (fresn*fresu < -ysmall)
        svl=svt
        fresl=fresn
        qrcl=qrcn
      else where (fresl > ysmall)
        svl=svt
        fresl=fresn
        qrcl=qrcn
      else where
        svu=svt
        fresu=fresn
        qrcu=qrcn
      end where
      !
      !         * new guess for optimal supersaturation value.
      !
      where (kadiab .and. qrcl >= qrci .and. svl > ysmall)
        svt=(svl/svu)*svl
      else where (fresu*fresl < ysmall)
        frac=0.5
        svt=svl*frac+(1.-frac)*svu
      else where (fresl > ysmall)
        svt=(svu/svl)*svu
      else where (fresl < -ysmall)
        svt=(svl/svu)*svl
      end where
      where (abs(svt) < ysmall)
        svt=ysmall
      end where
    end do
  else
    svt=svspec
    sv=svspec
  end if
  !
  !-----------------------------------------------------------------------
  !     * final supersaturation and liquid water content.
  !
  if (kcalc) sv=svt
  qrc=qrcn
  !
  !-----------------------------------------------------------------------
  !     * reset results to initial values for subsaturated conditions.
  !
  if (isextb > 0) then
    do is=1,isextb
      where (svn < 0. )
        cewetrb(:,:,is)=cewetri(:,:,is)
      end where
    end do
  end if
  if (isintb > 0) then
    do is=1,isintb
      where (svn < 0. )
        ciwetrb(:,:,is)=ciwetri(:,:,is)
      end where
    end do
  end if
  where (svn < 0. )
    sv=0.
    qrc=qrci
  end where
  !
  !-----------------------------------------------------------------------
  !     * diagnosis of truncation error of the iterative method.
  !
  convf=fresn/max(sv,ysmall)
  !
  !     * overwrite results flagged as invalid.
  !
  where ( .not.lmsk)
    qrc=qrci
    temp=tempi
    rs=yna
    sv=yna
    qrv=yna
    convf=yna
  end where
  if (isext > 0) then
    do il=1,ilga
      do l=1,leva
        if ( .not.lmsk(il,l) ) then
          do is=1,isextb
            cewetrb(il,l,is)=cewetri(il,l,is)
            cemod1 (il,l,is)=ina
            cemod2 (il,l,is)=ina
            cemod3 (il,l,is)=ina
            cerc   (il,l,is)=yna
            cesc   (il,l,is)=yna
          end do
          do is=1,isext
            cewmass(il,l,is)=yna
          end do
        end if
      end do
    end do
  end if
  if (isint > 0) then
    do il=1,ilga
      do l=1,leva
        if ( .not.lmsk(il,l) ) then
          do is=1,isintb
            ciwetrb(il,l,is)=ciwetri(il,l,is)
            cimod1 (il,l,is)=ina
            cimod2 (il,l,is)=ina
            cimod3 (il,l,is)=ina
            circ   (il,l,is)=yna
            cisc   (il,l,is)=yna
          end do
          do is=1,isint
            ciwmass(il,l,is)=yna
          end do
        end if
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate temporary arrays.
  !
  if (isextb > 0) then
    deallocate(cewetrn)
    deallocate(cewetri)
    deallocate(cebk)
    deallocate(cedvp)
    deallocate(cetkp)
    deallocate(cerat)
    deallocate(ceird)
  end if
  if (isintb > 0) then
    deallocate(ciwetrn)
    deallocate(ciwetri)
    deallocate(cibk)
    deallocate(cidvp)
    deallocate(citkp)
    deallocate(cirat)
    deallocate(ciird)
  end if
  deallocate(ak)
  deallocate(sfctw)
  deallocate(dv)
  deallocate(dvx)
  deallocate(tk)
  deallocate(tkx)
  deallocate(tmp1)
  deallocate(tmp2)
  deallocate(tmp3)
  deallocate(esw)
  deallocate(svi)
  deallocate(svn)
  deallocate(rsn)
  deallocate(tempn)
  deallocate(qrvn)
  deallocate(qrhyg)
  deallocate(hlsthyg)
  deallocate(fresn)
  deallocate(fresl)
  deallocate(fresu)
  deallocate(fresnt)
  deallocate(svtt)
  deallocate(svt)
  deallocate(svl)
  deallocate(svu)
  deallocate(tempi)
  deallocate(qrci)
  deallocate(qrcu)
  deallocate(qrcl)
  deallocate(qrcn)
  deallocate(qrct)
  deallocate(frac)
  deallocate(imodn)
  !
end subroutine cngrw
