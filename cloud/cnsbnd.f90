!> \file
!> \brief Calculates supersaturation for given cloud water content based
!>       on conservation of energy and water.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cnsbnd(svi,qrvn,rsn,tempn,qrc,hlst,dhdt,qr, &
                        drdt,zh,pres,dt,ilga,leva)
  !
  use cnparm, only : ylarge,ysmall
  use sdphys, only : cpres,eps1,grav,knorm,ksatf,ppa,ppb,rl,rw1,rw2,rw3
  use fpdef,  only : kdbl
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva) :: qr !< Total water mixing ratio
  real, intent(in), dimension(ilga,leva) :: drdt !< Total water mass mixing ratio tendency
  real, intent(in), dimension(ilga,leva) :: hlst !< Liquid water static energy
  real, intent(in), dimension(ilga,leva) :: dhdt !< Total energy
  real, intent(in), dimension(ilga,leva) :: zh !< Height of grid cell centres
  real, intent(in), dimension(ilga,leva) :: pres !< Pressure
  real, intent(in), dimension(ilga,leva) :: qrc !< Cloud water mixing ratio
  real, intent(in), dimension(ilga,leva) :: dt !< Model time step \f$[s]\f$
  real, intent(out), dimension(ilga,leva) :: rsn !< Updated saturated mixing ratio
  real, intent(out), dimension(ilga,leva) :: tempn !< Updated temperature
  real, intent(out), dimension(ilga,leva) :: qrvn !< Updated water vapour mixing ratio
  real, intent(out), dimension(ilga,leva) :: svi !< Supersaturation
  !
  !     internal work variables
  !
  real, dimension(ilga,leva) :: relh !<
  real, dimension(ilga,leva) :: tmp1 !<
  real, dimension(ilga,leva) :: tmp2 !<
  real, dimension(ilga,leva) :: tmp3 !<
  real, dimension(ilga,leva) :: esw !<
  real, dimension(ilga,leva) :: qrct !<
  real, parameter :: yepsm=0.999 !<
  integer :: l !<
  integer :: il !<
  integer :: i !<
  integer :: k !<
  !
  !-----------------------------------------------------------------------
  !
  !     * make sure cloud water mixing ratio does not exceed
  !     * total water mixing ratio.
  !
  qrct=qrc
  tmp1=qr+drdt*dt
  where (qrct >= tmp1)
    qrct=yepsm*tmp1
  end where
  !
  !     * update water vapour and temperature. either moist static
  !     * energy or liquid/ice water static energy is used, depending
  !     * on the model input.
  !
  qrvn=qr+drdt*dt-qrct
  if (knorm) then
    tempn=(hlst+dhdt*dt-rl*qrvn-grav*zh)/cpres
  else
    tempn=(hlst+dhdt*dt+rl*qrct-grav*zh)/cpres
  end if
  tempn = max(tempn, 180.)                                  ! ar: set lower limit for temperature as 180k
  !
  !     * update saturation mixing ratio.
  !
  if (ksatf) then
    if (kdbl) then
      tmp3=rw1+rw2/tempn
      tmp1=exp(tmp3)
      tmp3=rw3
      tmp2=tempn**tmp3
    else
      tmp3=rw1+rw2/tempn
      tmp1=exp(tmp3)
      tmp3=rw3
      tmp2=tempn**tmp3
    end if
    esw=1.e+02*tmp1*tmp2
  else
    do l=1,leva
      do il=1,ilga
        esw(il,l)=1.e+02*exp(ppa-ppb/tempn(il,l))
      end do
    end do
  end if
  where (abs(pres-esw) > ysmall)
    rsn=eps1*esw/(pres-esw)
  else where
    rsn=ylarge
  end where
  !
  !     * implied relative humidity and supersaturation for given
  !     * temperature and water vapour.
  !
  where (abs(rsn-ylarge) > ysmall)
    relh=qrvn/rsn*(1.+rsn/eps1)/(1.+qrvn/eps1)
  else where
    relh=0.
  end where
  svi=relh-1.
  !
end subroutine cnsbnd
