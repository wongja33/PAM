!> \file
!> \brief Calculation of various moments of the dry aerosol size
!>       distribution. Results will be used in subsequent calculations
!>       such as for water content. Calculations are for the sub-grid
!>       sections that are used in the condensation/evaporation
!>       calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine scmom(cen0,cephi0,cepsi,cin0,ciphi0,cipsi,cemom0, &
                       cemom2,cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
                       cimom0,cimom2,cimom3,cimom4,cimom5,cimom6,cimom7, &
                       cimom8,cephis0,cedphi0,ciphis0,cidphi0,pen0, &
                       pephi0,pepsi,pin0,piphi0,pipsi,ilga,leva)
  !
  use scparm, only : isesc,isext,isint
  use sdparm, only : isaext,isaint,iswext,iswint,r0,sextf,sintf,yna,ytiny
  use sdcode, only : sdint,sdint0
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva,isext) :: cephis0 !< Externally mixed aerosol dry particle size [ln(Rp /R0)] for CCN-related calculations
  real, intent(in), dimension(ilga,leva,isext) :: cedphi0 !< Externally mixed aerosol dry particle radius for CCN-related calculations
  real, intent(in), dimension(ilga,leva,isint) :: ciphis0 !< Internally mixed aerosol dry particle size [ln(Rp /R0)] for CCN-related calculations
  real, intent(in), dimension(ilga,leva,isint) :: cidphi0 !< Internally mixed aerosol dry particle radius for CCN-related calculations
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(out), dimension(ilga,leva,isext) :: cen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) (ext. mixture) for sub-grid sections
  real, intent(out), dimension(ilga,leva,isext) :: cephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width) (ext. mixture) for sub-grid sections
  real, intent(out), dimension(ilga,leva,isext) :: cepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) (ext. mixture) for sub-grid sections
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom0 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom2 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom3 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom4 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom5 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom6 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom7 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom8 !< Third and fourth moment of the size distribution for externally mixed types of
  !< aerosols and mass of water
  real, intent(out), dimension(ilga,leva,isint) :: cin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) (int. mixture) for sub-grid sections
  real, intent(out), dimension(ilga,leva,isint) :: ciphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width) (int. mixture) for sub-grid sections
  real, intent(out), dimension(ilga,leva,isint) :: cipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) (int. mixture) for sub-grid sections
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom0 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom2 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom3 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom4 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom5 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom6 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom7 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom8 !< Third and fourth moment of the size distribution for internally mixed types of aerosols
  !
  !     internal work variables
  !
  integer :: isc !<
  integer :: ism !<
  integer :: ist !<
  integer :: l !<
  integer :: il !<
  integer :: is !<
  integer :: isi !<
  real :: rmom !<
  !
  !-----------------------------------------------------------------------
  !
  !     * pla parameters for externally mixed aerosol types.
  !
  isc=0
  do is=1,iswext
    ism=sextf%iswet(is)%ism
    do ist=1,isesc
      isc=isc+1
      do l=1,leva
        do il=1,ilga
          cen0  (il,l,isc)=pen0  (il,l,ism)
          cephi0(il,l,isc)=pephi0(il,l,ism)
          cepsi (il,l,isc)=pepsi (il,l,ism)
        end do
      end do
    end do
  end do
  !
  !     * pla parameters for internally mixed aerosol types.
  !
  isc=0
  do is=1,iswint
    isi=sintf%iswet(is)%isi
    do ist=1,isesc
      isc=isc+1
      do l=1,leva
        do il=1,ilga
          cin0  (il,l,isc)=pin0  (il,l,isi)
          ciphi0(il,l,isc)=piphi0(il,l,isi)
          cipsi (il,l,isc)=pipsi (il,l,isi)
        end do
      end do
    end do
  end do
  !
  !     * third and fourth moment of the size distribution for externally
  !     * mixed types of aerosols and mass of water.
  !
  if (isext > 0) then
    cemom0=sdint0(cephi0,cepsi,cephis0,cedphi0,ilga,leva,isext)
    rmom=2.
    cemom2=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=3.
    cemom3=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=4.
    cemom4=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=5.
    cemom5=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=6.
    cemom6=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=7.
    cemom7=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom=8.
    cemom8=sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    where (abs(cepsi-yna) <= ytiny)
      cemom0=yna
      cemom2=yna
      cemom3=yna
      cemom4=yna
      cemom5=yna
      cemom6=yna
      cemom7=yna
      cemom8=yna
    end where
  end if
  !
  !     * third and fourth moment of the size distribution for internally
  !     * mixed types of aerosols.
  !
  if (isint > 0) then
    cimom0=sdint0(ciphi0,cipsi,ciphis0,cidphi0,ilga,leva,isint)
    rmom=2.
    cimom2=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=3.
    cimom3=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=4.
    cimom4=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=5.
    cimom5=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=6.
    cimom6=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=7.
    cimom7=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom=8.
    cimom8=sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    where (abs(cipsi-yna) <= ytiny)
      cimom0=yna
      cimom2=yna
      cimom3=yna
      cimom4=yna
      cimom5=yna
      cimom6=yna
      cimom7=yna
      cimom8=yna
    end where
  end if
  !
end subroutine scmom
