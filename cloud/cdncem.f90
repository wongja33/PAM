!> \file
!> \brief Cloud droplet number concentration based on simple empirical
!>       relationship between cdnc and sulphate concentration.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cdncem(emcdnc,pemas,pepsi,pimas,pipsi,pifrc, &
                        rhoa,ilga,leva)
  !
  use sdparm, only : iexso4,iinso4,isaext,isaint,isextso4,isintso4, &
                         kextso4,kint,kintso4,wamsul,wso4,yna,ysmall,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga,leva)             :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isaext)      :: pemas !< Aerosol (dry) mass concentration for externally mixed aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaext)      :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaint)      :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isaint)      :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(out), dimension(ilga,leva)            :: emcdnc !< CDNC concentration (per kg of air)
  real, dimension(ilga,leva)                         :: sulph !<
  real, dimension(ilga,leva)                         :: term !<
  integer :: is !<
  !
  !-----------------------------------------------------------------------
  !     * sulphate concentration (mg/m**3).
  !
  sulph=0.
  if (kintso4 > 0) then
    do is=1,isintso4
      where (abs(pipsi(:,:,iinso4(is))-yna) > ytiny)
        sulph=sulph+pimas(:,:,iinso4(is)) &
                                         *pifrc(:,:,iinso4(is),kintso4)
      end where
    end do
  end if
  if (kextso4 > 0) then
    do is=1,isextso4
      where (abs(pepsi(:,:,iexso4(is))-yna) > ytiny)
        sulph=sulph+pemas(:,:,iexso4(is))
      end where
    end do
  end if
  where (sulph > ysmall)
    sulph=sulph*wso4/wamsul*rhoa*1.e+09
  else where
    sulph=0.
  end where
  !
  !-----------------------------------------------------------------------
  !     * cdnc concentration (per kg of air).
  !
  term=0.3
  emcdnc=sulph**term
  emcdnc=emcdnc*66.*1.e+06/rhoa
  !
end subroutine cdncem
