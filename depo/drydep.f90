!> \file
!> \brief Dry deposition velocities for aerosol number and mass concentrations.
!!
!! @authors X. Ma, K. von Salzen, and M. Lazare
!
!-----------------------------------------------------------------------
subroutine drydep(ilga,leva,zspd,fcan,cdml,cdmnl,flnd,ican, &
                        fcs,fgs,fc,fg,t,p,dp,dt,rhoa,grav, &
                        anum,amas,drydn,stick,wetrc,dryrc, &
                        phi0,phis0,dphi0,psi,isec, &
                        adndt,admdt,flxn,flxm,vdn,vdm)
  !
  use sdparm, only : dnh2o,r0,yna,ytiny,ytiny8
  use sdcode, only : sdint, sdint0
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in), dimension(ilga) :: cdml !< Surface drag coefficient over land \f$[m^2/s^2]\f$
  real, intent(in), dimension(ilga) :: cdmnl !< Surface drag coefficient over water, ice, and open ocean \f$[m^2/s^2]\f$
  real, intent(in), dimension(ilga) :: flnd !< Fraction of land in a grid cell
  real, intent(in), dimension(ilga) :: zspd !< Surface wind speed \f$[m/sec]\f$
  real, intent(in), dimension(ilga) :: fcs !< Fraction of canopy over snow on ground in a grid cell
  real, intent(in), dimension(ilga) :: fgs !< Fraction of snow on bare ground in a grid cell
  real, intent(in), dimension(ilga) :: fc !< Fraction of snow-free canopy in a grid cell
  real, intent(in), dimension(ilga) :: fg !< Fraction of snow-free bare ground in a grid cell
  real, intent(in), dimension(ilga,ican+1) :: fcan !<  Fraction of canopy type
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva) :: dp !< Depth of grid cells \f$[Pa]\f$
  real, intent(in), dimension(ilga,leva) :: p !< Air pressure \f$[Pa]\f$
  real, intent(in) :: grav !< Gravtiational accelaration \f$[m/s^2]\f$
  !
  real, intent(in), dimension(isec) :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: psi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: anum !< Number
  real, intent(in), dimension(ilga,leva,isec) :: amas !< Mass
  real, intent(in), dimension(ilga,leva,isec) :: stick !< Sticking probability
  real, intent(out), dimension(ilga,leva,isec) :: adndt !< Surface removal number
  real, intent(out), dimension(ilga,leva,isec) :: admdt !< Surface removal mass
  real, intent(out), dimension(ilga,leva,isec) :: flxn !< Dry deposition flux number
  real, intent(out), dimension(ilga,leva,isec) :: flxm !< Dry deposition flux mass
  real, intent(out), dimension(ilga,isec) :: vdn !< Dry deposition velocity number \f$[m/s]\f$
  real, intent(out), dimension(ilga,isec) :: vdm !< Dry deposition velocity mass \f$[m/s]\f$
  !
  !     internal work variables
  !
  real(r8), dimension(ilga,leva,isec) :: am0 !<
  real(r8), dimension(ilga,leva,isec) :: am1 !<
  real(r8), dimension(ilga,leva,isec) :: am2 !<
  real(r8), dimension(ilga,leva,isec) :: am3 !<
  real(r8), dimension(ilga,leva,isec) :: am4 !<
  real(r8), dimension(ilga,leva,isec) :: am5 !<
  real, dimension(ilga,leva,isec) :: phi0w !<
  real, dimension(ilga,leva,isec) :: phis0w !<
  real, dimension(ilga,leva,isec) :: wetdn !<
  real, dimension(ilga) :: vcfl !<
  real, dimension(ilga,leva,isec) :: rawl !<
  real, dimension(ilga,leva,isec) :: radl !<
  real, dimension(ilga,leva,isec) :: rawr !<
  real, dimension(ilga,leva,isec) :: radr !<
  real, dimension(ilga,leva,isec) :: ascn !<
  real, dimension(ilga,leva,isec) :: ascm !<
  real, dimension(ilga,leva,isec) :: asc !<
  real, dimension(ilga,isec) :: vdl !<
  real, dimension(ilga,isec) :: vdr !<
  !
  real, dimension(ilga,leva,isec) :: pefn !<
  real, dimension(ilga,leva,isec) :: pefm !<
  real, dimension(ilga,leva,isec) :: dvdn !<
  real, dimension(ilga,leva,isec) :: dvdm !<
  real, dimension(ilga,isec) :: pesfn !<
  real, dimension(ilga,isec) :: pesfm !<
  integer :: is !<
  integer :: il !<
  integer :: ican !<
  integer :: isec !<
  integer :: l !<
  real :: dvdp !<
  real :: dephi !<
  real :: dt !<
  real :: fgr !<
  real :: alfgr !<
  real :: fgr3i !<
  real :: rmom !<
  !
  !-----------------------------------------------------------------------
  !
  !     * size-related parameters.
  !
  phi0w =phi0
  phis0w=phis0
  wetdn=drydn
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny .and. amas(il,l,is) > ytiny .and. abs(psi(il,l,is)-yna) > ytiny) then
          fgr=wetrc(il,l,is)/dryrc(is)
          alfgr=log(fgr)
          phi0w (il,l,is)=phi0w (il,l,is)+alfgr
          phis0w(il,l,is)=phis0w(il,l,is)+alfgr
          fgr3i=1./fgr**3
          wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
        end if
      end do
    end do
  end do
  !
  am0=sdint0(phi0w,psi,phis0w,dphi0,ilga,leva,isec)
  rmom=1.
  am1=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=2.
  am2=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=3.
  am3=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=4.
  am4=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=5.
  am5=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  !
  !----------------------------------------------------------------------- &
  !      dry deposition velocity.
  !
  do is=1,isec
    rawl(:,:,is)=r0*exp(phis0w(:,:,is))
    radl(:,:,is)=r0*exp(phis0 (:,:,is))
  end do
  call vsrad(rawl,radl,stick,t,p,rhoa,flnd,fcan, &
                 fcs,fgs,fc,fg,zspd,cdml,cdmnl, &
                 vdl,grav,ilga,leva,isec,ican)
  !
  do is=1,isec
    rawr(:,:,is)=r0*exp(phis0w(:,:,is)+dphi0(:,:,is))
    radr(:,:,is)=r0*exp(phis0 (:,:,is)+dphi0(:,:,is))
  end do
  call vsrad(rawr,radr,stick,t,p,rhoa,flnd,fcan, &
                 fcs,fgs,fc,fg,zspd,cdml,cdmnl, &
                 vdr,grav,ilga,leva,isec,ican)
  !
  dvdm=0.
  dvdn=0.
  flxn=0.
  flxm=0.
  adndt=0.
  admdt=0.
  !
  l=leva
  do is=1,isec
    do il=1,ilga
      if (am0(il,l,is) > ytiny8 .and. am3(il,l,is) > ytiny8 .and. abs(psi(il,l,is)-yna) > ytiny) then
        dvdp=vdr(il,is)-vdl(il,is)
        dephi=exp(phis0w(il,l,is)+dphi0(il,l,is)) &
               -exp(phis0w(il,l,is))
        dvdn(il,l,is)=vdl(il,is) &
                 +dvdp/dephi*(am1(il,l,is)/am0(il,l,is))/r0 &
                 -dvdp/dephi*exp(phis0w(il,l,is))
        dvdm(il,l,is)=vdl(il,is) &
                 +dvdp/dephi*(am4(il,l,is)/am3(il,l,is))/r0 &
                 -dvdp/dephi*exp(phis0w(il,l,is))
      end if
    end do
  end do
  !
  !     * maximum fall velocity according to cfl criterion.
  !
  vcfl=.999*dp(:,leva)/(rhoa(:,leva)*grav*dt)
  ascm=1.
  ascn=1.
  do is=1,isec
    do il=1,ilga
      if (2+abs(exponent(vcfl(il)) - exponent(dvdm(il,leva,is))) &
          < maxexponent(vcfl(il)) .and. dvdm(il,leva,is)/=0. ) then
        ascm(il,leva,is)=vcfl(il)/dvdm(il,leva,is)
      end if
      if (2+abs(exponent(vcfl(il)) - exponent(dvdn(il,leva,is))) &
          < maxexponent(vcfl(il)) .and. dvdn(il,leva,is)/=0. ) then
        ascn(il,leva,is)=vcfl(il)/dvdn(il,leva,is)
      end if
    end do
  end do
  asc=min(1.,ascm,ascn)
  dvdm=max(dvdm*asc,0.)
  dvdn=max(dvdn*asc,0.)
  !
  !-----------------------------------------------------------------------
  !     * surface removal.
  !
  l=leva
  do is=1,isec
    adndt(:,l,is)=-anum(:,l,is)*dvdn(:,l,is)*rhoa(:,l)*grav/dp(:,l)
    admdt(:,l,is)=-amas(:,l,is)*dvdm(:,l,is)*rhoa(:,l)*grav/dp(:,l)
  end do
  !
  !     * dry deposition flux.
  !
  do is=1,isec
    flxn(:,l,is)=anum(:,l,is)*rhoa(:,l)*dvdn(:,l,is)
    flxm(:,l,is)=amas(:,l,is)*rhoa(:,l)*dvdm(:,l,is)
  end do
  !
  !     * dry deposition velocy (m/s).
  !
  do is=1,isec
    vdn(:,is)=dvdn(:,l,is)
    vdm(:,is)=dvdm(:,l,is)
  end do
  !
end subroutine drydep
!> \file
!! \subsection ssec_details Details
!! The parameterization scheme of Zhang at al.
!! Reference: Zhang L. (2001), Atmospheric Enviorment,35,549-560 \cite Zhang2001
