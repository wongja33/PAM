!> \file
!> \brief Gravitational settling velocities for aerosol number and mass
!>       concentrations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine gspar(gsvdn,gsvdm,rhoa,vcfl,amu,amfp,anum,amas,pn0, &
                       phi0,psi,phis0,dphi0,drydn,wetrc, &
                       dryrc,grav,ilga,leva,isec)
  !
  use sdparm, only : dnh2o,r0,yna,ytiny
  use sdcode, only : sdint, sdint0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(out), dimension(ilga,leva,isec) :: gsvdn !< Gravitational settling velocity for aerosol number
  real, intent(out), dimension(ilga,leva,isec) :: gsvdm !< Gravitational settling velocity for aerosol mass
  real, intent(in), dimension(ilga,leva,isec) :: psi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: anum !< Number
  real, intent(in), dimension(ilga,leva,isec) :: amas !< Mass
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in) :: grav !< Gravtiational accelaration \f$[m/s^2]\f$
  real, intent(in), dimension(ilga,leva) :: vcfl !< Maximum fall velocity according to cfl criterion
  real, intent(in), dimension(ilga,leva) :: amu !< Dynamic viscosity of air
  real, intent(in), dimension(ilga,leva) :: amfp !< Mean free path (K. V. Beard, 1976) \cite Beard1976
  real, intent(in), dimension(isec) :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  !
  !     internal work variables
  !
  real, dimension(ilga,leva,isec) :: am0 !<
  real, dimension(ilga,leva,isec) :: am1 !<
  real, dimension(ilga,leva,isec) :: am2 !<
  real, dimension(ilga,leva,isec) :: am3 !<
  real, dimension(ilga,leva,isec) :: am4 !<
  real, dimension(ilga,leva,isec) :: am5 !<
  real, dimension(ilga,leva,isec) :: phi0w !<
  real, dimension(ilga,leva,isec) :: phis0w !<
  real, dimension(ilga,leva,isec) :: ascm !<
  real, dimension(ilga,leva,isec) :: ascn !<
  real, dimension(ilga,leva,isec) :: asc !<
  real, dimension(ilga,leva,isec) :: wetdn !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  real :: alfgr !<
  real :: fgr !<
  real :: fgr3i !<
  real :: apa !<
  real :: apb !<
  real :: rmom !<
  !
  !-----------------------------------------------------------------------
  !     * size-related parameters.
  !
  phi0w =phi0
  phis0w=phis0
  wetdn=drydn
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny .and. amas(il,l,is) > ytiny &
            .and. abs(psi(il,l,is)-yna) > ytiny) then
          fgr=wetrc(il,l,is)/dryrc(is)
          alfgr=log(fgr)
          phi0w (il,l,is)=phi0w (il,l,is)+alfgr
          phis0w(il,l,is)=phis0w(il,l,is)+alfgr
          fgr3i=1./fgr**3
          wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
        end if
      end do
    end do
  end do
  am0=sdint0(phi0w,psi,phis0w,dphi0,ilga,leva,isec)
  rmom=1.
  am1=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=2.
  am2=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=3.
  am3=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=4.
  am4=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=5.
  am5=sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  !
  !-----------------------------------------------------------------------
  !     * settling velocity.
  !
  gsvdm=0.
  gsvdn=0.
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (am0(il,l,is) > ytiny .and. am3(il,l,is) > ytiny) then
          apa=2.*(wetdn(il,l,is)-rhoa(il,l))*grav/(9.*amu(il,l))
          apb=apa*amfp(il,l) &
             *(1.257+0.4*exp(-1.1*(2.*wetrc(il,l,is))/(2.*amfp(il,l))))
          gsvdm(il,l,is)= &
                          (apa*am5(il,l,is)+apb*am4(il,l,is))/am3(il,l,is)
          gsvdn(il,l,is)= &
                          (apa*am2(il,l,is)+apb*am1(il,l,is))/am0(il,l,is)
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * ensure cfl criterion.
  !
  ascm=1.
  ascn=1.
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (2+abs(exponent(vcfl(il,l)) - exponent(gsvdm(il,l,is))) &
            < maxexponent(vcfl(il,l)) .and. gsvdm(il,l,is)/=0. ) then
          ascm(il,l,is)=vcfl(il,l)/gsvdm(il,l,is)
        end if
        if (2+abs(exponent(vcfl(il,l)) - exponent(gsvdn(il,l,is))) &
            < maxexponent(vcfl(il,l)) .and. gsvdn(il,l,is)/=0. ) then
          ascn(il,l,is)=vcfl(il,l)/gsvdn(il,l,is)
        end if
      end do
    end do
  end do
  asc=min(1.,ascm,ascn)
  gsvdm=gsvdm*asc
  gsvdn=gsvdn*asc
  !
end subroutine gspar
