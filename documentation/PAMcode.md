# Modelling framework and coding conventions  {#PAMcode}

1. @ref codeIntro
2. @ref PLArep
3. @ref setupAndInit
    - @ref dataTables
    - @ref aerosolProperties
    - @ref GCMinit
    - @ref errorsAndWarnings
4. @ref codingConventions
    - @ref dimAndAerosolTypeParams 
    - @ref arraysOnModelGrid 
    - @ref constantArrays
    - @ref otherParams
    - @ref subroutinesAndFunctions
5. @ref callGraph

# Introduction {#codeIntro}

This section provides a summary of the modelling framework that is 
available for simulations of aerosol size distributions in the CCCma
Canadian Global Atmospheric Climate Model CanAM4 based on the PLA
method. 

The basic approach is summarized in the next section. In the following
sections, variable names and the basic functionality of the PLA
modelling framework are described. 

The purpose of this documentation is to provide a basis for modifications
and applications of the code. Users are encouraged to add new aerosols
and chemical/microphysical processes by following basic principles 
that are outlined in this document.
An important aspect for the development of the model code was to
provide a flexible framework which puts only minimal constraints on
developers and users in terms of the possible number of aerosol 
species and size resolution. Key chemical, microphysical, and
numerical parameters for aerosols are provided via external input
files. The information in these files is used in the model
to create the necessary variables and their dimensions which reduces
the need for modifications to the model code when changes are
introduced to microphysical properties of the aerosol. However, the user
should be aware that some adjustments to this approach may be
necessary in the future owing to the fact that the approach is still 
under development. 

# PLA Representation of Aerosol Size Distributions {#PLArep}
\n
 The basic numerical approach for the representation of aerosol size
 distributions was described by von Salzen (2006) \cite vonSalzen2006 and Ma et
 al. (2008) \cite Ma2008. According to the PLA method, the aerosol number
 distribution for a given soluble or insoluble aerosol species is
 expressed as a series of orthogonal functions, i.e.,
\n

\f{equation}{
 \label{eq:pla1}
 n (\varphi) = \sum_{i} n_{i} (\varphi)
\f}

\n
 for the dimensionless size parameter \f$\varphi \equiv \ln (
 R_{p}/R_{0})\f$. \f$R_{0}\f$ is an arbitrary reference radius
 (e.g. 1~\f$\mu\f$) that is required for a dimensionally correct
 formulation. In the basic implementation of the method, \f$R_{p}\f$ is
 the particle radius for dry particles.
\n\n 
 The following set of functions in Eq.(\f$\ref{eq:pla1}\f$) is used:
\n

\f{equation}{
 \label{eq:pla2}
 n_{i}(\varphi) = n_{0,i} \exp\left[{- \psi_{i} \left(\varphi - \varphi_{0,i}\right)^{2}}\right]
 H \left( \varphi - \varphi_{i-1/2} \right) H \left( \varphi_{i+1/2} - \varphi \right)
\f}

\n
 where \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$ are basic
 parameters that determine the magnitude, width, respectively mean of
 the distribution. \f$H (x)\f$ denotes the Heaviside step function, i.e.
\n

\f{equation*}{
 H (x) = \begin{cases}
 0 & \text{if {x<0}}  \\
 \frac{1}{2} & \text{if {x=0}}  \\
 1 & \text{if {x>0} } 
 \end{cases}
\f}

\n 
so that \f$n_{i}=0\f$ outside each section \f$i\f$ with the size range
 \f$\varphi_{i-1/2}\leq\varphi\leq\varphi_{i+1/2}\f$, where
 \f$\varphi_{i+1/2} = \varphi_{i-1/2} + \Delta \varphi_{\star}\f$.
 The properties of the size distribution are allowed to vary between
 different sections in Eq.(\f$\ref{eq:pla2}\f$) but are constant within
 each section.
\n 
 Consequently, the aerosol mass size distribution is given by
\n

\f{equation}{
 \label{eq:pla3}
 m (\varphi) = \frac{4 \pi}{3} \rho_{p} \exp(3
 \varphi) \sum_{i} n_{i} (\varphi)
\f}

\n 
 Other moments of the aerosol size distribution can be expressed accordingly.
\n 
 Once the basic PLA parameters \f$n_{0,i}\f$, \f$\psi_{i}\f$, and
 \f$\varphi_{0,i}\f$ are known, the number size distribution is fully
 constrained in the model. The method that was described by von Salzen (2006) \cite vonSalzen2006 is
 used in the model to determine these parameters from the aerosol number and
 mass concentrations, \f$N_{i}\f$, resp. \f$M_{i}\f$. Vice versa, \f$N_{i}\f$,
 resp. \f$M_{i}\f$ can be determined once the basic PLA parameters and the
 density of the (dry) aerosol particles (\f$\rho_{p}\f$) are known.
\n 
 In the GCM, the PLA method is used for completely internally (i.e. all
 aerosol particles of a given size have the same chemical composition)
 and externally (i.e. a given particle contains only one individual
 soluble or insoluble component) mixed aerosols. For externally
 mixed aerosols, each distinct aerosol chemical species \f$k\f$ is assumed
 to occupy a fraction \f$f_{m,ik}\f$ of the total (dry) aerosol mass in each
 section \f$i\f$, where
\n

\f{equation}{
 \label{eq:plafrac}
 f_{m,ik} = \frac{M_{ik}}{M_{i}}
\f}

\n
 and where \f$M_{ik}\f$ is the mass of the aerosol species, with
\n

\f{equation}{
 \label{eq:plamass}
 M_{i} = \sum_{k=1}^{K} M_{ik}
\f}

\n
 The total number of aerosol species in each section is denoted by \f$K\f$. Similar
 to internally mixed aerosol, Eqs.(\f$\ref{eq:pla1}\f$) and (\f$\ref{eq:pla3}\f$)
 represent the number, respectively mass size distributions of the total
 externally mixed aerosol. 2 different sets of PLA base parameters
 (\f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$) are used for internally,
 respectively externally mixed aerosol in the model.
\n\n 


# Model Setup and Initialization {#setupAndInit}
\n 
 In order to be able to use the PLA code, the user will have to provide
 information about the aerosol chemical components that will be used in
 the GCM. The GCM model submission job also needs to contain a
 reference to PLA data tables as described in the following section.
 Furthermore, there are constraints on the number of tracers, their
 names, and their properties which are specified in the GCM submission
 job.
\n\n 
 If the model code will be compiled (\f${\tt samerun=off}\f$ in the GCM
 submission job) The following switches are available in the section
 with the title \f${\tt \#\#\#\mbox{ }update\mbox{ }model\mbox{ }\#\#\#}\f$
 in the GCM submission job:
\n
<table> 
 <tr><td>\f${\tt \%df \enspace pla}\f$</td><td>      Enables source code for PLA calculations in the GCM (mandatory)</td></tr>
 <tr><td>\f${\tt \%df \enspace xtrapla}\f$</td><td>  Generates model output for certain PLA results (optional)</td></tr>
</table>
\n 
 It also necessary to specify \f${\tt \%c\mbox{ }sdmod}\f$ in the same
 section of the GCM submission job so that the PLA modules will be
 compiled, if necessary.
\n\n 
 Note that for the current version of the code, single precision is not
 supported. It is therefore necessary to compile the model code in
 double precision (\f${\tt float1=off}\f$ in the critical parameter section
 of the GCM submission job).
\n\n 
 The following programs and scripts currently need to be made available in the user
 directory \f${\tt bin\_aix}\f$ on the frontend (e.g. zeta): \f${\tt proccd,
 sdjcl.cdk, call\_sdtbl.dk, sdtbl.dk}\f$. The user should run the scripts
\n\n
 \f${\tt/users/tor/acrn/rks/public/install\_pla\_links\_frontend}\f$
\n\n
 on the frontend and
\n\n 
 \f${\tt/users/tor/acrn/rks/public/install\_pla\_links\_backend}\f$
\n\n
 on the backend (e.g. alef) which will
 create links to the corresponding files from the user's directories
 \f${\tt bin\_aix}\f$ and \f${\tt bin\_irix}\f$. It is recommended to create these links
 rather than to copy these files so that any future changes and updates to
 these programs and scripts will be propagated to the user. Make sure
 that the correct script is used on the correct computer!
\n\n 
 It is also necessary to install modified versions of \f${\tt gcmparm}\f$
 and \f${\tt gcmparm\_input}\f$ in \f${\tt bin\_aix}\f$ and \f${\tt bin\_irix}\f$ so that the critical
 parameters \f${\tt sdn}\f$ (@ref dataTables) can be used. Note
 that all GCM submission jobs that are submitted
 after the installation of the modified version of \f${\tt gcmparm}\f$
 need at least to include a statement such as sdn="		0". The scripts
 \n\n
 \f${\tt/users/tor/acrn/rks/public/install\_gcmparm\_link\_frontend}\f$
 \n\n
 and
 \n\n
 \f${\tt/users/tor/acrn/rks/public/install\_gcmparm\_link\_backend}\f$
 \n\n
 will also create links for \f${\tt gcmparm}\f$ and \f${\tt
 gcmparm\_input}\f$. Users should consider to remove these
 links in the directories \f${\tt bin\_aix}\f$ and \f${\tt bin\_irix}\f$ on
 the frontend in case they want to use GCM submission jobs that do not
 contain the parameter \f${\tt sdn}\f$. In that case, the user will no
 longer be able to do any simulations with the PLA code and it will
 be necessary to re-create the links to \f${\tt gcmparm}\f$ and
 \f${\tt gcmparm\_input}\f$ in that case.
\n\n 


## PLA Data Tables {#dataTables}
\n 
 As described in the previous section, the basic PLA parameters
 (\f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$) can be determined
 from the aerosol number and mass concentrations (\f$N_{i}\f$ and \f$M_{i}\f$).
 As described by von Salzen (2006) \cite vonSalzen2006, the calculation of these parameters
 depends on the size of the sections, \f$\Delta
 \varphi_{\star}\f$. In particular, coefficients that are used in the
 transformations from number and mass concentrations to the basic PLA
 parameters depend on the size of the sections \cite vonSalzen2006. In
 the GCM, tabulated results are used to calculate these coefficients
 at each time step. The necessary PLA data tables for these calculations are
 provided as external model input files.
\n\n 
 In the GCM, different values of \f$\Delta \varphi_{\star}\f$ can in
 principle be used for different externally mixed aerosol species. The
 representation of the size distribution for internally mixed aerosol
 may also be based on a certain grid size. In order to account for
 possible different grid sizes for different types of aerosols in the
 GCM, it is possible to use different PLA data tables for the
 transformations from number and mass concentrations to the basic PLA
 parameters. Each individual PLA data table will be for a unique size
 resolution. The basic PLA tables are saved in text files on the
 computer system in \f${\tt $DATAPATH}\f$ (one table per size resolution
 value \f$\Delta \varphi_{\star}\f$). The values in these tables are
 pre-calculated using the program \f${\tt PRESD}\f$. Values in these tables
 cannot be changed without rerunning the program \f${\tt PRESD}\f$. The
 accuracy of the tabulated values needs to correspond to the accuracy
 of the model calculations (a check will be performed as part of
 the model initialization to ensure that there is no mismatch in accuracies).
\n\n 
 The GCM model submission job contains critical information about the
 PLA data tables that will be used in a simulation.
 The following parameters currently need to be included in the critical
 parameter section of the GCM submission job:
\n
<table> 
<tr><td> \f${\tt sdn}\f$ </td><td> Number of external PLA data tables</td></tr>
<tr><td> \f${\tt sdtabMM}\f$ </td><td> Names of PLA data tables </td></tr>
</table>
\n 
 The format for \f${\tt sdn}\f$ is \f${\tt"\mbox{ }\mbox{ }\mbox{ }\mbox{ }{\it{N}}"}\f$
 (total of 5 characters), where \f${\it{N}}\f$ is a number
 greater than 1 and less than 4 (internal compiler limitations currently
 do not allow any larger number of tables). \f${\it{MM}}\f$ is the number
 for each table with values 01, 02, \dots 0\f${\it{N}}\f$. The syntax for the
 latter parameters is \f${\tt sdtab{\it{MM}}={\it filename}}\f$. Example:
\n\n 
 \f${\tt
 \mbox{ }\mbox{ }sdn="\mbox{ }\mbox{ }\mbox{ }\mbox{ }3" \\
 \mbox{ }\mbox{ }sdtab01=urks\_sdtab\_1.1 \\
 \mbox{ }\mbox{ }sdtab02=urks\_sdtab\_1.2 \\
 \mbox{ }\mbox{ }sdtab03=urks\_sdtab\_1.3
 }\f$
\n\n 
 The approach in the code is to associate an individual size
 distribution (i.e. for an externally aerosol species of for internally
 mixed aerosol) with one of the basic PLA tables that are specified in
 the model submission job. The association algorithm is described later
 in this section.
\n\n 
 Note that the basic PLA code allows abitrary selections of section
 width through appropriate choices of PLA data tables. However, some
 physical parameterization may put constraints on section width. For
 example, coagulation calculations currently require doubling
 of particle volume between section boundaries.
\n\n 


## PLA Aerosol Properties {#aerosolProperties}
\n 
 Another critical input file that needs to be provided via the GCM
 submission job is:
\n
 <table> 
 <tr><td>\f${\tt sdparam}\f$</td><td>  Aerosol-type specific chemical, microphysical, and numerical
 parameters </td></tr>
</table>
\n 
 The syntax is \f${\tt sdparam={\it filename}}\f$. The file with the name
 \f${\it filename}\f$ is generally supplied by the user. It is a text file
 that needs to be made available on the computer system (in \f${\tt
 \$DATAPATH}\f$), e.g. by using the command
 \f${\tt save}\f$.
\n\n 
 The format of the file \f${\tt sdparam}\f$ conforms with the rules
 for Fortran 90/95 \f${\tt namelist}\f$ input records (note, however,
 that the first and last characters that are normally used for
 \f${\tt namelist}\f$ input records, \& and /, are omitted in this file since
 they will be added in the model preprocessing step).
\n\n 
 The file contains the following parameters for chemical,
 microphysical, or numerical properties for each individual chemical
 aerosol component:
\n 
<table>
 <tr><td> \f${\tt DENS}\f$ </td><td> Density (dry) (\f$\rm kg/m^{3}\f$) </td></tr>
 <tr><td> \f${\tt DPST}\f$ </td><td> PLA section size \f$\Delta \varphi_{\star}\f$ </td></tr>
 <tr><td> \f${\tt ISCB}\f$ </td><td> Index of first size section (only for internally mixed aerosol species) </td></tr>
 <tr><td> \f${\tt ISCE}\f$ </td><td> Index of last size section (only for internally mixed aerosol species) </td></tr>
 <tr><td> \f${\tt ISEC}\f$ </td><td> Total number of size sections </td></tr>
 <tr><td> \f${\tt KAPPA}\f$ </td><td> Kappa parameter for bulk microphysical aerosol properties </td></tr>
 <tr><td> \f${\tt MOLW}\f$ </td><td> Molecular weight (\f$\rm kg/mol\f$) </td></tr>
 <tr><td> \f${\tt NAME}\f$ </td><td> Name of the aerosol species </td></tr>
 <tr><td> \f${\tt NUIO}\f$ </td><td> Number of ions from dissociation of the species (=0 for insoluble species) </td></tr>
 <tr><td> \f${\tt PSIM}\f$ </td><td> Reference value of the basic PLA width parameter \f$\psi_{i,m}\f$ [von Salzen, 2006 \cite vonSalzen2006; Eq.(8)] </td></tr>
 <tr><td> \f${\tt RADB}\f$ </td><td> Radius (dry) of the smallest particle of the distribution (m) </td></tr>
</table>
\n 
 Mandatory parameters for each aerosol component are: \f${\tt NAME, ISEC,
 RADB, PSIM, DENS}\f$. In addition, either \f${\tt NUIO, MOLW}\f$ or \f${\tt
 KAPPA}\f$ need to be specified. Optional parameters are \f${\tt DPST}\f$
 (for externally mixed component) and \f${\tt DPST, ISCE, ISCB}\f$
 (for internally mixed component). The default value for \f${\tt DPST}\f$
 is the grid size from the first PLA data table (\f${\tt
 sdtab01}\f$). Default values for \f${\tt ISCE}\f$ and \f${\tt ISCB}\f$ are
 \f${\tt ISEC}\f$, resp. 1. If \f${\tt DPST}\f$ is specified and if more
 than PLA data table are provided, the code will select that table
 which produces the smallest difference between \f${\tt DPST}\f$ and the
 value of \f$\Delta \varphi_{\star}\f$ on which the tables are based.
\n\n 
 Each aerosol component needs to be flagged either as externally or
 internally mixed aerosol species. Two different flags are used to
 indicate the mixing state:
\n
<table> 
 <tr><td> \f${\tt AEXT}\f$ </td><td>  Externally mixed aerosol species </td></tr>
 <tr><td> \f${\tt AINT}\f$ </td><td>  Internally mixed aerosol species </td></tr>
</table>
\n  
 In the file \f${\tt sdparam}\f$, the mixing state flag precedes the name
 of each chemical, microphysical, or numerical parameter. The mixing
 state flag is followed by the aerosol type identifier \f${\tt
 \%TP({\it{k}})\%}\f$, where \f${\it k}\f$ is a number which is used
 to distinguish between different aerosol components. All
 parameters in the file that belong to a certain aerosol component
 have the same value of \f${\it k}\f$. \f${\it k}\f$ can be any integer number
 between 1 and 1000. Note that the following parameters for internal
 mixtures do not include an aerosol type identifier: \f${\tt ISEC, RADB, DPST, PSIM}\f$.
\n\n 
 The following example shows the input that would be required for
 a combination of different externally and internally mixed aerosol
 species:
\n
\verbatim 
! FIRST EXTERNALLY MIXED AEROSOL TYPE 
AEXT%TP(1)%NAME='SEASALT' 
AEXT%TP(1)%ISEC=2 
AEXT%TP(1)%RADB=0.5E-06 
AEXT%TP(1)%DPST=2. 
AEXT%TP(1)%PSIM=2.5,2.5 
AEXT%TP(1)%DENS=2165. 
AEXT%TP(1)%MOLW=67.18E-03 
AEXT%TP(1)%NUIO=2. 

! SECOND EXTERNALLY MIXED AEROSOL TYPE 
AEXT%TP(2)%NAME='MINDUST' 
AEXT%TP(2)%ISEC=3 
AEXT%TP(2)%RADB=0.2E-06 
AEXT%TP(2)%DPST=1.5 
AEXT%TP(2)%PSIM=1.8,2.,2. 
AEXT%TP(2)%DENS=2650. 
AEXT%TP(2)%MOLW=60.08E-03 
AEXT%TP(2)%NUIO=0. 

! INTERNALLY MIXED AEROSOL 
AINT%ISEC=3 
AINT%RADB=0.04E-06 
AINT%DPST=1.1 
AINT%PSIM=1.2,.7,1. 

! FIRST INTERNALLY MIXED AEROSOL TYPE 
AINT%TP(1)%NAME='(NH4)2SO4' 
AINT%TP(1)%DENS=1769. 
AINT%TP(1)%MOLW=132.13E-03 
AINT%TP(1)%NUIO=3. 

! SECOND INTERNALLY MIXED AEROSOL TYPE 
AINT%TP(2)%NAME='NH4NO3' 
AINT%TP(2)%DENS=1725. 
AINT%TP(2)%MOLW=80.04E-03 
AINT%TP(2)%NUIO=2.
\endverbatim
\n 
 For the above example, two externally mixed (\f${\tt SEASALT}\f$ and \f${\tt
 MINDUST}\f$) and two internally mixed aerosol components (\f${\tt
 (NH4)2SO4}\f$ and \f${\tt NH4NO3}\f$) are specified. Note that all
 internally mixed aerosol components need to fully overlap with
 the size distribution for \f${\tt (NH4)2SO4}\f$ so that
 \f${\tt ISCE}\f$ and \f${\tt ISCB}\f$ should not be specified in that case.
\n\n
 Tracer names usually have special meaning and should not be changed. If
 additional types of aerosol are required in addition to already
 existing types of aerosol, they may be added to the code using
 names that differ from already existing names. For example, the tracer
 name \f${\tt (NH4)2SO4}\f$ is used to indicate in the code that this tracer is
 subject to certain chemical or microphysical processes such as
 gas-to-particle conversion.
\n\n 
 The list of special aerosol components currently is: \f${\tt
 (NH4)2SO4}\f$, \f${\tt NH4NO3}\f$, \f${\tt SEASALT}\f$, \f${\tt ORGCARB}\f$,
 \f${\tt BLCCARB}\f$, \f${\tt MINDUST}\f$. The list of special
 names is provided via the subroutine \f${\tt SDCONF}\f$.
\n\n


## GCM Initialization Step {#GCMinit}
\n 
 In the GCM initialization step, the information that is contained
 in the file \f${\tt sdparam}\f$ is checked and used to allocate memory
 for arrays in the model. Any critical error that may occur during this
 step (i.e. owing to mismatches or omissions of critical parameters)
 will lead to a model abort which will be accompanied by an error
 message in the GCM output (see following section for explanation of
 error codes). In addition, the GCM will produce a summary of
 aerosol properties and tracer properties which should be reviewed
 by the user to ensure that the correct information is used in the GCM
 simulation. For instance, if used in a simulation, the above example
 may produce the following GCM output:
\n
\verbatim
*** Aerosol Properties

Total number of types of aerosols :   4
                 Externally mixed :   2
                 Internally mixed :   2

Externally mixed types of aerosols

  SEASALT                                                               
  Number of sections        :   2
  Radius range (micron)     :  0.500 -  6.732
  Width of sections         : 2.00000000 -> 1.30000000
    Section    PLA distribution width parameter
      1        2.50 ->   2.50 - 200.00
      2        2.50 ->   2.50 - 200.00
  Density (kg/m3)           : 2165.00
  Molecular weight (g/mol)  :   67.18
  Number of ions            :    2.00

  MINDUST                                                               
  Number of sections        :   3
  Radius range (micron)     :  0.200 -  9.880
  Width of sections         : 1.50000000 -> 1.30000000
    Section    PLA distribution width parameter
      1        1.80 ->   2.00 - 200.00
      2        2.00 ->   2.00 - 200.00
      3        2.00 ->   2.00 - 200.00
  Density (kg/m3)           : 2650.00
  Molecular weight (g/mol)  :   60.08
  Number of ions            :    0.00

Internally mixed types of aerosols

  Number of sections        :   3
  Radius range (micron)     :  0.040 -  1.085
  Width of sections         : 1.10000000 -> 1.10000000
    Section    PLA distribution width parameter
      1        1.20 ->   1.00 - 200.00
      2        0.70 ->   1.00 - 200.00
      3        1.00 ->   1.00 - 200.00

  (NH4)2SO4                                                             
  Density (kg/m3)           : 1769.00
  Molecular weight (g/mol)  :  132.13
  Number of ions            :    3.00

  NH4NO3                                                                
  Density (kg/m3)           : 1725.00
  Molecular weight (g/mol)  :   80.04
  Number of ions            :    2.00

*** Aerosol Tracers

Total number of aerosol tracers :  20 ->  19

Aerosol mass concentration
  Externally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A01              SEASALT               1               16
    A02              SEASALT               2               17
    A03              MINDUST               1               18
    A04              MINDUST               2               19
    A05              MINDUST               3               20
  Internally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A06              (NH4)2SO4             1               21
    A07              NH4NO3                1               22
    A08              (NH4)2SO4             2               23
    A09              NH4NO3                2               24
    A10              (NH4)2SO4             3               25
    A11              NH4NO3                3               26
Aerosol number concentration
  Externally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A12              SEASALT               1               27
    A13              SEASALT               2               28
    A14              MINDUST               1               29
    A15              MINDUST               2               30
    A16              MINDUST               3               31
  Internally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A17              (NH4)2SO4             1               32
    A17              NH4NO3                1               32
    A18              (NH4)2SO4             2               33
    A18              NH4NO3                2               33
    A19              (NH4)2SO4             3               34
    A19              NH4NO3                3               34

Number of wettable aerosol tracers :   8

Aerosol mass concentration
  Externally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A01              SEASALT               1               16
    A02              SEASALT               2               17
  Internally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A06              (NH4)2SO4             1               21
    A07              NH4NO3                1               22
    A08              (NH4)2SO4             2               23
    A09              NH4NO3                2               24
    A10              (NH4)2SO4             3               25
    A11              NH4NO3                3               26
Aerosol number concentration
  Externally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A12              SEASALT               1               27
    A13              SEASALT               2               28
  Internally mixed types of aerosols
  Aerosol Index      Aerosol Type      Section Index     GCM Tracer
    A17              (NH4)2SO4             1               32
    A17              NH4NO3                1               32
    A18              (NH4)2SO4             2               33
    A18              NH4NO3                2               33
    A19              (NH4)2SO4             3               34
    A19              NH4NO3                3               34
\endverbatim
\n
 There are different types of information contained in this
 output. The first section with the title \f${\tt*** Aerosol Properties}\f$
 contains a basic summary of the number of aerosol components. The
 second part of this section provides information about the chemical,
 microphysical, and numerical parameters for each one of the aerosol
 components. The second section with the title \f${\tt*** Aerosol
 Tracers}\f$ provides information about tracer and array indices. The
 latter section therefore indicates how the aerosol mass and number
 concentrations are assigned to actual tracers in the model.
\n 
 For example, in the first section on aerosol properties, the following
 information is provided for sea salt:
\n 
\verbatim
  SEASALT                                                               
  Number of sections        :   2
  Radius range (micron)     :  0.500 -  6.732
  Width of sections         : 2.00000000 -> 1.30000000
    Section    PLA distribution width parameter
      1        2.50 ->   2.50 - 200.00
      2        2.50 ->   2.50 - 200.00
  Density (kg/m3)           : 2165.00
  Molecular weight (g/mol)  :   67.18
  Number of ions            :    2.00
\endverbatim
\n 
 The third line indicates that the (dry) particle radius range for the
 size distribution for the \f${\tt SEASALT}\f$ tracer is from 0.500 to
 6.732 \f$\rm \mu m\f$. This is based on the input that was provided in the
 file \f${\tt sdparam}\f$ (i.e. via parameters \f${\tt ISEC, RADB, DPST}\f$)
 for that aerosol component.
\n\n 
 The \f$4^{\rm th}\f$ line indicates that the section size \f$\Delta
 \varphi_{\star} = 2.\f$ that was requested for that component (parameter
 \f${\tt DPST}\f$) was not found in the PLA data tables that are also
 provided in the GCM submission job. Instead, a smaller section size of \f$\Delta
 \varphi_{\star} = 1.3\f$ will be used in the simulation. In order to
 avoid such a substitution of the section size, the user should
 make sure to provide the appropriate PLA data tables that will match
 the information that is contained in the aerosol parameter file \f${\tt
 sdparam}\f$.
\n\n 
 The first number in the \f$6^{\rm th}\f$ and \f$7^{\rm th}\f$ line are the
 specified values of the reference value of the basic PLA width
 parameter for each section (parameter \f${\tt PSIM}\f$), i.e. \f$\psi_{i,m}
 = 2.50\f$. However, the specified values may differ from any of the
 values for the PLA section width parameter \f$\psi_{i}\f$ in the PLA data
 table [von Salzen, 2006 \cite vonSalzen2006; Eq.(8)]. The PLA data tables contain
 discrete values for the PLA section width parameter. In the above example, the range of the
 tabulated values that will be used is from \f$\psi_{i} =2.50\f$ to
 \f$\psi_{i} =200.00\f$. It may be possible to select a different range
 by providing a PLA data table with a different set of discrete values
 for \f$\psi_{i}\f$ (however, a certain section size-dependent lower limit
 exists, as discussed by von Salzen, 2006 \cite vonSalzen2006).
\n\n 
 In the section with the title \f${\tt*** Aerosol Tracers}\f$, the
 following line indicates that a total of 19 tracers are required in
 the GCM to hold the necessary aerosol number and mass concentrations
 for all of the aerosol components that were specified in the file
 \f${\tt sdparam}\f$:
\n\n 
 \verbatim
 Total number of aerosol tracers : 20 -> 19
 \endverbatim
\n 
 Generally, the number of tracers for externally mixed aerosol
 components, \f$N_{t,ext}\f$ can be calculated from the number of sections
 for the aerosol component:

\f{equation*}{
 N_{t,ext} = 2 * {\tt ISEC}
\f}

 owing to the fact that aerosol number and mass concentrations need to
 be provided for each individual section.
\n\n 
 For externally mixed aerosol components, the number of tracers, \f$N_{t,int}\f$, can
 be calculated from

\f{equation*}{ 
 N_{t,int} = {\tt ISEC} + \sum_{k=1}^{K_{int}} \left( {\tt ISCE}_{k} -
 {\tt ISCB}_{k} + 1 \right)
\f}

 where \f$K_{int}\f$ is the number of internally mixed aerosol components
 (= 2 in the above example), \f${\tt ISCE}_{k}\f$ the index of the last
 section for the aerosol component \f$k\f$ (= \f${\tt ISEC}\f$, if not explicitly
 specified in the file \f${\tt sdparam}\f$), and \f${\tt ISCB}_{k}\f$ the index
 of the first section for the aerosol component \f$k\f$ (= 1, if not explicitly
 specified in the file \f${\tt sdparam}\f$).
\n\n 
 However, in the GCM submission job for the above example, a total
 number of 20 tracers was made available for aerosol
 tracers. Generally, in the GCM, all tracers with names
 that have the form \f${\tt A{\it{NN}}}\f$ will be assumed to be aerosol
 tracers. \f${\it{NN}}\f$ is an integer number (\f${\it{NN}}\f$ = \f${\tt 01, 02,
 03, \dots 99}\f$). For example, the GCM submission job for the above
 example contained the following list of tracer names:
\n\n
\verbatim
# tracer names

  it01=LWC;  it02=IWC;  it03=BCO;  it04=BCY;  it05=OCO;  
  it06=OCY;  it07=SSA;  it08=SSC;  it09=DUA;  it10=DUC;  
  it11=DMS;  it12=SO2;  it13=SO4;  it14=HPO;  it15=H2O;  
  it16=A01;  it17=A02;  it18=A03;  it19=A04;  it20=A05;  
  it21=A06;  it22=A07;  it23=A08;  it24=A09;  it25=A10;  
  it26=A11;  it27=A12;  it28=A13;  it29=A14;  it30=A15;  
  it31=A16;  it32=A17;  it33=A18;  it34=A19;  it35=A20;  
  it36=GS6;
\endverbatim
\n
According to this list of tracer names, a total of 20 tracers (\f${\tt it16}\f$
 to \f${\tt it35}\f$) are used for the aerosol number and mass
 concentrations. However, the last tracer, \f$\tt it35\f$, is not used
 in the aerosol simulation because only 19 aerosol tracers are
 required for the above example. Tracer \f$\tt it35\f$ is therefore
 redundant. Such redundancy does not cause a model abort but it is
 advise able to avoid redundant aerosol tracers since the overhead
 that is associated with the transport of these tracers may cause
 an unnecessary degradation of model performance.
\n\n 
 Note that all tracers need to be explicitly specified in the GCM
 submission job. It is currently not possible to automatically generate
 a list of GCM tracers that would match the specification of aerosol
 components in the file \f${\tt sdparam}\f$.
\n\n 
 As usual, new tracers can be added to the model by modifying
 the tracer lists in the GCM model submission job and by selecting
 appropriate values for critical parameters such as \f${\tt ntrac}\f$
 (total number of tracers, including non-aerosol tracers) and
 \f${\tt ntraca}\f$ (total number of advected tracers, including
 non-aerosol tracers). It is very important to make sure that the
 parameter indices for tracers in the submission job are consistent
 with the aerosol tracer indices (e.g. for parameters \f${\tt xref01,}\f$
 \dots). Changes to the aerosol specification in the file
 \f${\tt sdparam}\f$ may affect the number and order of tracers in the GCM
 that are associated with the aerosols. The user should therefore make
 sure that the correct tracer parameters are specified in the GCM
 submission job (see information about tracer indices above).
\n\n 
 Finally, for the above example, the lists in the summary tables in the
 last part in the section with the title \f${\tt*** Aerosol Tracers}\f$ provide information
 about the indices and names of the different aerosol tracers in the
 GCM simulation. In this section, the first summary table is for all aerosol tracers in
 the simulation whereas the second summary table (after the line that
 gives the number of wettable aerosol tracers) is for a subset
 of aerosol tracers which are water-soluble (\f${\tt NUIO} > 0\f$).
\n\n 
 For example, the lines
\n\n 
 \verbatim
 Aerosol number concentration
 Externally mixed types of aerosols
 Aerosol Index Aerosol Type Section Index GCM Tracer
 A12 SEASALT 1 27
 \endverbatim
\n 
 indicate that the aerosol number concentration for the first section
 for the externally mixed aerosol component \f${\tt SEASALT}\f$ is
 associated with tracer \f${\tt A12}\f$. In the GCM, the internal index for
 that tracer is 27 (which, for example, is the third index for the
 tracer array \f${\tt XROW}\f$ in the physics driver of the model). As
 shown above, the corresponding mass for the first section the
 component \f${\tt SEASALT}\f$ is associated with tracer \f${\tt A01}\f$ and
 GCM tracer index 16.
\n\n 


## Error and Warning Codes {#errorsAndWarnings}
\n
 During the GCM initialization step, a number of errors can
 occur. Critical errors will cause the model abort (via a call to
 the subroutine \f${\tt XIT}\f$). Information about non-critical errors
 will be provided in the GCM output via warning messages (from
 calls to subroutine \f${\tt WRN}\f$. Error and warning message will
 contain the name of the subroutine in which the problem occurred
 and an error index.
\n\n 
 The following is a list of critical problems that may occur in the
 main PLA initialization subroutine \f${\tt SDCONF}\f$, including
 suggestions for solutions:
\n\n
<table>
<tr>
<td style="text-align: left;"><code>XIT</code> Index in <code>SDCONF</code></td>
<td style="text-align: left;">Cause and Potential Solution</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-1</code></td>
<td style="text-align: left;">Mismatch for number of sections for internally mixed aerosol components. Check values for parameters \f${\tt ISCB}\f$ and \f${\tt ISCE}\f$.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-2</code></td>
<td style="text-align: left;">Number of aerosol components from the file \f${\tt sdparam}\f$ is greater than the total number of tracers that are available for aerosol tracers in the GCM. Make sure that the number of aerosol tracers (tracer names \f${\tt A{\it{NN}}}\f$, \f${\it{NN}}\f$ = \f${\tt 01, 02,
03, \dots 99}\f$) is sufficiently large in the GCM submission job.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-3 </code></td>
<td style="text-align: left;">Actual number of PLA data tables does not match the specified number of tables. Check number of tables that are used.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-4 </code></td>
<td style="text-align: left;">Accuracy of calculations in the program that was used to generate the PLA data tables (\f${\tt PRESD}\f$ does not match the accuracy of the PLA calculations in the GCM. Only use PLA data tables that were created for the correct numerical precision (e.g. create tables with the same compiler settings as for the model).</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-5 </code></td>
<td style="text-align: left;">Section width specified in the input file does not allow calculation of coagulation. Make sure that the correct PLA data table is available for simulations including coagulation.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-6 </code></td>
<td style="text-align: left;">Insufficient number of tabulated results in the PLA data table for PLA variable \f$\psi_{i}\f$ for \f$\psi_{i} &gt; 0\f$ for externally mixed aerosol components. Check PLA data table format.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-7 </code></td>
<td style="text-align: left;">Insufficient number of tabulated results in the PLA data table for PLA variable \f$\psi_{i}\f$ for \f$\psi_{i} &lt; 0\f$ for externally mixed aerosol components. Check PLA data table format.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-8 </code></td>
<td style="text-align: left;">Insufficient number of tabulated results in the PLA data table for PLA variable \f$\psi_{i}\f$ for \f$\psi_{i} &gt; 0\f$ for internally mixed aerosol components. Check PLA data table format.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-9 </code></td>
<td style="text-align: left;">Insufficient number of tabulated results in the PLA data table for PLA variable \f$\psi_{i}\f$ for \f$\psi_{i} &lt; 0\f$ for internally mixed aerosol components. Check PLA data table format.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-10 </code></td>
<td style="text-align: left;">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt SEASALT}\f$. Ensure full overlap if using the special aerosol component \f${\tt SEASALT}\f$.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-11 </code></td>
<td style="text-align: left;">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt (NH4)2SO4}\f$. Ensure full overlap if using the special aerosol component \f${\tt (NH4)2SO4}\f$.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-12 </code></td>
<td style="text-align: left;">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt NH4NO3}\f$. Ensure full overlap if using the special aerosol component \f${\tt NH4NO3}\f$.</td>
</tr>
<tr class="odd">
<td align="right"><code>-13</code></td>
<td align="left">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt ORGCARB}\f$. Ensure full overlap if using the special aerosol component \f${\tt ORGCARB}\f$.</td>
</tr>
<tr class="even">
<td align="right"><code>-14</code></td>
<td align="left">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt BLCCARB}\f$. Ensure full overlap if using the special aerosol component \f${\tt BLCCARB}\f$.</td>
</tr>
<tr class="odd">
<td align="right"><code>-15</code></td>
<td align="left">For internally mixed aerosol, the size distribution(s) for one or more of the aerosol components does not fully overlap with the size distribution for aerosol component \f${\tt MINDUST}\f$. Ensure full overlap if using the special aerosol component \f${\tt MINDUST}\f$.</td>
</tr>
<tr class="even">
<td align="right"><code>16</code></td>
<td align="left">For externally mixed aerosol, the specified molecular weight for aerosol component \f${\tt SEASALT}\f$ does not match the molecular weight in the code. Correct molecular weight for this component in file \f${\tt sdparam}\f$.</td>
</tr>
<tr class="odd">
<td align="right"><code>-17</code></td>
<td align="left">For internally mixed aerosol, the specified molecular weight for aerosol component \f${\tt (NH4)2SO4}\f$ does not match the molecular weight in the code. Correct molecular weight for this component in file \f${\tt sdparam}\f$.</td>
</tr>
</table>
\n
 The following is a list of non-critical problems that may occur in the
 main PLA initialization subroutine \f${\tt SDCONF}\f$, including
 suggestions for solutions:
\n 
<table>
<tr class="header">
<td style="text-align: left;"><code>WRN</code> Index in <code>SDCONF</code></td>
<td style="text-align: left;">Cause and Potential Solution</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-1</code></td>
<td style="text-align: left;">The number of aerosol tracers in the GCM exceeds the number of aerosols that is required according to file \f${\tt
sdparam}\f$. Remove redundant aerosol tracers in the GCM submission job to increase efficiency of calculations.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-2</code></td>
<td style="text-align: left;">The reference value of the basic PLA width parameter (\f${\tt PSIM}\f$) is outside the range of the tabulated results in the PLA data table for PLA variable \f$\psi_{i} &gt; 0\f$ for externally mixed aerosol components. Increase (or decrease) \f${\tt PSIM}\f$ or replace the PLA data table to avoid using a wider (or narrower) size distribution than specified in file \f${\tt sdparam}\f$.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-3</code></td>
<td style="text-align: left;">The reference value of the basic PLA width parameter (\f${\tt PSIM}\f$) is outside the range of the tabulated results in the PLA data table for PLA variable \f$\psi_{i} &lt; 0\f$ for externally mixed aerosol components. Decrease (or increase) \f${\tt PSIM}\f$ or replace the PLA data table to avoid using a wider (or narrower) size distribution than specified in file \f${\tt sdparam}\f$.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-4</code></td>
<td style="text-align: left;">The reference value of the basic PLA width parameter (\f${\tt PSIM}\f$) is outside the range of the tabulated results in the PLA data table for PLA variable \f$\psi_{i} &gt; 0\f$ for internally mixed aerosol components. Increase (or decrease) \f${\tt PSIM}\f$ or replace the PLA data table to avoid using a wider (or narrower) size distribution than specified in file \f${\tt sdparam}\f$.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-5</code></td>
<td style="text-align: left;">The reference value of the basic PLA width parameter (\f${\tt PSIM}\f$) is outside the range of the tabulated results in the PLA data table for PLA variable \f$\psi_{i} &lt; 0\f$ for internally mixed aerosol components. Decrease (or increase) \f${\tt PSIM}\f$ or replace the PLA data table to avoid using a wider (or narrower) size distribution than specified in file \f${\tt sdparam}\f$.</td>
</tr>
</table>
\n 
 The following is a list of critical problems that may occur in the
 main PLA initialization subroutine \f${\tt SDREAD}\f$, including
 suggestions for solutions:
\n 
<table>
<tr class="header">
<td style="text-align: left;"><code>XIT</code> Index in <code>SDREAD</code></td>
<td style="text-align: left;">Cause and Potential Solution</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-1</code></td>
<td style="text-align: left;">Error while reading results from file \f${\tt
sdparam}\f$. Check if this file is available.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-2</code></td>
<td style="text-align: left;">Critical parameter check failed for properties and numerical constants for externally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that all mandatory parameters are specified for each aerosol species.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-3</code></td>
<td style="text-align: left;">Critical parameter check failed for the aerosol species name for externally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that aerosol species names are properly specified.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-4</code></td>
<td style="text-align: left;">Critical parameter check failed for properties and numerical constants for internally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that all mandatory parameters are specified for each aerosol species.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-5</code></td>
<td style="text-align: left;">Critical parameter check failed for the aerosol species name for internally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that aerosol species names are properly specified.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-6</code></td>
<td style="text-align: left;">Critical parameter check failed for the basic PLA width parameter (\f${\tt PSIM}\f$) for externally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that the number of specified values for \f${\tt sdparam}\f$ is correct.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-7</code></td>
<td style="text-align: left;">Critical parameter check failed for the basic PLA width parameter (\f${\tt PSIM}\f$) for internally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that the number of specified values for \f${\tt sdparam}\f$ is correct.</td>
</tr>
<tr class="even">
<td style="text-align: right;"><code>-8</code></td>
<td style="text-align: left;">Critical parameter check failed for parameters \f${\tt ISCB}\f$ and/or \f${\tt ISCE}\f$ for internally mixed aerosol species in file \f${\tt sdparam}\f$. Make sure that that at least one aerosol species is available for each section of the size distribution.</td>
</tr>
<tr class="odd">
<td style="text-align: right;"><code>-9</code></td>
<td style="text-align: left;">Critical parameter check failed for aerosol microphysical parameters. Make sure that either \f${\tt NUIO, MOLW}\f$ or \f${\tt KAPPA}\f$ are specified.</td>
</tr>
</table>
\n 
 The PLA data tables are contained in subroutines with the name \f${\tt
 SDTBL\it{NN}}\f$ where , \f${\it{NN}}\f$ = \f${\tt 01, 02,
 03, \dots 99}\f$). If there is an inconsistency between the total number of
 tables that is assumed in the code (i.e. the number of subroutines
 \f${\tt SDTBL\it{NN}}\f$) and the value of the parameter \f$\tt{sdn}\f$
 in the model, submission job, an error message with the code number
 \f$\tt{-1}\f$ will be printed by \f${\tt SDTBL\it{NN}}\f$.
\n\n


#  PLA Coding Conventions {#codingConventions}
\n
 Key physical, chemical, and numerical parameters for general PLA calculations
 in the GCM are in the module \f${\tt SDPARM}\f$. Most of these parameters are
 calculated in the subroutine \f${\tt SDCONF}\f$ based on the input that is
 provided in the file \f${\tt sdparam}\f$. They are shared among several
 different subroutines.
\n\n 
 Note that this module also includes basic chemical and physical parameters such as
 molecular weights etc.
\n\n 
 Specialized information that is required by individual
 parameterizations is contained in separate modules (e.g. module \f${\tt
 DUPARM}\f$ for mineral dust calculations).
\n\n 
 In the following, basic parameters that are used by a
 wide range of different PLA subroutines are described.
\n\n 


## Dimension and Aerosol Type Parameters {#dimAndAerosolTypeParams}
\n
 The following list is a summary of basic dimension parameters in the model.
<table>
<tr class="odd">
<td style="text-align: left;"><code>KEXT </code></td>
<td style="text-align: left;">Number of externally mixed aerosol types</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KINT </code></td>
<td style="text-align: left;">Number of internally mixed aerosol types</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISAERT </code></td>
<td style="text-align: left;">Number of aerosol tracers for aerosol mass concentrations (for externally plus internally mixed aerosol species)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISAEXT </code></td>
<td style="text-align: left;">Number of aerosol tracers for aerosol number concentration for externally mixed aerosol species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISAEXTB </code></td>
<td style="text-align: left;">Number of size section boundaries for aerosol number concentration for externally mixed aerosol species, <code>ISAEXTB = ISAEXT + KEXT</code></td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISAINT </code></td>
<td style="text-align: left;">Number of aerosol tracers for aerosol number concentration for internally mixed aerosol species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISAINTB </code></td>
<td style="text-align: left;">Number of size section boundaries for aerosol number concentration for internally mixed aerosol species, <code>ISAINTB = ISAINT + 1</code></td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>NTRACX </code></td>
<td style="text-align: left;">Total number of aerosol tracers (mass and number, externally and internally mixed), <code>NTRACX = ISAERT + ISAEXT + ISAINT</code></td>
</tr>
</table>
\n 
 In the model, \f${\tt ISAEXT}\f$ is calculated as the sum of the number
 of size sections for each individual aerosol type. Similar, \f${\tt
 ISAEXT}\f$ is the number of size sections that is required to save
 information about the particle number size distribution for internally
 mixed aerosol.
\n\n 
 The following list is for a subset of aerosol species which are
 considered to be wettable in the code (\f${\tt NUIO} > 0\f$ or
 \f${\tt KAPPA} > 0\f$), similar to the list above. These parameters
 will be used in calculations of aerosol activation, for example.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>KWEXT </code></td>
<td style="text-align: left;">Number of wettable, externally mixed aerosol types</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KWINT </code></td>
<td style="text-align: left;">Number of wettable, internally mixed aerosol types</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISWETT </code></td>
<td style="text-align: left;">Number of aerosol tracers for mass concentrations for wettable aerosol (for externally plus internally mixed aerosol species)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISWEXT </code></td>
<td style="text-align: left;">Number of aerosol tracers for number concentration for wettable, externally mixed aerosol species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISWINT </code></td>
<td style="text-align: left;">Number of aerosol tracers for number concentration for wettable, internally mixed aerosol species</td>
</tr>
</table>
\n 
 The following list shows aerosol-type specific dimension parameters.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>KEXTSO4 </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed sulphate</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KINTSO4 </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed sulphate</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISEXTSO4 </code></td>
<td style="text-align: left;">Number of size sections for externally mixed sulphate</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISINTSO4 </code></td>
<td style="text-align: left;">Number of size sections for internally mixed sulphate</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISO4S </code></td>
<td style="text-align: left;">Number of non-sulphate aerosol types in interally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KEXTSS </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed sea salt</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>KINTSS </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed sea salt</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISSS </code></td>
<td style="text-align: left;">Number of non-sea salt aerosol types in interally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>KEXTNO3 </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed nitrate</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KINTNO3 </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed nitrate</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>INO3S </code></td>
<td style="text-align: left;">Number of non-nitrate aerosol types in interally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KEXTOC </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed organic carbon</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>KINTOC </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed organic carbon</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IOCS </code></td>
<td style="text-align: left;">Number of non-organic carbon aerosol types in interally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>KEXTBC </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed black carbon</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KINTBC </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed black carbon</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IBCS </code></td>
<td style="text-align: left;">Number of non-black carbon aerosol types in interally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KEXTMD </code></td>
<td style="text-align: left;">Aerosol type index for externally mixed mineral dust</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>KINTMD </code></td>
<td style="text-align: left;">Aerosol type index for internally mixed mineral dust</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IMDS </code></td>
<td style="text-align: left;">Number of non-mineral dust aerosol types in interally mixed aerosol</td>
</tr>
</table>
\n


## PLA Arrays on Model Grid {#arraysOnModelGrid}
\n
 Basic information about the aerosol number and mass size distributions
 is provided in the following arrays: 
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PENUM </code></td>
<td style="text-align: left;">Aerosol number concentration for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEMAS </code></td>
<td style="text-align: left;">Aerosol (dry) mass concentration for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PINUM </code></td>
<td style="text-align: left;">Aerosol number concentration for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIMAS </code></td>
<td style="text-align: left;">Aerosol (dry) mass concentration for internally mixed aerosol (Eq. \f$\ref{eq:plamass}\f$)</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIFRC </code></td>
<td style="text-align: left;">Aerosol (dry) mass fraction for each aerosol type for internally mixed aerosol (Eq. \f$\ref{eq:plafrac}\f$)</td>
</tr>
</table>
\n 
 These are multi-dimensional arrays. The first dimension of these
 arrays corresponds to the horizontal model coordinate (with size \f${\tt
 ILGA}\f$). The second dimension corresponds to the vertical model
 corrdinate (with size \f${\tt LEVA}\f$). The third dimension is the
 aerosol index (with size \f${\tt ISAEXT}\f$ resp. \f${\tt ISAINT}\f$.) The
 aerosol information is stored similar to the order that is described in
 section~\ref{sec:init}, i.e. for externally mixed aerosol, the first
 array element refers to the first size section of the first aerosol
 type and the last array element refers to the last size section
 for the last aerosol type. For internally mixed aerosol, the first
 array index refers to the size section with the smallest particle
 sizes and the last index to the size section with the largest
 particle sizes of the complete aerosol size distribution of all
 internally mixed aerosol types. There are additional arrays
 available that will useful for access and extraction of specific
 aerosol information which will be described in subsequent sections.
\n\n 
 For internally mixed aerosol, the array \f${\tt PIFRC}\f$ is used to
 save the mass fraction for each individual aerosol type in
 each section of the aerosol. \f${\tt PIFRC}\f$ is a 4-dimensional array
 with the corresponding size \f${\tt KINT}\f$ for the last dimension.
\n\n 
 In the GCM, mass mixing ratios (mass of tracer over mass of total air)
 are used for \f${\tt PEMAS}\f$ and \f${\tt PIMAS}\f$. The units for the arrays
 \f${\tt PENUM}\f$ and \f${\tt PIUM}\f$ are number of particles per kg of total
 air.
\n\n 
 The PLA parameter that correspond to the above aerosol number and mass
 concentrations are shown in the following table. These dimensions
 of these arrays correspond to the dimensions of the aerosol number
 and mass concentrations arrays in the previous table.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PEN0 </code></td>
<td style="text-align: left;">First PLA parameter (\f$n_{0,i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEPHI0 </code></td>
<td style="text-align: left;">Third PLA parameter (\f$\varphi_{0,i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEPSI </code></td>
<td style="text-align: left;">Second PLA parameter (\f$\psi_{i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIN0 </code></td>
<td style="text-align: left;">First PLA parameter (\f$n_{0,i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIPHI0 </code></td>
<td style="text-align: left;">Third PLA parameter (\f$\varphi_{0,i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIPSI </code></td>
<td style="text-align: left;">Second PLA parameter (\f$\psi_{i}\f$ in Eq. \f$\ref{eq:pla2}\f$) for internally mixed aerosol</td>
</tr>
</table>
\n 
 Additional parameters are listed in the following table. The dimension
 and sizes of these arrays are corresponding to the arrays for
 aerosol number and mass concentrations in the first table in this
 section.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PEDDN </code></td>
<td style="text-align: left;">(Dry) density for externally mixed aerosol particles</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEDPHI0 </code></td>
<td style="text-align: left;"><code>= PEDPHIS</code> (@ref constantArrays)</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEPHIS0 </code></td>
<td style="text-align: left;"><code>= PEPHISS</code> (@ref constantArrays)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEWETRB </code></td>
<td style="text-align: left;">Particle radius for total (wet), externally mixed aerosol particles at size section boundaries</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEWETRC </code></td>
<td style="text-align: left;">Particle radius for total (wet), externally mixed aerosol particles for section centres</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIDDN </code></td>
<td style="text-align: left;">(Dry) density for internally mixed aerosol particles</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIDPHI0 </code></td>
<td style="text-align: left;"><code>= PIDPHIS</code> (@ref constantArrays)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIPHIS0 </code></td>
<td style="text-align: left;"><code>= PIPHISS</code> (@ref constantArrays)</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIWETRB </code></td>
<td style="text-align: left;">Particle radius for total (wet), internally mixed aerosol particles at size section boundaries</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIWETRC </code></td>
<td style="text-align: left;">Particle radius for total (wet), internally mixed aerosol particles for section centres</td>
</tr>
</table>
\n 
 SI-units are used for these variables, as is the case with any
 other variables that are used in the basic PLA calculations.
\n\n 
 Because PLA code often contains references to aerosol number and mass
 concentrations and to PLA parameters it is often necessary
 that these parameters and any of the additionally used parameters
 (e.g. particle densities) are consistent with each other. Use the tools that
 are described in @ref subroutinesAndFunctions, if necessary.
\n\n 


## Constant PLA Arrays {#constantArrays}
\n 
 The boundaries of the size sections and information relevant to the
 aerosol types are saved in the following arrays. These arrays are
 generated in the subroutine \f${\tt SDCONF}\f$ during the model
 initialization. The arrays and their components are provided via
 the module \f${\tt SDPARM}\f$.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PEDPHIS </code></td>
<td style="text-align: left;">Section size parameter (\f${\tt DPST}\f$) for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEDRYRB </code></td>
<td style="text-align: left;">Dry particle radius at the boundaries of the size sections for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEDRYRC </code></td>
<td style="text-align: left;">Dry particle radius in the centres of the size sections for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEISMAX </code></td>
<td style="text-align: left;">Aerosol index corresponding to the last size section for the current aerosol type for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEISMIN </code></td>
<td style="text-align: left;">Aerosol index corresponding to the first size section for the current aerosol type for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEISMNK </code></td>
<td style="text-align: left;">Aerosol index corresponding to the first size section for the current aerosol type for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PEISMXK </code></td>
<td style="text-align: left;">Aerosol index corresponding to the last size section for the current aerosol type for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PEPHISS </code></td>
<td style="text-align: left;">Dry particle size \f$\ln (R_{p}/R_{0})\f$ at the boundaries of the size sections for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PETYPE </code></td>
<td style="text-align: left;">Aerosol type index for pre-determined aerosol types (1 for sea salt, 2 for sulphate, 3 for nitrate, 4 for organic carbon, -1 for black carbon, -2 for mineral dust) for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIDPHIS </code></td>
<td style="text-align: left;">Section size parameter (\f${\tt DPST}\f$) for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIDRYRB </code></td>
<td style="text-align: left;">Dry particle radius at the boundaries of the size sections for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIDRYRC </code></td>
<td style="text-align: left;">Dry particle radius in the centres of the size sections for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIISMAX </code></td>
<td style="text-align: left;">Aerosol index corresponding to the last size section for the current aerosol type for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIISMIN </code></td>
<td style="text-align: left;">Aerosol index corresponding to the first size section for the current aerosol type for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PIPHISS </code></td>
<td style="text-align: left;">Dry particle size \f$\ln (R_{p}/R_{0})\f$ at the boundaries of the size sections for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PITYPE </code></td>
<td style="text-align: left;">Aerosol type index for pre-determined aerosol types (1 for sea salt, 2 for sulphate, 3 for nitrate, 4 for organic carbon, -1 for black carbon, -2 for mineral dust) for internally mixed aerosol</td>
</tr>
</table>
\n 
 The arrays in the table above a 1-dimensional arrays which
 have the size \f${\tt ISAEXT}\f$, resp. \f${\tt ISAINT}\f$. The indices
 of these arrays correspond to the aerosol indices that were introduced
 in @ref arraysOnModelGrid.
\n\n 
 In addition, indices are provided for specific types of
 aerosol. These arrays are useful for parameterizations that calculate
 processes that depend on the type of aerosol (e.g. as for sulphate
 in subroutine \f${\tt GTPCNV}\f$):
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>IEXBC </code></td>
<td style="text-align: left;">Section index for black carbon for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IEXMD </code></td>
<td style="text-align: left;">Section index for mineral dust for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IEXOC </code></td>
<td style="text-align: left;">Section index for organic carbon for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IEXSO4 </code></td>
<td style="text-align: left;">Section index for sulphate for externally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IEXSS </code></td>
<td style="text-align: left;">Section index for sea salt for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IINBC </code></td>
<td style="text-align: left;">Section index for black carbon for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IINMD </code></td>
<td style="text-align: left;">Section index for mineral dust for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IINOC </code></td>
<td style="text-align: left;">Section index for organic carbon for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IINSO4 </code></td>
<td style="text-align: left;">Section index for sulphate for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IINSS </code></td>
<td style="text-align: left;">Section index for sea salt for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ITBC </code></td>
<td style="text-align: left;">Aerosol type index for black carbon for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ITMD </code></td>
<td style="text-align: left;">Aerosol type index for mineral dust for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ITOC </code></td>
<td style="text-align: left;">Aerosol type index for organic carbon for internally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ITSO4 </code></td>
<td style="text-align: left;">Aerosol type index for sulphate for internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ITSS </code></td>
<td style="text-align: left;">Aerosol type index for sea salt for internally mixed aerosol</td>
</tr>
</table>
\n 
 Fortran 90 derived-data types are used for some parameters. For
 example, the particle dry radius is saved in the following arrays:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PEDRYR </code></td>
<td style="text-align: left;">(Dry) particle radius for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PIDRYR </code></td>
<td style="text-align: left;">(Dry) particle radius for internally mixed aerosol</td>
</tr>
</table>
\n 
 The structure components of these arrays are:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>VL </code></td>
<td style="text-align: left;">Value at the lower boundaries of the size sections</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>VR </code></td>
<td style="text-align: left;">Value at the upper boundaries of the size sections</td>
</tr>
</table>
\n 
 For example, the particle radius at the lower boundary for the size
 section that corresponds to the aerosol index \f$i\f$ for externally
 mixed aerosol is saved in \f${\tt PEDRYR(\mbox{\it i})\%VL}\f$.
\n\n
 Information that is relevant to the transformation of aerosol number
 and mass concentrations into the basic PLA parameters is saved in the
 following parameters:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>PEXT </code></td>
<td style="text-align: left;">PLA basic transformation parameters for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PINT </code></td>
<td style="text-align: left;">PLA basic transformation parameters for internally mixed aerosol</td>
</tr>
</table>
\n 
 The size of \f${\tt PEXT}\f$ and \f${\tt PINT}\f$ is \f${\tt ISAEXT}\f$,
 resp. \f${\tt ISAINT}\f$. \f${\tt PEXT}\f$ and \f${\tt PINT}\f$ have the following
 structure components:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>IRDB </code></td>
<td style="text-align: left;">Number of points in the PLA data table for variable \f$r_{i}\f$ [Eq. (6), von Salzen, 2006 \cite vonSalzen2006 )</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>FLG </code></td>
<td style="text-align: left;">Flag to indicate whether valid data exists for a point in a PLA data table</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PSIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\psi_{i}\f$ [Eq. (8), von Salzen, 2006 \cite vonSalzen2006 ]</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PHIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\Delta\varphi_{i}\f$ [Eq. (3), von Salzen, 2006 \cite vonSalzen2006 ]</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DPHIB </code></td>
<td style="text-align: left;">Tabulated derivatives \f$\partial \Delta\varphi_{i}/\partial r_{i}\f$</td>
</tr>
</table>
\n 
 The structure components \f${\tt PSIB}\f$, \f${\tt PHIB}\f$, and \f${\tt DPHIB}\f$
 have structure sub-components \f${\tt VL}\f$ and \f${\tt VR}\f$ in
 order to provide values at the upper and lower boundaries in the PLA data tables.
\n\n 
 There are different types of parameters for basic aerosol tracer, chemical, and
 physical parameters which will be described in the following. The
 first set of parameters links GCM tracer to PLA information.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>SEXTF </code></td>
<td style="text-align: left;">Tracer information for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SINTF </code></td>
<td style="text-align: left;">Tracer information for internally mixed aerosol</td>
</tr>
</table>
\n 
 \f${\tt SEXT}\f$ has the following structure components:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>ISAER </code></td>
<td style="text-align: left;">Tracer information for all aerosol types</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISWET </code></td>
<td style="text-align: left;">Tracer information for wettable aerosol types</td>
</tr>
</table>
\n 
 The size of these structure component arrays is \f${\tt ISAEXT}\f$,
 respectively \f${\tt ISWEXT}\f$. Each of them has the following
 structure sub-components (which are scalars):
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>ISI </code></td>
<td style="text-align: left;">Size section index</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISM </code></td>
<td style="text-align: left;">Tracer index for aerosol mass</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISMT </code></td>
<td style="text-align: left;">GCM tracer index for aerosol mass</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISN </code></td>
<td style="text-align: left;">Tracer index for aerosol number</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISNT </code></td>
<td style="text-align: left;">GCM tracer index for aerosol number</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ITYP </code></td>
<td style="text-align: left;">Aerosol type index</td>
</tr>
</table>
\n 
 Similar, for internally mixed aerosol, \f${\tt SINT}\f$ also has structure
 components \f${\tt ISAER}\f$ and \f${\tt ISWET}\f$. However, the structure
 sub-components are different for internally mixed aerosol:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>ISI </code></td>
<td style="text-align: left;">Size section index</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISM </code></td>
<td style="text-align: left;">Tracer index for aerosol mass</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISMT </code></td>
<td style="text-align: left;">GCM tracer index for aerosol mass</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISN </code></td>
<td style="text-align: left;">Tracer index for aerosol number</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISNT </code></td>
<td style="text-align: left;">GCM tracer index for aerosol number</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ITYP </code></td>
<td style="text-align: left;">Aerosol type index</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ITYPT </code></td>
<td style="text-align: left;">Number of aerosol types</td>
</tr>
</table>
\n 
 For internally mixed aerosol, \f${\tt ITYP}\f$, \f${\tt ISM}\f$, and \f${\tt
 ISMT}\f$ are arrays of size \f${\tt ITYPT}\f$.
\n 
 The tracer information structures are, for example, extensively used
 in the subroutine \f${\tt SDSUM}\f$. This subroutine may provide some
 useful examples to illustrate the use of the tracer information
 parameters.
\n 
 In addition to tracer information, aerosol chemical and physical
 properties are saved in the following structures:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>AEXTF </code></td>
<td style="text-align: left;">Basic properties for externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>AINTF </code></td>
<td style="text-align: left;">Basic properties for internally mixed aerosol</td>
</tr>
</table>
\n 
 In contrast to the previously introduced structures \f${\tt PEXT}\f$
 and \f${\tt PINT}\f$, these structures provide aerosol information
 according to aerosol type.
\n 
 The following table shows the sub-component for \f${\tt AEXTF}\f$ and
 \f${\tt AINTF}\f$:
\n 
<table>
<tr class="header">
<td style="text-align: left;"><code>TP </code></td>
<td style="text-align: left;">Aerosol type</td>
</tr>
</table>
\n 
 \f${\tt TP}\f$ is an array with size \f${\tt KEXT}\f$, respectively \f${\tt KINT}\f$.
 Different sub-components are used for externally and internally mixed
 aerosol for \f${\tt TP}\f$.
\n 
 The following table contains the sub-components for \f${\tt AEXTF\%TP}\f$.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>DENS </code></td>
<td style="text-align: left;">(Dry) density</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DPHIB </code></td>
<td style="text-align: left;">Tabulated derivatives \f$\partial \Delta\varphi_{i} /
\partial r_{i}\f$</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DPST </code></td>
<td style="text-align: left;">PLA section size \f$\Delta \varphi_{\star}\f$ from input (file \f${\tt sdparam}\f$)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DPSTAR </code></td>
<td style="text-align: left;">PLA section size \f$\Delta \varphi_{\star}\f$ from PLA data tables</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DRYR </code></td>
<td style="text-align: left;">Dry particle radius at the boundaries of the size sections</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DRYRC </code></td>
<td style="text-align: left;">Dry particle radius in the centres of the size sections</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>FLG </code></td>
<td style="text-align: left;">Flag to indicate whether valid data exists for a point in a PLA data table</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>IDRB </code></td>
<td style="text-align: left;">Number of points in the PLA data table for variable \f$r_{i}\f$</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISEC </code></td>
<td style="text-align: left;">Number of size sections for each type of aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KAPPA </code></td>
<td style="text-align: left;">Kappa aerosol microphysical parameter</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>MOLW </code></td>
<td style="text-align: left;">Molecular weight</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>NAME </code></td>
<td style="text-align: left;">Name of the aerosol species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>NTBL </code></td>
<td style="text-align: left;">Index of PLA data table which is used for each type of aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>NUIO </code></td>
<td style="text-align: left;">Number of ions from dissociation of the species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PHI </code></td>
<td style="text-align: left;">Dry particle size \f$\ln (R_{p}/R_{0}))\f$ at the boundaries of the size sections</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PHIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\Delta\varphi_{i}\f$</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PSIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\psi_{i}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PSIM </code></td>
<td style="text-align: left;">Reference value of the basic PLA width parameter \f$\psi_{i,m}\f$</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>RADB </code></td>
<td style="text-align: left;">Dry particle radius at the lower boundary of the size spectrum</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>RADE </code></td>
<td style="text-align: left;">Dry particle radius at the upper boundary of the size spectrum</td>
</tr>
</table>
\n 
 Structure components \f${\tt PHI}\f$, \f${\tt DRYR}\f$, \f${\tt PSIB}\f$, \f${\tt PHIB}\f$,
 and \f${\tt DPHIB}\f$ have structure sub-components \f${\tt VL}\f$ and \f${\tt VR}\f$.
\n 
 The following table contains the sub-components for \f${\tt AINTF\%TP}\f$.
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>DENS </code></td>
<td style="text-align: left;">(Dry) density</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISCB </code></td>
<td style="text-align: left;">Index of first size section</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>ISCE </code></td>
<td style="text-align: left;">Index of last size section</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>KAPPA </code></td>
<td style="text-align: left;">Kappa aerosol microphysical parameter</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>MOLW </code></td>
<td style="text-align: left;">Molecular weight</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>NAME </code></td>
<td style="text-align: left;">Name of the aerosol species</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>NUIO </code></td>
<td style="text-align: left;">Number of ions from dissociation of the species</td>
</tr>
</table>
\n 
 In addition to \f${\tt TP}\f$, \f${\tt AINTF}\f$ also has the following
 structure sub-components:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>DPHIB </code></td>
<td style="text-align: left;">Tabulated derivatives \f$\partial \Delta\varphi_{i} /
\partial r_{i}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DPST </code></td>
<td style="text-align: left;">PLA section size \f$\Delta \varphi_{\star}\f$ from input (file \f${\tt sdparam}\f$)</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DPSTAR </code></td>
<td style="text-align: left;">PLA section size \f$\Delta \varphi_{\star}\f$ from PLA data tables</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DRYR </code></td>
<td style="text-align: left;">Dry particle radius at the boundaries of the size sections</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DRYRC </code></td>
<td style="text-align: left;">Dry particle radius in the centres of the size sections</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>FLG </code></td>
<td style="text-align: left;">Flag to indicate whether valid data exists for a point in a PLA data table</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>IDRB </code></td>
<td style="text-align: left;">Number of points in the PLA data table for variable \f$r_{i}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>ISEC </code></td>
<td style="text-align: left;">Number of size sections for each type of aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>NTBL </code></td>
<td style="text-align: left;">Index of PLA data table which is used for each type of aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PHI </code></td>
<td style="text-align: left;">Dry particle size \f$\ln (R_{p}/R_{0}))\f$ at the boundaries of the size sections</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PHIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\Delta\varphi_{i}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>PSIB </code></td>
<td style="text-align: left;">Tabulated values of \f$\psi_{i}\f$</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PSIM </code></td>
<td style="text-align: left;">Reference value of the basic PLA width parameter \f$\psi_{i,m}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>RADB </code></td>
<td style="text-align: left;">Dry particle radius at the lower boundary of the size spectrum</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>RADE </code></td>
<td style="text-align: left;">Dry particle radius at the upper boundary of the size spectrum</td>
</tr>
</table>
\n 
 Similar to externally mixed aerosol, structure components \f${\tt PHI}\f$,
 \f${\tt DRYR}\f$, \f${\tt PSIB}\f$, \f${\tt PHIB}\f$, and \f${\tt DPHIB}\f$ have
 structure sub-components \f${\tt VL}\f$ and \f${\tt VR}\f$.
\n 



## Other parameters {#otherParams}
\n 
 The module \f${\tt SDPARM}\f$ contains other physical, chemical, and
 numerical parameters that are used in different PLA subroutines and
 functions. Generally relevant parameters should be included in this module.
\n 



## PLA Subroutines and Functions {#subroutinesAndFunctions}

\n 
 Subroutines related to initialization of PLA parameters and basic
 model input and output are:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>SDCONF </code></td>
<td style="text-align: left;">Configuration and initialization of constant PLA arrays</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDPEMI </code></td>
<td style="text-align: left;">Configuration of arrays for primary aerosol emissions</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>SDREAD </code></td>
<td style="text-align: left;">Input of aerosol information from parameter file</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDSUM </code></td>
<td style="text-align: left;">Control output of aerosol information</td>
</tr>
</table>
\n 
 The input of basic aerosol information and allocation of memory for
 PLA arrays occurs in the subroutine \f${\tt SDCONF}\f$. This subroutines
 calls the subroutine \f${\tt SDREAD}\f$ for the input of the information
 that provided via the file \f${\tt sdparam}\f$. A summary of basic results
 is provided at the end of \f${\tt SDCONF}\f$ via the subroutine \f${\tt SDSUM}\f$.
\n 
 Basic PLA functions are available via the module \f${\tt SDCODE}\f$. The
 following functions are currently available:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>ERFI </code></td>
<td style="text-align: left;">Imaginary error function.</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDINT </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for any given moment for multi-dimensional data</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>SDINT0 </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for the \f$0^{\tt th}\f$ moment for multi-dimensional data</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDINTB </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for any given moment for a single size distribution</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>SDINTB0 </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for the \f$0^{\tt th}\f$ moment for a single size distribution</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDINTL </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for any moment for multi-dimensional data using piecewise linear representation of size distribution</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>SDINTL0 </code></td>
<td style="text-align: left;">Calculates integrals over the size distribution for \f$0^{\tt th}\f$ moment for multi-dimensional data using piecewise linear representation of size distribution</td>
</tr>
</table>
\n 
 \f${\tt ERFI}\f$ may be used if the imaginary error function is not otherwise
 available. A Taylor expansion is used in this function to approximate
 the imaginary error function. The approach is not optimal and may
 cause considerable round-off errors. Normally, this function would not
 be used in the code as long as only positive values for \f${\tt PSIM}\f$ are used.
\n 
 The following basic PLA subroutines are currently available for
 numerical purposes:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>CORNME </code></td>
<td style="text-align: left;">Checks and, if necessary, corrects number and mass concentrations in order to avoid unphysical inputs to PLA calculations. For externally mixed aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>CORNMI </code></td>
<td style="text-align: left;">Checks and, if necessary, corrects number and mass concentrations in order to avoid unphysical inputs to PLA calculations. For internally mixed aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>NM2PLA </code></td>
<td style="text-align: left;">Converts aerosol number and mass concentrations to basic PLA parameters \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>NM2PARX </code></td>
<td style="text-align: left;">Converts aerosol number and mass concentrations to basic PLA parameters \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$ after updating aerosol density and checking/correcting number and mass concentrations</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PLA2NM </code></td>
<td style="text-align: left;">Converts basic PLA parameters \f$n_{0,i}\f$, \f$\psi_{i}\f$, and \f$\varphi_{0,i}\f$ to aerosol number and mass concentrations</td>
</tr>
</table>
\n 
 Note that \f${\tt CORNME}\f$, resp. \f${\tt CORNMI}\f$, need to be called
 before \f${\tt NM2PLA}\f$. These subroutines may produce adjustments to
 the aerosol number and mass concentrations in order to avoid
 numerical problems in \f${\tt NM2PLA}\f$ and other subroutines which may,
 for example, result from numerical round-off errors from tracer advection
 calculations in the model. These adjustments may in principle cause
 non-conservation. Information is available about the magnitude of
 the corrections from these subroutines.
\n 
 In addition, subroutines are available to calculate basic physical and
 chemical properties of the aerosol:
\n 
<table>
<tr class="odd">
<td style="text-align: left;"><code>SDAPROP </code></td>
<td style="text-align: left;">Calculation of wet aerosol particle radius</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SDDENS </code></td>
<td style="text-align: left;">Calculation of dry particle density for internally mixed aerosol</td>
</tr>
</table>
\n 
 Developers should be aware that any changes to aerosol number and mass
 concentrations in the code (i.e. from parameterizations) will
 generally be associated with changes in the basic PLA parameters and dry particle
 densities (for internally mixed aerosol) and vice versa. Developers
 are therefore advised to make sure that the aerosol properties are
 updated accordingly (i.e. through calling \f${\tt NM2PLA}\f$ respectively
 \f${\tt PLA2NM}\f$) before subroutines that require information about the
 aerosol size distribution.
\n 
 The following additional subroutines are currently available:
\n
<table>
<tr class="odd">
<td style="text-align: left;"><code>COAG </code></td>
<td style="text-align: left;">Coagulation (for internally mixed aerosol)</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>GRVSTL </code></td>
<td style="text-align: left;">Gravitational settling</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>GTPCNV </code></td>
<td style="text-align: left;">Gas-to-particle conversion (nucleation and condensation) for sulphate aerosol</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>GTSOA </code></td>
<td style="text-align: left;">Gas-to-particle conversion (condensation) for secondary organic aerosol</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>DYDEP </code></td>
<td style="text-align: left;">Dry deposition</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>DUSTEM </code></td>
<td style="text-align: left;">Mineral dust emissions</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>PRIAEM </code></td>
<td style="text-align: left;">Insertion of primary aerosol emissions</td>
</tr>
<tr class="even">
<td style="text-align: left;"><code>SCLDSP </code></td>
<td style="text-align: left;">Wet removal and in-cloud sulphate production</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><code>SSGEN </code></td>
<td style="text-align: left;">Sea salt aerosol generation</td>
</tr>
</table>
\n 
 Subroutines related to diagnosis of results, cloud microphyiscal
 parameterizations, and radiative calculations are not included in this document.
\n\n


# PAM Call Graph {#callGraph}

\image html main_pam.gv-1.png 
