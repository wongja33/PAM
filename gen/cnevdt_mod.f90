!> \file
!> \brief Field declarations for tabulated growth data for CCN calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module cnevdt
  !
  implicit none
  !
  !-----------------------------------------------------------------------
  !     pre-processed data from program scaled19 (do not change !)
  !     date (ccyymmdd/hhmmss.sss) = 20081031/210808.211
  !
  real, parameter :: yndef = -.999990e+05 !<
  integer, parameter :: ird = 300 !<
  real, dimension(ird) :: rat !<
  real, dimension(ird) :: bsc !<
  integer, parameter :: ir1p =  50 !<
  integer, parameter :: ib1p = 100 !<
  real, target, dimension(ib1p,ird) :: bs1p !<
  real, target, dimension(ir1p,ib1p,ird) :: xp1p !<
  real, target, dimension(ir1p,ib1p,ird) :: tp1p !<
  integer, parameter :: ir2p =  49 !<
  integer, parameter :: ib2p =  51 !<
  real, target, dimension(ib2p,ird) :: bs2p !<
  real, target, dimension(ir2p,ib2p,ird) :: xp2p !<
  real, target, dimension(ir2p,ib2p,ird) :: tp2p !<
  integer, parameter :: ir3p =  46 !<
  integer, parameter :: ib3p =  92 !<
  real, target, dimension(ib3p,ird) :: bs3p !<
  real, target, dimension(ir3p,ib3p,ird) :: xp3p !<
  real, target, dimension(ir3p,ib3p,ird) :: tp3p !<
  integer, parameter :: ir1n =  50 !<
  integer, parameter :: ib1n =   5 !<
  real, target, dimension(ib1n,ird) :: bs1n !<
  real, target, dimension(ir1n,ib1n,ird) :: xp1n !<
  real, target, dimension(ir1n,ib1n,ird) :: tp1n !<
  integer, parameter :: ir2n =  49 !<
  integer, parameter :: ib2n =   5 !<
  real, target, dimension(ib2n,ird) :: bs2n !<
  real, target, dimension(ir2n,ib2n,ird) :: xp2n !<
  real, target, dimension(ir2n,ib2n,ird) :: tp2n !<
  !-----------------------------------------------------------------------
end module cnevdt
