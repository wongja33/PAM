!> \file
!> \brief Check and correct number and mass in each section of the size
!>       disributions in order to avoid invalid inputs to microphysics
!>       and/or chemistry calculations. This subroutine should be used
!>       for internally mixed aerosol species. In contrast to externally
!>       mixed aerosol, this subroutine will adjust the aerosol number
!>       concentration, if necessary.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine cornmi (resm,resn,corm,corn,bnum,bmass,drydn, &
                         ismin,ismax,phiss,dphis,ilga,leva,isec)
  !
  use sdparm, only : r0,ycnst,yna,ysec,ytiny
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(out), dimension(ilga,leva) :: resm !< Mass residuum from numerical truncation in growth calculations
  real, intent(out), dimension(ilga,leva) :: resn !< Number residuum from numerical truncation in growth calculations
  real, intent(out), dimension(ilga,leva) :: corm !< Corrected mass
  real, intent(out), dimension(ilga,leva) :: corn !< Corrected number
  real, intent(inout), dimension(ilga,leva,isec) :: bnum !< Aerosol number concentration \f$[1/kg]\f$
  real, intent(inout), dimension(ilga,leva,isec) :: bmass !< Aerosol number concentration \f$[kg/kg]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(isec) :: phiss !< Dry particle size \f$(ln(Rp/R0))\f$ at the boundaries of the size section
  real, intent(in), dimension(isec) :: dphis !< Section width
  integer, intent(in), dimension(isec) :: ismax !< Maximum number of sections
  integer, intent(in), dimension(isec) :: ismin !< Minimum number of sections
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: anum !<
  real, allocatable, dimension(:,:,:) :: amass !<
  real, allocatable, dimension(:,:,:) :: phihat !<
  real, allocatable, dimension(:,:,:) :: arg !<
  real, allocatable, dimension(:,:,:) :: term !<
  real, parameter :: yscal=1.e+09 !<
  real :: onethird !<
  real :: r0p !<
  real :: acnst !<
  real :: rat0 !<
  real :: amasst !<
  real :: ratr !<
  real :: anumt !<
  real :: termt !<
  integer :: is !<
  integer :: i !<
  integer :: k !<
  integer :: l !<
  integer :: il !<
  integer :: iboff !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(anum  (ilga,leva,isec))
  allocate(amass (ilga,leva,isec))
  allocate(phihat(ilga,leva,isec))
  allocate(arg   (ilga,leva,isec))
  allocate(term  (ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  onethird=1./3.
  r0p=r0**3
  acnst=ycnst*r0p*yscal
  resn=0.
  resm=0.
  corn=0.
  corm=0.
  !
  !-----------------------------------------------------------------------
  !     * make sure there are only positive values for number and mass
  !     * in each section. also make sure that number and mass fall
  !     * into the correct section by checking whether the ratio
  !     * mass/number is in the correct range, i.e. the maximum and
  !     * minimum possible particle sizes for delta-function size
  !     * distributions are not smaller or bigger than the particle size
  !     * range defined by the boundaries of the sections. remove or
  !     * add number in case there is no physical solution.
  !
  arg=yna
  term=acnst*drydn*bnum
  amass=bmass*yscal
  where (bnum > ytiny .and. bmass > ytiny &
      .and. 2+abs(exponent(amass) - exponent(term)) &
      < maxexponent(amass) .and. term/=0. )
    arg=amass/term
  end where
  anum=0.
  amass=0.
  where (arg <= ytiny)
    arg=1.
    anum=bnum
    amass=bmass
    bnum =0.
    bmass=0.
  end where
  do is=1,isec
    resn(:,:)=resn(:,:)+anum (:,:,is)
    resm(:,:)=resm(:,:)+amass(:,:,is)
  end do
  corn = corn+resn
  corm = corm+resm
  phihat = log(arg) / 3.0
  !
  !-----------------------------------------------------------------------
  !     * residuals.
  !
  anum =0.
  amass=0.
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (bnum(il,l,is) > ytiny .and. bmass(il,l,is) > ytiny) then
          rat0=(phihat(il,l,is)-phiss(is))/dphis(is)
          iboff=int(rat0)
          if (rat0 < 0. ) iboff=iboff-1
          if (iboff /= 0) then
            amasst=bmass(il,l,is)
            if (iboff > 0) then
              ratr=ysec
            else
              ratr=1.-ysec
            end if
            anumt=amasst*yscal/(acnst*drydn(il,l,is) &
                                   *exp(3.*(ratr*dphis(is)+phiss(is))))
            termt=acnst*drydn(il,l,is)*anumt
            if ( .not.(anumt > ytiny &
                .and. 2+abs(exponent(amasst) - exponent(termt)) &
                < maxexponent(amasst) .and. termt/=0. ) ) then
              anumt=0.
              amasst=0.
              resn(il,l)=resn(il,l)+bnum (il,l,is)
              resm(il,l)=resm(il,l)+bmass(il,l,is)
            end if
            corn(il,l)=corn(il,l)-(anumt -bnum (il,l,is))
            corm(il,l)=corm(il,l)-(amasst-bmass(il,l,is))
            bnum (il,l,is)=anumt
            bmass(il,l,is)=amasst
          end if
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(anum)
  deallocate(amass)
  deallocate(phihat)
  deallocate(arg)
  deallocate(term)
  !
end subroutine cornmi
