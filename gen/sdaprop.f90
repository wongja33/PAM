!> \file
!> \brief Calculation of wet particle radius and hygroscopic aerosol
!>       properties for humidity growth.
!>       Make sure that dry aerosol density is up-to-date before
!>       the call of this subroutine.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdaprop (pewetrc,piwetrc,pen0,pephi0,pepsi,peddn, &
                          pin0,piphi0,pipsi,pifrc,piddn,ta,rha, &
                          ilga,leva)
  !
  use sdparm, only : aextf,aextfg,aintf,aintfg,isaext,isaint, &
                         kext,kextbc,kextcl,kextmd,kextno3,kextoc,kextsac, &
                         kextso4,kextss,kint,kintbc,kintcl,kintmd,kintno3, &
                         kintoc,kintsac,kintso4,kintss,pedryrc,pidryrc, &
                         r0,sextf,sintf,ylarges,yna,ypi,ytiny
  use sdphys, only : rgasm,rhoh2o,wh2o
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, external mixture
  real, intent(out), dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m]\f$, internal mixture
  real, intent(in), dimension(ilga,leva) :: rha !< Relative humidity for clear sky portion of grid cell
  real, intent(in), dimension(ilga,leva) :: ta !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, external mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$, amplitude) int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$, width), int. mixture
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaext) :: piddn !< Density of dry aerosol particle \f$[kg/m^3]\f$, internal mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol species mass fraction
  !
  integer :: il !<
  integer :: it !<
  integer :: is !<
  integer :: isi !<
  integer :: itx !<
  integer :: itox !<
  integer :: l !<
  integer :: kx !<
  integer :: kxt !<
  integer :: ntx !<
  real :: aphi !<
  real :: wgtx !<
  real :: pkaoc !<
  real :: pkabc !<
  real :: pkamd !<
  real :: ddry !<
  real :: vdry !<
  real, parameter :: yotrd = 1./3. !<
  integer, parameter :: npnt=20 ! number of reference points
  integer, parameter :: ntpt=5 ! number of aerosol species
  integer, parameter :: itmax=3 ! number of iterations
  real, dimension(npnt) :: rh !<
  data rh / 0.100, 0.382, 0.541, 0.643, 0.714 &
              , 0.766, 0.807, 0.838, 0.864, 0.885 &
              , 0.903, 0.919, 0.932, 0.943, 0.953 &
              , 0.962, 0.970, 0.978, 0.984, 0.990 /
  real, dimension(npnt,ntpt) :: pkatpi !<
  data pkatpi / 1.4974408, 0.9838770, 0.8290761, 0.7245096 &! (nh4)2so4
                  , 0.6532416, 0.6018254, 0.5618502, 0.5322418 &! (nh4)2so4
                  , 0.5083230, 0.4901409, 0.4760467, 0.4655360 &! (nh4)2so4
                  , 0.4593138, 0.4566030, 0.4571863, 0.4613250 &! (nh4)2so4
                  , 0.4691201, 0.4825648, 0.4982601, 0.5223324 &! (nh4)2so4
                  , 11.0825653,  3.9426887,  2.8485217,  2.3564157 &! sea salt
                  ,  2.0665963,  1.8737581,  1.7310804,  1.6282451 &! sea salt
                  ,  1.5455265,  1.4813753,  1.4286581,  1.3839465 &! sea salt
                  ,  1.3494499,  1.3218253,  1.2982354,  1.2785497 &! sea salt
                  ,  1.2626650,  1.2490141,  1.2412405,  1.2378957 &! sea salt
                  , 1.3266588, 0.5646486, 0.5823150, 0.5709559 &! nh4no3
                  , 0.5603971, 0.5560658, 0.5577084, 0.5635365 &! nh4no3
                  , 0.5726734, 0.5830175, 0.5939313, 0.6050492 &! nh4no3
                  , 0.6149372, 0.6238601, 0.6324733, 0.6408188 &! nh4no3
                  , 0.6490519, 0.6587952, 0.6681154, 0.6812770 &! nh4no3
                  , 0.       , 0.       , 0.       , 0. &! nh4cl
                  , 0.       , 0.       , 1.1404024, 1.0998064 &! nh4cl
                  , 1.0654703, 1.0378653, 1.0145106, 0.9942313 &! nh4cl
                  , 0.9783312, 0.9655310, 0.9546949, 0.9459390 &! nh4cl
                  , 0.9393936, 0.9347875, 0.9335700, 0.9362975 &! nh4cl
                  ,  6.2151999, 2.4391816, 1.7912617, 1.4808251 &! h2so4
                  , 1.2884889, 1.1546655, 1.0514783, 0.9740624 &! h2so4
                  , 0.9092117, 0.8567778, 0.8117985, 0.7718493 &! h2so4
                  , 0.7395788, 0.7125072, 0.6882302, 0.6668478 &! h2so4
                  , 0.6484994, 0.6313590, 0.6202098, 0.6129278 /    ! h2so4
  !
  integer, dimension(ilga,leva) :: ito !<
  real, dimension(ilga,leva) :: rhat !<
  real, dimension(ilga,leva) :: sfctw !<
  real, dimension(ilga,leva) :: ak !<
  real, dimension(ilga,leva) :: wgt !<
  real, dimension(ilga,leva) :: pkaas !<
  real, dimension(ilga,leva) :: pkaan !<
  real, dimension(ilga,leva) :: pkaac !<
  real, dimension(ilga,leva) :: pkass !<
  real, dimension(ilga,leva) :: pkasa !<
  real, dimension(ilga,leva) :: fgrwth !<
  real, dimension(ilga,leva) :: vnok !<
  real, dimension(ilga,leva) :: term !<
  real, dimension(ilga,leva) :: term1 !<
  real, dimension(ilga,leva) :: term2 !<
  real, dimension(ilga,leva) :: akelvl !<
  real, dimension(ilga,leva) :: akelvh !<
  real, dimension(ilga,leva) :: akelv !<
  real, dimension(ilga,leva) :: vfrc !<
  real, dimension(ilga,leva) :: vtot !<
  real, dimension(ilga,leva) :: dtot !<
  real, dimension(ilga,leva) :: avgvt !<
  real, dimension(ilga,leva) :: avggf !<
  real, dimension(ilga,leva) :: volsd !<
  real, dimension(ilga,leva) :: volsdt !<
  real, allocatable, dimension(:,:,:) :: pikap !<
  real, allocatable, dimension(:,:,:) :: pekap !<
  real, allocatable, dimension(:) :: pikmax !<
  real, allocatable, dimension(:) :: pekmax !<
  real, allocatable, dimension(:,:,:,:) :: pivfrc !<
  real(r8), dimension(ilga,leva) :: dbl1 !<
  real(r8), dimension(ilga,leva) :: dbl2 !<
  real(r8), dimension(ilga,leva) :: dbl3 !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory.
  !
  if (kext > 0) then
    allocate (pekap (ilga,leva,isaext))
    allocate (pekmax(isaext))
  end if
  if (kint > 0) then
    allocate (pikap (ilga,leva,isaint))
    allocate (pikmax(isaint))
    allocate (pivfrc(ilga,leva,isaint,kint))
  end if
  !
  !-----------------------------------------------------------------------
  !     * restrict relative humidity to valid range.
  !
  rhat=max(0.1,min(0.99,rha))
  !
  !     * surface tension of water.
  !
  sfctw=0.0761-1.55e-04*(ta-273.)
  !
  !     * a-term in koehler equation (for curvature effect).
  !
  ak=4.*wh2o*sfctw/(rgasm*ta*rhoh2o)
  ak=ak*(ypi/6.)**yotrd
  !
  !-----------------------------------------------------------------------
  !     * hygroscopicity parameters for individual chemical constituents.
  !     * the default approach is to use the kappa parameters from
  !     * ccn calculations. otherwise, more detailed information is used
  !     * (currently: ammonium sulphate, sodium chloride, ammonium nitrate).
  !
  !     * parameters for humidity interpolation.
  !
  ito=2
  do it=1,npnt-1
    do l=1,leva
      do il=1,ilga
        if (rhat(il,l) > rh(it) ) ito(il,l)=it+1
      end do
    end do
  end do
  do l=1,leva
    do il=1,ilga
      itox=ito(il,l)
      wgt(il,l)=(rh(itox)-rhat(il,l))/(rh(itox)-rh(itox-1))
    end do
  end do
  !
  !     * interpolation of tabulated hygroscopicities.
  !
  if (kextso4 > 0 .or. kintso4 > 0) then
    ntx=1
    do l=1,leva
      do il=1,ilga
        itox=ito(il,l)
        wgtx=wgt(il,l)
        pkaas(il,l)=wgtx*pkatpi(itox-1,ntx) &
                                        +(1.-wgtx)*pkatpi(itox,ntx)
      end do
    end do
  end if
  if (kextss > 0 .or. kintss > 0) then
    ntx=2
    do l=1,leva
      do il=1,ilga
        itox=ito(il,l)
        wgtx=wgt(il,l)
        pkass(il,l)=wgtx*pkatpi(itox-1,ntx) &
                                        +(1.-wgtx)*pkatpi(itox,ntx)
      end do
    end do
  end if
  if (kextno3 > 0 .or. kintno3 > 0) then
    ntx=3
    do l=1,leva
      do il=1,ilga
        itox=ito(il,l)
        wgtx=wgt(il,l)
        pkaan(il,l)=wgtx*pkatpi(itox-1,ntx) &
                                        +(1.-wgtx)*pkatpi(itox,ntx)
      end do
    end do
  end if
  if (kextcl > 0 .or. kintcl > 0) then
    ntx=4
    do l=1,leva
      do il=1,ilga
        itox=ito(il,l)
        wgtx=wgt(il,l)
        pkaac(il,l)=wgtx*pkatpi(itox-1,ntx) &
                                        +(1.-wgtx)*pkatpi(itox,ntx)
      end do
    end do
  end if
  if (kextsac > 0 .or. kintsac > 0) then
    ntx=5
    do l=1,leva
      do il=1,ilga
        itox=ito(il,l)
        wgtx=wgt(il,l)
        pkasa(il,l)=wgtx*pkatpi(itox-1,ntx) &
                                        +(1.-wgtx)*pkatpi(itox,ntx)
      end do
    end do
  end if
  !
  !     * hygroscopicity of the aerosol.
  !
  if (kext > 0) then
    do is=1,isaext
      kx=sextf%isaer(is)%ityp
      if (kx == kextoc) then
        pekap(:,:,is)=aextf%tp(kextoc)%kappa
      end if
      if (kx == kextbc) then
        pekap(:,:,is)=aextf%tp(kextbc)%kappa
      end if
      if (kx == kextmd) then
        pekap(:,:,is)=aextf%tp(kextmd)%kappa
      end if
      if (kx == kextso4) then
        pekap(:,:,is)=pkaas(:,:)
      end if
      if (kx == kextss) then
        pekap(:,:,is)=pkass(:,:)
      end if
      if (kx == kextno3) then
        pekap(:,:,is)=pkaan(:,:)
      end if
      if (kx == kextcl) then
        pekap(:,:,is)=pkaac(:,:)
      end if
      if (kx == kextsac) then
        pekap(:,:,is)=pkasa(:,:)
      end if
      pekmax(is)=maxval(pekap(:,:,is))
    end do
  end if
  if (kint > 0) then
    !
    !       * volume fractions derived from mass fractions.
    !
    do is=1,isaint
      do kxt=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(kxt)
        isi=sintf%isaer(is)%isi
        pivfrc(:,:,is,kx)= &
                               pifrc(:,:,isi,kx)*piddn(:,:,isi)/aintf%tp(kx)%dens
      end do
    end do
    !
    !       * apply zsr-rule to obtain volume-weighted mean hygroscopicity.
    !
    pikap=0.
    if (kintoc > 0) then
      pkaoc=aintf%tp(kintoc)%kappa
      pikap=pikap+pivfrc(:,:,:,kintoc)*pkaoc
    end if
    if (kintbc > 0) then
      pkabc=aintf%tp(kintbc)%kappa
      pikap=pikap+pivfrc(:,:,:,kintbc)*pkabc
    end if
    if (kintmd > 0) then
      pkamd=aintf%tp(kintmd)%kappa
      pikap=pikap+pivfrc(:,:,:,kintmd)*pkamd
    end if
    if (kintso4 > 0) then
      do is=1,isaint
        pikap(:,:,is)=pikap(:,:,is) &
                                  +pivfrc(:,:,is,kintso4)*pkaas(:,:)
      end do
    end if
    if (kintss > 0) then
      do is=1,isaint
        pikap(:,:,is)=pikap(:,:,is) &
                                  +pivfrc(:,:,is,kintss)*pkass(:,:)
      end do
    end if
    if (kintno3 > 0) then
      do is=1,isaint
        pikap(:,:,is)=pikap(:,:,is) &
                                  +pivfrc(:,:,is,kintno3)*pkaan(:,:)
      end do
    end if
    if (kintcl > 0) then
      do is=1,isaint
        pikap(:,:,is)=pikap(:,:,is) &
                                  +pivfrc(:,:,is,kintcl)*pkaac(:,:)
      end do
    end if
    if (kintsac > 0) then
      do is=1,isaint
        pikap(:,:,is)=pikap(:,:,is) &
                                  +pivfrc(:,:,is,kintsac)*pkasa(:,:)
      end do
    end if
    do is=1,isaint
      pikmax(is)=maxval(pikap(:,:,is))
    end do
  end if
  !
  !     * wet particle size for ensemble of test particles.
  !
  if (kext > 0) then
    do is=1,isaext
      if (pekmax(is) > ytiny) then
        volsdt=0.
        avgvt=0.
        do it=1,aextfg%sec(is)%ntstp
          ddry=2.*aextfg%sec(is)%radt(it)
          !
          !             * volume of dry test particle.
          !
          vdry=(ypi/6.)*ddry**3
          !
          !             * volume of particle in absence of kelvin effect.
          !
          fgrwth=1.+pekap(:,:,is)*rhat/(1.-rhat)
          vnok=vdry*fgrwth
          !
          !             * bracket solution for kelvin term.
          !
          term=ak*vdry**(-yotrd)
          term1=exp(term)
          akelvh=term1
          term2=-yotrd
          term=vnok
          term1=term**term2
          term=ak*term1
          term1=exp(term)
          akelvl=term1
          !
          !             * initial guess for kelvin term and volume fraction
          !             * of water (relative to dry particle).
          !
          akelv=.5*(akelvh+akelvl)
          vfrc=pekap(:,:,is)*(rhat/akelv)/(1.-rhat/akelv)
          !
          !             * iterative calculation of volume fraction of water
          !             * and total particle volume for each test particle.
          !
          do itx=1,itmax
            fgrwth=1.+vfrc
            vtot=vdry*fgrwth
            term2=-yotrd
            term=vtot
            term1=term**term2
            term=ak*term1
            term1=exp(term)
            akelv=term1
            vfrc=pekap(:,:,is)*(rhat/akelv)/(1.-rhat/akelv)
          end do
          fgrwth=1.+vfrc
          vtot=vdry*fgrwth
          !
          !             * volume size distribution and total volume
          !             * for test particles.
          !
          aphi=log(.5*ddry/r0)
          term=0.
          where (abs(pepsi(:,:,is)-yna) > ytiny)
            term=-pepsi(:,:,is)*(aphi-pephi0(:,:,is))**2
          end where
          term1=exp(term)
          where (abs(pepsi(:,:,is)-yna) > ytiny)
            volsd=vdry*pen0(:,:,is)*term1
            volsdt=volsdt+volsd
            avgvt=avgvt+vfrc*volsd
          end where
        end do
        !
        !           * volume-weighted volume fraction of water for each section.
        !
        where (2+abs(exponent(avgvt) - exponent(volsdt)) &
            < maxexponent(avgvt) .and. volsdt/=0. )
          avgvt=avgvt/volsdt
        end where
        !
        !           * average aerosol radius growth factor.
        !
        fgrwth=1.+avgvt
        term2=yotrd
        term=fgrwth
        dbl1=term2
        dbl2=term
        dbl3=dbl2**dbl1
        avggf=dbl3
        pewetrc(:,:,is)=pedryrc(is)*avggf
      else
        pewetrc(:,:,is)=pedryrc(is)
      end if
    end do
  end if
  if (kint > 0) then
    do is=1,isaint
      if (pikmax(is) > ytiny) then
        volsdt=0.
        avgvt=0.
        do it=1,aintfg%sec(is)%ntstp
          ddry=2.*aintfg%sec(is)%radt(it)
          !
          !             * volume of dry test particle.
          !
          vdry=(ypi/6.)*ddry**3
          !
          !             * volume of particle in absence of kelvin effect.
          !
          fgrwth=1.+pikap(:,:,is)*rhat/(1.-rhat)
          vnok=vdry*fgrwth
          !
          !             * bracket solution for kelvin term.
          !
          term=ak*vdry**(-yotrd)
          term1=exp(term)
          akelvh=term1
          term2=-yotrd
          term=vnok
          term1=term**term2
          term=ak*term1
          term1=exp(term)
          akelvl=term1
          !
          !             * initial guess for kelvin term and volume fraction
          !             * of water (relative to dry particle).
          !
          akelv=.5*(akelvh+akelvl)
          vfrc=pikap(:,:,is)*(rhat/akelv)/(1.-rhat/akelv)
          !
          !             * iterative calculation of volume fraction of water
          !             * and total particle volume for each test particle.
          !
          do itx=1,itmax
            fgrwth=1.+vfrc
            vtot=vdry*fgrwth
            term2=-yotrd
            term=vtot
            term1=term**term2
            term=ak*term1
            term1=exp(term)
            akelv=term1
            vfrc=pikap(:,:,is)*(rhat/akelv)/(1.-rhat/akelv)
          end do
          fgrwth=1.+vfrc
          vtot=vdry*fgrwth
          !
          !             * volume size distribution and total volume
          !             * for test particles.
          !
          aphi=log(.5*ddry/r0)
          term=0.
          where (abs(pipsi(:,:,is)-yna) > ytiny &
              .and. pipsi(:,:,is) < ylarges)
            term=-pipsi(:,:,is)*(aphi-piphi0(:,:,is))**2
          end where
          term1=exp(term)
          where (abs(pipsi(:,:,is)-yna) > ytiny)
            volsd=vdry*pin0(:,:,is)*term1
            volsdt=volsdt+volsd
            avgvt=avgvt+vfrc*volsd
          end where
        end do
        !
        !           * volume-weighted volume fraction of water for each section.
        !
        where (2+abs(exponent(avgvt) - exponent(volsdt)) &
            < maxexponent(avgvt) .and. volsdt/=0. )
          avgvt=avgvt/volsdt
        end where
        !
        !           * average aerosol radius growth factor.
        !
        fgrwth=1.+avgvt
        term2=yotrd
        term=fgrwth
        dbl1=term2
        dbl2=term
        dbl3=dbl2**dbl1
        avggf=dbl3
        piwetrc(:,:,is)=pidryrc(is)*avggf
      else
        piwetrc(:,:,is)=pidryrc(is)
      end if
    end do
  end if
  !
  !     * deallocate memory.
  !
  if (kext > 0) then
    deallocate (pekap)
    deallocate (pekmax)
  end if
  if (kint > 0) then
    deallocate (pikap)
    deallocate (pikmax)
    deallocate (pivfrc)
  end if
  !
end subroutine sdaprop
