!> \file
!> \brief Transformation of number and mass to PLA parameters for internally
!>       mixed types of aerosol, including calculation of aerosol density and
!>       mass and number corrections, if necessary.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine nm2pari(pin0,piphi0,pipsi,piphis0,pidphi0,piddn, &
                         pinum,pimas,pifrc,ilga,leva)
  !
  use sdparm, only : r0,kpint,kint,isaint,pidphis,piphiss,piismin,piismax
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(inout),  dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(inout),  dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(out), dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) int. mixture
  real, intent(in), dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in), dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  !
  !     internal work variables
  !
  real, dimension(ilga,leva) :: corn !<
  real, dimension(ilga,leva) :: corm !<
  real, dimension(ilga,leva) :: resn !<
  real, dimension(ilga,leva) :: resm !<
  real, dimension(ilga,leva) :: cornt !<
  real, dimension(ilga,leva) :: cormt !<
  real, dimension(ilga,leva) :: resnt !<
  real, dimension(ilga,leva) :: resmt !<
  !
  !-----------------------------------------------------------------------
  !     * update dry particle density (internally mixed aerosol).
  !
  if (isaint > 0) call sddens(piddn,pifrc,ilga,leva)
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  corn=0.
  corm=0.
  resn=0.
  resm=0.
  !
  !-----------------------------------------------------------------------
  !     * aerosol number and mass adjustments.
  !
  if (isaint > 0) then
    call cornmi(resmt,resnt,cormt,cornt,pinum,pimas,piddn, &
                    piismin,piismax,piphiss,pidphis,ilga,leva,isaint)
    corn=corn+cornt
    corm=corm+cormt
    resn=resn+resnt
    resm=resm+resmt
  end if
  !
  !     * update basic pla parameters.
  !
  if (isaint > 0) then
    call nm2pla(pin0,piphi0,pipsi,resn,resm,pinum,pimas, &
                    piddn,piphiss,pidphis,piphis0,pidphi0, &
                    ilga,leva,isaint,kpint)
  end if
  !
end subroutine nm2pari
