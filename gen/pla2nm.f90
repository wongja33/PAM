!> \file
!> \brief Transformation of PLA-parameters to number and mass.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pla2nm (bnum,bmass,pn0,phi0,psi,phis0,dphi0,drydn, &
                         ilga,leva,isec)
  !
  use sdparm, only : r0,ycnst,yna,ytiny
  use sdcode, only : sdint, sdint0
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< First PLA size distribution parameter (\f$n_{0,i}\f$, amplitude)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< Second PLA size distribution parameter (\f$\psi_{i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< Third PLA size distribution parameter (\f$\phi_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(out), dimension(ilga,leva,isec) :: bnum !< Aerosol number concentration \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isec) :: bmass !< Aerosol number concentration \f$[kg/kg]\f$
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: sdfn !<
  real, allocatable, dimension(:,:,:) :: sdfm !<
  real :: rmom !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work array.
  !
  allocate(sdfn(ilga,leva,isec))
  allocate(sdfm(ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * obtain number and mass concentrations from integration of
  !     * size disribution as defined by pla-parameters.
  !
  sdfn=sdint0(phi0,psi,phis0,dphi0,ilga,leva,isec)
  bnum=pn0*sdfn
  rmom=3.
  sdfm=sdint(phi0,psi,rmom,phis0,dphi0,ilga,leva,isec)
  bmass=pn0*sdfm*drydn*ycnst
  !
  !-----------------------------------------------------------------------
  !     * check whether pla parameters are valid and produce invalid
  !     * results if that is not the case.
  !
  where (abs(sdfn-yna) <= ytiny .or. abs(sdfm-yna) <= ytiny)
    bnum=yna
    bmass=yna
  end where
  !
  !-----------------------------------------------------------------------
  !     * deallocate work array.
  !
  deallocate(sdfn)
  deallocate(sdfm)
  !
end subroutine pla2nm
!> \file
!! \subsection ssec_details Details
!! PRIMARY INPUT/OUTPUT VARIABLES (ARGUMENT LIST):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | O     | BMASS  |  AEROSOL MASS CONCENTRATION (KG/KG)|
!! | O     | BNUM   |  AEROSOL NUMBER CONCENTRATION (1/KG)|
!! | I     | DPHI0  |  SECTION WIDTH|
!! | I     | DRYDN  |  DENSITY OF DRY AEROSOL PARTICLE (KG/M3)|
!! | I     | ILGA   |  NUMBER OF GRID POINTS IN HORIZONTAL DIRECTION|
!! | I     | ISEC   |  NUMBER OF SEPARATE AEROSOL TRACERS|
!! | I     | LEVA   |  NUMBER OF GRID POINTA IN VERTICAL DIRECTION|
!! | I     | PHI0   |  3RD PLA SIZE DISTRIBUTION PARAMETER (MODE SIZE)|
!! | I     | PN0    |  1ST PLA SIZE DISTRIBUTION PARAMETER (AMPLITUDE)|
!! | I     | PHIS0  |  LOWER DRY PARTICLE SIZE (=LOG(R/R0))|
!! | I     | PSI    |  2ND PLA SIZE DISTRIBUTION PARAMETER (WIDTH)|
!! \n\n
!!     SECONDARY INPUT/OUTPUT VARIABLES (VIA MODULES AND COMMON BLOCKS):
!! |In/Out | Name   | Details                                                  |
!! |:------|:-------|:---------------------------------------------------------|
!! | I     | YCNST  |  4*PI/3|
!! | I     | YNA    |  DEFAULT FOR UNDEFINED VALUE|
!!
!! -----------------------------------------------------------------------
