!> \file
!> \brief Input and saving of aerosol type information.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdread(nupar)
  !
  use sdpio,  only : aext,aint,ismax,kmax
  use sdparm, only : aextf,aintf,cna,iexkap,iinkap,ina,kext,kint,yna,ytiny
  !
  implicit none
  !
  integer, intent(in) :: nupar !< File unit number
  integer, dimension(kmax) :: kioext !<
  integer, dimension(kmax) :: kioint !<
  namelist /sdbpar/ aext,aint
  integer :: is !<
  integer :: kx !<
  integer :: ky !<
  integer :: icnte !<
  integer :: icntp !<
  integer :: icnt !<
  integer :: icntk !<
  integer :: isect !<
  integer :: ist !<
  integer :: iok !<
  integer :: iscbt !<
  integer :: iscet !<
  !
  !-----------------------------------------------------------------------
  !     * initialize work and io arrays.
  !
  kioext=ina
  kioint=ina
  do kx=1,kmax
    aext%tp(kx)%isec=ina
    aext%tp(kx)%dpst=yna
    aext%tp(kx)%radb=yna
    aext%tp(kx)%dens=yna
    aext%tp(kx)%molw=yna
    aext%tp(kx)%nuio=yna
    aext%tp(kx)%kappa=yna
    aext%tp(kx)%stick=yna
    aext%tp(kx)%psim(:)=yna
    aext%tp(kx)%name(1:1)=cna
  end do
  aint%isec=ina
  aint%dpst=yna
  aint%radb=yna
  aint%stick=yna
  aint%psim(:)=yna
  do kx=1,kmax
    aint%tp(kx)%iscb=1
    aint%tp(kx)%isce=ina
    aint%tp(kx)%dens=yna
    aint%tp(kx)%molw=yna
    aint%tp(kx)%nuio=yna
    aint%tp(kx)%kappa=yna
    aint%tp(kx)%name(1:1)=cna
  end do
  !
  !-----------------------------------------------------------------------
  !     * read namelists.
  !
  ! *** properties of the aerosol ***
  !
  ! parameter dictionary:
  !    dens  - dry aerosol density
  !    dpst  - pla section width
  !    iscb  - index of first section for pla calculations
  !    isce  - index of last section for pla calculations
  !    isec  - number of sections for pla calculations
  !    kappa - kappa parameter
  !    molw  - dry aerosol molecular weight
  !    name  - name of the aerosol type
  !    nuio  - number of ions from dissociation
  !    psim  - pla distribution width parameter
  !    radb  - radius of the smallest particle of the distribution
  !
  ! mandatory parameters: name,isec,radb,psim,dens,
  !                       (molw, nuio) or kappa
  !
  ! optional parameters:
  !    external mixtures: dpst
  !    internal mixtures: dpst,isce,iscb
  !
  ! note: si-units are required.

#if defined(GM_PAM)
    ! internally mixed aerosol
    aint%isec=3
    aint%radb=0.008e-06
    aint%dpst=1.61734342
    aint%psim(1:3)=(/2.,2.,2./)
    aint%stick=1.

    ! first internally mixed aerosol type
    aint%tp(1)%name='ORGCARB'
    aint%tp(1)%dens=1400.
    aint%tp(1)%kappa=0.2

    ! second internally mixed aerosol type
    aint%tp(2)%name='BLCCARB'
    aint%tp(2)%dens=1700.
    aint%tp(2)%kappa=0.

    ! third internally mixed aerosol type
    aint%tp(3)%name='(NH4)2SO4'
    aint%tp(3)%dens=1769.
    aint%tp(3)%kappa=0.61
    aint%tp(3)%molw=.132133

    ! first externally mixed aerosol type
    aext%tp(1)%name='SEASALT'
    aext%tp(1)%isec=2
    aext%tp(1)%radb=0.017e-06
    aext%tp(1)%dpst=2.80000000
    aext%tp(1)%psim(1:2)=(/2.,2./)
    aext%tp(1)%dens=2165.
    aext%tp(1)%kappa=1.28
    aext%tp(1)%stick=1.

    ! second externally mixed aerosol type
    aext%tp(2)%name='MINDUST'
    aext%tp(2)%isec=2
    aext%tp(2)%radb=0.05e-06
    aext%tp(2)%dpst=2.80000000
    aext%tp(2)%psim(1:2)=(/2.,2./)
    aext%tp(2)%dens=2650.
    aext%tp(2)%kappa=0.05
    aext%tp(2)%stick=1.

    ! third externally mixed aerosol type
    aext%tp(3)%name='BLCCARB'
    aext%tp(3)%isec=1
    aext%tp(3)%radb=0.008e-06
    aext%tp(3)%dpst=4.85203026 ! 2.80000000
    aext%tp(3)%psim(1)=2.
    aext%tp(3)%dens=1700.
    aext%tp(3)%kappa=0.
    aext%tp(3)%stick=0.1

    ! fourth externally mixed aerosol type
    aext%tp(4)%name='ORGCARB'
    aext%tp(4)%isec=1
    aext%tp(4)%radb=0.008e-06
    aext%tp(4)%dpst=4.85203026
    aext%tp(4)%psim(1)=2.
    aext%tp(4)%dens=1400.
    aext%tp(4)%kappa=0.
    aext%tp(4)%stick=0.1
!
#else
     if (nupar>0) read(nupar, nml=sdbpar, end=1001)
#endif
  !
  !-----------------------------------------------------------------------
  !     * check input for externally mixed aerosol.
  !
  do kx=1,kmax
    icnte=0
    icntp=0
    if (aext%tp(kx)%isec /= ina)              icnte=icnte+1
    if (abs(aext%tp(kx)%radb-yna) > ytiny)    icnte=icnte+1
    if (abs(aext%tp(kx)%psim(1)-yna) > ytiny) icnte=icnte+1
    if (aext%tp(kx)%name(1:1) /= cna)         icnte=icnte+1
    if (abs(aext%tp(kx)%dens-yna) > ytiny)    icnte=icnte+1
    if (abs(aext%tp(kx)%stick-yna) > ytiny)   icnte=icnte+1
    if (abs(aext%tp(kx)%molw-yna) > ytiny) then
      icntp=icntp+1
    end if
    if (abs(aext%tp(kx)%nuio-yna) > ytiny) then
      icntp=icntp+1
    end if
    if (abs(aext%tp(kx)%kappa-yna) <= ytiny) then
      if ( .not.((icntp == 2 .and. icnte == 6) .or. icntp == 0) &
          ) then
        call xit('SDREAD',-2)
      end if
    else
      if ( .not.(icnte == 6)                                 ) then
        call xit('SDREAD',-2)
      end if
    end if
  end do
  icnt=0
  do kx=1,kmax
    do ky=kx+1,kmax
      if (aext%tp(kx)%name == aext%tp(ky)%name &
          .and. aext%tp(kx)%name(1:1) /= cna) icnt=1
    end do
  end do
  if (icnt /= 0) then
    call xit('SDREAD',-3)
  end if
  !
  !     * check input for internally mixed aerosol.
  !
  icnte=0
  if (aint%isec /= ina)              icnte=icnte+1
  if (abs(aint%radb-yna) > ytiny)    icnte=icnte+1
  if (abs(aint%stick-yna) > ytiny)   icnte=icnte+1
  if (abs(aint%psim(1)-yna) > ytiny) icnte=icnte+1
  do kx=1,kmax
    icntp=0
    icntk=0
    if (aint%tp(kx)%name(1:1) /= cna) then
      icntp=icntp+1
      icntk=icntk+1
    end if
    if (abs(aint%tp(kx)%dens-yna) > ytiny) then
      icntp=icntp+1
      icntk=icntk+1
    end if
    if (abs(aint%tp(kx)%molw-yna) > ytiny) then
      icntp=icntp+1
    end if
    if (abs(aint%tp(kx)%nuio-yna) > ytiny) then
      icntp=icntp+1
    end if
    if (abs(aint%tp(kx)%kappa-yna) <= ytiny) then
      if ( .not.((icntp == 4 .and. icnte == 4) .or. icntp == 0) &
          ) then
        call xit('SDREAD',-4)
      end if
    else
      if ( .not.((icntk == 2 .and. icnte == 4) .or. icntk == 0) &
          ) then
        call xit('SDREAD',-4)
      end if
    end if
  end do
  icnt=0
  do kx=1,kmax
    do ky=kx+1,kmax
      if (aint%tp(kx)%name == aint%tp(ky)%name &
          .and. aint%tp(kx)%name(1:1) /= cna) icnt=1
    end do
  end do
  if (icnt /= 0) then
    call xit('SDREAD',-5)
  end if
  !
  !-----------------------------------------------------------------------
  !     * assume size distribution covers the entire range for
  !     * internally mixed aerosol species if not otherwise specified.
  !
  do kx=1,kmax
    if (aint%tp(kx)%isce == ina .and. aint%isec /= ina) then
      aint%tp(kx)%isce=aint%isec
    end if
  end do
  !
  !-----------------------------------------------------------------------
  !     * determine lengths of individual input arrays for externally
  !     * mixed aerosol.
  !
  do kx=1,kmax
    isect=ina
    do is=1,ismax
      if (abs(aext%tp(kx)%psim(is)-yna) > ytiny) then
        isect=is
      end if
    end do
    if (isect /= aext%tp(kx)%isec) then
      call xit('SDREAD',-6)
    end if
  end do
  !
  !     * determine lengths of individual input arrays for internally
  !     * mixed aerosol.
  !
  isect=ina
  do is=1,ismax
    if (abs(aint%psim(is)-yna) > ytiny) then
      isect=is
    end if
  end do
  if (isect /= aint%isec) then
    call xit('SDREAD',-7)
  end if
  !
  !-----------------------------------------------------------------------
  !     * determine total number of externally mixed aerosol types.
  !
  kext=0
  do kx=1,kmax
    if (aext%tp(kx)%name(1:1) /= cna) then
      kext=kext+1
      kioext(kext)=kx
    end if
  end do
  !
  !     * determine total number of internally mixed aerosol types.
  !
  kint=0
  do kx=1,kmax
    if (aint%tp(kx)%name(1:1) /= cna) then
      kint=kint+1
      kioint(kint)=kx
    end if
  end do
  !
  !-----------------------------------------------------------------------
  !     * check if there is at least one aerosol type in each section
  !     * of the internally mixed aerosol.
  !
  if (kint > 0) then
    ist=aint%isec
    do is=1,ist
      iok=0
      do kx=1,kint
        iscbt=aint%tp(kioint(kx))%iscb
        iscet=aint%tp(kioint(kx))%isce
        if (is >= iscbt .and. is <= iscet) then
          iok=iok+1
        end if
      end do
      if (iok == 0) then
        call xit('SDREAD',-8)
      end if
    end do
  end if
  !
  !     * check if detailed chemical information is used or
  !     * kappa approach for externally mixed aerosol.
  !
  if (kext > 0) then
    icnt=0
    do kx=1,kext
      if (abs(aext%tp(kx)%kappa-yna) > ytiny)   icnt=icnt+1
    end do
    if (icnt==kext) iexkap=1
    icnt=0
    do kx=1,kext
      if (abs(aext%tp(kx)%nuio-yna) > ytiny)    icnt=icnt+1
    end do
    if ( (icnt==kext .and. iexkap==1) &
        .or. (icnt /= kext .and. iexkap /= 1) ) then
      call xit('SDREAD',-9)
    end if
  end if
  !
  !     * check if detailed chemical information is used or
  !     * kappa approach for internally mixed aerosol.
  !
  if (kint > 0) then
    icnt=0
    do kx=1,kint
      if (abs(aint%tp(kx)%kappa-yna) > ytiny)   icnt=icnt+1
    end do
    if (icnt==kint) iinkap=1
    icnt=0
    do kx=1,kint
      if (abs(aint%tp(kx)%nuio-yna) > ytiny)    icnt=icnt+1
    end do
    if ( (icnt==kint .and. iinkap==1) &
        .or. (icnt /= kint .and. iinkap /= 1) ) then
      call xit('SDREAD',-9)
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * allocate internally used parameter arrays.
  !
  if (kext > 0) then
    allocate(aextf%tp(kext))
    do kx=1,kext
      allocate(aextf%tp(kx)%psim(aext%tp(kioext(kx))%isec))
    end do
  end if
  if (kint > 0) then
    allocate(aintf%tp(kint))
    allocate(aintf%psim(aint%isec))
  end if
  !
  !-----------------------------------------------------------------------
  !     * copy results for externally mixed aerosols into corresponding
  !     * intenrally used arrays.
  !
  if (kext > 0) then
    do kx=1,kext
      aextf%tp(kx)%name=aext%tp(kioext(kx))%name
      ist=aext%tp(kioext(kx))%isec
      aextf%tp(kx)%isec=ist
      do is=1,ist
        aextf%tp(kx)%psim(is)=aext%tp(kioext(kx))%psim(is)
      end do
      aextf%tp(kx)%dpst =aext%tp(kioext(kx))%dpst
      aextf%tp(kx)%radb =aext%tp(kioext(kx))%radb
      aextf%tp(kx)%dens =aext%tp(kioext(kx))%dens
      aextf%tp(kx)%molw =aext%tp(kioext(kx))%molw
      aextf%tp(kx)%nuio =aext%tp(kioext(kx))%nuio
      aextf%tp(kx)%kappa=aext%tp(kioext(kx))%kappa
      aextf%tp(kx)%stick=aext%tp(kioext(kx))%stick
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * copy results for internally mixed aerosols into corresponding
  !     * intenrally used arrays.
  !
  if (kint > 0) then
    ist=aint%isec
    aintf%isec=ist
    do is=1,ist
      aintf%psim(is)=aint%psim(is)
    end do
    aintf%dpst=aint%dpst
    aintf%radb=aint%radb
    aintf%stick=aint%stick
    do kx=1,kint
      aintf%tp(kx)%name=aint%tp(kioint(kx))%name
      iscbt=aint%tp(kioint(kx))%iscb
      iscet=aint%tp(kioint(kx))%isce
      if (iscbt >= 1 .and. iscet <= aint%isec) then
        aintf%tp(kx)%iscb=iscbt
        aintf%tp(kx)%isce=iscet
      else
        call xit('SDREAD',-8)
      end if
      aintf%tp(kx)%dens =aint%tp(kioint(kx))%dens
      aintf%tp(kx)%molw =aint%tp(kioint(kx))%molw
      aintf%tp(kx)%nuio =aint%tp(kioint(kx))%nuio
      aintf%tp(kx)%kappa=aint%tp(kioint(kx))%kappa
    end do
  end if
  !
  return
  1001 call xit('SDREAD',-1)
  !
end subroutine sdread
