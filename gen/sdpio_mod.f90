!> \file
!> \brief Arrays for model input/output of basic aerosol properties.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module sdpio
  !
  implicit none
  !
  integer, parameter :: ismax = 16 ! maximum number of sections
  integer, parameter :: kmax  = 16 ! maximum number of aerosol types
  type texttio
    integer :: isec !<
    real :: radb !<
    real :: dens !<
    real :: molw !<
    real :: nuio !<
    real :: dpst !<
    real :: kappa !<
    real :: stick !<
    character(len=20) :: name !<
    real, dimension(ismax) :: psim !<
  end type texttio
  type textio
    type(texttio), dimension(kmax) :: tp
  end type textio
  type(textio) :: aext
  type tinttio
    integer :: iscb !<
    integer :: isce !<
    real :: dens !<
    real :: molw !<
    real :: nuio !<
    real :: kappa !<
    character(len=20) :: name !<
  end type tinttio
  type tintio
    integer :: isec !<
    real :: radb !<
    real :: dpst !<
    real :: stick !<
    real, dimension(ismax) :: psim !<
    type(tinttio), dimension(kmax) :: tp
  end type tintio
  type(tintio) :: aint
  !
end module sdpio
