!> \file
!> \brief Time-averaged tendencies for aerosol uptake of gas-phase species.
!>       Instantaneously calculated tendencies are averaged and saved.
!>       Results are subsequently scaled to allow updates to PAM tracers
!>       on a per-time-step basis.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine avgtndg(scf,dgdt,sgpr,pgvb,pgcb,pgmb,gcnc,dt, &
                         kount,ilga,leva)
  !
  use sdparm, only : ysmall,yna,ytiny
  use compar, only : icfrq,iavgprd,iupdatp,nrmfld
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  integer, intent(in) :: kount !< Time step number
  real, intent(inout), dimension(ilga,leva) :: scf !< Scaling factor
  real, intent(inout), dimension(ilga,leva) :: pgvb !< PLA internal array
  real, intent(inout), dimension(ilga,leva,nrmfld) :: pgmb !< PLA internal array
  integer, intent(inout), dimension(ilga,leva,nrmfld) :: pgcb !< PLA internal array
  real, intent(inout), dimension(ilga,leva) :: dgdt !< Gas-phase tendency \f$[kg/kg/sec]\f$
  real, intent(in), dimension(ilga,leva) :: sgpr !< Gas-phase production rate \f$[kg/kg/sec]\f$
  real, intent(in), dimension(ilga,leva) :: gcnc !< Gas-phase concentration
  !
  !     internal work variables
  !
  real, dimension(ilga,leva) :: pgvm !<
  real, dimension(ilga,leva,nrmfld) :: pgmm !<
  integer, dimension(ilga,leva,nrmfld) :: pgcm !<
  integer :: nrmfldl !<
  integer :: ind !<
  integer :: ioff !<
  !
  !-----------------------------------------------------------------------
  !
  if (mod(kount,icfrq) /= 0) then
    dgdt=yna
  end if
  !
  !     * retrieve mean and accumulated result and counter from
  !     * previous time step.
  !
  pgvm=pgvb
  pgcm=pgcb
  pgmm=pgmb
  !
  !     * loop over all fields involved in calculation of running
  !     * mean results.
  !
  nrmfldl=ceiling(real(iavgprd)/real(iupdatp))
  do ind=1,nrmfldl
    ioff=(kount+iavgprd-1)-(ind-1)*iupdatp
    if (ioff >= 1 .and. mod(ioff,iavgprd)==0) then
      !
      !         * running mean tendencies.
      !
      where (pgcm(:,:,ind) > 0)
        pgvm(:,:)=pgmm(:,:,ind)/real(pgcm(:,:,ind))
      else where
        pgvm(:,:)=yna
      end where
      !
      !         * reset accumulated result and counter.
      !
      pgmm(:,:,ind)=0.
      pgcm(:,:,ind)=0
    end if
    !
    !       * accumulate instantaneous result and advance counter.
    !
    where (abs(dgdt(:,:)-yna) > ytiny)
      pgmm(:,:,ind)=pgmm(:,:,ind)+(dgdt(:,:)-sgpr(:,:))
      pgcm(:,:,ind)=pgcm(:,:,ind)+1
    end where
  end do
  !
  !     * save mean and accumulated results and counter.
  !
  pgvb=pgvm
  pgcb=pgcm
  pgmb=pgmm
  !
  !     * restore tendencies to mean values.
  !
  dgdt=pgvm+sgpr
  !
  !     * scale aerosol uptake tendencies to avoid negative
  !     * concentrations, if necessary.
  !
  where (dgdt(:,:) < -ysmall)
    scf(:,:)=min(scf(:,:),max(0., &
                             -(gcnc(:,:)+sgpr(:,:)*dt)/(dt*pgvm(:,:))))
  end where
  !
end subroutine avgtndg
