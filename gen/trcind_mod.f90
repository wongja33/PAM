!> \file
!> \brief Indices for gas-phase tracers
!!
!! @author S. Hanna
!
!-----------------------------------------------------------------------
module trcind
  !
  implicit none
  !
  integer :: iso2 !<
  integer :: ihpo !<
  integer :: igs6 !<
  integer :: igsp !<
  integer :: idms !<
  !
end module trcind
