!> \file
!> \brief Writes out summary of results from PLA initialization.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdsum
  !
  use sdparm, only : aextf,aintf,isaext,isaint,iswett,iswext,iswint, &
                         kext,kint,ntracs,r0,sextf,sintf,ylarge
  !
  implicit none
  !
  real :: vmin !<
  real :: vmax !<
  real :: val !<
  integer :: ind !<
  integer :: isect !<
  integer :: ir !<
  integer :: is !<
  integer :: ist !<
  integer :: kx !<
  character(len=17) :: ctmp17 !<
  character(len=3) :: ctmp2 !<
  !
  !-----------------------------------------------------------------------
  !
  write(6,'(1X)')
  write(6,'(A22)')    '*** Aerosol Properties'
  write(6,'(1X)')
  write(6,'(A36,I3)') 'Total number of types of aerosols : ', kext+kint
  write(6,'(A36,I3)') '                 Externally mixed : ', kext
  write(6,'(A36,I3)') '                 Internally mixed : ', kint
  if (kext > 0) then
    write(6,'(1X)')
    write(6,'(A34)')  'Externally mixed types of aerosols'
    do kx=1,kext
      write(6,'(1X)')
      write(6,'(2X,A70)') aextf%tp(kx)%name
      write(6,'(2X,A28,I3)')             'Number of sections        : ',aextf%tp(kx)%isec
      write(6,'(2X,A28,F6.3,A3,F6.3)')   'Radius range (micron)     : ',aextf%tp(kx)%radb/r0, ' - ', aextf%tp(kx)%rade/r0
      write(6,'(2X,A28,F10.8,A4,F10.8)') 'Width of sections         : ',aextf%tp(kx)%dpst,   ' -> ', aextf%tp(kx)%dpstar
      write(6,'(4X,A43)') 'Section    PLA distribution width parameter'
      do is=1,aextf%tp(kx)%isec
        vmin=ylarge
        vmax=-ylarge
        do ir=1,aextf%tp(kx)%idrb
          val=aextf%tp(kx)%psib(is,ir)%vl
          if (aextf%tp(kx)%flg(is,ir) ) then
            vmin=min(vmin,val)
            vmax=max(vmax,val)
          end if
        end do
        write(6,'(4X,I3,7X,F5.2,A4,F6.2,A3,F6.2)') is,aextf%tp(kx)%psim(is),' -> ',vmin,' - ',vmax
      end do
      write(6,'(2X,A28,F12.2)') 'Density (kg/m3)           : ', aextf%tp(kx)%dens
      write(6,'(2X,A28,F12.2)') 'Molecular weight (g/mol)  : ', aextf%tp(kx)%molw*1.e+03
      write(6,'(2X,A28,F12.2)') 'Number of ions            : ', aextf%tp(kx)%nuio
      write(6,'(2X,A28,F12.2)') 'Kappa                     : ', aextf%tp(kx)%kappa
      write(6,'(2X,A28,F12.2)') 'Sticking probability      : ', aextf%tp(kx)%stick
    end do
  end if
  if (kint > 0) then
    write(6,'(1X)')
    write(6,'(A34)') 'Internally mixed types of aerosols'
    write(6,'(1X)')
    write(6,'(2X,A28,I3)')              'Number of sections        : ',aintf%isec
    write(6,'(2X,A28,F6.3,A3,F6.3)')    'Radius range (micron)     : ',aintf%radb/r0,' - ' ,aintf%rade/r0
    write(6,'(2X,A28,F10.8,A4,F10.8)')  'Width of sections         : ',aintf%dpst,' -> ', aintf%dpstar
    write(6,'(2X,A28,F7.2)')            'Sticking probability      : ',aintf%stick
    write(6,'(4X,A43)') 'Section    PLA distribution width parameter'
    do is=1,aintf%isec
      vmin=ylarge
      vmax=-ylarge
      do ir=1,aintf%idrb
        val=aintf%psib(is,ir)%vl
        if (aintf%flg(is,ir) ) then
          vmin=min(vmin,val)
          vmax=max(vmax,val)
        end if
      end do
      write(6,'(4X,I3,7X,F5.2,A4,F6.2,A3,F6.2)') is,aintf%psim(is),' -> ',vmin,' - ',vmax
    end do
    do kx=1,kint
      write(6,'(1X)')
      write(6,'(2X,A70)') aintf%tp(kx)%name
      isect=aintf%tp(kx)%isce-aintf%tp(kx)%iscb+1
      if (isect /= aintf%isec) then
        write(6,'(2X,A28,I2)')           'Number of sections        : ', isect
        write(6,'(2X,A28,F6.3,A3,F6.3)') 'Radius range (micron)     : ', aintf%dryr(aintf%tp(kx)%iscb)%vl/r0, ' - ', &
                                                                             aintf%dryr(aintf%tp(kx)%isce)%vr/r0
      end if
      write(6,'(2X,A28,F12.2)') 'Density (kg/m3)           : ',aintf%tp(kx)%dens
      write(6,'(2X,A28,F12.2)') 'Molecular weight (g/mol)  : ',aintf%tp(kx)%molw*1.e+03
      write(6,'(2X,A28,F12.2)') 'Number of ions            : ',aintf%tp(kx)%nuio
      write(6,'(2X,A28,F12.2)') 'Kappa                     : ',aintf%tp(kx)%kappa
    end do
  end if
  write(6,'(1X)')
  write(6,'(A19)')    '*** Aerosol Tracers'
  write(6,'(1X)')
  write(6,'(A34,I3)') 'Total number of aerosol tracers : ',ntracs
  write(6,'(1X)')
  write(6,'(A26)') 'Aerosol mass concentration'
  if (kext > 0) then
    write(6,'(A36)')    '  Externally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,isaext
      ctmp17=aextf%tp(sextf%isaer(is)%ityp)%name
      ind=sextf%isaer(is)%ism
      if (ind < 10) then
        write(ctmp2,'(A2,I1)') 'A0',ind
      else
        write(ctmp2,'(A1,I2)') 'A',ind
      end if
      write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sextf%isaer(is)%isi
    end do
  end if
  if (kint > 0) then
    write(6,'(A36)')    '  Internally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        ctmp17=aintf%tp(sintf%isaer(is)%ityp(ist))%name
        ind=sintf%isaer(is)%ism(ist)
        if (ind < 10) then
          write(ctmp2,'(A2,I1)') 'A0',ind
        else
          write(ctmp2,'(A1,I2)') 'A',ind
        end if
        write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sintf%isaer(is)%isi
      end do
    end do
  end if
  write(6,'(A28)')      'Aerosol number concentration'
  if (kext > 0) then
    write(6,'(A36)')    '  Externally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,isaext
      ctmp17=aextf%tp(sextf%isaer(is)%ityp)%name
      ind=sextf%isaer(is)%isn
      if (ind < 10) then
        write(ctmp2,'(A2,I1)') 'A0',ind
      else
        write(ctmp2,'(A1,I2)') 'A',ind
      end if
      write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sextf%isaer(is)%isi
    end do
  end if
  if (kint > 0) then
    write(6,'(A36)')    '  Internally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,isaint
      do ist=1,sintf%isaer(is)%itypt
        ctmp17=aintf%tp(sintf%isaer(is)%ityp(ist))%name
        ind=sintf%isaer(is)%isn
        if (ind < 10) then
          write(ctmp2,'(A2,I1)') 'A0',ind
        else
          write(ctmp2,'(A1,I2)') 'A',ind
        end if
        write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sintf%isaer(is)%isi
      end do
    end do
  end if
  write(6,'(1X)')
  write(6,'(A36,I3)')   'Number of soluble aerosol tracers : ',iswett
  write(6,'(1X)')
  write(6,'(A26)')      'Aerosol mass concentration'
  if (kext > 0) then
    write(6,'(A36)')    '  Externally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,iswext
      ctmp17=aextf%tp(sextf%iswet(is)%ityp)%name
      ind=sextf%iswet(is)%ism
      if (ind < 10) then
        write(ctmp2,'(A2,I1)') 'A0',ind
      else
        write(ctmp2,'(A1,I2)') 'A',ind
      end if
      write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sextf%iswet(is)%isi
    end do
  end if
  if (kint > 0) then
    write(6,'(A36)')    '  Internally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,iswint
      do ist=1,sintf%iswet(is)%itypt
        ctmp17=aintf%tp(sintf%iswet(is)%ityp(ist))%name
        ind=sintf%iswet(is)%ism(ist)
        if (ind < 10) then
          write(ctmp2,'(A2,I1)') 'A0',ind
        else
          write(ctmp2,'(A1,I2)') 'A',ind
        end if
        write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sintf%iswet(is)%isi
      end do
    end do
  end if
  write(6,'(A28)')      'Aerosol number concentration'
  if (kext > 0) then
    write(6,'(A36)')    '  Externally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,iswext
      ctmp17=aextf%tp(sextf%iswet(is)%ityp)%name
      ind=sextf%iswet(is)%isn
      if (ind < 10) then
        write(ctmp2,'(A2,I1)') 'A0',ind
      else
        write(ctmp2,'(A1,I2)') 'A',ind
      end if
      write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sextf%iswet(is)%isi
    end do
  end if
  if (kint > 0) then
    write(6,'(A36)')    '  Internally mixed types of aerosols'
    write(6,'(2X,A50)') 'Aerosol Index      Aerosol Type      Section Index'
    do is=1,iswint
      do ist=1,sintf%iswet(is)%itypt
        ctmp17=aintf%tp(sintf%iswet(is)%ityp(ist))%name
        ind=sintf%iswet(is)%isn
        if (ind < 10) then
          write(ctmp2,'(A2,I1)') 'A0',ind
        else
          write(ctmp2,'(A1,I2)') 'A',ind
        end if
        write(6,'(4X,A3,14X,A17,3X,I3)') ctmp2,ctmp17,sintf%iswet(is)%isi
      end do
    end do
  end if
  write(6,'(1X)')
  !
end subroutine sdsum
