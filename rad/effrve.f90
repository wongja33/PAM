!>    \file
!>    \brief Radiation parameters for externally mixed types of aerosols.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine effrve(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                        pewetrc,peddn,tre,tve,tload,ilga,leva,iextp, &
                        kexttp,isexttp)
  !
  use sdparm, only : isaext,pedryrc,r0
  !
  implicit none
  real, intent(in),  dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  integer, intent(in), dimension(isexttp) :: iextp !< Aerosol tracer index
  real, intent(out),  dimension(ilga,leva) :: tre !< Effective radius
  real, intent(out),  dimension(ilga,leva) :: tve !< Effective variance
  real, intent(out),  dimension(ilga,leva) :: tload !< Load
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: kexttp !< Number of externally mixed aerosol types
  integer, intent(in) :: isexttp !< Number of size sections for externally mixed aerosol
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: senum !<
  real, allocatable, dimension(:,:,:) :: semas !<
  real, allocatable, dimension(:,:,:) :: sewetrc !<
  real, allocatable, dimension(:,:,:) :: seddn !<
  real, allocatable, dimension(:,:,:) :: sen0 !<
  real, allocatable, dimension(:,:,:) :: sepsi !<
  real, allocatable, dimension(:,:,:) :: sephi0 !<
  real, allocatable, dimension(:,:,:) :: sephis0 !<
  real, allocatable, dimension(:,:,:) :: sedphi0 !<
  real, allocatable, dimension(:) :: sedryrc !<
  integer :: is !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory.
  !
  if (kexttp > 0) then
    allocate (senum  (ilga,leva,isexttp))
    allocate (semas  (ilga,leva,isexttp))
    allocate (sen0   (ilga,leva,isexttp))
    allocate (sepsi  (ilga,leva,isexttp))
    allocate (sephi0 (ilga,leva,isexttp))
    allocate (sephis0(ilga,leva,isexttp))
    allocate (sedphi0(ilga,leva,isexttp))
    allocate (sewetrc(ilga,leva,isexttp))
    allocate (seddn  (ilga,leva,isexttp))
    allocate (sedryrc(isexttp))
  end if
  !
  !     type-specific aerosol size distribution parameters.
  !
  if (kexttp > 0) then
    do is=1,isexttp
      senum  (:,:,is)=penum  (:,:,iextp(is))
      semas  (:,:,is)=pemas  (:,:,iextp(is))
      sen0   (:,:,is)=pen0   (:,:,iextp(is))
      sepsi  (:,:,is)=pepsi  (:,:,iextp(is))
      sephi0 (:,:,is)=pephi0 (:,:,iextp(is))
      sephis0(:,:,is)=pephis0(:,:,iextp(is))
      sedphi0(:,:,is)=pedphi0(:,:,iextp(is))
      sewetrc(:,:,is)=pewetrc(:,:,iextp(is))
      seddn  (:,:,is)=peddn  (:,:,iextp(is))
      sedryrc(is) = pedryrc(iextp(is))
    end do
  end if
  !
  !     effective radius and variance for wet aerosol.
  !
  if (kexttp > 0) then
    call effrv(senum,semas,sen0,sephi0,sepsi,sephis0,sedphi0, &
                   sewetrc,sedryrc,seddn,tload,tre,tve,ilga,leva, &
                   isexttp)
  end if
  !
  !     * deallocate memory.
  !
  if (kexttp > 0) then
    deallocate (senum)
    deallocate (semas)
    deallocate (sen0)
    deallocate (sepsi)
    deallocate (sephi0)
    deallocate (sephis0)
    deallocate (sedphi0)
    deallocate (sewetrc)
    deallocate (sedryrc)
    deallocate (seddn)
  end if
  !
end subroutine effrve
