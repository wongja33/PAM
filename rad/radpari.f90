!>    \file
!>    \brief Radiation parameters (load, effective radius, effective variance)
!>           for internally mixed particles
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine radpari(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
                         piwetrc,piddn,pire,pive,piload, &
                         pifrc,fr1,fr2,ilga,leva)
  !
  use sdparm, only : isaint,kint,kintbc,kintso4,pidryrc,r0,ytiny
  !
  implicit none
  !
  real, intent(in),  dimension(ilga,leva,isaint) :: pinum !< Aerosol number concentration for internally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaint) :: piwetrc !< Total/wet paricle radius \f$[m]\f$, int. mixture
  real, intent(in),  dimension(ilga,leva,isaint) :: piddn !< (Dry) density for internally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in),  dimension(ilga,leva,isaint) :: pin0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) int. mixture
  real, intent(in),  dimension(ilga,leva,isaint) :: pipsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) int. mixture
  real, intent(in),  dimension(ilga,leva,isaint) :: piphi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), int. mixture
  real, intent(in),  dimension(ilga,leva,isaint) :: piphis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for internally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: pidphi0 !< Dry particle radius in the centres of the size
  !< sections for internally mixed aerosol \f$[m]\f$
  real, intent(in),  dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(out),  dimension(ilga,leva) :: pire !< Effective radius
  real, intent(out),  dimension(ilga,leva) :: pive !< Effective variance
  real, intent(out),  dimension(ilga,leva) :: piload !< Load
  real, intent(out),  dimension(ilga,leva) :: fr1 !< Mass fraction 1
  real, intent(out),  dimension(ilga,leva) :: fr2 !< Mass fraction 2
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real,  dimension(ilga,leva) :: tre !<
  real,  dimension(ilga,leva) :: tve !<
  real,  dimension(ilga,leva) :: tload !<
  real,  dimension(ilga,leva) :: atmp1 !<
  real,  dimension(ilga,leva) :: atmp2 !<
  real,  dimension(ilga,leva) :: atmp3 !<
  integer :: is !<
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  if (kint > 0) then
    fr1=1.
    fr2=0.
    piload=0.
    pire=0.
    pive=0.
  end if
  !
  !     internally mixed aerosol.
  !
  if (kint > 0) then
    call effrv(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
                   piwetrc,pidryrc,piddn,tload,tre,tve,ilga,leva, &
                   isaint)
    piload=tload
    pire=tre
    pive=tve
    !
    !       mass fractions for ammonium sulphate and black carbon.
    !
    atmp1=0.
    atmp2=0.
    atmp3=0.
    do is=1,isaint
      atmp1=atmp1+pimas(:,:,is)
    end do
    if (kintso4 > 0) then
      do is=1,isaint
        atmp2=atmp2+pimas(:,:,is)*pifrc(:,:,is,kintso4)
      end do
    end if
    if (kintbc > 0) then
      do is=1,isaint
        atmp3=atmp3+pimas(:,:,is)*pifrc(:,:,is,kintbc)
      end do
    end if
    where (atmp1 > ytiny)
      fr1=max(atmp2/atmp1,0.)
      fr2=max(atmp3/atmp1,0.)
    end where
    atmp1=fr1+fr2
    where (atmp1 > .9999)
      atmp2=1./atmp1
      fr1=fr1*atmp2
      fr2=fr2*atmp2
    end where
  end if
  !
end subroutine radpari
