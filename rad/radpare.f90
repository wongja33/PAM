!>    \file
!>    \brief Radiation parameters (load, effective radius, effective variance)
!>           for externally mixed particles.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine radpare(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                         pewetrc,peddn,pere,peve,peload, &
                         ilga,leva)
  !
  use sdparm, only : iexbc,iexmd,iexoc,iexss, &
                         isaext,isextbc,isextmd,isextoc,isextss, &
                         kext,kextbc,kextmd,kextoc,kextss, &
                         pedryrc,r0
  !
  implicit none
  real, intent(in),  dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed
  !< aerosol \f$[1/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pewetrc !< Total/wet paricle radius \f$[m]\f$, ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: peddn !< (Dry) density for externally mixed aerosol particles
  !< \f$[kg/m^3]\f$
  real, intent(in),  dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,
  !< amplitude) ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$,
  !< mode size) ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,
  !< width), ext. mixture
  real, intent(in),  dimension(ilga,leva,isaext) :: pephis0 !< Dry particle size ln(Rp /R0) at the boundaries of
  !< the size sections for externally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaext) :: pedphi0 !< Dry particle radius in the centres of the size
  !< sections for externally mixed aerosol \f$[m]\f$
  real, intent(out),  dimension(ilga,leva,kext) :: pere !< Effective radius
  real, intent(out),  dimension(ilga,leva,kext) :: peve !< Effective variance
  real, intent(out),  dimension(ilga,leva,kext) :: peload !< Load
  real,  dimension(ilga,leva,isaext) :: pewetrt !< Total/wet paricle radius \f$[m]\f$
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real,  dimension(ilga,leva) :: tre !< Effective radius
  real,  dimension(ilga,leva) :: tve !< Effective variance
  real,  dimension(ilga,leva) :: tload !< Load
  !
  !     internal work variables
  !
  integer :: is !<
  integer :: isx !<
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  if (kext > 0) then
    peload=0.
    pere=0.
    peve=0.
  end if
  !
  !     externally mixed sea salt aerosol.
  !
  if (kextss > 0) then
    call effrve(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                    pewetrc,peddn,tre,tve,tload,ilga,leva,iexss, &
                    kextss,isextss)
    peload(:,:,kextss)=tload
    pere(:,:,kextss)=tre
    peve(:,:,kextss)=tve
  end if
  !
  !     externally mixed mineral dust aerosol. only consider
  !     dry size distribution (for consistency with radiation).
  !
  if (kextmd > 0) then
    do is=1,isextmd
      isx=iexmd(is)
      pewetrt(:,:,isx)=pedryrc(isx)
    end do
    call effrve(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                    pewetrt,peddn,tre,tve,tload,ilga,leva,iexmd, &
                    kextmd,isextmd)
    peload(:,:,kextmd)=tload
    pere(:,:,kextmd)=tre
    peve(:,:,kextmd)=tve
  end if
  !
  !     externally mixed black carbon aerosol.
  !
  if (kextbc > 0) then
    do is=1,isextbc
      isx=iexbc(is)
      pewetrt(:,:,isx)=pedryrc(isx)
    end do
    call effrve(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                    pewetrt,peddn,tre,tve,tload,ilga,leva,iexbc, &
                    kextbc,isextbc)
    peload(:,:,kextbc)=tload
    pere(:,:,kextbc)=tre
    peve(:,:,kextbc)=tve
  end if
  !
  !     externally mixed organic carbon aerosol.
  !
  if (kextoc > 0) then
    do is=1,isextoc
      isx=iexoc(is)
      pewetrt(:,:,isx)=pedryrc(isx)
    end do
    call effrve(penum,pemas,pen0,pephi0,pepsi,pephis0,pedphi0, &
                    pewetrt,peddn,tre,tve,tload,ilga,leva,iexoc, &
                    kextoc,isextoc)
    peload(:,:,kextoc)=tload
    pere(:,:,kextoc)=tre
    peve(:,:,kextoc)=tve
  end if
  !
end subroutine radpare
