!>    \file
!>    \brief Given a monotonic vector V of length N and a value X,
!>           return the index MVIDX such that X is between
!>           V(MVIDX) and V(MVIDX+1).
!!
!!    @author L. Solheim
!
!-----------------------------------------------------------------------
integer function mvidx(v,n,x)
  !
  implicit none
  !
  real :: x !< Single real :: value
  real, dimension(n) :: v !< Monitonic vector (increasing or decreasing)
  integer :: n !< Size of V
  integer :: jl !<
  integer :: jm !<
  integer :: ju !<
  !
  !-----------------------------------------------------------------------
  !
  if (x==v(1)) then
    mvidx=1
    return
  end if
  if (x==v(n)) then
    mvidx=n-1
    return
  end if
  jl=1
  ju=n
10 if (ju-jl>1) then
    jm=(ju+jl)/2
    if ((v(n)>v(1)).eqv.(x>v(jm))) then
      jl=jm
    else
      ju=jm
    end if
    goto 10
  end if
  mvidx=jl
  return
end
!
!> \file
!! \subsection ssec_details Details
!! V must be monotonic, either increasing of decreasing.
!! There is no check on whether or not this vector is
!! monotonic.
!! This function returns 1 or N-1 if X is out of range.
!! \n
!! \n
!! Output:
!! \n
!!  \f$ V(MVIDX) \leq X \leq V(MVIDX+1)\f$
