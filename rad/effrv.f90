!>    \file
!>    \brief Calculation of effective radius and variance using moments.
!>
!!    @authors K. von Salzen and X.Ma
!
!-----------------------------------------------------------------------
subroutine effrv(anum,amas,pn0,phi0,psi,phis0,dphi0, &
                       wetrc,dryrc,drydn,ald,re,ve,ilga,leva,isec)
  !
  use sdparm, only : dnh2o,r0,ycnst,ytiny
  use sdcode, only : sdint, sdint0
  implicit none
  !
  real, intent(out), dimension(ilga,leva) :: ald !< Load
  real, intent(out), dimension(ilga,leva) :: re !< Effective radius
  real, intent(out), dimension(ilga,leva) :: ve !< Effective variance
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  real, intent(in), dimension(ilga,leva,isec) :: anum !< Number
  real, intent(in), dimension(ilga,leva,isec) :: amas !< Mass
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !< Dry particle size ln(Rp /R0) at the boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !< Section width
  real, intent(in), dimension(isec) :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: phi0w !<
  real, allocatable, dimension(:,:,:) :: phis0w !<
  real, allocatable, dimension(:,:,:) :: wetdn !<
  real, allocatable, dimension(:,:,:) :: am0 !<
  real, allocatable, dimension(:,:,:) :: am1 !<
  real, allocatable, dimension(:,:,:) :: am2 !<
  real, allocatable, dimension(:,:,:) :: am3 !<
  real, allocatable, dimension(:,:,:) :: am4 !<
  real, allocatable, dimension(:,:) :: tam0 !<
  real, allocatable, dimension(:,:) :: tam2 !<
  real, allocatable, dimension(:,:) :: tam3 !<
  real, allocatable, dimension(:,:) :: tam4 !<
  integer :: il !<
  integer :: is !<
  integer :: l !<
  real :: fgr !<
  real :: alfgr !<
  real :: fgr3i !<
  real :: rmom !<
  !
  !---------------------------------------------------------------------
  !     * allocate memory.
  !
  allocate(phi0w (ilga,leva,isec))
  allocate(phis0w(ilga,leva,isec))
  allocate(wetdn (ilga,leva,isec))
  allocate(am0  (ilga,leva,isec))
  allocate(am1  (ilga,leva,isec))
  allocate(am2  (ilga,leva,isec))
  allocate(am3  (ilga,leva,isec))
  allocate(am4  (ilga,leva,isec))
  allocate(tam0 (ilga,leva))
  allocate(tam2 (ilga,leva))
  allocate(tam3 (ilga,leva))
  allocate(tam4 (ilga,leva))
  !
  !     hygroscopic growth effects on radius and particle density.
  !
  phi0w =phi0
  phis0w=phis0
  wetdn=drydn
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny .and. amas(il,l,is) > ytiny) then
          fgr=wetrc(il,l,is)/dryrc(is)
          alfgr=log(fgr)
          phi0w (il,l,is)=phi0w (il,l,is)+alfgr
          phis0w(il,l,is)=phis0w(il,l,is)+alfgr
          fgr3i=1./fgr**3
          wetdn(il,l,is)=dnh2o*(1.-fgr3i)+drydn(il,l,is)*fgr3i
        end if
      end do
    end do
  end do
  !
  !     moments of the size distribution.
  !
  am0=pn0*sdint0(phi0w,psi,phis0w,dphi0,ilga,leva,isec)
  rmom=1.
  am1=pn0*sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=2.
  am2=pn0*sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=3.
  am3=pn0*sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  rmom=4.
  am4=pn0*sdint (phi0w,psi,rmom,phis0w,dphi0,ilga,leva,isec)
  !
  !     integrate moments over size distribution.
  !
  tam0=0.
  tam2=0.
  tam3=0.
  tam4=0.
  ald=0.
  do is=1,isec
    tam0=tam0+am0(:,:,is)
    tam2=tam2+am2(:,:,is)
    tam3=tam3+am3(:,:,is)
    tam4=tam4+am4(:,:,is)
    ald=ald+ycnst*wetdn(:,:,is)*am3(:,:,is)
  end do
  !
  !     effective radius and variance.
  !
  where (tam0 > 100. )
    where (tam3 > ytiny .and. tam2 > ytiny)
      re=tam3/tam2
    end where
    where (tam2 > ytiny .and. tam3**2 > ytiny .and. tam4 > ytiny)
      ve=tam2*tam4/tam3**2-1.
    end where
  else where
    re=0.
    ve=0.
    ald=0.
  end where
  !
  !     * deallocate memory.
  !
  deallocate(phis0w)
  deallocate(phi0w)
  deallocate(wetdn)
  deallocate(am0)
  deallocate(am1)
  deallocate(am2)
  deallocate(am3)
  deallocate(am4)
  deallocate(tam0)
  deallocate(tam2)
  deallocate(tam3)
  deallocate(tam4)
  !
end subroutine effrv
