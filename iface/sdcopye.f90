!>    \file
!>    \brief Copy size distribution information from input into corresponding PLA arrays.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
!
subroutine sdcopye(penum,pemas,pen0,pephi0,pepsi, &
                         indh,indv,nsampl,ns0,ilga,leva)
  !
  use sdi,    only : cext,sdimod
  use sdparm, only : ina,isaext,sextf,yna
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva,isaext) :: penum !< Aerosol number concentration for externally mixed aerosol \f$[1/kg]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pemas !< Aerosol (dry) mass concentration for externally mixed aerosol \f$[kg/kg]\f$
  real, intent(out), dimension(ilga,leva,isaext) :: pen0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$,amplitude) ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pephi0 !< 3rd pla size distribution parameter (\f$\phi_{0,i}\f$,width), ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pepsi !< 2nd pla size distribution parameter (\f$\psi_{i}\f$, mode size) ext. mixture
  integer, intent(in), dimension(ns0:nsampl) :: indh !<
  integer, intent(in), dimension(ns0:nsampl) :: indv !<
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: nsampl !< Number of points in the file
  integer, intent(in) :: ns0 !< First index to be selected
  !
  !     internal work variables
  !
  integer :: isi !<
  
  integer :: il !<
  integer :: is !<
  integer :: l !<
  integer :: nx !<
  integer :: kx !<
  
  !
  !-----------------------------------------------------------------------
  !     copy results for externally mixed aerosol into corresponding
  !     pla arrays.
  !
  if (isaext > 0) then
    pen0  =yna
    pephi0=yna
    pepsi =yna
    penum =yna
    pemas =yna
  end if
  do nx=ns0,nsampl
    il=indh(nx)
    l =indv(nx)
    if (l /= ina) then
      if (sdimod == 1) then
        if (isaext > 0) then
          do isi=1,isaext
            kx=sextf%isaer(isi)%ityp
            is=sextf%isaer(isi)%isi
            pen0  (il,l,isi)=cext(nx)%tp(kx)%pn0  (is)
            pephi0(il,l,isi)=cext(nx)%tp(kx)%pphi0(is)
            pepsi (il,l,isi)=cext(nx)%tp(kx)%ppsi (is)
          end do
        end if
      else if (sdimod == 2) then
        if (isaext > 0) then
          do isi=1,isaext
            kx=sextf%isaer(isi)%ityp
            is=sextf%isaer(isi)%isi
            penum(il,l,isi)=cext(nx)%tp(kx)%num(is)
            pemas(il,l,isi)=cext(nx)%tp(kx)%mas(is)
          end do
        end if
      end if
    else
      if (sdimod == 1) then
        if (isaext > 0) then
          do isi=1,isaext
            kx=sextf%isaer(isi)%ityp
            is=sextf%isaer(isi)%isi
            pen0  (il,:,isi)=cext(nx)%tp(kx)%pn0  (is)
            pephi0(il,:,isi)=cext(nx)%tp(kx)%pphi0(is)
            pepsi (il,:,isi)=cext(nx)%tp(kx)%ppsi (is)
          end do
        end if
      else if (sdimod == 2) then
        if (isaext > 0) then
          do isi=1,isaext
            kx=sextf%isaer(isi)%ityp
            is=sextf%isaer(isi)%isi
            penum(il,:,isi)=cext(nx)%tp(kx)%num(is)
            pemas(il,:,isi)=cext(nx)%tp(kx)%mas(is)
          end do
        end if
      end if
    end if
  end do
  !
end subroutine sdcopye
