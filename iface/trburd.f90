!> \file
!> \brief Aerosol mass burden
!!
!! @author Knut von Salzen
!
!-----------------------------------------------------------------------
subroutine trburd (voabrd,vbcbrd,vasbrd,vmdbrd,vssbrd,xrow,iaind, &
                         dshj,pressg,grav,ntrac,ntracp,il1,il2,ilg,ilev, &
                         ilevp1,msg)
  !
  use sdparm, only : iexbc,iexmd,iexoc,iexso4,iexss, &
                         iinbc,iinmd,iinoc,iinso4,iinss, &
                         isextbc,isextmd,isextoc,isextso4,isextss, &
                         isintbc,isintmd,isintoc,isintso4,isintss, &
                         kintbc,kintmd,kintoc,kintso4,kintss,sextf,sintf
  !
  implicit none
  !
  integer, intent(in), dimension(ntracp) :: iaind !< Aerosol tracer index array
  real, intent(in), dimension(ilg) :: pressg !< Surface pressure
  real, intent(in), dimension(ilg,ilev) :: dshj !< Vertical depth of the grid cells (hybrid variable)
  real, intent(in), dimension(ilg,ilevp1,ntrac) :: xrow !< Grid-cell mean tracer mass mixing ratio \f$[kg/kg]\f$
  real, intent(out), dimension(ilg) :: voabrd !< Load of organic aerosol mass \f$[kg/m^2]\f$
  real, intent(out), dimension(ilg) :: vbcbrd !< Load of black carbon aerosol mass \f$[kg/m^2]\f$
  real, intent(out), dimension(ilg) :: vasbrd !< Load of ammonium sulphate aerosol mass \f$[kg/m^2]\f$
  real, intent(out), dimension(ilg) :: vmdbrd !< Load of mineral dust aerosol mass \f$[kg/m^2]\f$
  real, intent(out), dimension(ilg) :: vssbrd !< Load of seai salt aerosol mass \f$[kg/m^2]\f$
  real, intent(in) :: grav !<  Gravtiational acceleration \f$[m/s^2]\f$
  integer, intent(in) :: il1 !< Start index of first horizontal grid cell
  integer, intent(in) :: il2 !< Index of last horizontal grid cell
  integer, intent(in) :: ilg !< Horizontal (lat/lon) grid cell index
  integer, intent(in) :: ilev !< Number of vertical grid cells
  integer, intent(in) :: ilevp1 !< Number of vertical grid cells
  integer, intent(in) :: msg !< Array dimension for PLA aerosol calculations
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: ntracp !< Number of aerosol tracers
  !
  !     internal work variables
  !
  integer :: is !<
  integer :: il !<
  integer :: l !<
  integer :: ioff !<
  integer :: isx !<
  integer :: ist !<
  integer :: kx !<
  integer :: imlex !<
  integer :: imlin !<
  !
  !     * initialization.
  !
  ioff=ilevp1-ilev
  voabrd=0.
  vbcbrd=0.
  vasbrd=0.
  vmdbrd=0.
  vssbrd=0.
  !
  !     organic carbon burden.
  !
  if (isextoc > 0) then
    do isx=1,isextoc
      is=iexoc(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          voabrd(il)=voabrd(il) &
                      +xrow(il,l+ioff,imlex)*pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintoc > 0) then
    do isx=1,isintoc
      is=iinoc(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintoc) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              voabrd(il)=voabrd(il) &
                      +xrow(il,l+ioff,imlin)*pressg(il)*dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     black carbon burden.
  !
  if (isextbc > 0) then
    do isx=1,isextbc
      is=iexbc(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          vbcbrd(il)=vbcbrd(il) &
                      +xrow(il,l+ioff,imlex)*pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintbc > 0) then
    do isx=1,isintbc
      is=iinbc(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintbc) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              vbcbrd(il)=vbcbrd(il) &
                      +xrow(il,l+ioff,imlin)*pressg(il)*dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     sulphate burden.
  !
  if (isextso4 > 0) then
    do isx=1,isextso4
      is=iexso4(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          vasbrd(il)=vasbrd(il) &
                      +xrow(il,l+ioff,imlex)*pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintso4 > 0) then
    do isx=1,isintso4
      is=iinso4(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintso4) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              vasbrd(il)=vasbrd(il) &
                      +xrow(il,l+ioff,imlin)*pressg(il)*dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     mineral dust burden.
  !
  if (isextmd > 0) then
    do isx=1,isextmd
      is=iexmd(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          vmdbrd(il)=vmdbrd(il) &
                      +xrow(il,l+ioff,imlex)*pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintmd > 0) then
    do isx=1,isintmd
      is=iinmd(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintmd) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              vmdbrd(il)=vmdbrd(il) &
                      +xrow(il,l+ioff,imlin)*pressg(il)*dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
  !     sea salt burden.
  !
  if (isextss > 0) then
    do isx=1,isextss
      is=iexss(isx)
      imlex=iaind(sextf%isaer(is)%ism)
      do il=il1,il2
        do l=msg+1,ilev
          vssbrd(il)=vssbrd(il) &
                      +xrow(il,l+ioff,imlex)*pressg(il)*dshj(il,l)/grav
        end do
      end do
    end do
  end if
  if (isintss > 0) then
    do isx=1,isintss
      is=iinss(isx)
      do ist=1,sintf%isaer(is)%itypt
        kx=sintf%isaer(is)%ityp(ist)
        if (kx == kintss) then
          imlin=iaind(sintf%isaer(is)%ism(ist))
          do il=il1,il2
            do l=msg+1,ilev
              vssbrd(il)=vssbrd(il) &
                      +xrow(il,l+ioff,imlin)*pressg(il)*dshj(il,l)/grav
            end do
          end do
        end if
      end do
    end do
  end if
  !
end subroutine trburd
