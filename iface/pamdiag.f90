!>    \file
!>    \brief PAM diagnostic calcuations
!!
!!    @authors K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pamdiag(xrow,iaind,ilg,ilevp1,ntrac,il1,il2,ntracp, &
                         ilev,dp,throw,rhc,ph,cldt,rmu, &
                         ssldrow,ressrow,vessrow,dsldrow,redsrow, &
                         vedsrow,bcldrow,rebcrow,vebcrow,ocldrow, &
                         reocrow,veocrow,amldrow,reamrow,veamrow, &
                         fr1row,fr2row,sulifrc,rsuifrc,vsuifrc, &
                         f1sufrc,f2sufrc,bclifrc,rbcifrc,vbcifrc, &
                         f1bcfrc,f2bcfrc,oclifrc,rocifrc,vocifrc, &
                         f1ocfrc,f2ocfrc,sdvlrow,sdnurow,sdmarow, &
                         sdacrow,sdcorow,sssnrow,smdnrow,sianrow, &
                         sssmrow,smdmrow,sewmrow,ssumrow,socmrow, &
                         sbcmrow,siwmrow,tnssrow,tnmdrow,tniarow, &
                         tmssrow,tmmdrow,tmocrow,tmbcrow,tmsprow, &
                         snssrow,snmdrow,sniarow,smssrow,smmdrow, &
                         smocrow,smbcrow,smsprow,siwhrow,sewhrow, &
                         pm25row,pm10row,dm25row,dm10row,saverad,irfrc)
  !
  use sdparm, only : ntracs,ntract,kint,kext
  use compar, only : isdiag,isdnum,kxtra1,kxtra2
  !
  implicit none
  !
  !-----------------------------------------------------------------------
  !     * input variables.
  !
  integer, intent(in), dimension(ntracp) :: iaind !< Aerosol tracer index array
  real, intent(in), dimension(ilg,ilevp1,ntrac) :: xrow !< Grid-cell mean tracer mass mixing ratio \f$[kg/kg]\$
  real, intent(in), dimension(ilg,ilevp1) :: throw !< Grid-cell mean temperature of air \f$[K]\f$
  real, intent(in), dimension(ilg,ilev) :: dp !< Depth of grid cells \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: rhc !< Relative humidity for clear sky portion of grid cell
  real, intent(in), dimension(ilg,ilev) :: ph !< Pressure of grid cell centres \f$[Pa]\f$
  real, intent(in), dimension(ilg) :: cldt !< Total cloud fraction
  real, intent(in), dimension(ilg) :: rmu !< Solar zenith angle
  real, intent(in) :: saverad !< Weighting factor for time integration of PLA diagnostic results over 1/SAVERAD number of model time steps
  integer, intent(in) :: ilg !< Horizontal (lat/lon) grid cell inde
  integer, intent(in) :: il1 !< Start index of first horizontal grid cell
  integer, intent(in) :: il2 !< Index of last horizontal grid cellx
  integer, intent(in) :: ilev !< Number of vertical grid cells
  integer, intent(in) :: ilevp1 !< Number of vertical grid cells
  integer, intent(in) :: ntrac !< Number of tracers
  integer, intent(in) :: ntracp !< Number of aerosol tracers
  integer, intent(in) :: irfrc !< Flag for calculation of radiative forcing
  !
  !     * output variables.
  !
  real, intent(out), dimension(ilg,ilev) :: ssldrow !< Sea salt aerosol mass loading for each layer
  real, intent(out), dimension(ilg,ilev) :: vessrow !< Sea salt effective variance
  real, intent(out), dimension(ilg,ilev) :: ressrow !< Sea salt effective radius
  real, intent(out), dimension(ilg,ilev) :: dsldrow !< Dust mass loading for each layer
  real, intent(out), dimension(ilg,ilev) :: vedsrow !< Dust effective variance
  real, intent(out), dimension(ilg,ilev) :: redsrow !< Dust effective radius
  real, intent(out), dimension(ilg,ilev) :: bcldrow !< Black carbon mass loading for each layer
  real, intent(out), dimension(ilg,ilev) :: vebcrow !< Black carbon effective variance
  real, intent(out), dimension(ilg,ilev) :: rebcrow !< Black carbon effective radius
  real, intent(out), dimension(ilg,ilev) :: ocldrow !< Organic carbon mass loading for each layer
  real, intent(out), dimension(ilg,ilev) :: veocrow !< Organic carbon effective variance
  real, intent(out), dimension(ilg,ilev) :: reocrow !< Organic carbon effective radius
  real, intent(out), dimension(ilg,ilev) :: amldrow !< Sulfate mass loading for each layer
  real, intent(out), dimension(ilg,ilev) :: veamrow !< Sulfate effective variance
  real, intent(out), dimension(ilg,ilev) :: reamrow !< Sulfate effective radius
  real, intent(out), dimension(ilg,ilev) :: fr1row !< Used for calculations of radiation in the driving model
  real, intent(out), dimension(ilg,ilev) :: fr2row !< Used for calculations of radiation in the driving model
  real, intent(out), dimension(ilg,ilev) :: sulifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: rsuifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: vsuifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f1sufrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f2sufrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: bclifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: rbcifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: vbcifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f1bcfrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f2bcfrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: oclifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: rocifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: vocifrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f1ocfrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,ilev) :: f2ocfrc !< Used for diagnosis of radiative forcings
  real, intent(out), dimension(ilg,isdiag) :: sdvlrow !< Aerosol volume size distribution
  real, intent(out), dimension(ilg,isdnum) :: snssrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: snmdrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: sniarow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: smssrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: smmdrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: smocrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: smbcrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: smsprow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: siwhrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,isdnum) :: sewhrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sdnurow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sdmarow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sdacrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sdcorow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sssnrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: smdnrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sianrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sssmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: smdmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sewmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: ssumrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: socmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: sbcmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,isdnum) :: siwmrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tnssrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tnmdrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tniarow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tmssrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tmmdrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tmocrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tmbcrow !< Extra diagnostic output
  real, intent(out), dimension(ilg) :: tmsprow !< Extra diagnostic output
  real, intent(out), dimension(ilg,ilev,kint+kext) :: pm25row !< PM2.5
  real, intent(out), dimension(ilg,ilev,kint+kext) :: pm10row !< PM10
  real, intent(out), dimension(ilg,ilev,kint+kext) :: dm25row !< PM2.5 dry
  real, intent(out), dimension(ilg,ilev,kint+kext) :: dm10row !< PM10 dry
  !
  !     * diagnostic input variables.
  !
  real, allocatable, dimension(:,:,:) :: trac !< Tracer array
  real, allocatable, dimension(:,:) :: dpa !< Difference in air pressure between bottom and top of grid cell \f$[Pa]\f$
  real, allocatable, dimension(:,:) :: ta !< Air temperature \f$[K]\f$
  real, allocatable, dimension(:,:) :: rha !< Relative humidity \f$[dimensionless]\f$
  real, allocatable, dimension(:,:) :: pha !< Pressure of grid cell centres \f$[Pa]\f$
  real, allocatable, dimension(:) :: clda !< Cloud fraction
  real, allocatable, dimension(:) :: rmua !< Solar zenith angle
  !
  !     * diagnostic output variables.
  !
  real, allocatable, dimension(:,:) :: ssld !<
  real, allocatable, dimension(:,:) :: ress !<
  real, allocatable, dimension(:,:) :: vess !<
  real, allocatable, dimension(:,:) :: mdld !<
  real, allocatable, dimension(:,:) :: remd !<
  real, allocatable, dimension(:,:) :: vemd !<
  real, allocatable, dimension(:,:) :: bcld !<
  real, allocatable, dimension(:,:) :: rebc !<
  real, allocatable, dimension(:,:) :: vebc !<
  real, allocatable, dimension(:,:) :: ocld !<
  real, allocatable, dimension(:,:) :: reoc !<
  real, allocatable, dimension(:,:) :: veoc !<
  real, allocatable, dimension(:,:) :: amld !<
  real, allocatable, dimension(:,:) :: ream !<
  real, allocatable, dimension(:,:) :: veam !<
  real, allocatable, dimension(:,:) :: fr1 !<
  real, allocatable, dimension(:,:) :: fr2 !<
  real, allocatable, dimension(:,:) :: so4ldi !<
  real, allocatable, dimension(:,:) :: reso4i !<
  real, allocatable, dimension(:,:) :: veso4i !<
  real, allocatable, dimension(:,:) :: fr1so4i !<
  real, allocatable, dimension(:,:) :: fr2so4i !<
  real, allocatable, dimension(:,:) :: bcldi !<
  real, allocatable, dimension(:,:) :: rebci !<
  real, allocatable, dimension(:,:) :: vebci !<
  real, allocatable, dimension(:,:) :: fr1bci !<
  real, allocatable, dimension(:,:) :: fr2bci !<
  real, allocatable, dimension(:,:) :: ocldi !<
  real, allocatable, dimension(:,:) :: reoci !<
  real, allocatable, dimension(:,:) :: veoci !<
  real, allocatable, dimension(:,:) :: fr1oci !<
  real, allocatable, dimension(:,:) :: fr2oci !<
  real, allocatable, dimension(:,:) :: sdvl !<
  real, allocatable, dimension(:) :: tnss !<
  real, allocatable, dimension(:) :: tnmd !<
  real, allocatable, dimension(:) :: tnia !<
  real, allocatable, dimension(:) :: tmss !<
  real, allocatable, dimension(:) :: tmmd !<
  real, allocatable, dimension(:) :: tmoc !<
  real, allocatable, dimension(:) :: tmbc !<
  real, allocatable, dimension(:) :: tmsp !<
  real, allocatable, dimension(:,:) :: snss !<
  real, allocatable, dimension(:,:) :: snmd !<
  real, allocatable, dimension(:,:) :: snia !<
  real, allocatable, dimension(:,:) :: smss !<
  real, allocatable, dimension(:,:) :: smmd !<
  real, allocatable, dimension(:,:) :: smoc !<
  real, allocatable, dimension(:,:) :: smbc !<
  real, allocatable, dimension(:,:) :: smsp !<
  real, allocatable, dimension(:,:) :: siwh !<
  real, allocatable, dimension(:,:) :: sewh !<
  real, allocatable, dimension(:,:,:) :: sdnu !<
  real, allocatable, dimension(:,:,:) :: sdma !<
  real, allocatable, dimension(:,:,:) :: sdac !<
  real, allocatable, dimension(:,:,:) :: sdco !<
  real, allocatable, dimension(:,:,:) :: sssn !<
  real, allocatable, dimension(:,:,:) :: smdn !<
  real, allocatable, dimension(:,:,:) :: sian !<
  real, allocatable, dimension(:,:,:) :: sssm !<
  real, allocatable, dimension(:,:,:) :: smdm !<
  real, allocatable, dimension(:,:,:) :: sewm !<
  real, allocatable, dimension(:,:,:) :: ssum !<
  real, allocatable, dimension(:,:,:) :: socm !<
  real, allocatable, dimension(:,:,:) :: sbcm !<
  real, allocatable, dimension(:,:,:) :: siwm !<
  real, allocatable, dimension(:,:,:) :: pm2p5 !<
  real, allocatable, dimension(:,:,:) :: pm10 !<
  real, allocatable, dimension(:,:,:) :: pm2p5d !<
  real, allocatable, dimension(:,:,:) :: pm10d !<
  !
  !     internal work variables
  !
  integer :: ilga !< Number of grid points in the horizontal direction
  integer :: leva !< Number of grid points in the vertical direction
  integer :: msgp1a !<
  integer :: msgp2a !<
  integer :: n !<
  integer :: ns !<
  !
  !-----------------------------------------------------------------------
  !     * calculations for full vertical domain.
  !
  leva=ilev
  if (leva > ilev) call xit('PAMDIAG',-1)
  ilga=il2-il1+1
  !
  !     * allocate memory.
  !
  allocate(trac(ilga,leva,ntract))
  allocate(ta  (ilga,leva))
  allocate(rha (ilga,leva))
  allocate(dpa (ilga,leva))
  allocate(pha (ilga,leva))
  allocate(ssld(ilga,leva))
  allocate(ress(ilga,leva))
  allocate(vess(ilga,leva))
  allocate(mdld(ilga,leva))
  allocate(remd(ilga,leva))
  allocate(vemd(ilga,leva))
  allocate(bcld(ilga,leva))
  allocate(rebc(ilga,leva))
  allocate(vebc(ilga,leva))
  allocate(ocld(ilga,leva))
  allocate(reoc(ilga,leva))
  allocate(veoc(ilga,leva))
  allocate(amld(ilga,leva))
  allocate(ream(ilga,leva))
  allocate(veam(ilga,leva))
  allocate(fr1 (ilga,leva))
  allocate(fr2 (ilga,leva))
  allocate(so4ldi(ilga,leva))
  allocate(reso4i(ilga,leva))
  allocate(veso4i(ilga,leva))
  allocate(fr1so4i(ilga,leva))
  allocate(fr2so4i(ilga,leva))
  allocate(bcldi(ilga,leva))
  allocate(rebci(ilga,leva))
  allocate(vebci(ilga,leva))
  allocate(fr1bci(ilga,leva))
  allocate(fr2bci(ilga,leva))
  allocate(ocldi(ilga,leva))
  allocate(reoci(ilga,leva))
  allocate(veoci(ilga,leva))
  allocate(fr1oci(ilga,leva))
  allocate(fr2oci(ilga,leva))
  allocate(sdvl(ilga,isdiag))
  allocate(tnss(ilga))
  allocate(tnmd(ilga))
  allocate(tnia(ilga))
  allocate(tmss(ilga))
  allocate(tmmd(ilga))
  allocate(tmoc(ilga))
  allocate(tmbc(ilga))
  allocate(tmsp(ilga))
  allocate(pm2p5(ilga,leva,kext+kint))
  allocate(pm10(ilga,leva,kext+kint))
  allocate(pm2p5d(ilga,leva,kext+kint))
  allocate(pm10d(ilga,leva,kext+kint))
  allocate(snss(ilga,isdnum))
  allocate(snmd(ilga,isdnum))
  allocate(snia(ilga,isdnum))
  allocate(smss(ilga,isdnum))
  allocate(smmd(ilga,isdnum))
  allocate(smoc(ilga,isdnum))
  allocate(smbc(ilga,isdnum))
  allocate(smsp(ilga,isdnum))
  allocate(siwh(ilga,isdnum))
  allocate(sewh(ilga,isdnum))
  allocate(sdnu(ilga,leva,isdnum))
  allocate(sdma(ilga,leva,isdnum))
  allocate(sdac(ilga,leva,isdnum))
  allocate(sdco(ilga,leva,isdnum))
  allocate(sssn(ilga,leva,isdnum))
  allocate(smdn(ilga,leva,isdnum))
  allocate(sian(ilga,leva,isdnum))
  allocate(sssm(ilga,leva,isdnum))
  allocate(smdm(ilga,leva,isdnum))
  allocate(sewm(ilga,leva,isdnum))
  allocate(ssum(ilga,leva,isdnum))
  allocate(socm(ilga,leva,isdnum))
  allocate(sbcm(ilga,leva,isdnum))
  allocate(siwm(ilga,leva,isdnum))
  allocate(clda(ilga))
  allocate(rmua(ilga))
  !
  !     copy aerosol tracer fields.
  !
  msgp2a=ilevp1-leva+1
  msgp1a=ilev  -leva+1
  do ns=1,ntracs
    n=iaind(ns)
    trac (1:ilga,1:leva,ns)=xrow(il1:il2,msgp2a:ilevp1,n)
  end do
  !
  !     temperature and relative humidity.
  !
  ta  =throw(il1:il2,msgp2a:ilevp1)
  rha =rhc  (il1:il2,msgp1a:ilev)
  dpa =dp   (il1:il2,msgp1a:ilev)
  pha =ph   (il1:il2,msgp1a:ilev)
  clda=cldt (il1:il2)
  rmua=rmu  (il1:il2)
  !
  !     diagnosis of radiation and other parameters based
  !     on size distributions predicted by pam.
  !
  call pamd(trac,ta,rha,pha,ssld,ress,vess,mdld,remd,vemd, &
                bcld,rebc,vebc,ocld,reoc,veoc,amld,ream,veam, &
                fr1,fr2,so4ldi,reso4i,veso4i,fr1so4i,fr2so4i, &
                bcldi,rebci,vebci,fr1bci,fr2bci,ocldi, &
                reoci,veoci,fr1oci,fr2oci,pm2p5,pm10,pm2p5d,pm10d, &
                dpa,clda,rmua,tnss,tnmd,tnia,tmss,tmmd,tmoc, &
                tmbc,tmsp,snss,snmd,snia,smss,smmd,smoc,smbc, &
                smsp,siwh,sewh,sdnu,sdma,sdac,sdco,sssn,smdn, &
                sian,sssm,smdm,sewm,ssum,socm,sbcm,siwm,sdvl, &
                leva,ilga,irfrc)
  !
  !     results that are used for calculations of radiation
  !     in the driving model.
  !
  ssldrow(il1:il2,msgp1a:ilev)=ssld
  ressrow(il1:il2,msgp1a:ilev)=ress*1.e+06
  vessrow(il1:il2,msgp1a:ilev)=vess
  dsldrow(il1:il2,msgp1a:ilev)=mdld
  redsrow(il1:il2,msgp1a:ilev)=remd*1.e+06
  vedsrow(il1:il2,msgp1a:ilev)=vemd
  bcldrow(il1:il2,msgp1a:ilev)=bcld
  rebcrow(il1:il2,msgp1a:ilev)=rebc*1.e+06
  vebcrow(il1:il2,msgp1a:ilev)=vebc
  ocldrow(il1:il2,msgp1a:ilev)=ocld
  reocrow(il1:il2,msgp1a:ilev)=reoc*1.e+06
  veocrow(il1:il2,msgp1a:ilev)=veoc
  amldrow(il1:il2,msgp1a:ilev)=amld
  reamrow(il1:il2,msgp1a:ilev)=ream*1.e+06
  veamrow(il1:il2,msgp1a:ilev)=veam
  fr1row (il1:il2,msgp1a:ilev)=fr1
  fr2row (il1:il2,msgp1a:ilev)=fr2
  !
  !     results that are used for diagnosis of radiative forcings.
  !
  sulifrc(il1:il2,msgp1a:ilev)=so4ldi
  rsuifrc(il1:il2,msgp1a:ilev)=reso4i*1.e+06
  vsuifrc(il1:il2,msgp1a:ilev)=veso4i
  f1sufrc(il1:il2,msgp1a:ilev)=fr1so4i
  f2sufrc(il1:il2,msgp1a:ilev)=fr2so4i
  bclifrc(il1:il2,msgp1a:ilev)=bcldi
  rbcifrc(il1:il2,msgp1a:ilev)=rebci*1.e+06
  vbcifrc(il1:il2,msgp1a:ilev)=vebci
  f1bcfrc(il1:il2,msgp1a:ilev)=fr1bci
  f2bcfrc(il1:il2,msgp1a:ilev)=fr2bci
  oclifrc(il1:il2,msgp1a:ilev)=ocldi
  rocifrc(il1:il2,msgp1a:ilev)=reoci*1.e+06
  vocifrc(il1:il2,msgp1a:ilev)=veoci
  f1ocfrc(il1:il2,msgp1a:ilev)=fr1oci
  f2ocfrc(il1:il2,msgp1a:ilev)=fr2oci
  !
  !     extra diagnostic output.
  !
  if (kxtra1) then
    pm25row(il1:il2,msgp1a:ilev,1:kint+kext)=pm25row(il1:il2,msgp1a:ilev,1:kint+kext) &
                                    +pm2p5*saverad
    pm10row(il1:il2,msgp1a:ilev,1:kint+kext)=pm10row(il1:il2,msgp1a:ilev,1:kint+kext) &
                                    +pm10*saverad
    dm25row(il1:il2,msgp1a:ilev,1:kint+kext)=dm25row(il1:il2,msgp1a:ilev,1:kint+kext) &
                                    +pm2p5d*saverad
    dm10row(il1:il2,msgp1a:ilev,1:kint+kext)=dm10row(il1:il2,msgp1a:ilev,1:kint+kext) &
                                    +pm10d*saverad
  end if
  if (kxtra2) then
    sdvlrow(il1:il2,:)=sdvl(:,:)
    if (isdnum > 1) then
      snssrow(il1:il2,:)=snssrow(il1:il2,:)+snss*saverad
      snmdrow(il1:il2,:)=snmdrow(il1:il2,:)+snmd*saverad
      sniarow(il1:il2,:)=sniarow(il1:il2,:)+snia*saverad
      smssrow(il1:il2,:)=smssrow(il1:il2,:)+smss*saverad
      smmdrow(il1:il2,:)=smmdrow(il1:il2,:)+smmd*saverad
      smocrow(il1:il2,:)=smocrow(il1:il2,:)+smoc*saverad
      smbcrow(il1:il2,:)=smbcrow(il1:il2,:)+smbc*saverad
      smsprow(il1:il2,:)=smsprow(il1:il2,:)+smsp*saverad
      siwhrow(il1:il2,:)=siwhrow(il1:il2,:)+siwh*saverad
      sewhrow(il1:il2,:)=sewhrow(il1:il2,:)+sewh*saverad
      sdnurow(il1:il2,msgp1a:ilev,:)=sdnurow(il1:il2,msgp1a:ilev,:) &
                                        +sdnu*saverad
      sdmarow(il1:il2,msgp1a:ilev,:)=sdmarow(il1:il2,msgp1a:ilev,:) &
                                        +sdma*saverad
      sdacrow(il1:il2,msgp1a:ilev,:)=sdacrow(il1:il2,msgp1a:ilev,:) &
                                        +sdac*saverad
      sdcorow(il1:il2,msgp1a:ilev,:)=sdcorow(il1:il2,msgp1a:ilev,:) &
                                        +sdco*saverad
      sssnrow(il1:il2,msgp1a:ilev,:)=sssnrow(il1:il2,msgp1a:ilev,:) &
                                        +sssn*saverad
      smdnrow(il1:il2,msgp1a:ilev,:)=smdnrow(il1:il2,msgp1a:ilev,:) &
                                        +smdn*saverad
      sianrow(il1:il2,msgp1a:ilev,:)=sianrow(il1:il2,msgp1a:ilev,:) &
                                        +sian*saverad
      sssmrow(il1:il2,msgp1a:ilev,:)=sssmrow(il1:il2,msgp1a:ilev,:) &
                                        +sssm*saverad
      smdmrow(il1:il2,msgp1a:ilev,:)=smdmrow(il1:il2,msgp1a:ilev,:) &
                                        +smdm*saverad
      sewmrow(il1:il2,msgp1a:ilev,:)=sewmrow(il1:il2,msgp1a:ilev,:) &
                                        +sewm*saverad
      ssumrow(il1:il2,msgp1a:ilev,:)=ssumrow(il1:il2,msgp1a:ilev,:) &
                                        +ssum*saverad
      socmrow(il1:il2,msgp1a:ilev,:)=socmrow(il1:il2,msgp1a:ilev,:) &
                                        +socm*saverad
      sbcmrow(il1:il2,msgp1a:ilev,:)=sbcmrow(il1:il2,msgp1a:ilev,:) &
                                        +sbcm*saverad
      siwmrow(il1:il2,msgp1a:ilev,:)=siwmrow(il1:il2,msgp1a:ilev,:) &
                                        +siwm*saverad
      tnssrow(il1:il2)=tnssrow(il1:il2)+tnss*saverad
      tnmdrow(il1:il2)=tnmdrow(il1:il2)+tnmd*saverad
      tniarow(il1:il2)=tniarow(il1:il2)+tnia*saverad
      tmssrow(il1:il2)=tmssrow(il1:il2)+tmss*saverad
      tmmdrow(il1:il2)=tmmdrow(il1:il2)+tmmd*saverad
      tmocrow(il1:il2)=tmocrow(il1:il2)+tmoc*saverad
      tmbcrow(il1:il2)=tmbcrow(il1:il2)+tmbc*saverad
      tmsprow(il1:il2)=tmsprow(il1:il2)+tmsp*saverad
    end if
  end if
  !
  !     the same fields as in the previous section but for the
  !     region that is outside of the pam domain.
  !
  if (leva < ilev) then
    ssldrow(il1:il2,1:msgp1a-1)=0.
    ressrow(il1:il2,1:msgp1a-1)=0.
    vessrow(il1:il2,1:msgp1a-1)=0.
    dsldrow(il1:il2,1:msgp1a-1)=0.
    redsrow(il1:il2,1:msgp1a-1)=0.
    vedsrow(il1:il2,1:msgp1a-1)=0.
    bcldrow(il1:il2,1:msgp1a-1)=0.
    rebcrow(il1:il2,1:msgp1a-1)=0.
    vebcrow(il1:il2,1:msgp1a-1)=0.
    ocldrow(il1:il2,1:msgp1a-1)=0.
    reocrow(il1:il2,1:msgp1a-1)=0.
    veocrow(il1:il2,1:msgp1a-1)=0.
    amldrow(il1:il2,1:msgp1a-1)=0.
    reamrow(il1:il2,1:msgp1a-1)=0.
    veamrow(il1:il2,1:msgp1a-1)=0.
    fr1row (il1:il2,1:msgp1a-1)=0.
    fr2row (il1:il2,1:msgp1a-1)=0.
    sulifrc(il1:il2,1:msgp1a-1)=0.
    rsuifrc(il1:il2,1:msgp1a-1)=0.
    vsuifrc(il1:il2,1:msgp1a-1)=0.
    f1sufrc(il1:il2,1:msgp1a-1)=0.
    f2sufrc(il1:il2,1:msgp1a-1)=0.
    bclifrc(il1:il2,1:msgp1a-1)=0.
    rbcifrc(il1:il2,1:msgp1a-1)=0.
    vbcifrc(il1:il2,1:msgp1a-1)=0.
    f1bcfrc(il1:il2,1:msgp1a-1)=0.
    f2bcfrc(il1:il2,1:msgp1a-1)=0.
    oclifrc(il1:il2,1:msgp1a-1)=0.
    rocifrc(il1:il2,1:msgp1a-1)=0.
    vocifrc(il1:il2,1:msgp1a-1)=0.
    f1ocfrc(il1:il2,1:msgp1a-1)=0.
    f2ocfrc(il1:il2,1:msgp1a-1)=0.
    sdnurow(il1:il2,1:msgp1a-1,:)=0.
    sdmarow(il1:il2,1:msgp1a-1,:)=0.
    sdacrow(il1:il2,1:msgp1a-1,:)=0.
    sdcorow(il1:il2,1:msgp1a-1,:)=0.
    sssnrow(il1:il2,1:msgp1a-1,:)=0.
    smdnrow(il1:il2,1:msgp1a-1,:)=0.
    sianrow(il1:il2,1:msgp1a-1,:)=0.
    sssmrow(il1:il2,1:msgp1a-1,:)=0.
    smdmrow(il1:il2,1:msgp1a-1,:)=0.
    sewmrow(il1:il2,1:msgp1a-1,:)=0.
    ssumrow(il1:il2,1:msgp1a-1,:)=0.
    socmrow(il1:il2,1:msgp1a-1,:)=0.
    sbcmrow(il1:il2,1:msgp1a-1,:)=0.
    siwmrow(il1:il2,1:msgp1a-1,:)=0.
    pm25row(il1:il2,1:msgp1a-1,1:kint+kext)=0.
    pm10row(il1:il2,1:msgp1a-1,1:kint+kext)=0.
    dm25row(il1:il2,1:msgp1a-1,1:kint+kext)=0.
    dm10row(il1:il2,1:msgp1a-1,1:kint+kext)=0.
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate memory.
  !
  deallocate(trac)
  deallocate(ta)
  deallocate(rha)
  deallocate(dpa)
  deallocate(pha)
  deallocate(ssld)
  deallocate(ress)
  deallocate(vess)
  deallocate(mdld)
  deallocate(remd)
  deallocate(vemd)
  deallocate(bcld)
  deallocate(rebc)
  deallocate(vebc)
  deallocate(ocld)
  deallocate(reoc)
  deallocate(veoc)
  deallocate(amld)
  deallocate(ream)
  deallocate(veam)
  deallocate(fr1)
  deallocate(fr2)
  deallocate(so4ldi)
  deallocate(reso4i)
  deallocate(veso4i)
  deallocate(fr1so4i)
  deallocate(fr2so4i)
  deallocate(bcldi)
  deallocate(rebci)
  deallocate(vebci)
  deallocate(fr1bci)
  deallocate(fr2bci)
  deallocate(ocldi)
  deallocate(reoci)
  deallocate(veoci)
  deallocate(fr1oci)
  deallocate(fr2oci)
  deallocate(sdvl)
  deallocate(tnss)
  deallocate(tnmd)
  deallocate(tnia)
  deallocate(tmss)
  deallocate(tmmd)
  deallocate(tmoc)
  deallocate(tmbc)
  deallocate(tmsp)
  deallocate(pm2p5)
  deallocate(pm10)
  deallocate(pm2p5d)
  deallocate(pm10d)
  deallocate(snss)
  deallocate(snmd)
  deallocate(snia)
  deallocate(smss)
  deallocate(smmd)
  deallocate(smoc)
  deallocate(smbc)
  deallocate(smsp)
  deallocate(siwh)
  deallocate(sewh)
  deallocate(sdnu)
  deallocate(sdma)
  deallocate(sdac)
  deallocate(sdco)
  deallocate(sssn)
  deallocate(smdn)
  deallocate(sian)
  deallocate(sssm)
  deallocate(smdm)
  deallocate(sewm)
  deallocate(ssum)
  deallocate(socm)
  deallocate(sbcm)
  deallocate(siwm)
  deallocate(clda)
  deallocate(rmua)
  !
end subroutine pamdiag
