!>    \file
!>    \brief Arrays for model input/output of aerosol size distribution
!>           properties (i.e. shape and concentration).
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------

module sdi
  !
  implicit none
  !
  integer, parameter :: ismax=20 !< Maximum number of sections
  integer, parameter :: kmax=4 !< Maximum number of aerosol types
  integer, parameter :: nmax=100000 !< Maximum number of separate input size distributions
  real, parameter :: ynai=-9999. !<
  !
  type texttio
    integer :: isec !<
    real, dimension(ismax) :: pn0 !<
    real, dimension(ismax) :: pphi0 !<
    real, dimension(ismax) :: ppsi !<
    real, dimension(ismax) :: num !<
    real, dimension(ismax) :: mas !<
  end type texttio
  type textio
    type(texttio), dimension(kmax) :: tp
  end type textio
  type(textio), dimension(nmax) :: bext,cext
  type tinttio
    real, dimension(ismax) :: conc !<
    real, dimension(ismax) :: mas !<
  end type tinttio
  type tintio
    integer :: isec !<
    real, dimension(ismax) :: pn0 !<
    real, dimension(ismax) :: pphi0 !<
    real, dimension(ismax) :: ppsi !<
    real, dimension(ismax) :: num !<
    type(tinttio), dimension(kmax) :: tp
  end type tintio
  type(tintio), dimension(nmax) :: bint,cint
  integer :: sdimod !<
  !
end module sdi
