!>    \file
!>    \brief Input and saving of initial aerosol size disitribution information.
!!
!!    @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine sdinit(nsampl,ismod)
  !
  use sdi,    only : bext,bint,cext,cint,ismax,kmax,nmax,sdimod,ynai
  use sdparm, only : aextf,aintf,ina,isaext,isaint,kext,kint,ytiny
  !
  implicit none
  !
  integer, intent(inout) :: nsampl !< Number of points in the file
  integer, intent(inout) :: ismod !<
  integer :: is !<
  integer :: kx !<
  integer :: nx !<
  integer :: icnte !<
  integer :: icnti1 !<
  integer :: icnti2 !<
  integer :: icnti2r !<
  namelist /init/ bext,bint,sdimod
  !
  !-----------------------------------------------------------------------
  !     check if arrays are sufficiently large. increase array
  !     dimensions in module sdi if this error occurs.
  !
  if (kmax < kext .or. kmax < kint) then
    call xit ('SDINIT',-1)
  end if
  if (ismax < aintf%isec) then
    call xit ('SDINIT',-2)
  end if
  do kx=1,kext
    if (ismax < aextf%tp(kx)%isec) then
      call xit ('SDINIT',-3)
    end if
  end do
  !-----------------------------------------------------------------------
  !     initialize work and io arrays and allocate memory.
  !
  sdimod=ina
  do nx=1,nmax
    do is=1,ismax
      do kx=1,kmax
        bext(nx)%tp(kx)%pn0(is)=ynai
        bext(nx)%tp(kx)%pphi0(is)=ynai
        bext(nx)%tp(kx)%ppsi(is)=ynai
        bext(nx)%tp(kx)%num(is)=ynai
        bext(nx)%tp(kx)%mas(is)=ynai
        bint(nx)%tp(kx)%conc(is)=ynai
        bint(nx)%tp(kx)%mas(is)=ynai
      end do
      bint(nx)%pn0(is)=ynai
      bint(nx)%pphi0(is)=ynai
      bint(nx)%ppsi(is)=ynai
      bint(nx)%num(is)=ynai
    end do
  end do
  do nx=1,nmax
    do is=1,ismax
      do kx=1,kmax
        cext(nx)%tp(kx)%pn0(is)=ynai
        cext(nx)%tp(kx)%pphi0(is)=ynai
        cext(nx)%tp(kx)%ppsi(is)=ynai
        cext(nx)%tp(kx)%num(is)=ynai
        cext(nx)%tp(kx)%mas(is)=ynai
        cint(nx)%tp(kx)%conc(is)=ynai
        cint(nx)%tp(kx)%mas(is)=ynai
      end do
      cint(nx)%pn0(is)=ynai
      cint(nx)%pphi0(is)=ynai
      cint(nx)%ppsi(is)=ynai
      cint(nx)%num(is)=ynai
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     read namelist.
  !
  open(10,file='OUT1')
  read(10,nml=INIT)
  close(10)
  !
  !-----------------------------------------------------------------------
  !     copy input data.
  !
  nsampl=0
  if (sdimod == 1) then
    do nx=1,nmax
      !
      !       check input data if consistent with specification of aerosol
      !       types and size distribution.
      !
      icnte=0
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (abs(bext(nx)%tp(kx)%pn0  (is)-ynai) > ytiny) &
              icnte=icnte+1
          if (abs(bext(nx)%tp(kx)%pphi0(is)-ynai) > ytiny) &
              icnte=icnte+1
          if (abs(bext(nx)%tp(kx)%ppsi (is)-ynai) > ytiny) &
              icnte=icnte+1
        end do
      end do
      icnti1=0
      do is=1,aintf%isec
        if (abs(bint(nx)%pn0  (is)-ynai) > ytiny) &
            icnti1=icnti1+1
        if (abs(bint(nx)%pphi0(is)-ynai) > ytiny) &
            icnti1=icnti1+1
        if (abs(bint(nx)%ppsi (is)-ynai) > ytiny) &
            icnti1=icnti1+1
      end do
      icnti2=0
      icnti2r=0
      do kx=1,kint
        do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
          icnti2r=icnti2r+1
          if (abs(bint(nx)%tp(kx)%conc(is)-ynai) > ytiny) &
              icnti2=icnti2+1
        end do
      end do
      if (icnte == 3*isaext .and. icnti1 == 3*isaint &
          .and. icnti2 == icnti2r) then
        !
        !         reorder input data in order to avoid potential holes in the
        !         input data.
        !
        nsampl=nsampl+1
        do kx=1,kext
          do is=1,aextf%tp(kx)%isec
            cext(nsampl)%tp(kx)%pn0  (is)=bext(nx)%tp(kx)%pn0  (is)
            cext(nsampl)%tp(kx)%pphi0(is)=bext(nx)%tp(kx)%pphi0(is)
            cext(nsampl)%tp(kx)%ppsi (is)=bext(nx)%tp(kx)%ppsi (is)
          end do
        end do
        do is=1,aintf%isec
          cint(nsampl)%pn0  (is)=bint(nx)%pn0  (is)
          cint(nsampl)%pphi0(is)=bint(nx)%pphi0(is)
          cint(nsampl)%ppsi (is)=bint(nx)%ppsi (is)
        end do
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            cint(nsampl)%tp(kx)%conc(is)=bint(nx)%tp(kx)%conc(is)
          end do
        end do
      end if
    end do
  else if (sdimod == 2) then
    do nx=1,nmax
      !
      !       check input data if consistent with specification of aerosol
      !       types and size distribution.
      !
      icnte=0
      do kx=1,kext
        do is=1,aextf%tp(kx)%isec
          if (abs(bext(nx)%tp(kx)%num(is)-ynai) > ytiny) &
              icnte=icnte+1
          if (abs(bext(nx)%tp(kx)%mas(is)-ynai) > ytiny) &
              icnte=icnte+1
        end do
      end do
      icnti1=0
      do is=1,aintf%isec
        if (abs(bint(nx)%num(is)-ynai) > ytiny) icnti1=icnti1+1
      end do
      icnti2=0
      icnti2r=0
      do kx=1,kint
        do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
          icnti2r=icnti2r+1
          if (abs(bint(nx)%tp(kx)%mas(is)-ynai) > ytiny) &
              icnti2=icnti2+1
        end do
      end do
      if (icnte == 2*isaext .and. icnti1 == isaint &
          .and. icnti2 == icnti2r) then
        !
        !         reorder input data in order to avoid potential holes in the
        !         input data.
        !
        nsampl=nsampl+1
        do kx=1,kext
          do is=1,aextf%tp(kx)%isec
            cext(nsampl)%tp(kx)%num(is)=bext(nx)%tp(kx)%num(is)
            cext(nsampl)%tp(kx)%mas(is)=bext(nx)%tp(kx)%mas(is)
          end do
        end do
        do is=1,aintf%isec
          cint(nsampl)%num(is)=bint(nx)%num(is)
        end do
        do kx=1,kint
          do is=aintf%tp(kx)%iscb,aintf%tp(kx)%isce
            cint(nsampl)%tp(kx)%mas(is)=bint(nx)%tp(kx)%mas(is)
          end do
        end do
      end if
    end do
  else
    call xit('SDINIT',-4)
  end if
  !
  !-----------------------------------------------------------------------
  !     check number of aerosol samples.
  !
  if (nsampl==0) then
    call xit('SDINIT',-5)
    write(6,'(A34)') 'NO VALID AEROSOL DATA IN FILE INIT'
  end if
  ismod=sdimod
  !
end subroutine sdinit
