!>    \file
!>    \brief  Driver for PLA aerosol model (PAM).
!!
!!    @authors K. von Salzen and M. Lazare
!
!-----------------------------------------------------------------------
subroutine pamdriv(oednrow,oercrow,oidnrow,oircrow,svvbrow, &
                         psvvrow,svmbrow,svcbrow,pnvbrow,pnmbrow, &
                         pncbrow,psvbrow,psmbrow,pscbrow,qnvbrow, &
                         qnmbrow,qncbrow,qsvbrow,qsmbrow,qscbrow, &
                         qgvbrow,qgmbrow,qgcbrow,qdvbrow,qdmbrow, &
                         qdcbrow,onvbrow,onmbrow,oncbrow,osvbrow, &
                         osmbrow,oscbrow,ogvbrow,ogmbrow,ogcbrow, &
                         devbrow,pdevrow,dembrow,decbrow,divbrow, &
                         pdivrow,dimbrow,dicbrow,revbrow,prevrow, &
                         rembrow,recbrow,rivbrow,privrow,rimbrow, &
                         ricbrow, &
                         xrow,sfrcrol,iaind,ilg,ilevp1,ntrac,msgp2, &
                         il1,il2,iso2,ihpo,igs6,igsp,ntracp, &
                         ilev,ican,msgp1,zh,zf,ph,pf,dp,throw,qrow, &
                         qtn,hmn,rhc,rht,wg,wsub,zmratep,zmlwc,zfsnow, &
                         zfrain,zclf,clrfr,clrfs,zfevap,zfsubl, &
                         pbltrow,smfrac,fcs,fgs,fc,fg, &
                         zspd,zspdso,zspdsbs,cdml,cdmnl,ustarbs, &
                         flnd,focn,sicn,gtrow,fnrol, &
                         fcanrow,spotrow,st02row,st03row,st04row, &
                         st06row,st13row,st14row,st15row,st16row, &
                         st17row,suz0row,bsfrac,pdsfrow,atau,zfs,pfs, &
                         sgpp,ogpp,docem1,docem2,docem3,docem4,dbcem1, &
                         dbcem2,dbcem3,dbcem4,dsuem1,dsuem2,dsuem3, &
                         dsuem4,o3row,hno3row,nh3row,nh4row,timst, &
                         co2_ppm,kount,tmin,zcdnrow,bcicrow,depbrol, &
                         cornrow,cormrow,rsn1row,rsm1row, &
                         vrn1row,vrm1row,rsn2row,rsm2row,vrn2row, &
                         vrm2row,rsn3row,rsm3row,vrn3row,vrm3row, &
                         vncnrow,vasnrow,vscdrow,vgsprow,sncnrow, &
                         ssunrow,scndrow,sgsprow,defxrow,defnrow, &
                         acasrow,acoarow,acbcrow,acssrow,acmdrow, &
                         ntrow,n20row,n50row,n100row,n200row, &
                         wtrow,w20row,w50row,w100row,w200row,ccnrow, &
                         ccnerow,cc02row,cc04row,cc08row,cc16row, &
                         rcrirow,supsrow,voaerow,vbcerow,vaserow, &
                         vmderow,vsserow,voawrow,vbcwrow,vaswrow, &
                         vmdwrow,vsswrow,voadrow,vbcdrow,vasdrow, &
                         vmddrow,vssdrow,voagrow,vbcgrow,vasgrow, &
                         vmdgrow,vssgrow,vasirow,vas1row,vas2row, &
                         vas3row,henrrow,o3frow,h2o2frow,wparrow, &
                         vccnrow,vcnerow,dmacrow,dmcorow,dnacrow, &
                         dncorow,duwdrow,dustrow,duthrow,fallrow, &
                         fa10row,fa2row ,fa1row ,usmkrow,defarow, &
                         defcrow,sicn_crt,saverad,isvdust,leva,mynode &
#if defined(GM_PAM)
                         ,busper, busvol, busper_size, busvol_size &
#endif
                         )
  !
  use sdparm,  only : isaint,jgs6,jgsp,jhpo,jso2,kext,kint,ntracs,ntract,wh2so4,ws
  use compar,  only : iavgprd,isaintt,isdiag,isdust,iupdatp,kextt,kintt,kxtra1,kxtra2,nrmfld
  use duparm1, only :cccmabf,w0
#if defined(GM_PAM)
      use chm_species_info_mod, only: sm
      use chm_species_idx_mod
      use chm_ptopo_grid_mod,   only: chm_ni
#endif
  !
  implicit none
  !
  !-----------------------------------------------------------------------
  !     * input variables.
  !
  integer, intent(in) :: il1 !< start index of first horizontal grid cell
  integer, intent(in) :: il2 !< index of last horizontal grid cell
  integer, intent(in) :: kount !< time step number
  integer, intent(in) :: isvdust !< switch for turning on/off extra diagnostic calculations for mineral dust processes
  integer, intent(in) :: ilg !< horizontal (lat/lon) grid cell index
  integer, intent(in) :: ilevp1 !< number of vertical grid cells
  integer, intent(in) :: ntrac !< number of tracers
  integer, intent(in) :: msgp1 !< currently unused parameter
  integer, intent(in) :: msgp2 !< index of highest vertical grid cell
  integer, intent(in) :: ntracp !< number of aerosol tracers
  integer, intent(in) :: ilev !< number of vertical grid cells
  integer, intent(in) :: leva !< number of grid points in the vertical direction
  integer, intent(in) :: ican !< number of land surface scheme canopy types
  integer, intent(in) :: iso2 !< tracer index for sulphur dioxide gas
  integer, intent(in) :: ihpo !< tracer index for hydrogen peroxide gas
  integer, intent(in) :: igs6 !< tracer index for sulphuric acid gas
  integer, intent(in) :: igsp !< tracer index for secondary organic aerosol precursor gas
  integer, intent(in), dimension(ntracp) :: iaind !< aerosol tracer index array
  real, intent(in) :: timst !< model time step
  real, intent(in) :: co2_ppm !< carbon dioxide volume mixing ratio (ppm)
  real, intent(in) :: sicn_crt !< critical sea ice fraction
  real, intent(in) :: saverad !< weighting factor for time integration of pla diagnostic results over 1/saverad number of model time steps
  real, intent(in), dimension(ntrac) :: tmin !< minimum tracer mass mixing ratio (kg/kg)
  real, intent(in), dimension(ilg,ilevp1) :: throw !< grid-cell mean temperature of air (k)
  real, intent(in), dimension(ilg,ilevp1) :: qrow !< grid-cell mean water vapour mass mixing ratio (kg/kg)
  real, intent(in), dimension(ilg,ilev) :: zh !< height of grid cell centres
  real, intent(in), dimension(ilg,ilev) :: zf !< height of grid cell interfaces (m)
  real, intent(in), dimension(ilg,ilev) :: ph !< pressure of grid cell centres (pa)
  real, intent(in), dimension(ilg,ilev) :: pf !< pressure of grid cell interfaces (pa)
  real, intent(in), dimension(ilg,ilev) :: dp !< depth of grid cells (pa)
  real, intent(in), dimension(ilg,ilev) :: qtn !< total water mass mixing ratio in cloud updraft (kg/kg)
  real, intent(in), dimension(ilg,ilev) :: hmn !< cloud water static energy in cloud updraft (j/kg)
  real, intent(in), dimension(ilg,ilev) :: rhc !< relative humidity for clear sky portion of grid cell
  real, intent(in), dimension(ilg,ilev) :: rht !< grid-cell mean relative humidity
  real, intent(in), dimension(ilg,ilev) :: wg !< grid-cell mean vertical velocity (m/sec)
  real, intent(in), dimension(ilg,ilev) :: wsub !< subgrid-scale component of vertical velocity in cloud updraft (m/sec)
  real, intent(in), dimension(ilg,ilev) :: zmratep !< autoconversion + accretion rate in cloudy portion of grid cell
  real, intent(in), dimension(ilg,ilev) :: zmlwc !< cloud liquid water mass mixing ratio in cloudy portion of grid cell (kg/kg)
  real, intent(in), dimension(ilg,ilev) :: zfsnow !< grid-cell mean snowfall rate (kg/m2/sec), at bottom of each grid cell
  real, intent(in), dimension(ilg,ilev) :: zfrain !< grid-cell mean rainfall rate (kg/m2/sec), at bottom of each grid cell
  real, intent(in), dimension(ilg,ilev) :: zclf !< cloud fraction
  real, intent(in), dimension(ilg,ilev) :: clrfr !< rain fraction (clear-sky part of grid cell that contains rain)
  real, intent(in), dimension(ilg,ilev) :: clrfs !< snow fraction (clear-sky part of grid cell that contains snow)
  real, intent(in), dimension(ilg,ilev) :: zfevap !< fraction of rain that evaporates in a model time step
  real, intent(in), dimension(ilg,ilev) :: zfsubl !< fraction of snow that sublimates in a model time step
  real, intent(in), dimension(ilg,ilev) :: sgpp !< sulphuric acid (gas) chemical production or emission rate (kg of sulphur/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: ogpp !< secondary organic aerosol precursor gas chemical production or emission rate (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: docem1 !< organic aerosol emission rate for open vegetation and biofuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: docem2 !< organic aerosol emission rate for fossil fuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: docem3 !< organic aerosol emission rate for aircraft (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: docem4 !< organic aerosol emission rate for shipping (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dbcem1 !< black aerosol emission rate for open vegetation and biofuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dbcem2 !< black aerosol emission rate for fossil fuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dbcem3 !< black aerosol emission rate for aircraft (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dbcem4 !< black aerosol emission rate for shipping (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dsuem1 !< ammonium sulphate emission rate for open vegetation and biofuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dsuem2 !< ammonium sulphate emission rate for fossil fuel burning (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dsuem3 !< ammonium sulphate emission rate for aircraft (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: dsuem4 !< ammonium sulphate emission rate for shipping (kg/kg/sec)
  real, intent(in), dimension(ilg,ilev) :: o3row !< ozone volume mixing ratio (m3/m3)
  real, intent(in), dimension(ilg,ilev) :: hno3row !< nitric acid volume mixing ratio (m3/m3)
  real, intent(in), dimension(ilg,ilev) :: nh3row !< ammonia volume mixing ratio (m3/m3)
  real, intent(in), dimension(ilg,ilev) :: nh4row !< ammonium volume mixing ratio (m3/m3)
  real, intent(in), dimension(ilg,ican+1) :: fcanrow !<  fraction of canopy type (1: tall coniferous,  2: tall broadleaf, 3: arable and crops, 4: grass, swamp and tundra)
  real, intent(in), dimension(ilg) :: smfrac !< soil moisture fraction
  real, intent(in), dimension(ilg) :: fcs !< fraction of canopy over snow on ground in a grid cell
  real, intent(in), dimension(ilg) :: fgs !< fraction of snow on bare ground in a grid cell
  real, intent(in), dimension(ilg) :: fc !< fraction of snow-free canopy in a grid cell
  real, intent(in), dimension(ilg) :: fg !< fraction of snow-free bare ground in a grid cell
  real, intent(in), dimension(ilg) :: pbltrow !< vertical index of grid cell immediately below the planetary boundary layer
  real, intent(in), dimension(ilg) :: zspd !< model surface horizontal wind speed (m/sec)
  real, intent(in), dimension(ilg) :: zspdso !< model surface horizontal wind speed over open ocean (m/sec)
  real, intent(in), dimension(ilg) :: zspdsbs !< model surface horizontal wind speed over bare ground (m/sec)
  real, intent(in), dimension(ilg) :: cdml !< surface drag coefficient over land (m2/sec2)
  real, intent(in), dimension(ilg) :: cdmnl !< surface drag coefficient over water, ice, and open ocean (m2/sec2)
  real, intent(in), dimension(ilg) :: ustarbs !< friction velocity over bare ground (m/sec)
  real, intent(in), dimension(ilg) :: flnd !< fraction of land in a grid cell
  real, intent(in), dimension(ilg) :: focn !< fraction of open ocean in a grid cell
  real, intent(in), dimension(ilg) :: sicn !< sea ice fraction
  real, intent(in), dimension(ilg) :: gtrow !< temperature in bare ground (k)
  real, intent(in), dimension(ilg) :: fnrol !< fraction of snow on ground
  real, intent(in), dimension(ilg) :: suz0row !< aerodynamic roughness length for bare ground
  real, intent(in), dimension(ilg) :: spotrow !< fraction of paleo lake beds
  real, intent(in), dimension(ilg) :: st02row !< fraction of bare ground with medium grain size
  real, intent(in), dimension(ilg) :: st03row !< fraction of bare ground with fine grain size
  real, intent(in), dimension(ilg) :: st04row !< fraction of bare ground with coarse to medium grain size
  real, intent(in), dimension(ilg) :: st06row !< fraction of bare ground with fine to medium grain size
  real, intent(in), dimension(ilg) :: st13row !< fraction of bare ground for asian taklimakan
  real, intent(in), dimension(ilg) :: st14row !< fraction of bare ground for asian loess
  real, intent(in), dimension(ilg) :: st15row !< fraction of bare ground for asian gobi
  real, intent(in), dimension(ilg) :: st16row !< fraction of bare ground for asian desert and sandland
  real, intent(in), dimension(ilg) :: st17row !< fraction of bare ground for asian other mixture soil
  real, intent(in), dimension(ilg) :: bsfrac !< fraction of bare ground, method 1 (default)
  real, intent(in), dimension(ilg) :: pdsfrow !< fraction of bare ground, method 2
  real, intent(in), dimension(ilg) :: atau !< aging time scale of hydrophobic black carbon (sec)
  real, intent(in), dimension(ilg) :: zfs !< height of surface (bottom of atmosphere) (m)
  real, intent(in), dimension(ilg) :: pfs !< pressure at surface (bottom of atmosphere) (pa)
  integer, intent(in) :: mynode !< processor id
#if defined(GM_PAM)
  integer(kind=4), intent(in)    :: busper_size !<
  integer(kind=4), intent(in)    :: busvol_size !<
  real(kind=4)   , intent(inout) :: busper(busper_size) !<
  real(kind=4)   , intent(inout) :: busvol(busvol_size) !<
#endif
  !
  !     * input/output variables.
  !
  real, intent(inout), dimension(ilg,ilev,kextt) :: oednrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt) :: oercrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: oidnrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: oircrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: svvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: psvvrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: svmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: svcbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev,isaintt) :: pnvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: pnmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: pncbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt) :: psvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: psmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: pscbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev,isaintt) :: qnvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: qnmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: qncbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt) :: qsvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: qsmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: qscbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev) :: qgvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: qgmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: qgcbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: qdvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: qdmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: qdcbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt) :: onvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: onmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,nrmfld) :: oncbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt) :: osvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: osmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,isaintt,kintt,nrmfld) :: oscbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: ogvbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: ogmbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: ogcbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev,kextt) :: devbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt) :: pdevrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt,nrmfld) :: dembrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt,nrmfld) :: decbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: divbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: pdivrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: dimbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: dicbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev,kextt) :: revbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt) :: prevrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt,nrmfld) :: rembrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,kextt,nrmfld) :: recbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: rivbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev) :: privrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: rimbrow !< pla internal array
  real, intent(inout), dimension(ilg,ilev,nrmfld) :: ricbrow !< pla internal array
  !
  real, intent(inout), dimension(ilg,ilev) :: cornrow !< pla numerical diagnostic array (number correction)
  real, intent(inout), dimension(ilg,ilev) :: cormrow !< pla numerical diagnostic array (mass correction)
  real, intent(inout), dimension(ilg,ilev) :: rsn1row !< pla numerical diagnostic array (number residuum 1)
  real, intent(inout), dimension(ilg,ilev) :: rsm1row !< pla numerical diagnostic array (mass residuum 1)
  real, intent(inout), dimension(ilg,ilev) :: rsn2row !< pla numerical diagnostic array (number residuum 2)
  real, intent(inout), dimension(ilg,ilev) :: rsm2row !< pla numerical diagnostic array (mass residuum 2)
  real, intent(inout), dimension(ilg,ilev) :: rsn3row !< pla numerical diagnostic array (number residuum 3)
  real, intent(inout), dimension(ilg,ilev) :: rsm3row !< pla numerical diagnostic array (mass residuum 3)
  real, intent(inout), dimension(ilg)   :: vrn1row !< pla numerical diagnostic array (vertically integrated number residuum 1)
  real, intent(inout), dimension(ilg)   :: vrm1row !< pla numerical diagnostic array (vertically integrated mass residuum 1)
  real, intent(inout), dimension(ilg)   :: vrn2row !< pla numerical diagnostic array (vertically integrated number residuum 2)
  real, intent(inout), dimension(ilg)   :: vrm2row !< pla numerical diagnostic array (vertically integrated mass residuum 2)
  real, intent(inout), dimension(ilg)   :: vrn3row !< pla numerical diagnostic array (vertically integrated number residuum 3)
  real, intent(inout), dimension(ilg)   :: vrm3row !< pla numerical diagnostic array (vertically integrated mass residuum 3)
  real, intent(inout), dimension(ilg) :: vncnrow !< pla diagnostic array (vertically integrated nucleation rate for ammonium sulphate number) (/m2/sec)
  real, intent(inout), dimension(ilg) :: vasnrow !< pla diagnostic array (vertically integrated nucleation rate for ammoinum sulphate mass) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vscdrow !< pla diagnostic array (vertically integrated condensation rate for ammonium sulphate mass) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vgsprow !< pla diagnostic array (vertically integrated gas-phase production rate of sulphuric acid) (kg/m2/sec)
  real, intent(inout), dimension(ilg,ilevp1,ntrac) :: xrow !< grid-cell mean tracer mass mixing ratio (kg/kg)
  real, intent(inout), dimension(ilg,ilev,ntrac) :: sfrcrol !< pla internal array
  !
  !     * output variables.
  !
  real, intent(out), dimension(ilg,ilev) :: zcdnrow !< cloud droplet number concentration, hole-filled and time intepolated (1/m3)
  real, intent(out), dimension(ilg,ilev) :: bcicrow !< activated black carbon mass mixing ratio in cloudy portion of grid cell (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: sncnrow !< pla diagnostic array (nucleation rate for ammonium sulphate number) (/m3/sec)
  real, intent(inout), dimension(ilg,ilev) :: ssunrow !< pla diagnostic array (nucleation rate for ammonium sulphate mass) (kg/m3/sec)
  real, intent(inout), dimension(ilg,ilev) :: scndrow !< pla diagnostic array (condensation rate for ammonium sulphate mass) (kg/m3/sec)
  real, intent(inout), dimension(ilg,ilev) :: sgsprow !< pla diagnostic array (gas-phase production rate of sulphuric acid) (kg/m3/sec)
  real, intent(inout), dimension(ilg,ilev) :: ccnrow !< pla diagnostic array (instantaneous cloud droplet number concentration) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: ccnerow !< pla diagnostic array (cloud droplet number concentration, empirical approach) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: cc02row !< pla diagnostic array (cloud condensation nuclei concentration at 0.2% supersaturation) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: cc04row !< pla diagnostic array (cloud condensation nuclei concentration at 0.4% supersaturation) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: cc08row !< pla diagnostic array (cloud condensation nuclei concentration at 0.8% supersaturation) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: cc16row !< pla diagnostic array (cloud condensation nuclei concentration at 1.6% supersaturation) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: rcrirow !< pla diagnostic array (critical aerosol radius for cloud activation) (m)
  real, intent(inout), dimension(ilg,ilev) :: supsrow !< pla diagnostic array (maximum water vapour supersaturation in cloud) (%)
  real, intent(inout), dimension(ilg,ilev) :: henrrow !< pla diagnostic array (henrys law constant for sulphur dioxide)
  real, intent(inout), dimension(ilg,ilev) :: o3frow !< pla diagnostic array (ammonium sulphate production rate due to in-cloud ozone oxidation) (kg/kg/sec)
  real, intent(inout), dimension(ilg,ilev) :: wparrow !< pla diagnostic array (vertical velocity in cloud updraft) (m/sec)
  real, intent(inout), dimension(ilg,ilev) :: ntrow !< pla diagnostic array (aerosol number concentration) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: n20row !< pla diagnostic array (aerosol number concentration, diameter > 20nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: n50row !< pla diagnostic array (aerosol number concentration, diameter > 50nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: n100row !< pla diagnostic array (aerosol number concentration, diameter > 100nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: n200row !< pla diagnostic array (aerosol number concentration, diameter > 200nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: wtrow !< pla diagnostic array (wet aerosol number concentration) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: w20row !< pla diagnostic array (wet aerosol number concentration, diameter > 20nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: w50row !< pla diagnostic array (wet aerosol number concentration, diameter > 50nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: w100row !< pla diagnostic array (wet aerosol number concentration, diameter > 100nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: w200row !< pla diagnostic array (wet aerosol number concentration, diameter > 200nm) (/cm3)
  real, intent(inout), dimension(ilg,ilev) :: acasrow !< pla diagnostic array (ammonium sulphate mass mixing ratio) (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: acoarow !< pla diagnostic array (organic aerosol mass mixing ratio) (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: acbcrow !< pla diagnostic array (black carbon mass mixing ratio) (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: acssrow !< pla diagnostic array (sea salt mass mixing ratio) (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: acmdrow !< pla diagnostic array (mineral dust mass mixing ratio) (kg/kg)
  real, intent(inout), dimension(ilg,ilev) :: h2o2frow !< pla diagnostic array (ammonium sulphate production rate due to in-cloud hydrogen peroxide oxidation) (kg/kg/sec)
  real, intent(inout), dimension(ilg) :: depbrol !< total (dry + wet) black carbon deposition flux (kg/m2/sec)
  real, intent(inout), dimension(ilg,isdiag) :: defxrow !< pla diagnostic array (emission size distribution for mineral dust mass) (micro-g/m3)
  real, intent(inout), dimension(ilg,isdiag) :: defnrow !< pla diagnostic array (emission size distribution for mineral dust number) (/cm3)
  real, intent(inout), dimension(ilg) :: voaerow !< pla diagnostic array (vertically integrated emission flux of organic aerosol) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vbcerow !< pla diagnostic array (vertically integrated emission flux of black carbon) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vaserow !< pla diagnostic array (vertically integrated emission flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vmderow !< pla diagnostic array (vertically integrated emission flux of mineral dust) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vsserow !< pla diagnostic array (vertically integrated emission flux of sea salt) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: voawrow !< pla diagnostic array (vertically integrated wet deposition flux of organic aerosol) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vbcwrow !< pla diagnostic array (vertically integrated wet deposition flux of black carbon) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vaswrow !< pla diagnostic array (vertically integrated wet deposition flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vmdwrow !< pla diagnostic array (vertically integrated wet deposition flux of mineral dust) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vsswrow !< pla diagnostic array (vertically integrated wet deposition flux of sea salt) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: voadrow !< pla diagnostic array (vertically integrated dry deposition flux of organic aerosol) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vbcdrow !< pla diagnostic array (vertically integrated dry deposition flux of black carbon) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vasdrow !< pla diagnostic array (vertically integrated dry deposition flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vmddrow !< pla diagnostic array (vertically integrated dry deposition flux of mineral dust) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vssdrow !< pla diagnostic array (vertically integrated dry deposition flux of sea salt) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: voagrow !< pla diagnostic array (vertically integrated gas-to-particle conversion flux of organic aerosol) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vbcgrow !< pla diagnostic array (vertically integrated gas-to-particle conversion flux of black carbon) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vasgrow !< pla diagnostic array (vertically integrated gas-to-particle conversion flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vmdgrow !< pla diagnostic array (vertically integrated gas-to-particle conversion flux of mineral dust) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vssgrow !< pla diagnostic array (vertically integrated gas-to-particle conversion flux of sea salt) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vasirow !< pla diagnostic array (vertically integrated in-cloud oxidation flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vas1row !< pla diagnostic array (vertically integrated in-cloud scavenging flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vas2row !< pla diagnostic array (vertically integrated below-cloud scavenging flux of ammonium sulphate) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vas3row !< pla diagnostic array (vertically integrated re-evaporation of ammonium sulphate in rain) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: vccnrow !< pla diagnostic array (vertically integrated instantaneous cloud droplet number concentration) (/m2)
  real, intent(inout), dimension(ilg) :: vcnerow !< pla diagnostic array (vertically integrated instantaneous cloud droplet number concentration, empirical approach) (/m2)
  real, intent(inout), dimension(ilg,isdust) :: dmacrow !< pla diagnostic array (emission mass size distribution flux for mineral dust 1) (kg/m2/sec)
  real, intent(inout), dimension(ilg,isdust) :: dmcorow !< pla diagnostic array (emission mass size distribution flux for mineral dust 2) (kg/m2/sec)
  real, intent(inout), dimension(ilg,isdust) :: dnacrow !< pla diagnostic array (emission number size distribution flux for mineral dust 1) (kg/m2/sec)
  real, intent(inout), dimension(ilg,isdust) :: dncorow !< pla diagnostic array (emission number size distribution flux for mineral dust 2) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: duwdrow !< pla diagnostic array (wind speed in mineral dust calculation) (m/sec)
  real, intent(inout), dimension(ilg) :: dustrow !< pla diagnostic array (friction velocity in mineral dust calculation (m/sec)
  real, intent(inout), dimension(ilg) :: duthrow !< pla diagnostic array (friction velocity threshold in mineral dust calculation) (m/sec)
  real, intent(inout), dimension(ilg) :: fallrow !< pla diagnostic array (fluxall array in mineral dust calculation)
  real, intent(inout), dimension(ilg) :: fa10row !< pla diagnostic array (fa10 array in mineral dust calculation)
  real, intent(inout), dimension(ilg) :: fa2row !< pla diagnostic array (fa2 array in mineral dust calculation)
  real, intent(inout), dimension(ilg) :: fa1row !< pla diagnostic array (fa1 array in mineral dust calculation)
  real, intent(inout), dimension(ilg) :: usmkrow !< pla diagnostic array (time fraction of threshold exceedence in mineral dust calculation)
  real, intent(inout), dimension(ilg) :: defarow !< pla diagnostic array (emission flux of mineral dust mass for accumulation mode) (kg/m2/sec)
  real, intent(inout), dimension(ilg) :: defcrow !< pla diagnostic array (emission flux of mineral dust mass for coarse mode) (kg/m2/sec)
  !
  !     * pam in/output variables and parameters
  !
  integer, allocatable, dimension(:,:,:,:)   :: oncbio !<
  integer, allocatable, dimension(:,:,:,:,:) :: oscbio !<
  integer, allocatable, dimension(:,:,:,:)   :: pncbio !<
  integer, allocatable, dimension(:,:,:,:,:) :: pscbio !<
  integer, allocatable, dimension(:,:,:)     :: ogcbio !<
  integer, allocatable, dimension(:,:,:)     :: svcbio !<
  integer, allocatable, dimension(:,:,:,:)   :: decbio !<
  integer, allocatable, dimension(:,:,:)     :: dicbio !<
  integer, allocatable, dimension(:,:,:,:)   :: recbio !<
  integer, allocatable, dimension(:,:,:)     :: ricbio !<
  !
  real, allocatable, dimension(:,:,:)     :: trac !<
  real, allocatable, dimension(:,:,:)     :: trrat !<
  real, allocatable, dimension(:,:,:)     :: oednio !<
  real, allocatable, dimension(:,:,:)     :: oercio !<
  real, allocatable, dimension(:,:)       :: oidnio !<
  real, allocatable, dimension(:,:)       :: oircio !<
  real, allocatable, dimension(:,:)       :: svvbio !<
  real, allocatable, dimension(:,:)       :: psvvio !<
  real, allocatable, dimension(:,:,:)     :: svmbio !<
  real, allocatable, dimension(:,:,:)     :: pnvbio !<
  real, allocatable, dimension(:,:,:,:)   :: pnmbio !<
  real, allocatable, dimension(:,:,:,:)   :: psvbio !<
  real, allocatable, dimension(:,:,:,:,:) :: psmbio !<
  real, allocatable, dimension(:,:,:)     :: onvbio !<
  real, allocatable, dimension(:,:,:,:)   :: onmbio !<
  real, allocatable, dimension(:,:,:,:)   :: osvbio !<
  real, allocatable, dimension(:,:,:,:,:) :: osmbio !<
  real, allocatable, dimension(:,:)       :: ogvbio !<
  real, allocatable, dimension(:,:,:)     :: ogmbio !<
  real, allocatable, dimension(:,:,:)     :: devbio !<
  real, allocatable, dimension(:,:,:)     :: pdevio !<
  real, allocatable, dimension(:,:,:,:)   :: dembio !<
  real, allocatable, dimension(:,:)       :: divbio !<
  real, allocatable, dimension(:,:)       :: pdivio !<
  real, allocatable, dimension(:,:,:)     :: dimbio !<
  real, allocatable, dimension(:,:,:)     :: revbio !<
  real, allocatable, dimension(:,:,:)     :: previo !<
  real, allocatable, dimension(:,:,:,:)   :: rembio !<
  real, allocatable, dimension(:,:)       :: rivbio !<
  real, allocatable, dimension(:,:)       :: privio !<
  real, allocatable, dimension(:,:,:)     :: rimbio !<
  !
  !     * pam input variables and parameters.
  !
  integer :: icana !<
  integer :: ilga !<
  integer :: knt !<
  real :: dt !<
  real :: co2 !<
  real, allocatable, dimension(:,:) :: zha !<
  real, allocatable, dimension(:,:) :: zfa !<
  real, allocatable, dimension(:,:) :: pha !<
  real, allocatable, dimension(:,:) :: pfa !<
  real, allocatable, dimension(:,:) :: dpa !<
  real, allocatable, dimension(:,:) :: ta !<
  real, allocatable, dimension(:,:) :: rva !<
  real, allocatable, dimension(:,:) :: qtna !<
  real, allocatable, dimension(:,:) :: hmna !<
  real, allocatable, dimension(:,:) :: rha !<
  real, allocatable, dimension(:,:) :: rhta !<
  real, allocatable, dimension(:,:) :: wcta !<
  real, allocatable, dimension(:,:) :: spmrep !<
  real, allocatable, dimension(:,:) :: szmlwc !<
  real, allocatable, dimension(:,:) :: spfsnow !<
  real, allocatable, dimension(:,:) :: spfrain !<
  real, allocatable, dimension(:,:) :: szclf !<
  real, allocatable, dimension(:,:) :: sclrfr !<
  real, allocatable, dimension(:,:) :: sclrfs !<
  real, allocatable, dimension(:,:) :: spfevap !<
  real, allocatable, dimension(:,:) :: spfsubl !<
  real, allocatable, dimension(:,:) :: sgpr !<
  real, allocatable, dimension(:,:) :: soap !<
  real, allocatable, dimension(:,:) :: docem1s !<
  real, allocatable, dimension(:,:) :: docem2s !<
  real, allocatable, dimension(:,:) :: docem3s !<
  real, allocatable, dimension(:,:) :: docem4s !<
  real, allocatable, dimension(:,:) :: dbcem1s !<
  real, allocatable, dimension(:,:) :: dbcem2s !<
  real, allocatable, dimension(:,:) :: dbcem3s !<
  real, allocatable, dimension(:,:) :: dbcem4s !<
  real, allocatable, dimension(:,:) :: dsuem1s !<
  real, allocatable, dimension(:,:) :: dsuem2s !<
  real, allocatable, dimension(:,:) :: dsuem3s !<
  real, allocatable, dimension(:,:) :: dsuem4s !<
  real, allocatable, dimension(:,:) :: xo3 !<
  real, allocatable, dimension(:,:) :: xna !<
  real, allocatable, dimension(:,:) :: xam !<
  real, allocatable, dimension(:,:) :: xnh3 !<
  real, allocatable, dimension(:,:) :: fcana !<
  real, allocatable, dimension(:)  :: smfrdu !<
  real, allocatable, dimension(:)  :: fcsa !<
  real, allocatable, dimension(:)  :: fgsa !<
  real, allocatable, dimension(:)  :: fca !<
  real, allocatable, dimension(:)  :: fga !<
  real, allocatable, dimension(:)  :: ataus !<
  real, allocatable, dimension(:)  :: zspddd !<
  real, allocatable, dimension(:)  :: zspdss !<
  real, allocatable, dimension(:)  :: zspddu !<
  real, allocatable, dimension(:)  :: cdmla !<
  real, allocatable, dimension(:)  :: cdmnla !<
  real, allocatable, dimension(:)  :: ustard !<
  real, allocatable, dimension(:)  :: flnddu !<
  real, allocatable, dimension(:)  :: focndu !<
  real, allocatable, dimension(:)  :: sicndu !<
  real, allocatable, dimension(:)  :: gtrdu !<
  real, allocatable, dimension(:)  :: fndu !<
  real, allocatable, dimension(:)  :: zfsa !<
  real, allocatable, dimension(:)  :: pfsa !<
  real, allocatable, dimension(:)   :: suz0 !<
  real, allocatable, dimension(:)   :: slai !<
  real, allocatable, dimension(:)   :: spot !<
  real, allocatable, dimension(:)   :: st02 !<
  real, allocatable, dimension(:)   :: st03 !<
  real, allocatable, dimension(:)   :: st04 !<
  real, allocatable, dimension(:)   :: st06 !<
  real, allocatable, dimension(:)   :: st13 !<
  real, allocatable, dimension(:)   :: st14 !<
  real, allocatable, dimension(:)   :: st15 !<
  real, allocatable, dimension(:)   :: st16 !<
  real, allocatable, dimension(:)   :: st17 !<
  integer, allocatable, dimension(:) :: pblts !<
  !
  !     * pam output variables.
  !
  real, allocatable, dimension(:,:) :: cdnc !<
  real, allocatable, dimension(:,:) :: bcic !<
  real, allocatable, dimension(:,:) :: corn !<
  real, allocatable, dimension(:,:) :: corm !<
  real, allocatable, dimension(:,:) :: rsn1 !<
  real, allocatable, dimension(:,:) :: rsm1 !<
  real, allocatable, dimension(:,:) :: rsn2 !<
  real, allocatable, dimension(:,:) :: rsm2 !<
  real, allocatable, dimension(:,:) :: rsn3 !<
  real, allocatable, dimension(:,:) :: rsm3 !<
  real, allocatable, dimension(:)   :: vrn1 !<
  real, allocatable, dimension(:)   :: vrm1 !<
  real, allocatable, dimension(:)   :: vrn2 !<
  real, allocatable, dimension(:)   :: vrm2 !<
  real, allocatable, dimension(:)   :: vrn3 !<
  real, allocatable, dimension(:)   :: vrm3 !<
  real, allocatable, dimension(:)   :: vncn !<
  real, allocatable, dimension(:)   :: vasn !<
  real, allocatable, dimension(:)   :: vcnd !<
  real, allocatable, dimension(:)   :: vgsp !<
  real, allocatable, dimension(:,:) :: sncn !<
  real, allocatable, dimension(:,:) :: ssun !<
  real, allocatable, dimension(:,:) :: scnd !<
  real, allocatable, dimension(:,:) :: sgsp !<
  real, allocatable, dimension(:,:) :: ccn !<
  real, allocatable, dimension(:,:) :: ccne !<
  real, allocatable, dimension(:,:) :: cc02 !<
  real, allocatable, dimension(:,:) :: cc04 !<
  real, allocatable, dimension(:,:) :: cc08 !<
  real, allocatable, dimension(:,:) :: cc16 !<
  real, allocatable, dimension(:,:) :: rcri !<
  real, allocatable, dimension(:,:) :: sups !<
  real, allocatable, dimension(:,:) :: henr !<
  real, allocatable, dimension(:,:) :: o3frc !<
  real, allocatable, dimension(:,:) :: h2o2frc !<
  real, allocatable, dimension(:,:) :: cnt !<
  real, allocatable, dimension(:,:) :: cn20 !<
  real, allocatable, dimension(:,:) :: cn50 !<
  real, allocatable, dimension(:,:) :: cn100 !<
  real, allocatable, dimension(:,:) :: cn200 !<
  real, allocatable, dimension(:,:) :: cntw !<
  real, allocatable, dimension(:,:) :: cn20w !<
  real, allocatable, dimension(:,:) :: cn50w !<
  real, allocatable, dimension(:,:) :: cn100w !<
  real, allocatable, dimension(:,:) :: cn200w !<
  real, allocatable, dimension(:,:) :: acas !<
  real, allocatable, dimension(:,:) :: acoa !<
  real, allocatable, dimension(:,:) :: acbc !<
  real, allocatable, dimension(:,:) :: acss !<
  real, allocatable, dimension(:,:) :: acmd !<
  real, allocatable, dimension(:,:) :: defx !<
  real, allocatable, dimension(:,:) :: defn !<
  real, allocatable, dimension(:) :: voae !<
  real, allocatable, dimension(:) :: vbce !<
  real, allocatable, dimension(:) :: vase !<
  real, allocatable, dimension(:) :: vmde !<
  real, allocatable, dimension(:) :: vsse !<
  real, allocatable, dimension(:) :: voaw !<
  real, allocatable, dimension(:) :: vbcw !<
  real, allocatable, dimension(:) :: vasw !<
  real, allocatable, dimension(:) :: vmdw !<
  real, allocatable, dimension(:) :: vssw !<
  real, allocatable, dimension(:) :: voad !<
  real, allocatable, dimension(:) :: vbcd !<
  real, allocatable, dimension(:) :: vasd !<
  real, allocatable, dimension(:) :: vmdd !<
  real, allocatable, dimension(:) :: vssd !<
  real, allocatable, dimension(:) :: voag !<
  real, allocatable, dimension(:) :: vbcg !<
  real, allocatable, dimension(:) :: vasg !<
  real, allocatable, dimension(:) :: vmdg !<
  real, allocatable, dimension(:) :: vssg !<
  real, allocatable, dimension(:) :: vasi !<
  real, allocatable, dimension(:) :: vas1 !<
  real, allocatable, dimension(:) :: vas2 !<
  real, allocatable, dimension(:) :: vas3 !<
  real, allocatable, dimension(:) :: vccn !<
  real, allocatable, dimension(:) :: vcne !<
  real, allocatable, dimension(:,:) :: dmac !<
  real, allocatable, dimension(:,:) :: dmco !<
  real, allocatable, dimension(:,:) :: dnac !<
  real, allocatable, dimension(:,:) :: dnco !<
  real, allocatable, dimension(:) :: duwd !<
  real, allocatable, dimension(:) :: dust !<
  real, allocatable, dimension(:) :: duth !<
  real, allocatable, dimension(:) :: fall !<
  real, allocatable, dimension(:) :: fa10 !<
  real, allocatable, dimension(:) :: fa2 !<
  real, allocatable, dimension(:) :: fa1 !<
  real, allocatable, dimension(:) :: usmk !<
  real, allocatable, dimension(:) :: defa !<
  real, allocatable, dimension(:) :: defc !<
  !
  !     * internal variables.
  !
  integer :: n !<
  integer :: ns !<
  integer :: l !<
  integer :: lo !<
  integer :: msgp1a !<
  integer :: msgp2a !<
  real, allocatable, dimension(:,:) :: dsa !<
  !
  !-----------------------------------------------------------------------
  !     size of pla arrays. the size of the vertical domain is
  !     determined by leva (total number of grid points, starting
  !     at the bottom of the domain).
  !
  !      leva=ilev-1
  if (leva > ilev) call xit('PAMDRIV',-1)
  ilga=il2-il1+1
  icana=ican
  !
  !     initializations of scalars.
  !
  allocate(dsa(ilga,leva))
  dsa=1.
  knt=kount
  dt=timst
  co2=co2_ppm
  !
  !     test whether number of tracers is sufficient. exit if
  !     insufficient memory (increase ntracs in that case). warning
  !     if actual number of tracers is less than the number of
  !     tracers that are available (reduce ntracs to reduce memory,
  !     if necessary).
  !
  if (ntracs > ntracp .or. ntracs < 1) then
    if (mynode==0) write(6,'(A17,I3,1X,I3)') '  NTRACS,NTRACP = ',ntracs,ntracp
    call xit ('PAMDRIV',-2)
  end if
  if (ntracs < ntracp .and. mynode==0) then
    write(6,'(A17,I3,1X,I3)')    '  NTRACS,NTRACP = ',ntracs,ntracp
    call wrn('PAMDRIV',-2)
  end if
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for tracer arrays.
  !
  allocate(trac (ilga,leva,ntract))
  allocate(trrat(ilga,leva,ntract))
  !
  !     * allocate memory for other input/output arrays.
  !
  allocate(zha    (ilga,leva))
  allocate(zfa    (ilga,leva))
  allocate(pha    (ilga,leva))
  allocate(pfa    (ilga,leva))
  allocate(dpa    (ilga,leva))
  allocate(ta     (ilga,leva))
  allocate(rva    (ilga,leva))
  allocate(qtna   (ilga,leva))
  allocate(hmna   (ilga,leva))
  allocate(rha    (ilga,leva))
  allocate(rhta   (ilga,leva))
  allocate(wcta   (ilga,leva))
  allocate(spmrep (ilga,leva))
  allocate(szmlwc (ilga,leva))
  allocate(spfsnow(ilga,leva))
  allocate(spfrain(ilga,leva))
  allocate(szclf  (ilga,leva))
  allocate(sclrfr (ilga,leva))
  allocate(sclrfs (ilga,leva))
  allocate(spfevap(ilga,leva))
  allocate(spfsubl(ilga,leva))
  allocate(sgpr   (ilga,leva))
  allocate(soap   (ilga,leva))
  allocate(docem1s(ilga,leva))
  allocate(docem2s(ilga,leva))
  allocate(docem3s(ilga,leva))
  allocate(docem4s(ilga,leva))
  allocate(dbcem1s(ilga,leva))
  allocate(dbcem2s(ilga,leva))
  allocate(dbcem3s(ilga,leva))
  allocate(dbcem4s(ilga,leva))
  allocate(dsuem1s(ilga,leva))
  allocate(dsuem2s(ilga,leva))
  allocate(dsuem3s(ilga,leva))
  allocate(dsuem4s(ilga,leva))
  allocate(xo3    (ilga,leva))
  allocate(xna    (ilga,leva))
  allocate(xam    (ilga,leva))
  allocate(xnh3   (ilga,leva))
  allocate(fcana  (ilga,icana+1))
  allocate(ataus  (ilga))
  allocate(smfrdu (ilga))
  allocate(fcsa   (ilga))
  allocate(fgsa   (ilga))
  allocate(fca    (ilga))
  allocate(fga    (ilga))
  allocate(zspddd (ilga))
  allocate(zspdss (ilga))
  allocate(zspddu (ilga))
  allocate(cdmla  (ilga))
  allocate(cdmnla (ilga))
  allocate(ustard (ilga))
  allocate(pblts  (ilga))
  allocate(flnddu (ilga))
  allocate(focndu (ilga))
  allocate(sicndu (ilga))
  allocate(gtrdu  (ilga))
  allocate(fndu   (ilga))
  allocate(zfsa   (ilga))
  allocate(pfsa   (ilga))
  !
  allocate(spot   (ilga))
  allocate(st02   (ilga))
  allocate(st03   (ilga))
  allocate(st04   (ilga))
  allocate(st06   (ilga))
  allocate(st13   (ilga))
  allocate(st14   (ilga))
  allocate(st15   (ilga))
  allocate(st16   (ilga))
  allocate(st17   (ilga))
  allocate(suz0   (ilga))
  allocate(slai   (ilga))
  !
  allocate(oednio(ilga,leva,kext))
  allocate(oercio(ilga,leva,kext))
  allocate(oidnio(ilga,leva))
  allocate(oircio(ilga,leva))
  allocate(svvbio(ilga,leva))
  allocate(psvvio(ilga,leva))
  allocate(svmbio(ilga,leva,nrmfld))
  allocate(svcbio(ilga,leva,nrmfld))
  allocate(pnvbio(ilga,leva,isaint))
  allocate(pnmbio(ilga,leva,isaint,nrmfld))
  allocate(pncbio(ilga,leva,isaint,nrmfld))
  allocate(psvbio(ilga,leva,isaint,kint))
  allocate(psmbio(ilga,leva,isaint,kint,nrmfld))
  allocate(pscbio(ilga,leva,isaint,kint,nrmfld))
  allocate(onvbio(ilga,leva,isaint))
  allocate(onmbio(ilga,leva,isaint,nrmfld))
  allocate(oncbio(ilga,leva,isaint,nrmfld))
  allocate(osvbio(ilga,leva,isaint,kint))
  allocate(osmbio(ilga,leva,isaint,kint,nrmfld))
  allocate(oscbio(ilga,leva,isaint,kint,nrmfld))
  allocate(ogvbio(ilga,leva))
  allocate(ogmbio(ilga,leva,nrmfld))
  allocate(ogcbio(ilga,leva,nrmfld))
  allocate(devbio(ilga,leva,kext))
  allocate(pdevio(ilga,leva,kext))
  allocate(dembio(ilga,leva,kext,nrmfld))
  allocate(decbio(ilga,leva,kext,nrmfld))
  allocate(divbio(ilga,leva))
  allocate(pdivio(ilga,leva))
  allocate(dimbio(ilga,leva,nrmfld))
  allocate(dicbio(ilga,leva,nrmfld))
  allocate(revbio(ilga,leva,kext))
  allocate(previo(ilga,leva,kext))
  allocate(rembio(ilga,leva,kext,nrmfld))
  allocate(recbio(ilga,leva,kext,nrmfld))
  allocate(rivbio(ilga,leva))
  allocate(privio(ilga,leva))
  allocate(rimbio(ilga,leva,nrmfld))
  allocate(ricbio(ilga,leva,nrmfld))
  allocate(cdnc(ilga,leva))
  allocate(bcic(ilga,leva))
  !
  allocate(corn(ilga,leva))
  allocate(corm(ilga,leva))
  allocate(rsn1(ilga,leva))
  allocate(rsm1(ilga,leva))
  allocate(rsn2(ilga,leva))
  allocate(rsm2(ilga,leva))
  allocate(rsn3(ilga,leva))
  allocate(rsm3(ilga,leva))
  allocate(sncn(ilga,leva))
  allocate(ssun(ilga,leva))
  allocate(scnd(ilga,leva))
  allocate(sgsp(ilga,leva))
  allocate(acas(ilga,leva))
  allocate(acoa(ilga,leva))
  allocate(acbc(ilga,leva))
  allocate(acss(ilga,leva))
  allocate(acmd(ilga,leva))
  allocate(cnt (ilga,leva))
  allocate(cn20(ilga,leva))
  allocate(cn50(ilga,leva))
  allocate(cn100(ilga,leva))
  allocate(cn200(ilga,leva))
  allocate(cntw(ilga,leva))
  allocate(cn20w(ilga,leva))
  allocate(cn50w(ilga,leva))
  allocate(cn100w(ilga,leva))
  allocate(cn200w(ilga,leva))
  allocate(ccn (ilga,leva))
  allocate(ccne(ilga,leva))
  allocate(cc02(ilga,leva))
  allocate(cc04(ilga,leva))
  allocate(cc08(ilga,leva))
  allocate(cc16(ilga,leva))
  allocate(rcri(ilga,leva))
  allocate(sups(ilga,leva))
  allocate(henr(ilga,leva))
  allocate(o3frc(ilga,leva))
  allocate(h2o2frc(ilga,leva))
  allocate(vrn1(ilga))
  allocate(vrm1(ilga))
  allocate(vrn2(ilga))
  allocate(vrm2(ilga))
  allocate(vrn3(ilga))
  allocate(vrm3(ilga))
  allocate(vncn(ilga))
  allocate(vasn(ilga))
  allocate(vcnd(ilga))
  allocate(vgsp(ilga))
  allocate(defx(ilga,isdiag))
  allocate(defn(ilga,isdiag))
  allocate(voae(ilga))
  allocate(vbce(ilga))
  allocate(vase(ilga))
  allocate(vmde(ilga))
  allocate(vsse(ilga))
  allocate(voaw(ilga))
  allocate(vbcw(ilga))
  allocate(vasw(ilga))
  allocate(vmdw(ilga))
  allocate(vssw(ilga))
  allocate(voad(ilga))
  allocate(vbcd(ilga))
  allocate(vasd(ilga))
  allocate(vmdd(ilga))
  allocate(vssd(ilga))
  allocate(voag(ilga))
  allocate(vbcg(ilga))
  allocate(vasg(ilga))
  allocate(vmdg(ilga))
  allocate(vssg(ilga))
  allocate(vasi(ilga))
  allocate(vas1(ilga))
  allocate(vas2(ilga))
  allocate(vas3(ilga))
  allocate(vccn(ilga))
  allocate(vcne(ilga))
  allocate(dmac(ilga,isdust))
  allocate(dmco(ilga,isdust))
  allocate(dnac(ilga,isdust))
  allocate(dnco(ilga,isdust))
  allocate(duwd(ilga))
  allocate(dust(ilga))
  allocate(duth(ilga))
  allocate(fall(ilga))
  allocate(fa10(ilga))
  allocate(fa2 (ilga))
  allocate(fa1 (ilga))
  allocate(usmk(ilga))
  allocate(defa(ilga))
  allocate(defc(ilga))
  !
  !-----------------------------------------------------------------------
  !     copy aerosol tracer fields.
  !
  msgp2a=ilevp1-leva+1
  msgp1a=ilev  -leva+1
  do ns=1,ntracs
    n=iaind(ns)
    trac (1:ilga,1:leva,ns)=xrow   (il1:il2,msgp2a:ilevp1,n)
    trrat(1:ilga,1:leva,ns)=sfrcrol(il1:il2,msgp1a:ilev,n)
  end do
  !
  !     copy gas tracer fields.
  !
  trac (1:ilga,1:leva,jgs6)=xrow   (il1:il2,msgp2a:ilevp1,igs6)
  trac (1:ilga,1:leva,jgsp)=xrow   (il1:il2,msgp2a:ilevp1,igsp)
  trac (1:ilga,1:leva,jso2)=xrow   (il1:il2,msgp2a:ilevp1,iso2)
  trac (1:ilga,1:leva,jhpo)=xrow   (il1:il2,msgp2a:ilevp1,ihpo)
  trrat(1:ilga,1:leva,jgs6)=sfrcrol(il1:il2,msgp1a:ilev,igs6)
  trrat(1:ilga,1:leva,jgsp)=sfrcrol(il1:il2,msgp1a:ilev,igsp)
  trrat(1:ilga,1:leva,jso2)=sfrcrol(il1:il2,msgp1a:ilev,iso2)
  trrat(1:ilga,1:leva,jhpo)=sfrcrol(il1:il2,msgp1a:ilev,ihpo)
  !
  !     copy meteorological :: variables for pla calculations.
  !
  zfsa=zfs  (il1:il2)
  pfsa=pfs  (il1:il2)
  zha =zh   (il1:il2,msgp1a:ilev)
  pha =ph   (il1:il2,msgp1a:ilev)
  dpa =dp   (il1:il2,msgp1a:ilev)
  ta  =throw(il1:il2,msgp2a:ilevp1)
  rva =qrow (il1:il2,msgp2a:ilevp1)*dsa
  qtna=qtn  (il1:il2,msgp1a:ilev)
  hmna=hmn  (il1:il2,msgp1a:ilev)
  rha =rhc  (il1:il2,msgp1a:ilev)
  rhta=rht  (il1:il2,msgp1a:ilev)
  wcta=max(wg(il1:il2,msgp1a:ilev),0.)+wsub (il1:il2,msgp1a:ilev)
  deallocate(dsa)
  !
  !     copy level information at grid cell interfaces.
  !     this involves shifting the vertical grid.
  !
  do l=1,leva
    lo=l-leva+ilev-1
    zfa(:,l)=zf(il1:il2,lo)
    pfa(:,l)=pf(il1:il2,lo)
  end do
  !
  !     cloud microphysical parameters.
  !
  spmrep =zmratep(il1:il2,msgp1a:ilev)
  szmlwc =zmlwc  (il1:il2,msgp1a:ilev)
  spfsnow=zfsnow (il1:il2,msgp1a:ilev)
  spfrain=zfrain (il1:il2,msgp1a:ilev)
  szclf  =zclf   (il1:il2,msgp1a:ilev)
  sclrfr =clrfr  (il1:il2,msgp1a:ilev)
  sclrfs =clrfs  (il1:il2,msgp1a:ilev)
  spfevap=zfevap (il1:il2,msgp1a:ilev)
  spfsubl=zfsubl (il1:il2,msgp1a:ilev)
  !
  !     chemical sources.
  !
  sgpr=sgpp(il1:il2,msgp1a:ilev)*wh2so4/ws
  soap=ogpp(il1:il2,msgp1a:ilev)
  !
  !     primary emissions of aerosol.
  !
  docem1s=docem1(il1:il2,msgp1a:ilev)
  docem2s=docem2(il1:il2,msgp1a:ilev)
  docem3s=docem3(il1:il2,msgp1a:ilev)
  docem4s=docem4(il1:il2,msgp1a:ilev)
  dbcem1s=dbcem1(il1:il2,msgp1a:ilev)
  dbcem2s=dbcem2(il1:il2,msgp1a:ilev)
  dbcem3s=dbcem3(il1:il2,msgp1a:ilev)
  dbcem4s=dbcem4(il1:il2,msgp1a:ilev)
  dsuem1s=dsuem1(il1:il2,msgp1a:ilev)
  dsuem2s=dsuem2(il1:il2,msgp1a:ilev)
  dsuem3s=dsuem3(il1:il2,msgp1a:ilev)
  dsuem4s=dsuem4(il1:il2,msgp1a:ilev)
  !
  !     background concentrations.
  !
  xo3 =o3row  (il1:il2,msgp1a:ilev)
  xna =hno3row(il1:il2,msgp1a:ilev)
  xnh3=nh3row (il1:il2,msgp1a:ilev)
  xam =nh3row (il1:il2,msgp1a:ilev) &
          +nh4row (il1:il2,msgp1a:ilev)
  !
  !     aging time scale.
  !
  ataus =atau   (il1:il2)
  !
  !     surface parameters.
  !
  pblts =max(nint(pbltrow(il1:il2))-ilev,-leva+1)
  smfrdu=smfrac (il1:il2)
  fcsa  =fcs    (il1:il2)
  fgsa  =fgs    (il1:il2)
  fca   =fc     (il1:il2)
  fga   =fg     (il1:il2)
  zspddd=zspd   (il1:il2)
  zspdss=zspdso (il1:il2)
  zspddu=zspdsbs(il1:il2)
  cdmla =cdml   (il1:il2)
  cdmnla=cdmnl  (il1:il2)
  ustard=ustarbs(il1:il2)
  flnddu=flnd   (il1:il2)
  focndu=focn   (il1:il2)
  sicndu=sicn   (il1:il2)
  gtrdu =gtrow  (il1:il2)
  fndu  =fnrol  (il1:il2)
  fcana =fcanrow(il1:il2,1:ican+1)
  !
  !     dust parameters.
  !
  spot=spotrow(il1:il2)
  st02=st02row(il1:il2)
  st03=st03row(il1:il2)
  st04=st04row(il1:il2)
  st06=st06row(il1:il2)
  st13=st13row(il1:il2)
  st14=st14row(il1:il2)
  st15=st15row(il1:il2)
  st16=st16row(il1:il2)
  st17=st17row(il1:il2)
  suz0=suz0row(il1:il2)
  !
  !      bsfrac is the bare soil fraction passed from cccma gcm land scheme class (default).
  !      bsfrac is combined with the soil moisture criterion (w0) to obtain the potential dust
  !      source area.
  !      pdsfrow is a prescribed potential dust source areal :: fraction obtained from an offline
  !      terrestrial model, in which the soil moisture & snow cover criteria were satisfied.
  !      pdsfrow is only applied for the purpose of dust parameterization development.
  !
  if (cccmabf) then
    slai=bsfrac(il1:il2)
  else
    slai=pdsfrow(il1:il2)
  end if
  !
  !     check if the number of fields for averaging of cloud
  !     droplet results is sufficient (increase nrmfld, if necessary).
  !
  if (ceiling(real(iavgprd)/real(iupdatp)) > nrmfld) then
    call xit('PAMDRIV',-3)
  end if
  !
  !     parameter check for pam internal input/output fields.
  !
  if (kextt /= kext) call xit('PAMDRIV',-4)
  if (kintt /= kint) call xit('PAMDRIV',-5)
  if (isaintt /= isaint) call xit('PAMDRIV',-6)
  !
  !     pam internal input/output fields that are required
  !     for averaging and distributing tendencies from subroutines
  !     that are not called every time step.
  !
  oednio=oednrow(il1:il2,msgp1a:ilev,:)
  oercio=oercrow(il1:il2,msgp1a:ilev,:)
  oidnio=oidnrow(il1:il2,msgp1a:ilev)
  oircio=oircrow(il1:il2,msgp1a:ilev)
  svvbio=svvbrow(il1:il2,msgp1a:ilev)
  psvvio=psvvrow(il1:il2,msgp1a:ilev)
  svmbio=svmbrow(il1:il2,msgp1a:ilev,:)
  svcbio=nint(svcbrow(il1:il2,msgp1a:ilev,:))
  !
  pnvbio=pnvbrow(il1:il2,msgp1a:ilev,:)
  pnmbio=pnmbrow(il1:il2,msgp1a:ilev,:,:)
  pncbio=nint(pncbrow(il1:il2,msgp1a:ilev,:,:))
  psvbio=psvbrow(il1:il2,msgp1a:ilev,:,:)
  psmbio=psmbrow(il1:il2,msgp1a:ilev,:,:,:)
  pscbio=nint(pscbrow(il1:il2,msgp1a:ilev,:,:,:))
  !
  onvbio=onvbrow(il1:il2,msgp1a:ilev,:)
  onmbio=onmbrow(il1:il2,msgp1a:ilev,:,:)
  oncbio=nint(oncbrow(il1:il2,msgp1a:ilev,:,:))
  osvbio=osvbrow(il1:il2,msgp1a:ilev,:,:)
  osmbio=osmbrow(il1:il2,msgp1a:ilev,:,:,:)
  oscbio=nint(oscbrow(il1:il2,msgp1a:ilev,:,:,:))
  ogvbio=ogvbrow(il1:il2,msgp1a:ilev)
  ogmbio=ogmbrow(il1:il2,msgp1a:ilev,:)
  ogcbio=nint(ogcbrow(il1:il2,msgp1a:ilev,:))
  !
  devbio=devbrow(il1:il2,msgp1a:ilev,:)
  pdevio=pdevrow(il1:il2,msgp1a:ilev,:)
  dembio=dembrow(il1:il2,msgp1a:ilev,:,:)
  decbio=nint(decbrow(il1:il2,msgp1a:ilev,:,:))
  divbio=divbrow(il1:il2,msgp1a:ilev)
  pdivio=pdivrow(il1:il2,msgp1a:ilev)
  dimbio=dimbrow(il1:il2,msgp1a:ilev,:)
  dicbio=nint(dicbrow(il1:il2,msgp1a:ilev,:))
  !
  revbio=revbrow(il1:il2,msgp1a:ilev,:)
  previo=prevrow(il1:il2,msgp1a:ilev,:)
  rembio=rembrow(il1:il2,msgp1a:ilev,:,:)
  recbio=nint(recbrow(il1:il2,msgp1a:ilev,:,:))
  rivbio=rivbrow(il1:il2,msgp1a:ilev)
  privio=privrow(il1:il2,msgp1a:ilev)
  rimbio=rimbrow(il1:il2,msgp1a:ilev,:)
  ricbio=nint(ricbrow(il1:il2,msgp1a:ilev,:))
  !
  !-----------------------------------------------------------------------
  !     aerosol microphysical and chemical calculations.
  !
  call pam(trac,trrat, &
               zha,zfa,pha,pfa,dpa,ta,rva,qtna,hmna,rha,rhta,wcta, &
               spmrep,szmlwc,spfsnow,spfrain,szclf,sclrfr,sclrfs, &
               spfevap,spfsubl,sgpr,soap, &
               docem1s,docem2s,docem3s,docem4s, &
               dbcem1s,dbcem2s,dbcem3s,dbcem4s, &
               dsuem1s,dsuem2s,dsuem3s,dsuem4s, &
               xo3,xna,xam,xnh3,fcana,pblts,smfrdu,fcsa,fgsa,fca, &
               fga,zspddd,zspdss,zspddu,cdmla,cdmnla,ustard, &
               flnddu,focndu,sicndu,gtrdu,fndu, &
               spot,st02,st03,st04,st06,st13,st14,st15, &
               st16,st17,suz0,slai,ataus,zfsa,pfsa, &
               oednio,oercio,oidnio,oircio,svvbio,psvvio,svmbio, &
               svcbio,pnvbio,pnmbio,pncbio,psvbio,psmbio,pscbio, &
               onvbio,onmbio, &
               oncbio,osvbio,osmbio,oscbio,ogvbio,ogmbio,ogcbio, &
               devbio,pdevio,dembio,decbio,divbio,pdivio,dimbio, &
               dicbio,revbio,previo,rembio,recbio,rivbio,privio, &
               rimbio,ricbio, &
               cdnc,bcic,acas,acoa,acbc,acss,acmd, &
               corn,corm,rsn1,rsm1,rsn2,rsm2,rsn3,rsm3,sncn,ssun, &
               scnd,sgsp,ccn,ccne,cc02,cc04,cc08,cc16,cnt,cn20, &
               cn50,cn100,cn200,cntw,cn20w,cn50w,cn100w,cn200w, &
               rcri,sups,vrn1,vrm1,vrn2,vrm2,vrn3,vrm3,vncn,vasn, &
               vcnd,vgsp,defx,defn,voae,vbce,vase,vmde,vsse,voaw, &
               vbcw,vasw,vmdw,vssw,voad,vbcd,vasd,vmdd,vssd,voag, &
               vbcg,vasg,vmdg,vssg,vasi,vas1,vas2,vas3,henr,o3frc, &
               h2o2frc,vccn,vcne,dmac,dmco,dnac,dnco,duwd,dust, &
               duth,fall,fa10,fa2,fa1,usmk,defa,defc,dt, &
               sicn_crt,co2,icana,leva,ilga,knt,isvdust &
#if defined (GM_PAM)
               ,busvol, busvol_size &
#endif
               )
  !
  !-----------------------------------------------------------------------
  !     copy aerosol tracer fields.
  !
  do ns=1,ntracs
    n=iaind(ns)
    xrow   (il1:il2,msgp2a:ilevp1,n) = trac (1:ilga,1:leva,ns)
    sfrcrol(il1:il2,msgp1a:ilev  ,n) = trrat(1:ilga,1:leva,ns)
  end do
  !
  !     copy gas tracer fields.
  !
  xrow   (il1:il2,msgp2a:ilevp1,igs6)=trac (1:ilga,1:leva,jgs6)
  xrow   (il1:il2,msgp2a:ilevp1,igsp)=trac (1:ilga,1:leva,jgsp)
  xrow   (il1:il2,msgp2a:ilevp1,iso2)=trac (1:ilga,1:leva,jso2)
  xrow   (il1:il2,msgp2a:ilevp1,ihpo)=trac (1:ilga,1:leva,jhpo)
  sfrcrol(il1:il2,msgp1a:ilev  ,igs6)=trrat(1:ilga,1:leva,jgs6)
  sfrcrol(il1:il2,msgp1a:ilev  ,igsp)=trrat(1:ilga,1:leva,jgsp)
  sfrcrol(il1:il2,msgp1a:ilev  ,iso2)=trrat(1:ilga,1:leva,jso2)
  sfrcrol(il1:il2,msgp1a:ilev  ,ihpo)=trrat(1:ilga,1:leva,jhpo)
  !
  !     make sure concentrations are sufficiently large for further
  !     calculations (hybrid variable).
  !
  if (ntracs > 0) then
    do n=iaind(1),iaind(ntracs)
      xrow(il1:il2,msgp2a:ilevp1,n)=max(xrow(il1:il2,msgp2a:ilevp1,n),tmin(n))
    end do
  end if
  xrow(il1:il2,msgp2a:ilevp1,igs6)=max(xrow(il1:il2,msgp2a:ilevp1,igs6),tmin(igs6))
  xrow(il1:il2,msgp2a:ilevp1,igsp)=max(xrow(il1:il2,msgp2a:ilevp1,igsp),tmin(igsp))
  xrow(il1:il2,msgp2a:ilevp1,iso2)=max(xrow(il1:il2,msgp2a:ilevp1,iso2),tmin(iso2))
  xrow(il1:il2,msgp2a:ilevp1,ihpo)=max(xrow(il1:il2,msgp2a:ilevp1,ihpo),tmin(ihpo))
  !
  !     pam internal input/output fields that are required
  !     for averaging and distributing tendencies from subroutines
  !     that are not called every time step.
  !
  oednrow(il1:il2,msgp1a:ilev,:)=oednio
  oercrow(il1:il2,msgp1a:ilev,:)=oercio
  oidnrow(il1:il2,msgp1a:ilev)  =oidnio
  oircrow(il1:il2,msgp1a:ilev)  =oircio
  svvbrow(il1:il2,msgp1a:ilev)  =svvbio
  psvvrow(il1:il2,msgp1a:ilev)  =psvvio
  svmbrow(il1:il2,msgp1a:ilev,:)=svmbio
  svcbrow(il1:il2,msgp1a:ilev,:)=real(svcbio)
  !
  pnvbrow(il1:il2,msgp1a:ilev,:)    =pnvbio
  pnmbrow(il1:il2,msgp1a:ilev,:,:)  =pnmbio
  pncbrow(il1:il2,msgp1a:ilev,:,:)  =real(pncbio)
  psvbrow(il1:il2,msgp1a:ilev,:,:)  =psvbio
  psmbrow(il1:il2,msgp1a:ilev,:,:,:)=psmbio
  pscbrow(il1:il2,msgp1a:ilev,:,:,:)=real(pscbio)
  !
  onvbrow(il1:il2,msgp1a:ilev,:)    =onvbio
  onmbrow(il1:il2,msgp1a:ilev,:,:)  =onmbio
  oncbrow(il1:il2,msgp1a:ilev,:,:)  =real(oncbio)
  osvbrow(il1:il2,msgp1a:ilev,:,:)  =osvbio
  osmbrow(il1:il2,msgp1a:ilev,:,:,:)=osmbio
  oscbrow(il1:il2,msgp1a:ilev,:,:,:)=real(oscbio)
  ogvbrow(il1:il2,msgp1a:ilev)      =ogvbio
  ogmbrow(il1:il2,msgp1a:ilev,:)    =ogmbio
  ogcbrow(il1:il2,msgp1a:ilev,:)    =real(ogcbio)
  !
  devbrow(il1:il2,msgp1a:ilev,:)    =devbio
  pdevrow(il1:il2,msgp1a:ilev,:)    =pdevio
  dembrow(il1:il2,msgp1a:ilev,:,:)  =dembio
  decbrow(il1:il2,msgp1a:ilev,:,:)  =real(decbio)
  divbrow(il1:il2,msgp1a:ilev)      =divbio
  pdivrow(il1:il2,msgp1a:ilev)      =pdivio
  dimbrow(il1:il2,msgp1a:ilev,:)    =dimbio
  dicbrow(il1:il2,msgp1a:ilev,:)    =real(dicbio)
  !
  revbrow(il1:il2,msgp1a:ilev,:)    =revbio
  prevrow(il1:il2,msgp1a:ilev,:)    =previo
  rembrow(il1:il2,msgp1a:ilev,:,:)  =rembio
  recbrow(il1:il2,msgp1a:ilev,:,:)  =real(recbio)
  rivbrow(il1:il2,msgp1a:ilev)      =rivbio
  privrow(il1:il2,msgp1a:ilev)      =privio
  rimbrow(il1:il2,msgp1a:ilev,:)    =rimbio
  ricbrow(il1:il2,msgp1a:ilev,:)    =real(ricbio)
  !
  !     results that are used for calculations of cloud
  !     processes in the driving model.
  !
  zcdnrow(il1:il2,msgp1a:ilev)=cdnc
  !! !!
  ! id  oaerrad
  !      bcicrow(il1:il2,msgp1a:ilev)=0.
  bcicrow(il1:il2,msgp1a:ilev)=bcic
  depbrol(il1:il2)=depbrol(il1:il2)-min(vbcw,0.)-min(vbcd,0.)
  !
  !     the same fields as in the previous section but for the
  !     region that is outside of the pam domain.
  !
  if (leva < ilev) then
    do l=1,msgp1a-1
      zcdnrow(il1:il2,l)=cdnc(1:ilga,1)
      !! !!
      ! id  oaerrad
      !          bcicrow(il1:il2,l)=0.
      bcicrow(il1:il2,l)=bcic(1:ilga,1)
    end do
  end if
  !
  if (isvdust > 0) then
    duwdrow(il1:il2)=duwdrow(il1:il2)+duwd*saverad
    dustrow(il1:il2)=dustrow(il1:il2)+dust*saverad
    duthrow(il1:il2)=duthrow(il1:il2)+duth*saverad
    fallrow(il1:il2)=fallrow(il1:il2)+fall*saverad
    fa10row(il1:il2)=fa10row(il1:il2)+fa10*saverad
    fa2row (il1:il2)=fa2row (il1:il2)+fa2 *saverad
    fa1row (il1:il2)=fa1row (il1:il2)+fa1 *saverad
    usmkrow(il1:il2)=usmkrow(il1:il2)+usmk*saverad
    defarow(il1:il2)=defarow(il1:il2)+defa*saverad
    defcrow(il1:il2)=defcrow(il1:il2)+defc*saverad
  end if
  !
  if (kxtra1) then
    ssunrow(il1:il2,msgp1a:ilev)=ssunrow(il1:il2,msgp1a:ilev)+ssun*saverad
    vasnrow(il1:il2)=vasnrow(il1:il2)+vasn*saverad
    vncnrow(il1:il2)=vncnrow(il1:il2)+vncn*saverad
    vscdrow(il1:il2)=vscdrow(il1:il2)+vcnd*saverad
    vgsprow(il1:il2)=vgsprow(il1:il2)+vgsp*saverad
    sncnrow(il1:il2,msgp1a:ilev)=sncn
    scndrow(il1:il2,msgp1a:ilev)=scnd
    sgsprow(il1:il2,msgp1a:ilev)=sgsp
    acasrow(il1:il2,msgp1a:ilev)=acasrow(il1:il2,msgp1a:ilev)+acas*saverad
    acoarow(il1:il2,msgp1a:ilev)=acoarow(il1:il2,msgp1a:ilev)+acoa*saverad
    acbcrow(il1:il2,msgp1a:ilev)=acbcrow(il1:il2,msgp1a:ilev)+acbc*saverad
    acssrow(il1:il2,msgp1a:ilev)=acssrow(il1:il2,msgp1a:ilev)+acss*saverad
    acmdrow(il1:il2,msgp1a:ilev)=acmdrow(il1:il2,msgp1a:ilev)+acmd*saverad
    ntrow  (il1:il2,msgp1a:ilev)=  ntrow(il1:il2,msgp1a:ilev)+  cnt*saverad
    n20row (il1:il2,msgp1a:ilev)= n20row(il1:il2,msgp1a:ilev)+ cn20*saverad
    n50row (il1:il2,msgp1a:ilev)= n50row(il1:il2,msgp1a:ilev)+ cn50*saverad
    n100row(il1:il2,msgp1a:ilev)=n100row(il1:il2,msgp1a:ilev)+cn100*saverad
    n200row(il1:il2,msgp1a:ilev)=n200row(il1:il2,msgp1a:ilev)+cn200*saverad
    wtrow  (il1:il2,msgp1a:ilev)=  wtrow(il1:il2,msgp1a:ilev)+  cntw*saverad
    w20row (il1:il2,msgp1a:ilev)= w20row(il1:il2,msgp1a:ilev)+ cn20w*saverad
    w50row (il1:il2,msgp1a:ilev)= w50row(il1:il2,msgp1a:ilev)+ cn50w*saverad
    w100row(il1:il2,msgp1a:ilev)=w100row(il1:il2,msgp1a:ilev)+cn100w*saverad
    w200row(il1:il2,msgp1a:ilev)=w200row(il1:il2,msgp1a:ilev)+cn200w*saverad
    ccnrow (il1:il2,msgp1a:ilev)=ccn
    ccnerow(il1:il2,msgp1a:ilev)=ccne
    cc02row(il1:il2,msgp1a:ilev)=cc02row(il1:il2,msgp1a:ilev)+cc02*saverad
    cc04row(il1:il2,msgp1a:ilev)=cc04row(il1:il2,msgp1a:ilev)+cc04*saverad
    cc08row(il1:il2,msgp1a:ilev)=cc08row(il1:il2,msgp1a:ilev)+cc08*saverad
    cc16row(il1:il2,msgp1a:ilev)=cc16row(il1:il2,msgp1a:ilev)+cc16*saverad
    rcrirow(il1:il2,msgp1a:ilev)=rcri
    supsrow(il1:il2,msgp1a:ilev)=sups
    henrrow(il1:il2,msgp1a:ilev)=henr
    o3frow(il1:il2,msgp1a:ilev)=o3frc
    h2o2frow(il1:il2,msgp1a:ilev)=h2o2frc
    wparrow(il1:il2,msgp1a:ilev)=wcta
    voaerow(il1:il2)=voaerow(il1:il2)+voae*saverad
    vbcerow(il1:il2)=vbcerow(il1:il2)+vbce*saverad
    vaserow(il1:il2)=vaserow(il1:il2)+vase*saverad
    vmderow(il1:il2)=vmderow(il1:il2)+vmde*saverad
    vsserow(il1:il2)=vsserow(il1:il2)+vsse*saverad
    voawrow(il1:il2)=voawrow(il1:il2)+voaw*saverad
    vbcwrow(il1:il2)=vbcwrow(il1:il2)+vbcw*saverad
    vaswrow(il1:il2)=vaswrow(il1:il2)+vasw*saverad
    vmdwrow(il1:il2)=vmdwrow(il1:il2)+vmdw*saverad
    vsswrow(il1:il2)=vsswrow(il1:il2)+vssw*saverad
    voadrow(il1:il2)=voadrow(il1:il2)+voad*saverad
    vbcdrow(il1:il2)=vbcdrow(il1:il2)+vbcd*saverad
    vasdrow(il1:il2)=vasdrow(il1:il2)+vasd*saverad
    vmddrow(il1:il2)=vmddrow(il1:il2)+vmdd*saverad
    vssdrow(il1:il2)=vssdrow(il1:il2)+vssd*saverad
    voagrow(il1:il2)=voagrow(il1:il2)+voag*saverad
    vbcgrow(il1:il2)=vbcgrow(il1:il2)+vbcg*saverad
    vasgrow(il1:il2)=vasgrow(il1:il2)+vasg*saverad
    vmdgrow(il1:il2)=vmdgrow(il1:il2)+vmdg*saverad
    vssgrow(il1:il2)=vssgrow(il1:il2)+vssg*saverad
    vasirow(il1:il2)=vasirow(il1:il2)+vasi*saverad
    vas1row(il1:il2)=vas1row(il1:il2)+vas1*saverad
    vas2row(il1:il2)=vas2row(il1:il2)+vas2*saverad
    vas3row(il1:il2)=vas3row(il1:il2)+vas3*saverad
    vccnrow(il1:il2)=vccn
    vcnerow(il1:il2)=vcne
    !
    !       the same fields as in the previous section but for the
    !       region that is outside of the pam domain.
    !
    if (leva < ilev) then
      do l=1,msgp1a-1
        ssunrow(il1:il2,l)=0.
        sncnrow(il1:il2,l)=0.
        scndrow(il1:il2,l)=0.
        sgsprow(il1:il2,l)=0.
        acasrow(il1:il2,l)=0.
        acoarow(il1:il2,l)=0.
        acbcrow(il1:il2,l)=0.
        acssrow(il1:il2,l)=0.
        acmdrow(il1:il2,l)=0.
        ntrow  (il1:il2,l)=0.
        n20row (il1:il2,l)=0.
        n50row (il1:il2,l)=0.
        n100row(il1:il2,l)=0.
        n200row(il1:il2,l)=0.
        wtrow  (il1:il2,l)=0.
        w20row (il1:il2,l)=0.
        w50row (il1:il2,l)=0.
        w100row(il1:il2,l)=0.
        w200row(il1:il2,l)=0.
        ccnrow (il1:il2,l)=0.
        ccnerow(il1:il2,l)=0.
        cc02row(il1:il2,l)=0.
        cc04row(il1:il2,l)=0.
        cc08row(il1:il2,l)=0.
        cc16row(il1:il2,l)=0.
        rcrirow(il1:il2,l)=0.
        supsrow(il1:il2,l)=0.
        henrrow(il1:il2,l)=0.
        o3frow (il1:il2,l)=0.
        h2o2frow(il1:il2,l)=0.
        wparrow(il1:il2,l)=0.
      end do
    end if
  end if
  if (kxtra2) then
    !
    !       check if sufficient number of output fields available for
    !       pla diagnostic output fields.
    !
    cornrow(il1:il2,msgp1a:ilev)=cornrow(il1:il2,msgp1a:ilev) &
                                   +corn*saverad
    cormrow(il1:il2,msgp1a:ilev)=cormrow(il1:il2,msgp1a:ilev) &
                                   +corm*saverad
    rsn1row(il1:il2,msgp1a:ilev)=rsn1row(il1:il2,msgp1a:ilev) &
                                   +rsn1*saverad
    rsm1row(il1:il2,msgp1a:ilev)=rsm1row(il1:il2,msgp1a:ilev) &
                                   +rsm1*saverad
    rsn2row(il1:il2,msgp1a:ilev)=rsn2row(il1:il2,msgp1a:ilev) &
                                   +rsn2*saverad
    rsm2row(il1:il2,msgp1a:ilev)=rsm2row(il1:il2,msgp1a:ilev) &
                                   +rsm2*saverad
    vrn1row(il1:il2)=vrn1row(il1:il2)+vrn1*saverad
    vrm1row(il1:il2)=vrm1row(il1:il2)+vrm1*saverad
    vrn2row(il1:il2)=vrn2row(il1:il2)+vrn2*saverad
    vrm2row(il1:il2)=vrm2row(il1:il2)+vrm2*saverad
    rsn3row(il1:il2,msgp1a:ilev)=rsn3row(il1:il2,msgp1a:ilev) &
                                     +rsn3*saverad
    rsm3row(il1:il2,msgp1a:ilev)=rsm3row(il1:il2,msgp1a:ilev) &
                                     +rsm3*saverad
    vrn3row(il1:il2)=vrn3row(il1:il2)+vrn3*saverad
    vrm3row(il1:il2)=vrm3row(il1:il2)+vrm3*saverad
    defxrow(il1:il2,:)=defxrow(il1:il2,:)+defx(:,:)*saverad
    defnrow(il1:il2,:)=defnrow(il1:il2,:)+defn(:,:)*saverad
    dmacrow(il1:il2,:)=dmacrow(il1:il2,:) &
                                    +dmac(:,:)*saverad
    dmcorow(il1:il2,:)=dmcorow(il1:il2,:) &
                                    +dmco(:,:)*saverad
    dnacrow(il1:il2,:)=dnacrow(il1:il2,:) &
                                    +dnac(:,:)*saverad
    dncorow(il1:il2,:)=dncorow(il1:il2,:) &
                                    +dnco(:,:)*saverad
    !
    !       the same fields as in the previous section but for the
    !       region that is outside of the pam domain.
    !
    if (leva < ilev) then
      do l=1,msgp1a-1
        cornrow(il1:il2,l)=0.
        cormrow(il1:il2,l)=0.
        rsn1row(il1:il2,l)=0.
        rsm1row(il1:il2,l)=0.
        rsn2row(il1:il2,l)=0.
        rsm2row(il1:il2,l)=0.
        rsn3row(il1:il2,l)=0.
        rsm3row(il1:il2,l)=0.
      end do
    end if
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate memory for tracer arrays.
  !
  deallocate(trac)
  deallocate(trrat)
  !
  !     * deallocate memory for other input/output arrays.
  !
  deallocate(zha)
  deallocate(zfa)
  deallocate(pha)
  deallocate(pfa)
  deallocate(dpa)
  deallocate(ta)
  deallocate(rva)
  deallocate(qtna)
  deallocate(hmna)
  deallocate(rha)
  deallocate(rhta)
  deallocate(wcta)
  deallocate(spmrep)
  deallocate(szmlwc)
  deallocate(spfsnow)
  deallocate(spfrain)
  deallocate(szclf)
  deallocate(sclrfr)
  deallocate(sclrfs)
  deallocate(spfevap)
  deallocate(spfsubl)
  deallocate(sgpr)
  deallocate(soap)
  deallocate(docem1s)
  deallocate(docem2s)
  deallocate(docem3s)
  deallocate(docem4s)
  deallocate(dbcem1s)
  deallocate(dbcem2s)
  deallocate(dbcem3s)
  deallocate(dbcem4s)
  deallocate(dsuem1s)
  deallocate(dsuem2s)
  deallocate(dsuem3s)
  deallocate(dsuem4s)
  deallocate(xo3)
  deallocate(xna)
  deallocate(xam)
  deallocate(xnh3)
  deallocate(fcana)
  deallocate(ataus)
  deallocate(smfrdu)
  deallocate(fcsa)
  deallocate(fgsa)
  deallocate(fca)
  deallocate(fga)
  deallocate(zspddd)
  deallocate(zspdss)
  deallocate(zspddu)
  deallocate(cdmla)
  deallocate(cdmnla)
  deallocate(ustard)
  deallocate(pblts)
  deallocate(flnddu)
  deallocate(focndu)
  deallocate(sicndu)
  deallocate(gtrdu)
  deallocate(fndu)
  deallocate(zfsa)
  deallocate(pfsa)
  !
  deallocate(spot)
  deallocate(st02)
  deallocate(st03)
  deallocate(st04)
  deallocate(st06)
  deallocate(st13)
  deallocate(st14)
  deallocate(st15)
  deallocate(st16)
  deallocate(st17)
  deallocate(suz0)
  deallocate(slai)
  !
  deallocate(oednio)
  deallocate(oercio)
  deallocate(oidnio)
  deallocate(oircio)
  deallocate(svvbio)
  deallocate(psvvio)
  deallocate(svmbio)
  deallocate(svcbio)
  deallocate(pnvbio)
  deallocate(pnmbio)
  deallocate(pncbio)
  deallocate(psvbio)
  deallocate(psmbio)
  deallocate(pscbio)
  deallocate(onvbio)
  deallocate(onmbio)
  deallocate(oncbio)
  deallocate(osvbio)
  deallocate(osmbio)
  deallocate(oscbio)
  deallocate(ogvbio)
  deallocate(ogmbio)
  deallocate(ogcbio)
  deallocate(devbio)
  deallocate(pdevio)
  deallocate(dembio)
  deallocate(decbio)
  deallocate(divbio)
  deallocate(pdivio)
  deallocate(dimbio)
  deallocate(dicbio)
  deallocate(revbio)
  deallocate(previo)
  deallocate(rembio)
  deallocate(recbio)
  deallocate(rivbio)
  deallocate(privio)
  deallocate(rimbio)
  deallocate(ricbio)
  deallocate(cdnc)
  deallocate(bcic)
  !
  deallocate(corn)
  deallocate(corm)
  deallocate(rsn1)
  deallocate(rsm1)
  deallocate(rsn2)
  deallocate(rsm2)
  deallocate(rsn3)
  deallocate(rsm3)
  deallocate(sncn)
  deallocate(ssun)
  deallocate(scnd)
  deallocate(sgsp)
  deallocate(acas)
  deallocate(acoa)
  deallocate(acbc)
  deallocate(acss)
  deallocate(acmd)
  deallocate(cnt)
  deallocate(cn20)
  deallocate(cn50)
  deallocate(cn100)
  deallocate(cn200)
  deallocate(cntw)
  deallocate(cn20w)
  deallocate(cn50w)
  deallocate(cn100w)
  deallocate(cn200w)
  deallocate(ccn)
  deallocate(ccne)
  deallocate(cc02)
  deallocate(cc04)
  deallocate(cc08)
  deallocate(cc16)
  deallocate(rcri)
  deallocate(sups)
  deallocate(henr)
  deallocate(o3frc)
  deallocate(h2o2frc)
  deallocate(vrn1)
  deallocate(vrm1)
  deallocate(vrn2)
  deallocate(vrm2)
  deallocate(vrn3)
  deallocate(vrm3)
  deallocate(vncn)
  deallocate(vasn)
  deallocate(vcnd)
  deallocate(vgsp)
  deallocate(defx)
  deallocate(defn)
  deallocate(voae)
  deallocate(vbce)
  deallocate(vase)
  deallocate(vmde)
  deallocate(vsse)
  deallocate(voaw)
  deallocate(vbcw)
  deallocate(vasw)
  deallocate(vmdw)
  deallocate(vssw)
  deallocate(voad)
  deallocate(vbcd)
  deallocate(vasd)
  deallocate(vmdd)
  deallocate(vssd)
  deallocate(voag)
  deallocate(vbcg)
  deallocate(vasg)
  deallocate(vmdg)
  deallocate(vssg)
  deallocate(vasi)
  deallocate(vas1)
  deallocate(vas2)
  deallocate(vas3)
  deallocate(vccn)
  deallocate(vcne)
  deallocate(dmac)
  deallocate(dmco)
  deallocate(dnac)
  deallocate(dnco)
  deallocate(duwd)
  deallocate(dust)
  deallocate(duth)
  deallocate(fall)
  deallocate(fa10)
  deallocate(fa2)
  deallocate(fa1)
  deallocate(usmk)
  deallocate(defa)
  deallocate(defc)
  !
end subroutine pamdriv
!
!>    \file
!!    \subsection ssec_details Details
!!    List of variables (Type, I: INPUT, O: OUTPUT, IO: IN+OUTPUT)
!!    |NAME     | IN/OUT   | TYPE DESCRIPTION (UNITS)                                 |
!!    |:--------|:---------|:---------------------------------------------------------|
!!    |OEDNROW  |IO        |PLA INTERNAL ARRAY|
!!    |OERCROW  |IO        |PLA INTERNAL ARRAY|
!!    |OIDNROW  |IO        |PLA INTERNAL ARRAY|
!!    |OIRCROW  |IO        |PLA INTERNAL ARRAY|
!!    |SVVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PSVVROW  |IO        |PLA INTERNAL ARRAY|
!!    |SVMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |SVCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PNVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PNMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PNCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PSVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PSMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PSCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QNVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QNMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QNCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QSVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QSMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QSCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QGVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QGMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QGCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QDVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QDMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |QDCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |ONVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |ONMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |ONCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OSVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OSMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OSCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OGVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OGMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |OGCBROW  |IO        |PLA INTERNAL ARRAY|
!!    |DEVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PDEVROW  |IO        |PLA INTERNAL ARRAY|
!!    |DEMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |DECBROW  |IO        |PLA INTERNAL ARRAY|
!!    |DIVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PDIVROW  |IO        |PLA INTERNAL ARRAY|
!!    |DIMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |DICBROW  |IO        |PLA INTERNAL ARRAY|
!!    |REVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PREVROW  |IO        |PLA INTERNAL ARRAY|
!!    |REMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |RECBROW  |IO        |PLA INTERNAL ARRAY|
!!    |RIVBROW  |IO        |PLA INTERNAL ARRAY|
!!    |PRIVROW  |IO        |PLA INTERNAL ARRAY|
!!    |RIMBROW  |IO        |PLA INTERNAL ARRAY|
!!    |RICBROW  |IO        |PLA INTERNAL ARRAY|
!!    |XROW     |IO        |GRID-CELL MEAN TRACER MASS MIXING RATIO (KG/KG)|
!!    |SFRCROL  |IO        |PLA INTERNAL ARRAY|
!!    |IAIND    |I         |AEROSOL TRACER INDEX ARRAY|
!!    |ILG      |I         |HORIZONTAL (LAT/LON) GRID CELL INDEX|
!!    |ILEVP1   |I         |NUMBER OF VERTICAL GRID CELLS|
!!    |NTRAC    |I         |NUMBER OF TRACERS|
!!    |MSGP2    |I         |INDEX OF HIGHEST VERTICAL GRID CELL|
!!    |IL1      |I         |START INDEX OF FIRST HORIZONTAL GRID CELL|
!!    |IL2      |I         |INDEX OF LAST HORIZONTAL GRID CELL|
!!    |ISO2     |I         |TRACER INDEX FOR SULPHUR DIOXIDE GAS|
!!    |IHPO     |I         |TRACER INDEX FOR HYDROGEN PEROXIDE GAS|
!!    |IGS6     |I         |TRACER INDEX FOR SULPHURIC ACID GAS|
!!    |IGSP     |I         |TRACER INDEX FOR SECONDARY ORGANIC AEROSOL PRECURSOR GAS|
!!    |NTRACP   |I         |NUMBER OF AEROSOL TRACERS|
!!    |ILEV     |I         |NUMBER OF VERTICAL GRID CELLS|
!!    |ICAN     |I         |NUMBER OF LAND SURFACE SCHEME CANOPY TYPES|
!!    |MSGP1    |I         |CURRENTLY UNUSED PARAMETER|
!!    |ZH       |I         |HEIGHT OF GRID CELL CENTRES|
!!    |ZF       |I         |HEIGHT OF GRID CELL INTERFACES (M)|
!!    |PH       |I         |PRESSURE OF GRID CELL CENTRES (PA)|
!!    |PF       |I         |PRESSURE OF GRID CELL INTERFACES (PA)|
!!    |DP       |I         |DEPTH OF GRID CELLS (PA)|
!!    |THROW    |I         |GRID-CELL MEAN TEMPERATURE OF AIR (K)|
!!    |QROW     |I         |GRID-CELL MEAN WATER VAPOUR MASS MIXING RATIO (KG/KG)|
!!    |QTN      |I         |TOTAL WATER MASS MIXING RATIO IN CLOUD UPDRAFT (KG/KG)|
!!    |HMN      |I         |CLOUD WATER STATIC ENERGY IN CLOUD UPDRAFT (J/KG)|
!!    |RHC      |I         |RELATIVE HUMIDITY FOR CLEAR SKY PORTION OF GRID CELL|
!!    |RHT      |I         |GRID-CELL MEAN RELATIVE HUMIDITY|
!!    |WG       |I         |GRID-CELL MEAN VERTICAL VELOCITY (M/SEC)|
!!    |WSUB     |I         |SUBGRID-SCALE COMPONENT OF VERTICAL VELOCITY IN CLOUD UPDRAFT (M/SEC)|
!!    |ZMRATEP  |I         |AUTOCONVERSION + ACCRETION RATE IN CLOUDY PORTION OF GRID CELL|
!!    |ZMLWC    |I         |CLOUD LIQUID WATER MASS MIXING RATIO IN CLOUDY PORTION OF GRID CELL (KG/KG)|
!!    |ZFSNOW   |I         |GRID-CELL MEAN SNOWFALL RATE (KG/M2/SEC), AT BOTTOM OF EACH GRID CELL|
!!    |ZFRAIN   |I         |GRID-CELL MEAN RAINFALL RATE (KG/M2/SEC), AT BOTTOM OF EACH GRID CELL|
!!    |ZCLF     |I         |CLOUD FRACTION|
!!    |CLRFR    |I         |RAIN FRACTION (CLEAR-SKY PART OF GRID CELL THAT CONTAINS RAIN)|
!!    |CLRFS    |I         |SNOW FRACTION (CLEAR-SKY PART OF GRID CELL THAT CONTAINS SNOW)|
!!    |ZFEVAP   |I         |FRACTION OF RAIN THAT EVAPORATES IN A MODEL TIME STEP|
!!    |ZFSUBL   |I         |FRACTION OF SNOW THAT SUBLIMATES IN A MODEL TIME STEP|
!!    |PBLTROW  |I         |VERTICAL INDEX OF GRID CELL IMMEDIATELY BELOW THE PLANETARY BOUNDARY LAYER|
!!    |SMFRAC   |I         |SOIL MOISTURE FRACTION|
!!    |FCS      |I         |FRACTION OF CANOPY OVER SNOW ON GROUND IN A GRID CELL|
!!    |FGS      |I         |FRACTION OF SNOW ON BARE GROUND IN A GRID CELL|
!!    |FC       |I         |FRACTION OF SNOW-FREE CANOPY IN A GRID CELL|
!!    |FG       |I         |FRACTION OF SNOW-FREE BARE GROUND IN A GRID CELL|
!!    |ZSPD     |I         |MODEL SURFACE HORIZONTAL WIND SPEED (M/SEC)|
!!    |ZSPDSO   |I         |MODEL SURFACE HORIZONTAL WIND SPEED OVER OPEN OCEAN (M/SEC)|
!!    |ZSPDSBS  |I         |MODEL SURFACE HORIZONTAL WIND SPEED OVER BARE GROUND (M/SEC)|
!!    |CDML     |I         |SURFACE DRAG COEFFICIENT OVER LAND (M2/SEC2)|
!!    |CDMNL    |I         |SURFACE DRAG COEFFICIENT OVER WATER, ICE, AND OPEN OCEAN (M2/SEC2)|
!!    |USTARBS  |I         |FRICTION VELOCITY OVER BARE GROUND (M/SEC)|
!!    |FLND     |I         |FRACTION OF LAND IN A GRID CELL|
!!    |FOCN     |I         |FRACTION OF OPEN OCEAN IN A GRID CELL|
!!    |SICN     |I         |SEA ICE FRACTION|
!!    |GTROW    |I         |TEMPERATURE IN BARE GROUND (K)|
!!    |FNROL    |I         |FRACTION OF SNOW ON GROUND|
!!    |FCANROW  |I         |FRACTION OF CANOPY TYPE (1: TALL CONIFEROUS,  2: TALL BROADLEAF, 3: ARABLE AND CROPS, 4: GRASS, SWAMP AND TUNDRA)|
!!    |SPOTROW  |I         |FRACTION OF PALEO LAKE BEDS|
!!    |ST02ROW  |I         |FRACTION OF BARE GROUND WITH MEDIUM GRAIN SIZE|
!!    |ST03ROW  |I         |FRACTION OF BARE GROUND WITH FINE GRAIN SIZE|
!!    |ST04ROW  |I         |FRACTION OF BARE GROUND WITH COARSE TO MEDIUM GRAIN SIZE|
!!    |ST06ROW  |I         |FRACTION OF BARE GROUND WITH FINE TO MEDIUM GRAIN SIZE|
!!    |ST13ROW  |I         |FRACTION OF BARE GROUND FOR ASIAN TAKLIMAKAN|
!!    |ST14ROW  |I         |FRACTION OF BARE GROUND FOR ASIAN LOESS|
!!    |ST15ROW  |I         |FRACTION OF BARE GROUND FOR ASIAN GOBI|
!!    |ST16ROW  |I         |FRACTION OF BARE GROUND FOR ASIAN DESERT AND SANDLAND|
!!    |ST17ROW  |I         |FRACTION OF BARE GROUND FOR ASIAN OTHER MIXTURE SOIL|
!!    |SUZ0ROW  |I         |AERODYNAMIC ROUGHNESS LENGTH FOR BARE GROUND|
!!    |BSFRAC   |I         |FRACTION OF BARE GROUND, METHOD 1 (DEFAULT)|
!!    |PDSFROW  |I         |FRACTION OF BARE GROUND, METHOD 2|
!!    |ATAU     |I         |AGING TIME SCALE OF HYDROPHOBIC BLACK CARBON (SEC)|
!!    |ZFS      |I         |HEIGHT OF SURFACE (BOTTOM OF ATMOSPHERE) (M)|
!!    |PFS      |I         |PRESSURE AT SURFACE (BOTTOM OF ATMOSPHERE) (PA)|
!!    |SGPP     |I         |SULPHURIC ACID (GAS) CHEMICAL PRODUCTION OR EMISSION RATE (KG OF SULPHUR/KG/SEC)|
!!    |OGPP     |I         |SECONDARY ORGANIC AEROSOL PRECURSOR GAS CHEMICAL PRODUCTION OR EMISSION RATE (KG/KG/SEC)|
!!    |DOCEM1   |I         |ORGANIC AEROSOL EMISSION RATE FOR OPEN VEGETATION AND BIOFUEL BURNING (KG/KG/SEC)|
!!    |DOCEM2   |I         |ORGANIC AEROSOL EMISSION RATE FOR FOSSIL FUEL BURNING (KG/KG/SEC)|
!!    |DOCEM3   |I         |ORGANIC AEROSOL EMISSION RATE FOR AIRCRAFT (KG/KG/SEC)|
!!    |DOCEM4   |I         |ORGANIC AEROSOL EMISSION RATE FOR SHIPPING (KG/KG/SEC)|
!!    |DBCEM1   |I         |BLACK AEROSOL EMISSION RATE FOR OPEN VEGETATION AND BIOFUEL BURNING (KG/KG/SEC)|
!!    |DBCEM2   |I         |BLACK AEROSOL EMISSION RATE FOR FOSSIL FUEL BURNING (KG/KG/SEC)|
!!    |DBCEM3   |I         |BLACK AEROSOL EMISSION RATE FOR AIRCRAFT (KG/KG/SEC)|
!!    |DBCEM4   |I         |BLACK AEROSOL EMISSION RATE FOR SHIPPING (KG/KG/SEC)|
!!    |DSUEM1   |I         |AMMONIUM SULPHATE EMISSION RATE FOR OPEN VEGETATION AND BIOFUEL BURNING (KG/KG/SEC)|
!!    |DSUEM2   |I         |AMMONIUM SULPHATE EMISSION RATE FOR FOSSIL FUEL BURNING (KG/KG/SEC)|
!!    |DSUEM3   |I         |AMMONIUM SULPHATE EMISSION RATE FOR AIRCRAFT (KG/KG/SEC)|
!!    |DSUEM4   |I         |AMMONIUM SULPHATE EMISSION RATE FOR SHIPPING (KG/KG/SEC)|
!!    |O3ROW    |I         |OZONE VOLUME MIXING RATIO (M3/M3)|
!!    |HNO3ROW  |I         |NITRIC ACID VOLUME MIXING RATIO (M3/M3)|
!!    |NH3ROW   |I         |AMMONIA VOLUME MIXING RATIO (M3/M3)|
!!    |NH4ROW   |I         |AMMONIUM VOLUME MIXING RATIO (M3/M3)|
!!    |TIMST    |I         |MODEL TIME STEP|
!!    |CO2_PPM  |I         |CARBON DIOXIDE VOLUME MIXING RATIO (PPM)|
!!    |KOUNT    |I         |TIME STEP NUMBER|
!!    |TMIN     |I         |MINIMUM TRACER MASS MIXING RATIO (KG/KG)|
!!    |ZCDNROW  |O         |CLOUD DROPLET NUMBER CONCENTRATION, HOLE-FILLED AND TIME INTEPOLATED (1/M3)|
!!    |BCICROW  |O         |ACTIVATED BLACK CARBON MASS MIXING RATIO IN CLOUDY PORTION OF GRID CELL (KG/KG)|
!!    |DEPBROL  |IO        |TOTAL (DRY + WET) BLACK CARBON DEPOSITION FLUX (KG/M2/SEC)|
!!    |CORNROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (NUMBER CORRECTION)|
!!    |CORMROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (MASS CORRECTION)|
!!    |RSN1ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (NUMBER RESIDUUM 1)|
!!    |RSM1ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (MASS RESIDUUM 1)|
!!    |VRN1ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED NUMBER RESIDUUM 1)|
!!    |VRM1ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED MASS RESIDUUM 1)|
!!    |RSN2ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (NUMBER RESIDUUM 2)|
!!    |RSM2ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (MASS RESIDUUM 2)|
!!    |VRN2ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED NUMBER RESIDUUM 2)|
!!    |VRM2ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED MASS RESIDUUM 2)|
!!    |RSN3ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (NUMBER RESIDUUM 3)|
!!    |RSM3ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (MASS RESIDUUM 3)|
!!    |VRN3ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED NUMBER RESIDUUM 3)|
!!    |VRM3ROW  |IO        |PLA NUMERICAL DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED MASS RESIDUUM 3)|
!!    |VNCNROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED NUCLEATION RATE FOR AMMONIUM SULPHATE NUMBER) (/M2/SEC)|
!!    |VASNROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED NUCLEATION RATE FOR AMMOINUM SULPHATE MASS) (KG/M2/SEC)|
!!    |VSCDROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED CONDENSATION RATE FOR AMMONIUM SULPHATE MASS) (KG/M2/SEC)|
!!    |VGSPROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-PHASE PRODUCTION RATE OF SULPHURIC ACID) (KG/M2/SEC)|
!!    |SNCNROW  |O         |PLA DIAGNOSTIC ARRAY (NUCLEATION RATE FOR AMMONIUM SULPHATE NUMBER) (/M3/SEC)|
!!    |SSUNROW  |IO        |PLA DIAGNOSTIC ARRAY (NUCLEATION RATE FOR AMMONIUM SULPHATE MASS) (KG/M3/SEC)|
!!    |SCNDROW  |O         |PLA DIAGNOSTIC ARRAY (CONDENSATION RATE FOR AMMONIUM SULPHATE MASS) (KG/M3/SEC)|
!!    |SGSPROW  |O         |PLA DIAGNOSTIC ARRAY (GAS-PHASE PRODUCTION RATE OF SULPHURIC ACID) (KG/M3/SEC)|
!!    |DEFXROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION SIZE DISTRIBUTION FOR MINERAL DUST MASS) (MICRO-G/M3)|
!!    |DEFNROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION SIZE DISTRIBUTION FOR MINERAL DUST NUMBER) (/CM3)|
!!    |ACASROW  |IO        |PLA DIAGNOSTIC ARRAY (AMMONIUM SULPHATE MASS MIXING RATIO) (KG/KG)|
!!    |ACOAROW  |IO        |PLA DIAGNOSTIC ARRAY (ORGANIC AEROSOL MASS MIXING RATIO) (KG/KG)|
!!    |ACBCROW  |IO        |PLA DIAGNOSTIC ARRAY (BLACK CARBON MASS MIXING RATIO) (KG/KG)|
!!    |ACSSROW  |IO        |PLA DIAGNOSTIC ARRAY (SEA SALT MASS MIXING RATIO) (KG/KG)|
!!    |ACMDROW  |IO        |PLA DIAGNOSTIC ARRAY (MINERAL DUST MASS MIXING RATIO) (KG/KG)|
!!    |NTROW    |IO        |PLA DIAGNOSTIC ARRAY (AEROSOL NUMBER CONCENTRATION) (/CM3)|
!!    |N20ROW   |IO        |PLA DIAGNOSTIC ARRAY (AEROSOL NUMBER CONCENTRATION, DIAMETER > 20NM) (/CM3)|
!!    |N50ROW   |IO        |PLA DIAGNOSTIC ARRAY (AEROSOL NUMBER CONCENTRATION, DIAMETER > 50NM) (/CM3)|
!!    |N100ROW  |IO        |PLA DIAGNOSTIC ARRAY (AEROSOL NUMBER CONCENTRATION, DIAMETER > 100NM) (/CM3)|
!!    |N200ROW  |IO        |PLA DIAGNOSTIC ARRAY (AEROSOL NUMBER CONCENTRATION, DIAMETER > 200NM) (/CM3)|
!!    |WTROW    |IO        |PLA DIAGNOSTIC ARRAY (WET AEROSOL NUMBER CONCENTRATION) (/CM3)|
!!    |W20ROW   |IO        |PLA DIAGNOSTIC ARRAY (WET AEROSOL NUMBER CONCENTRATION, DIAMETER > 20NM) (/CM3)|
!!    |W50ROW   |IO        |PLA DIAGNOSTIC ARRAY (WET AEROSOL NUMBER CONCENTRATION, DIAMETER > 50NM) (/CM3)|
!!    |W100ROW  |IO        |PLA DIAGNOSTIC ARRAY (WET AEROSOL NUMBER CONCENTRATION, DIAMETER > 100NM) (/CM3)|
!!    |W200ROW  |IO        |PLA DIAGNOSTIC ARRAY (WET AEROSOL NUMBER CONCENTRATION, DIAMETER > 200NM) (/CM3)|
!!    |CCNROW   |O         |PLA DIAGNOSTIC ARRAY (INSTANTANEOUS CLOUD DROPLET NUMBER CONCENTRATION) (/CM3)|
!!    |CCNEROW  |O         |PLA DIAGNOSTIC ARRAY (CLOUD DROPLET NUMBER CONCENTRATION, EMPIRICAL APPROACH) (/CM3)|
!!    |CC02ROW  |IO        |PLA DIAGNOSTIC ARRAY (CLOUD CONDENSATION NUCLEI CONCENTRATION AT 0.2% SUPERSATURATION) (/CM3)|
!!    |CC04ROW  |IO        |PLA DIAGNOSTIC ARRAY (CLOUD CONDENSATION NUCLEI CONCENTRATION AT 0.4% SUPERSATURATION) (/CM3)|
!!    |CC08ROW  |IO        |PLA DIAGNOSTIC ARRAY (CLOUD CONDENSATION NUCLEI CONCENTRATION AT 0.8% SUPERSATURATION) (/CM3)|
!!    |CC16ROW  |IO        |PLA DIAGNOSTIC ARRAY (CLOUD CONDENSATION NUCLEI CONCENTRATION AT 1.6% SUPERSATURATION) (/CM3)|
!!    |RCRIROW  |O         |PLA DIAGNOSTIC ARRAY (CRITICAL AEROSOL RADIUS FOR CLOUD ACTIVATION) (M)|
!!    |SUPSROW  |O         |PLA DIAGNOSTIC ARRAY (MAXIMUM WATER VAPOUR SUPERSATURATION IN CLOUD) (%)|
!!    |VOAEROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED EMISSION FLUX OF ORGANIC AEROSOL) (KG/M2/SEC)|
!!    |VBCEROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED EMISSION FLUX OF BLACK CARBON) (KG/M2/SEC)|
!!    |VASEROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED EMISSION FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VMDEROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED EMISSION FLUX OF MINERAL DUST) (KG/M2/SEC)|
!!    |VSSEROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED EMISSION FLUX OF SEA SALT) (KG/M2/SEC)|
!!    |VOAWROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED WET DEPOSITION FLUX OF ORGANIC AEROSOL) (KG/M2/SEC)|
!!    |VBCWROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED WET DEPOSITION FLUX OF BLACK CARBON) (KG/M2/SEC)|
!!    |VASWROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED WET DEPOSITION FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VMDWROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED WET DEPOSITION FLUX OF MINERAL DUST) (KG/M2/SEC)|
!!    |VSSWROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED WET DEPOSITION FLUX OF SEA SALT) (KG/M2/SEC)|
!!    |VOADROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED DRY DEPOSITION FLUX OF ORGANIC AEROSOL) (KG/M2/SEC)|
!!    |VBCDROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED DRY DEPOSITION FLUX OF BLACK CARBON) (KG/M2/SEC)|
!!    |VASDROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED DRY DEPOSITION FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VMDDROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED DRY DEPOSITION FLUX OF MINERAL DUST) (KG/M2/SEC)|
!!    |VSSDROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED DRY DEPOSITION FLUX OF SEA SALT) (KG/M2/SEC)|
!!    |VOAGROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-TO-PARTICLE CONVERSION FLUX OF ORGANIC AEROSOL) (KG/M2/SEC)|
!!    |VBCGROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-TO-PARTICLE CONVERSION FLUX OF BLACK CARBON) (KG/M2/SEC)|
!!    |VASGROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-TO-PARTICLE CONVERSION FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VMDGROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-TO-PARTICLE CONVERSION FLUX OF MINERAL DUST) (KG/M2/SEC)|
!!    |VSSGROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED GAS-TO-PARTICLE CONVERSION FLUX OF SEA SALT) (KG/M2/SEC)|
!!    |VASIROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED IN-CLOUD OXIDATION FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VAS1ROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED IN-CLOUD SCAVENGING FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VAS2ROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED BELOW-CLOUD SCAVENGING FLUX OF AMMONIUM SULPHATE) (KG/M2/SEC)|
!!    |VAS3ROW  |IO        |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED RE-EVAPORATION OF AMMONIUM SULPHATE IN RAIN) (KG/M2/SEC)|
!!    |HENRROW  |O         |PLA DIAGNOSTIC ARRAY (HENRYS LAW CONSTANT FOR SULPHUR DIOXIDE)|
!!    |O3FROW   |O         |PLA DIAGNOSTIC ARRAY (AMMONIUM SULPHATE PRODUCTION RATE DUE TO IN-CLOUD OZONE OXIDATION) (KG/KG/SEC)|
!!    |H2O2FROW |O         |PLA DIAGNOSTIC ARRAY (AMMONIUM SULPHATE PRODUCTION RATE DUE TO IN-CLOUD HYDROGEN PEROXIDE OXIDATION) (KG/KG/SEC)|
!!    |WPARROW  |O         |PLA DIAGNOSTIC ARRAY (VERTICAL VELOCITY IN CLOUD UPDRAFT) (M/SEC)|
!!    |VCCNROW  |O         |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED INSTANTANEOUS CLOUD DROPLET NUMBER CONCENTRATION) (/M2)|
!!    |VCNEROW  |O         |PLA DIAGNOSTIC ARRAY (VERTICALLY INTEGRATED INSTANTANEOUS CLOUD DROPLET NUMBER CONCENTRATION, EMPIRICAL APPROACH) (/M2)|
!!    |DMACROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION MASS SIZE DISTRIBUTION FLUX FOR MINERAL DUST 1) (KG/M2/SEC)|
!!    |DMCOROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION MASS SIZE DISTRIBUTION FLUX FOR MINERAL DUST 2) (KG/M2/SEC)|
!!    |DNACROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION NUMBER SIZE DISTRIBUTION FLUX FOR MINERAL DUST 1) (KG/M2/SEC)|
!!    |DNCOROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION NUMBER SIZE DISTRIBUTION FLUX FOR MINERAL DUST 2) (KG/M2/SEC)|
!!    |DUWDROW  |IO        |PLA DIAGNOSTIC ARRAY (WIND SPEED IN MINERAL DUST CALCULATION) (M/SEC)|
!!    |DUSTROW  |IO        |PLA DIAGNOSTIC ARRAY (FRICTION VELOCITY IN MINERAL DUST CALCULATION (M/SEC)|
!!    |DUTHROW  |IO        |PLA DIAGNOSTIC ARRAY (FRICTION VELOCITY THRESHOLD IN MINERAL DUST CALCULATION) (M/SEC)|
!!    |FALLROW  |IO        |PLA DIAGNOSTIC ARRAY (FLUXALL ARRAY IN MINERAL DUST CALCULATION)|
!!    |FA10ROW  |IO        |PLA DIAGNOSTIC ARRAY (FA10 ARRAY IN MINERAL DUST CALCULATION)|
!!    |FA2ROW   |IO        |PLA DIAGNOSTIC ARRAY (FA2 ARRAY IN MINERAL DUST CALCULATION)|
!!    |FA1ROW   |IO        |PLA DIAGNOSTIC ARRAY (FA1 ARRAY IN MINERAL DUST CALCULATION)|
!!    |USMKROW  |IO        |PLA DIAGNOSTIC ARRAY (TIME FRACTION OF THRESHOLD EXCEEDENCE IN MINERAL DUST CALCULATION)|
!!    |DEFAROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION FLUX OF MINERAL DUST MASS FOR ACCUMULATION MODE) (KG/M2/SEC)|
!!    |DEFCROW  |IO        |PLA DIAGNOSTIC ARRAY (EMISSION FLUX OF MINERAL DUST MASS FOR COARSE MODE) (KG/M2/SEC)|
!!    |SICN_CRT |I         |CRITICAL SEA ICE FRACTION|
!!    |SAVERAD  |I         |WEIGHTING FACTOR FOR TIME INTEGRATION OF PLA DIAGNOSTIC RESULTS OVER 1/SAVERAD NUMBER OF MODEL TIME STEPS|
!!    |ISVDUST  |I         |SWITCH FOR TURNING ON/OFF EXTRA DIAGNOSTIC CALCULATIONS FOR MINERAL DUST PROCESSES|
!!    |MYNODE   |I         |PROCESSOR ID|
