!> \file
!> \brief Various terms in coagulation equation for aerosol number.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine coagn(dndts,kern,fmom0,fmom3,fmom0i,fmom0d,ilga,leva)
  !
  use sdparm, only : iro,isaint,isfint,isftri,isftrim,itr,itrm
  use fpdef,  only : r8
  !
  implicit none
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  real, intent(out), dimension(ilga,leva,isfint,isaint) :: dndts !< Number tendency
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom0 !< Moment of size distribution
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom3 !< Moment of size distribution
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0i !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0d !< Coefficient for linear volume interpolation of
  !< partial moments between adjacent section boundaries
  real, intent(in), dimension(ilga,leva,isftri) :: kern !< Coagulation coefficient (kernel)
  !
  !     internal work variables
  !
  real(r8), dimension(ilga,leva) :: dndtt !<
  integer :: is !<
  integer :: ik !<
  integer :: irx !<
  integer :: its !<
  integer :: itsm !<
  integer :: ir1 !<
  integer :: ir2 !<
  !
  !-----------------------------------------------------------------------
  !
  dndts=0.
  !
  !-----------------------------------------------------------------------
  !     * first, third and fifth term in number tendency equation.
  !
  do is=2,isfint
    ir1=is-1
    ir2=ir1
    its=itr(ir1,ir2)
    dndtt=.5*kern(:,:,its)*fmom0(:,:,ir1)**2
    irx=iro(ir1,ir2)
    dndts(:,:,is-1,irx)=dndts(:,:,is-1,irx)-2.*dndtt
    dndts(:,:,is  ,irx)=dndts(:,:,is  ,irx)+dndtt
  end do
  is=isfint+1
  ir1=is-1
  ir2=ir1
  its=itr(ir1,ir2)
  dndtt=.5*kern(:,:,its)*fmom0(:,:,ir1)**2
  irx=iro(ir1,ir2)
  dndts(:,:,is-1,irx)=dndts(:,:,is-1,irx)-2.*dndtt
  !
  !     * second and fourth term in number tendency equation.
  !
  do is=3,isfint
    ir1=is-1
    do ik=1,is-2
      ir2=ik
      its=itr(ir1,ir2)
      itsm=itrm(ir1,ir2)
      dndtt=fmom0(:,:,ir2)*kern(:,:,its)*fmom0i(:,:,itsm) &
               +fmom3(:,:,ir2)*kern(:,:,its)*fmom0d(:,:,itsm)
      irx=iro(ir1,ir2)
      dndts(:,:,is-1,irx)=dndts(:,:,is-1,irx)-dndtt
      dndts(:,:,is  ,irx)=dndts(:,:,is  ,irx)+dndtt
    end do
  end do
  is=isfint+1
  ir1=is-1
  do ik=1,is-2
    ir2=ik
    its=itr(ir1,ir2)
    itsm=itrm(ir1,ir2)
    dndtt=fmom0(:,:,ir2)*kern(:,:,its)*fmom0i(:,:,itsm) &
             +fmom3(:,:,ir2)*kern(:,:,its)*fmom0d(:,:,itsm)
    irx=iro(ir1,ir2)
    dndts(:,:,is-1,irx)=dndts(:,:,is-1,irx)-dndtt
  end do
  !
  !     * sixth term in number tendency equation.
  !
  do is=1,isfint
    ir1=is
    do ik=1,is-1
      ir2=ik
      its=itr(ir1,ir2)
      dndtt=-kern(:,:,its)*fmom0(:,:,ir1)*fmom0(:,:,ir2)
      irx=iro(ir1,ir2)
      dndts(:,:,ik,irx)=dndts(:,:,ik,irx)+dndtt
    end do
  end do
  !
end subroutine coagn
