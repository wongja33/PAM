!> \file
!> \brief Brownian coagulation coefficient (kernel) for aerosol coagulation
!        calculations.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
module coadat
  !
  integer :: ida !<
  integer :: idb !<
  integer :: idc !<
  integer :: idd !<
  real, allocatable, dimension(:,:,:,:) :: cker !<
  real, allocatable, dimension(:) :: cpre !<
  real, allocatable, dimension(:) :: ctem !<
  real, allocatable, dimension(:) :: cphi !<
  !
end module coadat
