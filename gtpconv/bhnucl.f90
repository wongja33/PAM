!> \file
!> \brief Calculation of coefficients for nucleation rate using Kulmala's
!>       parameterization
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine bhnucl(sxp,anucl,t,rh,pmass,yscale,ilga,leva)
  !
  use sdparm, only : akb,ara,atune,avo,wamsul
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva) :: sxp !< Nucleation coefficient
  real(r8), intent(out), dimension(ilga,leva) :: anucl !< Nucleation coefficient
  real, intent(in), dimension(ilga,leva) :: t !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rh !< Relative Humidity \f$[dimensionless]\f$
  real, intent(in) :: pmass !< Nucleation particle mass \f$[]\f$
  real, intent(in) :: yscale !< Factor for conversion mol->fmol (1.e+15)
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real(r8), allocatable, dimension(:,:) :: tb !<
  real(r8), allocatable, dimension(:,:) :: rhb !<
  real(r8), allocatable, dimension(:,:) :: anw !<
  real(r8), allocatable, dimension(:,:) :: term !<
  real(r8), allocatable, dimension(:,:) :: termx !<
  real(r8), allocatable, dimension(:,:) :: termy !<
  real(r8), allocatable, dimension(:,:) :: adelta !<
  real(r8), allocatable, dimension(:,:) :: apara !<
  real(r8), allocatable, dimension(:,:) :: aparb !<
  real(r8), allocatable, dimension(:,:) :: aparc !<
  real(r8), allocatable, dimension(:,:) :: apard !<
  real(r8), allocatable, dimension(:,:) :: aconcr !<
  real(r8), allocatable, dimension(:,:) :: sxpt !<
  real(r8) :: apare !<
  !
  real(r8), parameter :: yvolc =1.e+06_r8 ! for conversion 1/cm**3->1/m**3
  !
  !-----------------------------------------------------------------------
  !
  !     * allocate work arrays.
  !
  allocate(tb    (ilga,leva))
  allocate(rhb   (ilga,leva))
  allocate(anw   (ilga,leva))
  allocate(term  (ilga,leva))
  allocate(termx (ilga,leva))
  allocate(termy (ilga,leva))
  allocate(adelta(ilga,leva))
  allocate(apara (ilga,leva))
  allocate(aparb (ilga,leva))
  allocate(aparc (ilga,leva))
  allocate(apard (ilga,leva))
  allocate(aconcr(ilga,leva))
  allocate(sxpt  (ilga,leva))
  !
  !     restrict range for temperature and relative humdity.
  !
  tb=t
  rhb=rh
  !                                                  d ! proc-depend
  !      tb=dmax1(dmin1(tb,298._r8),233._r8)
  !      rhb=dmax1(dmin1(rhb,1.0_r8),0.01_r8)
  !                                                   ! proc-depend
  tb=max(min(tb,298._r8),233._r8)
  rhb=max(min(rhb,1.0_r8),0.01_r8)
  !
  !     calculate water vapour concentration (in 1/cm3).
  !
  term=log(tb)
  termx=77.34491296_r8-7235.42451_r8/tb-8.2_r8*term &
           +5.7113e-03_r8*tb
  anw=exp(termx)
  anw=rhb*anw/(akb*tb*yvolc)
  adelta=tb/273.15_r8
  apara=25.1289_r8-4890.8_r8/tb-2.2479_r8*adelta*rhb
  aparb=7643.4_r8/tb-1.9712_r8*adelta/rhb
  aparc=-1743.3_r8/tb
  term=log(anw)
  apard=1.2233_r8-0.0154_r8*ara/(ara+rhb)-0.0415_r8*term &
           +0.0016_r8*tb
  apare=0.0102_r8
  !
  !     calculate threshold h2so4 concentration.
  !
  termx=-14.5125_r8+0.1335_r8*tb-10.5462_r8*rhb+1958.4_r8*rhb/tb
  aconcr=exp(termx)
  !
  !-----------------------------------------------------------------------
  !     * exponent.
  !
  sxpt=apara+aparb*apare
  sxp=sxpt
  !
  !     get factor for nucleation rate in molecules/cm3/sec for
  !     input concentrations in molecules/cm3. scale aconcr
  !     instead of scaling anucl in the final result for numerical
  !     stability.
  !
  termy=yscale
  term=-1._r8/apara
  termx=termy**term
  aconcr=aconcr*termx
  term=exp(aparb*apard+aparc)
  termx=aconcr**apara
  term=term/termx
  !
  !-----------------------------------------------------------------------
  !     convert nucleation rate to nmol/m3/sec for input
  !     concentrations in nmol/m3.
  !
  termy=avo/(yscale*yvolc)
  termx=termy**sxpt
  termy=pmass*termx*yvolc
  anucl=term*termy*atune/wamsul
  !
  !     * deallocate work arrays.
  !
  deallocate(tb)
  deallocate(rhb)
  deallocate(anw)
  deallocate(term)
  deallocate(termx)
  deallocate(termy)
  deallocate(adelta)
  deallocate(apara)
  deallocate(aparb)
  deallocate(aparc)
  deallocate(apard)
  deallocate(aconcr)
  deallocate(sxpt)
  !
end subroutine bhnucl
!
!> \file
!! \subsection ssec_details Details
!! This subroutine calculates the coefficients for the nucleation rate
!! (ANUCL*C**SXP) according to von Salzen et al. \cite vonSalzen2000 using the
!! parameterization of Kulmala et al. \cite Kulmala1998. It is assumed that
!! sufficient ammonia is available to form (NH4)2S04 particles once the
!! particles are produced by nucleation. Although the patricles intially
!! contain water they are injected as dry particles at the lower
!! boundary of the size spectrum.
