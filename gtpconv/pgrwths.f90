!> \file
!> \brief Particle growth from condensation of aerosol mass with an
!>       associated change in particle radius R -> R+C/R.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine pgrwths (tmas,resm,frc,i0l,i0r, &
                          dp0l,dp1l,dp2l,dp3l,dm1l,dm2l,dm3l, &
                          dp0r,dp1r,dp2r,dp3r,dm1r,dm2r,dm3r, &
                          dp0s,dp1s,dp2s,dp3s,dm1s,dm2s,dm3s, &
                          cgr1,cgr2,pn0,drydn,ilga,leva,isec)
  !
  use sdparm, only : ycnst,ytiny
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva,isec) :: tmas !< Mass size distribution
  real, intent(inout), dimension(ilga,leva) :: resm !< Mass residuum from numerical truncation in growth calculations
  real, intent(in), dimension(ilga,leva,isec) :: cgr1 !< Growth rate
  real, intent(in), dimension(ilga,leva,isec) :: cgr2 !< Growth rate
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< 1st PLA size distribution parameter (\f$n_{0,i}\f$)
  real, intent(in), dimension(ilga,leva,isec) :: drydn !< Density of dry aerosol particle \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: frc !< Fraction
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp1l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp2l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm1l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm2l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm3l !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp1r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp2r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm1r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm2r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm3r !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp0s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp1s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp2s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dp3s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm1s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm2s !<
  real(r8), intent(in), dimension(ilga,leva,isec) :: dm3s !<
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  integer, intent(in) :: isec !< Number of separate aerosol tracers
  integer, intent(in), dimension(ilga,leva,isec) :: i0l !< Left-hand side part of the modified section
  integer, intent(in), dimension(ilga,leva,isec) :: i0r !< Right-hand side part of the modified section
  !
  !    internal work variables
  !
  real(r8) :: c0 !<
  real(r8) :: c1 !<
  real(r8) :: c2 !<
  real(r8) :: tp0l !<
  real(r8) :: tp1l !<
  real(r8) :: tp2l !<
  real(r8) :: tp3l !<
  real(r8) :: tp0r !<
  real(r8) :: tp1r !<
  real(r8) :: tp2r !<
  real(r8) :: tp3r !<
  real(r8) :: tp0s !<
  real(r8) :: tp1s !<
  real(r8) :: tp2s !<
  real(r8) :: tp3s !<
  real(r8) :: tm1l !<
  real(r8) :: tm2l !<
  real(r8) :: tm3l !<
  real(r8) :: tm1r !<
  real(r8) :: tm2r !<
  real(r8) :: tm3r !<
  real(r8) :: tm1s !<
  real(r8) :: tm2s !<
  real(r8) :: tm3s !<
  integer :: il !<
  integer :: is !<
  integer :: isi !<
  integer :: l !<
  real :: atmp !<
  real :: ct !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  tmas=0.
  !
  !-----------------------------------------------------------------------
  !     calculate number and mass in each section.
  !
  do isi=1,isec
    do l=1,leva
      do il=1,ilga
        !
        !      calculate contribution in each section from the left-hand
        !      side part of the modified section.
        !
        is=i0l(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            ) then
          !
          !        mass size distribution from
          !        m(phi(t))=f*(r(t=0)+c/r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp0l=dp0l(il,l,isi)
          tp1l=dp1l(il,l,isi)
          tp2l=dp2l(il,l,isi)
          tp3l=dp3l(il,l,isi)
          tm1l=dm1l(il,l,isi)
          tm2l=dm2l(il,l,isi)
          tm3l=dm3l(il,l,isi)
          tmas(il,l,is)=tmas(il,l,is)+c0*tp3l &
                                    +ct*(3._r8*tp2l*c2+3._r8*tp1l*(c1 &
                                    +c2**2)+tp0l*c2*(6._r8*c1+c2**2) &
                                    +3._r8*tm1l*c1*(c1+c2**2) &
                                    +3._r8*tm2l*(c1**2*c2)+tm3l*c1**3)
        end if
        !
        !        calculate contribution in each section from the right-hand
        !        side part of the modified section.
        !
        is=i0r(il,l,isi)
        if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
            .and. i0l(il,l,isi) /= is) then
          !
          !        mass size distribution from
          !        m(phi(t))=f*(r(t=0)+c/r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp0r=dp0r(il,l,isi)
          tp1r=dp1r(il,l,isi)
          tp2r=dp2r(il,l,isi)
          tp3r=dp3r(il,l,isi)
          tm1r=dm1r(il,l,isi)
          tm2r=dm2r(il,l,isi)
          tm3r=dm3r(il,l,isi)
          tmas(il,l,is)=tmas(il,l,is)+c0*tp3r &
                                    +ct*(3._r8*tp2r*c2+3._r8*tp1r*(c1 &
                                    +c2**2)+tp0r*c2*(6._r8*c1+c2**2) &
                                    +3._r8*tm1r*c1*(c1+c2**2) &
                                    +3._r8*tm2r*(c1**2*c2)+tm3r*c1**3)
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     residuum due to particles growing to sizes larger than
  !     the spectrum cutoff.
  !
  do isi=1,isec
    do l=1,leva
      do il=1,ilga
        if ( (i0r(il,l,isi) == (isec+1) .or. i0l(il,l,isi) == 0) &
            .and. pn0(il,l,isi) > ytiny) then
          !
          !        mass size distribution from
          !        m(phi(t))=f*(r(t=0)+c/r(t=0))**3*n(phi(t=0))
          !
          ct=pn0(il,l,isi)*drydn(il,l,isi)*ycnst
          c0=ct*frc(il,l,isi)
          c1=cgr1(il,l,isi)
          c2=cgr2(il,l,isi)
          tp0s=dp0s(il,l,isi)
          tp1s=dp1s(il,l,isi)
          tp2s=dp2s(il,l,isi)
          tp3s=dp3s(il,l,isi)
          tm1s=dm1s(il,l,isi)
          tm2s=dm2s(il,l,isi)
          tm3s=dm3s(il,l,isi)
          resm(il,l)=resm(il,l)+c0*tp3s &
                                    +ct*(3._r8*tp2s*c2+3._r8*tp1s*(c1 &
                                    +c2**2)+tp0s*c2*(6._r8*c1+c2**2) &
                                    +3._r8*tm1s*c1*(c1+c2**2) &
                                    +3._r8*tm2s*(c1**2*c2)+tm3s*c1**3)
        end if
      end do
    end do
  end do
  !
end subroutine pgrwths
!> \file
!! \subsection ssec_details Details
!! \note
!! It is assumed that sections initially have the same size and that
!! growth leads to a an equal or smaller width of each section.
!! The subroutine treats growth in particle size space based on
!! a Lagrangian approach.
