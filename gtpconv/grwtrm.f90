!> \file
!> \brief Rate coefficient for condensation of H2SO4 onto
!>        pre-existing aerosol (CNDGP, IN 1/S)
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine grwtrm(cndgp,cgr1,cgr2,ct1,ct2,pn0,anum,wetrc, &
                        dryrc,dryr,ddn,phi0,psi,phiss,dphis, &
                        frp,dfso4,wrat,rhoa,leva,ilga,isec)
  !
  use sdparm, only : ace,r0,ypi,ytiny
  use sdcode, only : sdint
  use fpdef,  only : r8
  !
  implicit none
  !
  real(r8), intent(out), dimension(ilga,leva,isec) :: cndgp !< Rate coefficient for
  !< condensation of H2SO4 onto
  !< pre-existing aerosol \f$[1/sec]\f$
  real(r8), intent(out), dimension(ilga,leva,isec) :: ct1 !< Term for calculation of section-mean
  !< condensation rate.
  real(r8), intent(out), dimension(ilga,leva,isec) :: ct2 !< Term for calculation of section-mean
  !< condensation rate.
  real, intent(out), dimension(ilga,leva,isec) :: cgr1 !< Term for calculation of growth factor
  !< (CG, \f$[m^5/kg/s]\f$)
  real, intent(out), dimension(ilga,leva,isec) :: cgr2 !< Term for calculation of growth factor
  !< (CG, \f$[m^5/kg/s]\f$)
  real, intent(in), dimension(ilga,leva,isec) :: anum !<
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !< Radius for total (wet), externally
  !< mixed aerosol particles \f$[m]\f$
  real, intent(in), dimension(ilga,leva,isec) :: ddn !< (Dry) particle density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !< Third PLA size distribution parameter
  !< (\f$\phi_{0,i}\f$, width)
  real, intent(in), dimension(ilga,leva,isec) :: psi !< Second PLA size distribution parameter
  !< (\f$\psi_{i}\f$, mode size)
  real, intent(in), dimension(ilga,leva,isec) :: phiss !< Dry particle size \f$(ln(Rp/R0))\f$ at the
  !< boundaries of the size sections
  real, intent(in), dimension(ilga,leva,isec) :: dphis !< Section width
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !< First PLA size distribution parameter
  !< (\f$n_{0,i}\f$, amplitude)
  real(r8), intent(in), dimension(ilga,leva) :: dfso4 !< Diffusivity of H2SO4
  real, intent(in), dimension(ilga,leva) :: frp !< Mean free path of H2SO4
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(isec) :: dryrc !< Dry particle radius in the centres of the size sections \f$[m]\f$
  real, intent(in), dimension(isec+1) :: dryr !< (Dry) particle radius \f$[m]\f$
  real, intent(in) :: wrat !< ratio of molecular weight of (NH4)2SO4 to molecular weight of sulphuric acid
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real(r8), dimension(ilga,leva,isec) :: dbl !<
  integer :: il !<
  integer :: is !<
  integer :: isec !<
  integer :: l !<
  real :: afl !<
  real :: rmom1 !<
  real :: rmom2 !<
  real(r8) :: fgr !<
  real(r8) :: wetrl !<
  real(r8) :: wetrr !<
  real(r8) :: aknl !<
  real(r8) :: aknr !<
  real(r8) :: fnl !<
  real(r8) :: fnr !<
  real(r8) :: anl !<
  real(r8) :: anr !<
  real(r8) :: dr !<
  real(r8) :: dafdr !<
  real(r8) :: cterm !<
  !
  !     * intrinsic functions.
  !
  real(r8) :: zfn !<
  real(r8) :: zan !<
  real(r8) :: ag1 !<
  real(r8) :: ag2 !<
  zfn(ag1)=(1.+ag1)/(1.+1.71*ag1+1.33*ag1*ag1)
  zan(ag1,ag2)=1./(1.+1.33*ag1*ag2*(1./ace-1.))
  !
  !-----------------------------------------------------------------------
  !
  cgr1=0.
  cgr2=0.
  ct1=0.
  ct2=0.
  do is=1,isec
    do l=1,leva
      do il=1,ilga
        if (anum(il,l,is) > ytiny) then
          fgr=wetrc(il,l,is)/dryrc(is)
          !
          !         term a*f in growth equation at lower and upper boundaries
          !         of the section from linearization.
          !
          wetrl=fgr*dryr(is)
          aknl=frp(il,l)/wetrl
          fnl=zfn(aknl)
          anl=zan(aknl,fnl)
          wetrr=fgr*dryr(is+1)
          aknr=frp(il,l)/wetrr
          fnr=zfn(aknr)
          anr=zan(aknr,fnr)
          afl=fnl*anl
          dr=dryr(is+1)-dryr(is)
          dafdr=(fnr*anr-afl)/dr
          !
          !         terms for calculation of section-mean condensation rate.
          !
          cterm=4.*ypi*dfso4(il,l)*fgr*rhoa(il,l)
          ct1(il,l,is)=cterm*(afl-dafdr*dryr(is))
          ct2(il,l,is)=cterm*dafdr
          !
          !         terms for calculation of growth factor (cg,\f$[m^5/kg/sec]\f$).
          !
          cterm=dfso4(il,l)*fgr*wrat/ddn(il,l,is)
          cgr1(il,l,is)=cterm*(afl-dafdr*dryr(is))
          cgr2(il,l,is)=cterm*dafdr
        end if
      end do
    end do
  end do
  rmom1=1.
  rmom2=2.
  dbl=pn0
  cndgp=dbl*(ct1*sdint(phi0,psi,rmom1,phiss,dphis,ilga,leva,isec) &
                +ct2*sdint(phi0,psi,rmom2,phiss,dphis,ilga,leva,isec))
  cndgp=max(0._r8,cndgp)
  !
end subroutine grwtrm
