!> \file
!> \brief  Dunne's parameterization of nucleation
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine dunnepar(sclncl,adu,cnh3n,cnh3ch,rkbn,rkbch,rktn,rktch, &
                          dualfa,prion,ta,rh,rhoa,h2so4,nh3,yscale, &
                          ym3tocm3,ysdunne,ilga,leva)
  !
  use sdparm, only : avo
  use sdphys, only : wa
  use fpdef,  only : r8
  !
  implicit none
  !
  real, intent(out), dimension(20) :: adu !< Nucleation parameters
  real(r8), intent(out), dimension(ilga,leva) :: cnh3n !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: cnh3ch !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: rkbn !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: rkbch !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: rktn !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: rktch !< Coefficient in Dunne's parameterization
  real(r8), intent(out), dimension(ilga,leva) :: dualfa !< Ion-ion recombination rate (Brasseur and Chattel
  !< \cite Brasseur1983)
  real(r8), intent(out), dimension(ilga,leva) :: prion !< Ion production rate \f$[ion\:pairs/cm^3/sec]\f$)
  real(r8), intent(out), dimension(ilga,leva) :: sclncl !< Nucleation rate scaling parameter
  real(r8), intent(in), dimension(ilga,leva) :: h2so4 !< Sulphuric acid concentration
  real, intent(in), dimension(ilga,leva) :: ta !< Air temperature \f$[K]\f$
  real, intent(in), dimension(ilga,leva) :: rh !< Relative humidity \f$[dimensionless]\f$
  real, intent(in), dimension(ilga,leva) :: rhoa !< Air density \f$[kg/m^3]\f$
  real, intent(in), dimension(ilga,leva) :: nh3 !< Ammonia concentration
  real, intent(in) :: yscale !< Factor for conversion mol->fmol (1.e+15)
  real, intent(out) :: ym3tocm3 !< Factor for conversion m3->cm3 (1.e+15)
  real, intent(out) :: ysdunne !< Scaling factor (1.e-06)
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  real(r8), dimension(ilga,leva) :: temp !<
  real(r8), dimension(ilga,leva) :: aird !<
  real(r8), dimension(ilga,leva) :: nh3p !<
  real(r8), dimension(ilga,leva) :: h2so4p !<
  !
  !-----------------------------------------------------------------------
  !
  ym3tocm3 = 1e6
  ysdunne  = 1e-6
  !
  !     collection of parameters in nucleation parameterization.
  !
  adu = (/3.95451  , 9.702973, 12.62259,  -0.007066146, 182.4495 , 1.203451, &
            -4.188065 , 9.003471, 636801.6,   2.891024   , -11.48166, 25.49469, &
            0.1810722, 3.373738, 4.071246, -23.8002     ,  37.03029, 0.227413, &
            206.9792 , 3.138719 /)
  temp = dble(ta)/1000.d0
  !
  !     calculate number density of air (molecules/cm3).
  !
  aird = dble(rhoa)*dble(avo)/dble(wa)/dble(ym3tocm3)
  !
  !     scale ammonia concentration to convert from volume mixing ratio
  !     to 10e6*molecules/cm3
  !
  nh3p = dble(nh3)*aird*dble(ysdunne)
  !
  !     scale sulphuric acid concentration to convert from fmol/m3
  !     to 10e6*molecules/cm3
  !
  h2so4p = dble(h2so4)*(dble(avo)/(dble(yscale)*dble(ym3tocm3)))*dble(ysdunne)
  !
  !     Calculate coefficients in Dunne's parameterization.
  !
  !       cnh3n=adu(9) * (nh3p**(1-adu(8))) / (1 + adu(9) * (nh3p**(1-adu(8)))*(h2so4p**adu(10)) )
  !       cnh3n  = dble(adu( 9)) * (nh3p**(1.d0-dble(adu( 8)))) / (1.d0 + dble(adu( 9)) * (nh3p**(1.d0-dble(adu( 8))))*(h2so4p**dble(adu(10))) )
  !       do i=1,ilga
  !           do k=1,leva
  !               print *,i,k,nh3p(i,k)
  !               cnh3n(i,k)  = dble(adu(9)) * (nh3p(i,k)**(1.d0-dble(adu(8))))
  !               print *,i,k,cnh3n(i,k)
  !           end do
  !       end do
  nh3p   = max(nh3p, 1d-20)
  cnh3n  = dble(adu(9)) * (nh3p**(1.d0-dble(adu(8))))
  cnh3n  = 1.d0 / cnh3n
  cnh3n  = 1.d0 / (cnh3n + (h2so4p**dble(adu(10))))

  !     cnh3ch=adu(19) * (nh3p**(1-adu(15))) / (1 + adu(19) * (nh3p**(1-adu(15)))*(h2so4p**adu(20)) )
  cnh3ch = dble(adu(19)) * (nh3p**(1.d0-dble(adu(15)))) / (1.d0 + dble(adu(19)) * &
               (nh3p**(1.d0-dble(adu(15))))*(h2so4p**dble(adu(20))) )
  rkbn   =  dexp(dble(adu( 2)) - dexp(dble(adu( 3))*(temp-dble(adu( 4)))))
  rkbch  =  dexp(dble(adu(11)) - dexp(dble(adu(12))*(temp-dble(adu(13)))))
  rktn   = (dexp(dble(adu( 5)) - dexp(dble(adu( 6))*(temp-dble(adu( 7)))))) * &
               dble(cnh3n) * (nh3p / 100.d0)**dble(adu(8)) * 100.d0**dble(adu(8))
  rktch  = (dexp(dble(adu(16)) - dexp(dble(adu(17))*(temp-dble(adu(18)))))) * &
               dble(cnh3ch) * (nh3p**dble(adu(15)))
  !
  !     calculate ion-ion recombination rate (brasseur and chattel).
  !
  dualfa=6.d-08*dsqrt(300.d0/dble(ta))+6.d-26*aird*(300.d0/dble(ta))**4.d0
  !
  !     ion production rate (ion pairs/cm3/sec).
  !
  prion=5.d0
  !
  !     get nucleation rate scaling parameter to account for dependency
  !     on relative humidity.
  !
  sclncl = max((1.d0+1.5d0*(dble(rh)-0.38d0)+0.045d0*((dble(rh)-0.38d0)**3.d0)*(dble(ta)-208.d0)**2.d0), 0.d0)
  !
  !     * deallocate work arrays.
  !
end subroutine dunnepar
!> \file
!! \subsection ssec_details Details
!! Ion-ion recombination rate is from Brasseur and Chattel \cite Brasseur1983
