!> \file
!> \brief  Insert primary aerosol emissions.
!!
!! @author K. von Salzen
!
!-----------------------------------------------------------------------
subroutine priaem (pedndt,pedmdt,pidfdt,pidndt,pidmdt,pimas,pifrc, &
                         docem1,docem2,docem3,docem4, &
                         dbcem1,dbcem2,dbcem3,dbcem4, &
                         dsuem1,dsuem2,dsuem3,dsuem4,dt,ilga,leva)
  !
  use sdparm, only : iexbc,iexoc,iexso4,iinbc,iinoc,iinso4, &
                         isaext,isaint, &
                         isextbc,isextoc,isextso4, &
                         isintbc,isintoc,isintso4, &
                         kext,kextbc,kextoc,kextso4, &
                         kint,kintbc,kintoc,kintso4,ytiny
  use sdemi,  only : pebct1,pebct2,pebct3,pebct4, &
                         peoct1,peoct2,peoct3,peoct4, &
                         pesut1,pesut2,pesut3,pesut4, &
                         pibct1,pibct2,pibct3,pibct4, &
                         pioct1,pioct2,pioct3,pioct4, &
                         pisut1,pisut2,pisut3,pisut4
  !
  implicit none
  !
  real, intent(out), dimension(ilga,leva,isaext) :: pedndt !< Number tendency \f$[1/kg/sec]\f$, ext. mixture
  real, intent(out), dimension(ilga,leva,isaext) :: pedmdt !< Mass tendency \f$[kg/kg/sec]\f$, ext. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidndt !< Number tendency \f$[1/kg/sec]\f$, int. mixture
  real, intent(out), dimension(ilga,leva,isaint) :: pidmdt !< Mass tendency \f$[kg/kg/sec]\f$, int. mixture
  real, intent(out), dimension(ilga,leva,isaint,kint) :: pidfdt !< Aerosol species mass fraction tendency \f$[1/sec]\f$, int mmixture
  real, intent(in) :: dt !< Model time step \f$[s]\f$
  real, intent(in),  dimension(ilga,leva,isaint,kint) :: pifrc !< Aerosol (dry) mass fraction for each aerosol type
  !< for internally mixed aerosol
  real, intent(in),  dimension(ilga,leva,isaint) :: pimas !< Aerosol (dry) mass concentration for internally mixed
  !< aerosol \f$[kg/kg]\f$
  real, intent(in),  dimension(ilga,leva) :: docem1 !< Organic aerosol emission rate for open vegetation and biofuel !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: docem2 !< Organic aerosol emission rate for fossil fuel  !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: docem3 !< Organic aerosol emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: docem4 !< Organic aerosol emission rate for shipping \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dbcem1 !< Black carbon emission rate for open vegetation and biofuel  !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dbcem2 !< Black carbon emission rate for fossil fuel  !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dbcem3 !< Black carbon emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dbcem4 !< Black carbon emission rate for shipping \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dsuem1 !< Ammonium sulphate emission rate for open vegetation and biofuel  !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dsuem2 !< Ammoniun sulphate emission rate for fossil fuel  !< burning \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dsuem3 !< Ammonium sulphate emission rate for aircraft \f$[kg/kg/sec]\f$
  real, intent(in),  dimension(ilga,leva) :: dsuem4 !< Ammonium sulphate emission rate for shipping \f$[kg/kg/sec]\f$
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: leva !< Number of grid points in the vertical direction
  !
  !     internal work variables
  !
  real, allocatable, dimension(:,:,:) :: cimast !<
  real, allocatable, dimension(:,:,:,:) :: cimaf !<
  real, allocatable, dimension(:,:,:) :: ceocn !<
  real, allocatable, dimension(:,:,:) :: ceocm !<
  real, allocatable, dimension(:,:,:) :: cebcn !<
  real, allocatable, dimension(:,:,:) :: cebcm !<
  real, allocatable, dimension(:,:,:) :: cesun !<
  real, allocatable, dimension(:,:,:) :: cesum !<
  real, allocatable, dimension(:,:,:) :: ciocn !<
  real, allocatable, dimension(:,:,:) :: ciocm !<
  real, allocatable, dimension(:,:,:) :: cibcn !<
  real, allocatable, dimension(:,:,:) :: cibcm !<
  real, allocatable, dimension(:,:,:) :: cisun !<
  real, allocatable, dimension(:,:,:) :: cisum !<
  integer :: is !<
  integer :: il !<
  integer :: l !<
  !
  !      fraction of emissions assigned to internally mixed aerosol mode:
  !      1 - open vegetation and biofuel burning
  !      2 - fossil fuel burning
  !      3 - aircraft
  !      4 - shipping
  !      (see also sdpemi).
  !
  !     bc
  real, parameter :: fintbc1=1. !<
  real, parameter :: fintbc2=0.2 !<
  real, parameter :: fintbc3=0.2 !<
  real, parameter :: fintbc4=0.2 !<
  !
  !     organic carbon.
  real, parameter :: fintoc1=1. !<
  real, parameter :: fintoc2=0.5 !<
  real, parameter :: fintoc3=0.5 !<
  real, parameter :: fintoc4=0.5 !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  if (isextoc > 0) then
    allocate (ceocn(ilga,leva,isextoc))
    allocate (ceocm(ilga,leva,isextoc))
  end if
  if (isextbc > 0) then
    allocate (cebcn(ilga,leva,isextbc))
    allocate (cebcm(ilga,leva,isextbc))
  end if
  if (isextso4 > 0) then
    allocate (cesun(ilga,leva,isextso4))
    allocate (cesum(ilga,leva,isextso4))
  end if
  if (isintoc > 0) then
    allocate (ciocn(ilga,leva,isintoc))
    allocate (ciocm(ilga,leva,isintoc))
  end if
  if (isintbc > 0) then
    allocate (cibcn(ilga,leva,isintbc))
    allocate (cibcm(ilga,leva,isintbc))
  end if
  if (isintso4 > 0) then
    allocate (cisun(ilga,leva,isintso4))
    allocate (cisum(ilga,leva,isintso4))
  end if
  if (isaint > 0) then
    allocate (cimast(ilga,leva,isaint))
    allocate (cimaf (ilga,leva,isaint,kint))
  end if
  !
  !-----------------------------------------------------------------------
  !     convert changes in bulk concentrations for oc, bc, and sulphate
  !     to changes in aerosol concentrations for natural and
  !     anthropogenic emissions (units: kg/kg). for externally mixed
  !     types of aerosol. different types of emissions with different
  !     size distributions and total tracer mass fluxes are considered.
  !
  if (kextoc > 0) then
    do is=1,isextoc
      ceocn(:,:,is)=(peoct1%efnum(is)*docem1(:,:)*(1.-fintoc1) &
                        +peoct2%efnum(is)*docem2(:,:)*(1.-fintoc2) &
                        +peoct3%efnum(is)*docem3(:,:)*(1.-fintoc3) &
                        +peoct4%efnum(is)*docem4(:,:)*(1.-fintoc4))*dt
      ceocm(:,:,is)=(peoct1%efmas(is)*docem1(:,:)*(1.-fintoc1) &
                        +peoct2%efmas(is)*docem2(:,:)*(1.-fintoc2) &
                        +peoct3%efmas(is)*docem3(:,:)*(1.-fintoc3) &
                        +peoct4%efmas(is)*docem4(:,:)*(1.-fintoc4))*dt
    end do
  end if
  if (kextbc > 0) then
    do is=1,isextbc
      cebcn(:,:,is)=(pebct1%efnum(is)*dbcem1(:,:)*(1.-fintbc1) &
                        +pebct2%efnum(is)*dbcem2(:,:)*(1.-fintbc2) &
                        +pebct3%efnum(is)*dbcem3(:,:)*(1.-fintbc3) &
                        +pebct4%efnum(is)*dbcem4(:,:)*(1.-fintbc4))*dt
      cebcm(:,:,is)=(pebct1%efmas(is)*dbcem1(:,:)*(1.-fintbc1) &
                        +pebct2%efmas(is)*dbcem2(:,:)*(1.-fintbc2) &
                        +pebct3%efmas(is)*dbcem3(:,:)*(1.-fintbc3) &
                        +pebct4%efmas(is)*dbcem4(:,:)*(1.-fintbc4))*dt
    end do
  end if
  if (kextso4 > 0) then
    do is=1,isextso4
      cesun(:,:,is)=(pesut1%efnum(is)*dsuem1(:,:) &
                        +pesut2%efnum(is)*dsuem2(:,:) &
                        +pesut3%efnum(is)*dsuem3(:,:) &
                        +pesut4%efnum(is)*dsuem4(:,:))*dt
      cesum(:,:,is)=(pesut1%efmas(is)*dsuem1(:,:) &
                        +pesut2%efmas(is)*dsuem2(:,:) &
                        +pesut3%efmas(is)*dsuem3(:,:) &
                        +pesut4%efmas(is)*dsuem4(:,:))*dt
    end do
  end if
  !
  !     do the same as above for internally mixed types of aerosol.
  !
  if (kintoc > 0) then
    do is=1,isintoc
      ciocn(:,:,is)=(pioct1%efnum(is)*docem1(:,:)*fintoc1 &
                        +pioct2%efnum(is)*docem2(:,:)*fintoc2 &
                        +pioct3%efnum(is)*docem3(:,:)*fintoc3 &
                        +pioct4%efnum(is)*docem4(:,:)*fintoc4)*dt
      ciocm(:,:,is)=(pioct1%efmas(is)*docem1(:,:)*fintoc1 &
                        +pioct2%efmas(is)*docem2(:,:)*fintoc2 &
                        +pioct3%efmas(is)*docem3(:,:)*fintoc3 &
                        +pioct4%efmas(is)*docem4(:,:)*fintoc4)*dt
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      cibcn(:,:,is)=(pibct1%efnum(is)*dbcem1(:,:)*fintbc1 &
                        +pibct2%efnum(is)*dbcem2(:,:)*fintbc2 &
                        +pibct3%efnum(is)*dbcem3(:,:)*fintbc3 &
                        +pibct4%efnum(is)*dbcem4(:,:)*fintbc4)*dt
      cibcm(:,:,is)=(pibct1%efmas(is)*dbcem1(:,:)*fintbc1 &
                        +pibct2%efmas(is)*dbcem2(:,:)*fintbc2 &
                        +pibct3%efmas(is)*dbcem3(:,:)*fintbc3 &
                        +pibct4%efmas(is)*dbcem4(:,:)*fintbc4)*dt
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      cisun(:,:,is)=(pisut1%efnum(is)*dsuem1(:,:) &
                        +pisut2%efnum(is)*dsuem2(:,:) &
                        +pisut3%efnum(is)*dsuem3(:,:) &
                        +pisut4%efnum(is)*dsuem4(:,:))*dt
      cisum(:,:,is)=(pisut1%efmas(is)*dsuem1(:,:) &
                        +pisut2%efmas(is)*dsuem2(:,:) &
                        +pisut3%efmas(is)*dsuem3(:,:) &
                        +pisut4%efmas(is)*dsuem4(:,:))*dt
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     mass and number tendencies for externally mixed aerosol.
  !
  if (kext > 0) then
    pedndt=0.
    pedmdt=0.
  end if
  if (kextoc > 0) then
    do is=1,isextoc
      pedndt(:,:,iexoc(is))=ceocn(:,:,is)/dt
      pedmdt(:,:,iexoc(is))=ceocm(:,:,is)/dt
    end do
  end if
  if (kextbc > 0) then
    do is=1,isextbc
      pedndt(:,:,iexbc(is))=pedndt(:,:,iexbc(is)) &
                               +cebcn(:,:,is)/dt
      pedmdt(:,:,iexbc(is))=pedmdt(:,:,iexbc(is)) &
                               +cebcm(:,:,is)/dt
    end do
  end if
  if (kextso4 > 0) then
    do is=1,isextso4
      pedndt(:,:,iexso4(is))=pedndt(:,:,iexso4(is)) &
                                +cesun(:,:,is)/dt
      pedmdt(:,:,iexso4(is))=pedmdt(:,:,iexso4(is)) &
                                +cesum(:,:,is)/dt
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     total aerosol mass after accounting for changes from emissions
  !     for internally mixed types of aerosol.
  !
  if (kint > 0) then
    cimast=pimas
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      cimast(:,:,iinoc(is))=cimast(:,:,iinoc(is)) &
                               +ciocm(:,:,is)
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      cimast(:,:,iinbc(is))=cimast(:,:,iinbc(is)) &
                               +cibcm(:,:,is)
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      cimast(:,:,iinso4(is))=cimast(:,:,iinso4(is)) &
                                +cisum(:,:,is)
    end do
  end if
  !
  !     new mass fractions for internally mixed types of aerosol.
  !
  if (kint > 0) then
    do is=1,isaint
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,is) > ytiny) then
            cimaf(il,l,is,:)=pifrc(il,l,is,:)*pimas(il,l,is) &
                            /cimast(il,l,is)
          else
            cimaf(il,l,is,:)=pifrc(il,l,is,:)
          end if
        end do
      end do
    end do
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,iinoc(is)) > ytiny) then
            cimaf(il,l,iinoc(is),kintoc)=cimaf(il,l,iinoc(is),kintoc) &
                                 +ciocm(il,l,is)/cimast(il,l,iinoc(is))
          end if
        end do
      end do
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,iinbc(is)) > ytiny) then
            cimaf(il,l,iinbc(is),kintbc)=cimaf(il,l,iinbc(is),kintbc) &
                                 +cibcm(il,l,is)/cimast(il,l,iinbc(is))
          end if
        end do
      end do
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      do l=1,leva
        do il=1,ilga
          if (cimast(il,l,iinso4(is)) > ytiny) then
            cimaf(il,l,iinso4(is),kintso4)= &
                                            cimaf(il,l,iinso4(is),kintso4) &
                                            +cisum(il,l,is)/cimast(il,l,iinso4(is))
          end if
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     mass, number, and mass fraction tendencies for internally
  !     mixed types of aerosol.
  !
  if (kint > 0) then
    pidndt=0.
    pidmdt=0.
    pidfdt=(cimaf-pifrc)/dt
  end if
  if (kintoc > 0) then
    do is=1,isintoc
      pidndt(:,:,iinoc(is))=ciocn(:,:,is)/dt
      pidmdt(:,:,iinoc(is))=ciocm(:,:,is)/dt
    end do
  end if
  if (kintbc > 0) then
    do is=1,isintbc
      pidndt(:,:,iinbc(is))=pidndt(:,:,iinbc(is)) &
                               +cibcn(:,:,is)/dt
      pidmdt(:,:,iinbc(is))=pidmdt(:,:,iinbc(is)) &
                               +cibcm(:,:,is)/dt
    end do
  end if
  if (kintso4 > 0) then
    do is=1,isintso4
      pidndt(:,:,iinso4(is))=pidndt(:,:,iinso4(is)) &
                                +cisun(:,:,is)/dt
      pidmdt(:,:,iinso4(is))=pidmdt(:,:,iinso4(is)) &
                                +cisum(:,:,is)/dt
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocation.
  !
  if (isextoc > 0) then
    deallocate (ceocn)
    deallocate (ceocm)
  end if
  if (isextbc > 0) then
    deallocate (cebcn)
    deallocate (cebcm)
  end if
  if (isextso4 > 0) then
    deallocate (cesun)
    deallocate (cesum)
  end if
  if (isintoc > 0) then
    deallocate (ciocn)
    deallocate (ciocm)
  end if
  if (isintbc > 0) then
    deallocate (cibcn)
    deallocate (cibcm)
  end if
  if (isintso4 > 0) then
    deallocate (cisun)
    deallocate (cisum)
  end if
  if (isaint > 0) then
    deallocate (cimast)
    deallocate (cimaf)
  end if
  !
end subroutine priaem
