!> \file
!> \brief This routine calculates the surface dust emission flux
!>       in two sections for PLA
!>
!! @authors K. von Salzen, M. Lazare, and Y. Peng
!
!-----------------------------------------------------------------------
subroutine dufcc (flnd,gt,smfr,fn,zspd,ustard, &
                        suz0,slai,spot,st02,st03,st04,st06, &
                        st13,st14,st15,st16,st17, &
                        uth,srel,srelv,su_srelv, &
                        tdflx,tdsize,tdvar,tdmask,ilga,isec, &
                        isvdust,defa,defc, &
                        fall,fa10,fa2,fa1, &
                        duwd,dust,duth,usmk, &
                        diagmas,diagnum,isdust)
  !
  use duparm1, only : aeff,cuscpla,dmin,dstep,gravi, &
                           maxai,maxci,minai,minci, &
                           nats,nbin,nclass,nmode,nspe,ntrace, &
                           pi,roa,rop,solspe,umin,w0,xeff,z01,z02,z0s
  !
  implicit none
  !
  !      * i/o fields.
  !
  integer, intent(in) :: ilga !< Number of grid points in the horizontal direction
  integer, intent(in) :: isec !< Number of size sections
  integer, intent(in) :: isvdust !< Switch for turning on/off extra diagnostic calculations for mineral dust processes
  integer, intent(in) :: isdust !< Extra output for mineral dust
  real, intent(out), dimension(ilga,isdust*2) :: diagmas !< Diagnostic calculation for model output of mass \f$[kg/m^2/sec]\f$
  real, intent(out), dimension(ilga,isdust*2) :: diagnum !< Diagnostic calculation for model output of number \f$[1/m^2/sec]\f$
  real, intent(in),  dimension(ilga) :: flnd !< Fraction of land
  real, intent(in),  dimension(ilga) :: gt !< Surface temperature, \f$[K]\f$
  real, intent(in),  dimension(ilga) :: smfr !< Soil moisture fraction
  real, intent(in),  dimension(ilga) :: fn !< Fraction
  real, intent(in),  dimension(ilga) :: zspd !< Model surface horizontal wind speed \f$[m/sec]\f$
  real, intent(in),  dimension(ilga) :: ustard !< Friction velocity \f$[m/sec]\f$
  real, intent(in),  dimension(ilga) :: suz0 !< Aerodynamic roughness length for bare ground
  real, intent(in),  dimension(ilga) :: slai !<
  real, intent(in),  dimension(ilga) :: spot !< Compositional fraction of preferential soil type 1 (paleo lake bed).
  real, intent(in),  dimension(ilga) :: st02 !< Compositional fraction of soil type 2 (medium)
  real, intent(in),  dimension(ilga) :: st03 !< Compositional fraction of soil type 3 (fine)
  real, intent(in),  dimension(ilga) :: st04 !< Compositional fraction of soil type 4 (coarse medium)
  real, intent(in),  dimension(ilga) :: st06 !< Compositional fraction of soil type 6 (medium fine)
  real, intent(in),  dimension(ilga) :: st13 !< Compositional fraction of soil type 13 (Asian taklimakan)
  real, intent(in),  dimension(ilga) :: st14 !< Compositional fraction of soil type 14 (Asian loess)
  real, intent(in),  dimension(ilga) :: st15 !< Compositional fraction of soil type 15 (Asian gobi)
  real, intent(in),  dimension(ilga) :: st16 !< Compositional fraction of soil type 16 (Asian desert and sandland)
  real, intent(in),  dimension(ilga) :: st17 !< Compositional fraction of soil type 17 (Asian other mixture soil)
  real, dimension(ilga,isec), intent(out) :: tdflx !< Dust emission flux
  real, dimension(ilga,isec), intent(out) :: tdsize !< Dust emission size
  real, dimension(ilga,isec), intent(out) :: tdvar !< Dust emission variance
  real, dimension(ilga) :: tdmask !< Dust emissions mask
  real, intent(out), dimension(ilga) :: defa !< Emission flux of mineral dust mass for accumulation mode \f$[kg/m^2/sec]\f$
  real, intent(out), dimension(ilga) :: defc !< Emission flux of mineral dust mass for coarse mode) \f$[kg/m2/sec]\f$
  real, intent(out), dimension(ilga) :: fall !< Fluxall array in mineral dust calculation
  real, intent(out), dimension(ilga) :: fa10 !< FA10 in mineral dust calculations
  real, intent(out), dimension(ilga) :: fa2 !< FA2 in mineral dust calculations
  real, intent(out), dimension(ilga) :: fa1 !< FA1 in mineral dust calculations
  real, intent(out), dimension(ilga) :: duwd !< Wind speed in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: dust !< Friction velocity in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: duth !< Friction velocity threshold in mineral dust calculation \f$[m/sec]\f$
  real, intent(out), dimension(ilga) :: usmk !< Time fraction of threshold exceedence in mineral dust calculation
  !
  !      * internal work fields.
  !
  real, dimension(nclass) :: uth !<
  real, dimension(nats,nclass) :: srel !<
  real, dimension(nats,nclass) :: srelv !<
  real, dimension(nats,nclass) :: su_srelv !<
  !
  real, dimension(ntrace) :: dpk !<
  real, dimension(ntrace) :: dbmin !<
  real, dimension(ntrace) :: dbmax !<
  real, dimension(ilga) :: c_eff !<
  real, dimension(ilga) :: dust_mask !<
  real, dimension(ilga) :: ustar_d !<
  real, dimension(ilga) :: du_wind !<
  real, dimension(ilga) :: du_th !<
  real, dimension(ilga) :: ustar_acrit !<
  real, dimension(ilga) :: duz01 !<
  real, dimension(ilga) :: duz02 !<
  real, dimension(ilga) :: st01 !<
  !
  real, dimension(ilga,ntrace) :: flux_6h !<
  real, dimension(ilga,ntrace) :: fluxbin !<
  real, dimension(ilga,nclass) :: fluxtyp !<
  real, dimension(ilga,nclass) :: fluxdiam1 !<
  real, dimension(ilga,nclass) :: fluxdiam2 !<
  real, dimension(ilga,nclass) :: fluxdiam3 !<
  real, dimension(ilga,nclass) :: fluxdiam4 !<
  real, dimension(ilga,nclass) :: fluxdiam6 !<
  real, dimension(ilga,nclass) :: fluxdiam_pf !<
  real, dimension(ilga,nclass) :: fluxdiam13 !<
  real, dimension(ilga,nclass) :: fluxdiam14 !<
  real, dimension(ilga,nclass) :: fluxdiam15 !<
  real, dimension(ilga,nclass) :: fluxdiam16 !<
  real, dimension(ilga,nclass) :: fluxdiam17 !<
  real, dimension(ilga) :: fluxall !<
  real, dimension(ilga) :: fluxa10 !<
  real, dimension(ilga) :: fluxa2 !<
  real, dimension(ilga) :: fluxa1 !<
  real, dimension(ilga) :: flux_ai !<
  real, dimension(ilga) :: flux_ci !<
  !
  !      * auxiliary variables
  !
  real :: dpb(nclass+1) !<
  real :: dpc(nclass) !<
  real, dimension(ilga,nclass) :: fluxmas !<
  real, dimension(ilga,nclass) :: fluxnum !<
  real :: drb(isec) !<
  real :: dre(isec) !<
  real :: drm(isec) !<
  real, dimension(ilga,isec) :: flux_num !<
  integer :: inb(isec) !<
  integer :: ine(isec) !<
  !
  real :: aaa !<
  real :: bb !<
  real :: ccc !<
  real :: ddd !<
  real :: ee !<
  real :: ff !<
  real :: cd !<
  real :: st12346 !<
  real :: d1 !<
  real :: feff !<
  real :: rdp !<
  real :: dlast !<
  real :: ustar !<
  real :: uthp(ilga,nats) !<
  real :: alpha !<
  real :: waa(nats) !<
  real :: wbb(ilga) !<
  real :: fdp1 !<
  real :: fdp2 !<
  !
  integer :: is !<
  integer :: il !<
  integer :: j !<
  integer :: kk !<
  integer :: n !<
  integer :: nn !<
  integer :: i_soil !<
  integer :: ino !<
  integer :: isf !<
  integer :: kkmin !<
  integer :: kkk !<
  integer :: dn(ilga) !<
  integer :: dk(ilga,nclass) !<
  !
  integer :: zschm !<
  integer :: wschm !<
  integer :: solschm !<
  !       define z0 scheme, 1: new scheme prigent et al 2002 \cite prigent2005, 0: old scheme z01=z02=0.001
  data zschm /1/
  !       define soil water scheme, 1: new scheme fecan \cite fecan1999, 0: old scheme 0.99
  data wschm /1/
  !       define soil property scheme, 1: new scheme cheng \cite cheng2008, 0: old scheme
  data solschm /0/
  !    --------------------------------------------------------------
  !     * initialization of diagnostic arrays.
  !
  if (isvdust>0) then
    defa=0.
    defc=0.
    fall=0.
    fa10=0.
    fa2=0.
    fa1=0.
    duwd=0.
    dust=0.
    duth=0.
    usmk=0.
  end if

  !      initial calculation (c_eff)
  !
  !      effective fraction calculation (roughness z0 related)
  !      constant z0 temporarily
  !      marticorena et al., 1997, equations (15)-(17)\cite marticorena1997
  !
  !      initial values ! d1 is set to zero temporarily
  !
  d1 = 0.
  feff = 0.
  aaa = 0.
  bb = 0.
  ccc = 0.
  ddd = 0.
  ee = 0.
  ff = 0.
  do il=1,ilga
    c_eff(il)       = 0.0
    duz01(il)       = 0.0
    duz02(il)       = 0.0
  end do
  !
  do il=1,ilga
    !
    if (zschm == 1) then
      !       new z0 scheme, monthly average
      duz01(il)=suz0(il)
      duz02(il)=suz0(il)
    else
      !       old z0 scheme
      duz01(il)=z01
      duz02(il)=z02
    end if
    !
    if (duz01(il)==0..or.duz01(il)<z0s.or.duz02(il)<z0s) then
      feff = 0.
    else
      aaa = log(duz01(il)/z0s)
      bb = log(aeff*(xeff/z0s)**0.8)
      ccc = 1.- aaa/bb
      if (d1 == 0.) then
        ff = 1.
      else
        ddd = log(duz02(il)/duz01(il))
        ee = log(aeff*(d1/duz01(il))**0.8)
        ff = 1.-ddd/ee
      end if  ! d1=0
      feff = ff*ccc
      if (feff < 0.) feff = 0.
      if (feff > 1.) feff = 1.
    end if  ! z01=0
    !
    c_eff(il) = feff
    !
  end do  ! il
  !
  !      initial calculation (c_eff) done
  !
  !   --------------------------------------------------------------
  !
  !     initial calculation (winds and masks)
  !
  !      ! initial values
  !
  ustar=0.0
  rdp=0.0
  dlast=0.0
  do kk=1,nclass
    dpb(kk)  = 0.0
    dpc(kk)  = 0.0
  end do
  do il=1,ilga
    uthp(il,:)      = 0.0
    dust_mask(il)   = 0.0
    ustar_d(il)     = 0.0
    du_wind(il)     = 0.0
    du_th(il)       = 0.0
    ustar_acrit(il) = 0.0
  end do
  !
  !      calculate bin minimum/maximum radius in um
  !      rdp is diameter in cm
  !
  rdp=dmin
  dpb(1)=rdp
  dlast=dmin
  do nn=1,ntrace
    dpk(nn)=0.
    dbmin(nn)=0.
    dbmax(nn)=0.
  end do
  !
  nn=1
  do kk=1,nclass
    if (mod(kk,nbin)==0) then
      dbmax(nn)=rdp*5000.
      dbmin(nn)=dlast*5000.
      dpk(nn)=sqrt(dbmax(nn)*dbmin(nn))
      nn=nn+1
      dlast=rdp
    end if
    rdp=rdp*exp(dstep)
    dpb(kk+1)=rdp
  end do
  !
  !      calculate the friction wind speed ustar
  !
  do il=1,ilga
    !      set wind speed, use surface (10m) wind instead of zspd
    du_wind(il)=zspd(il)
    if (duz02(il) == 0.) then
      ustar=0.
    else
      ustar=100.*ustard(il)
    end if
    ustar_d(il)=ustar
    !
    !         if (c_eff(il) == 0.) then
    du_th(il)=0.
    !         else
    !          du_th(il)=umin*cuscpla/c_eff(il)
    !         end if
    !
    !      check critical wind speed (wind stress threshold scaled)
    if (ustar>0..and.ustar>=du_th(il)) then
      if (slai(il)>0.0 .and. duz02(il)>0.0) then
        !      set grid point as a potential dust source point
        dust_mask(il)=1.
      end if
      !      store the time fraction with ustar > dust threshold for output
      ustar_acrit(il)=1.
    else
      ustar_acrit(il)=0.
    end if   ! ustar
    !
  end do    ! il
  !
  !
  !      initial calculation (winds and masks) done
  !
  !   --------------------------------------------------------------
  !
  !      emission flux calculation
  !
  !      ! initialization
  !
  alpha=0.0
  fdp1=0.0
  fdp2=0.0
  do is=1,isec
    drb(is)=0.0
    dre(is)=0.0
    drm(is)=0.0
    inb(is)=0.0
    ine(is)=0.0
  end do
  do j=1,nats
    waa(j)=0.0
  end do
  !
  do il=1,ilga
    wbb(il)=0.0
    dn(il) =0
    st01(il)=1.
    !
    do is=1,isec
      flux_num(il,is)=0.
    end do
    !
    do nn=1,ntrace
      fluxbin(il,nn)=0.
      flux_6h(il,nn)=0.
    end do
    !
    do kk=1,nclass
      dk(il,kk)     =0
      fluxtyp(il,kk)=0.
      fluxmas(il,kk)=0.
      fluxnum(il,kk)=0.
      fluxdiam1(il,kk)=0.
      fluxdiam2(il,kk)=0.
      fluxdiam3(il,kk)=0.
      fluxdiam4(il,kk)=0.
      fluxdiam6(il,kk)=0.
      fluxdiam_pf(il,kk)=0.
      fluxdiam13(il,kk)=0.
      fluxdiam14(il,kk)=0.
      fluxdiam15(il,kk)=0.
      fluxdiam16(il,kk)=0.
      fluxdiam17(il,kk)=0.
    end do
    !
  end do ! il
  !
  !      ! calculation
  !
  cd=1.*roa/gravi
  !
  do kk=1,nclass
    do il=1,ilga
      if (dust_mask(il)==1 .and. c_eff(il)>0.) then
        !
        !         soil moisture modification
        !
        if (wschm == 1) then
          !          new ws scheme
          if (flnd(il)>0. .and. gt(il)>273.15) then
            wbb(il)=min(smfr(il)/rop,1.)*100.
          else
            wbb(il)=0.
          end if
          do j=1,nats
            waa(j)=0.0014*(solspe(nspe-2,j)*100.)**2+0.17* &
               (solspe(nspe-2,j)*100.)
            if (wbb(il)<=waa(j).or.waa(j)==0.) then
              uthp(il,j)=uth(kk)
            else
              uthp(il,j)=uth(kk)*sqrt(1.+1.21*(wbb(il)-waa(j))**0.68)
            end if
          end do ! j
        else ! soil moisture
          !          old ws scheme
          do j=1,nats
            uthp(il,j)=uth(kk)
          end do ! j
        end if ! soil moisture
        !
        !         fluxdiam calculation (fdp1,fdp2 follows marticorena)
        !
        !           flux for soil type #1
        i_soil=1
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam1(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !           flux for soil type #2
        i_soil=2
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam2(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !           flux for soil type #3
        i_soil=3
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam3(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !           flux for soil type #4
        i_soil=4
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam4(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !           flux for soil type #6
        i_soil=6
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam6(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !           flux for soil type in preferential source area
        i_soil=10
        fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
        fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
        if (fdp1 > 0.) then
          alpha=solspe(nmode*3+1,i_soil)
          fluxdiam_pf(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
        end if
        !
        !          add asian soil types
        if (solschm==1) then
          !          flux for soil type #13
          i_soil=13
          fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
          fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
          if (fdp1 > 0.) then
            alpha=solspe(nmode*3+1,i_soil)
            fluxdiam13(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
          end if
          !
          !           flux for soil type #14
          i_soil=14
          fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
          fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
          if (fdp1 > 0.) then
            alpha=solspe(nmode*3+1,i_soil)
            fluxdiam14(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
          end if
          !
          !           flux for soil type #15
          i_soil=15
          fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
          fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
          if (fdp1 > 0.) then
            alpha=solspe(nmode*3+1,i_soil)
            fluxdiam15(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
          end if
          !
          !           flux for soil type #16
          i_soil=16
          fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
          fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
          if (fdp1 > 0.) then
            alpha=solspe(nmode*3+1,i_soil)
            fluxdiam16(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
          end if
          !
          !           flux for soil type #17
          i_soil=17
          fdp1=(1.-(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))
          fdp2=(1.+(uthp(il,i_soil)/(c_eff(il)*ustar_d(il))))**2
          if (fdp1 > 0.) then
            alpha=solspe(nmode*3+1,i_soil)
            fluxdiam17(il,kk)=max(0.,srel(i_soil,kk) &
             *fdp1*fdp2*cd*ustar_d(il)**3*alpha)
          end if
          !
        end if ! asian soil
        !
        !     initialize fluxtyp of all soil types at first bin (kk=1)
        !
        if (kk==1) then
          if (solschm==1) then
            fluxtyp(il,kk)=fluxtyp(il,kk) &
                       +fluxdiam1(il,kk)*(1.-spot(il))* &
          (st01(il)-st02(il)-st03(il)-st04(il)-st06(il) &
          -st13(il)-st14(il)-st15(il)-st16(il)-st17(il)) &
                       +fluxdiam2(il,kk)*(1.-spot(il))*st02(il) &
                       +fluxdiam3(il,kk)*(1.-spot(il))*st03(il) &
                       +fluxdiam4(il,kk)*(1.-spot(il))*st04(il) &
                       +fluxdiam6(il,kk)*(1.-spot(il))*st06(il) &
                       +fluxdiam13(il,kk)*(1.-spot(il))*st13(il) &
                       +fluxdiam14(il,kk)*(1.-spot(il))*st14(il) &
                       +fluxdiam15(il,kk)*(1.-spot(il))*st15(il) &
                       +fluxdiam16(il,kk)*(1.-spot(il))*st16(il) &
                       +fluxdiam17(il,kk)*(1.-spot(il))*st17(il) &
                       +fluxdiam_pf(il,kk)*spot(il)
          else
            fluxtyp(il,kk)=fluxtyp(il,kk) &
                       +fluxdiam1(il,kk)*(1.-spot(il))* &
          (st01(il)-st02(il)-st03(il)-st04(il)-st06(il)) &
                       +fluxdiam2(il,kk)*(1.-spot(il))*st02(il) &
                       +fluxdiam3(il,kk)*(1.-spot(il))*st03(il) &
                       +fluxdiam4(il,kk)*(1.-spot(il))*st04(il) &
                       +fluxdiam6(il,kk)*(1.-spot(il))*st06(il) &
                       +fluxdiam_pf(il,kk)*spot(il)
          end if ! asian soil
        else
          dn(il)=dn(il)+1           ! increase number of dislocated particle classes
          dk(il,dn(il))=kk          ! store dislocated particle class
        end if  ! kk=1
        !
      end if     ! dust_mask=1 and c_eff>1
    end do      ! il
  end do       ! kk
  !
  !      calculate fluxtyp of all soil types for all 192 bins (kkk=2~192)
  !
  kkmin=1
  do il=1,ilga
    if (dust_mask(il)==1 .and. c_eff(il)>0.) then
      do n=1,dn(il)
        do kkk=1,dk(il,n)           ! scaling with relative contribution of dust size fraction
          i_soil=1
          if (solschm==1) then
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *(st01(il)-st02(il)-st03(il)-st04(il)-st06(il) &
                               -st13(il)-st14(il)-st15(il)-st16(il)-st17(il)) &
                               *fluxdiam1(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          else
            fluxtyp(il,kkk) = fluxtyp(il,kkk) + &
                                   max(st01(il)-st02(il)-st03(il)-st04(il)-st06(il), 0.) * &              ! ar: could be negative, leads to fail in dustp: log(tdsize(il,is)/r0)
                                   fluxdiam1(il,dk(il,n))*srelv(i_soil,kkk)/((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          end if  ! asian soil
          !
          i_soil=2
          fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st02(il) &
                               *fluxdiam2(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          !
          i_soil=3
          fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st03(il) &
                               *fluxdiam3(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          !
          i_soil=4
          fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st04(il) &
                               *fluxdiam4(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          !
          i_soil=6
          fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st06(il) &
                               *fluxdiam6(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          !
          if (du_wind(il)> 8.)  i_soil=11
          if (du_wind(il)<= 8.)  i_soil=10
          fluxtyp(il,kkk) = fluxtyp(il,kkk)+spot(il) &
                               *fluxdiam_pf(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
          !
          if (solschm==1) then
            i_soil=13
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st13(il) &
                               *fluxdiam13(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
            !
            i_soil=14
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st14(il) &
                               *fluxdiam14(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
            !
            i_soil=15
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st15(il) &
                               *fluxdiam15(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
            !
            i_soil=16
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st16(il) &
                               *fluxdiam16(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
            !
            i_soil=17
            fluxtyp(il,kkk) = fluxtyp(il,kkk)+(1.-spot(il)) &
                               *st17(il) &
                               *fluxdiam17(il,dk(il,n))*srelv(i_soil,kkk)/ &
                               ((su_srelv(i_soil,dk(il,n))-su_srelv(i_soil,kkmin)))
            !
          end if ! asian soil
          !
        end do  ! kkk
      end do   ! n
    end if    ! dust_mask
  end do      ! il
  !
  !     calculate fluxes in each internal tracer classes (8)
  !
  do nn=1,ntrace
    do il=1,ilga
      if (dust_mask(il)==1 .and. c_eff(il)>0.) then
        do kk=(nn-1)*nbin+1,nn*nbin
          fluxbin(il,nn)=fluxbin(il,nn)+fluxtyp(il,kk)
        end do
        ! mask out dust fluxes, where soil moisture threshold is reached
        if (smfr(il)>w0) fluxbin(il,nn)=0.
        ! fluxbin: g/cm2/sec; flux_6h: g/m2/sec
        ! exclude snow covered region and weighted by vegetation cover
        if (fn(il)>0.1) fluxbin(il,nn)=0.
        flux_6h(il,nn)=fluxbin(il,nn)*10000.*slai(il)
      end if ! dust_mask
    end do  ! il
  end do   ! nn
  !
  !     emission flux calculation done
  !
  !   --------------------------------------------------------------
  !
  !      diagnostic calculation for model output
  !
  !      ! initialization for output arrays
  !
  do il=1,ilga
    fluxall(il)=0.
    fluxa10(il)=0.
    fluxa2(il)=0.
    fluxa1(il)=0.
    flux_ai(il)=0.
    flux_ci(il)=0.
    tdflx(il,:)=0.
    tdsize(il,:)=0.
    tdvar(il,:)=0.
    tdmask(il)=0.
    !
    do isf=1,isdust*2
      diagmas(il,isf)= 0.0
      diagnum(il,isf)= 0.0
    end do
    !
  end do
  !
  do il=1,ilga
    if (dust_mask(il)==1 .and. c_eff(il)>0.) then
      !
      !       unit of emission flux: flux_6h [g/m2/sec]
      !
      do nn=1,ntrace
        fluxall(il)=fluxall(il)+flux_6h(il,nn)
        if (dbmax(nn)<=10.) fluxa10(il)=fluxa10(il)+flux_6h(il,nn)
        if (dbmax(nn)<=2.) fluxa2(il)=fluxa2(il)+flux_6h(il,nn)
        if (dbmax(nn)<=1.) fluxa1(il)=fluxa1(il)+flux_6h(il,nn)
      end do
      !
      !       sum over internal tracer classes for ai and ci modes
      !
      !       accumulation mode
      !
      do nn=minai,maxai
        flux_ai(il)=flux_ai(il)+flux_6h(il,nn)
      end do
      !
      !       coarse mode
      !
      do nn=minci,maxci
        flux_ci(il)=flux_ci(il)+flux_6h(il,nn)
      end do
      !
      !       emission mass flux: g/m2/sec -> kg/m2/sec
      !
      flux_ai(il)=max(flux_ai(il)*1.e-3,0.)
      flux_ci(il)=max(flux_ci(il)*1.e-3,0.)
      fluxall(il)=max(fluxall(il)*1.e-3,0.)
      fluxa10(il)=max(fluxa10(il)*1.e-3,0.)
      fluxa2(il)=max(fluxa2(il)*1.e-3,0.)
      fluxa1(il)=max(fluxa1(il)*1.e-3,0.)
      !
      !       save dust emission fluxes
      !
      if (isvdust>0) then
        defa(il)=flux_ai(il)
        defc(il)=flux_ci(il)
        !
        fall(il)=fluxall(il)
        fa10(il)=fluxa10(il)
        fa2(il) =fluxa2(il)
        fa1(il) =fluxa1(il)
      end if
      !
    end if ! (dust_mask and c_eff)
    !
    !       save dust wind components
    !
    if (isvdust>0) then
      duwd(il)=du_wind(il)
      dust(il)=ustar_d(il)
      duth(il)=du_th(il)
      usmk(il)=ustar_acrit(il)
    end if
    !
  end do ! il
  !
  !   --------------------------------------------------------------
  !
  !     dust emission for pla sections and diagnostic for checking
  !
  !       ! unit of radius
  !
  do kk=1,nclass+1
    dpb(kk)=dpb(kk)*0.5*1.e4          ! convert unit to um
  end do
  !
  !      dust emission size distri. (for diagnostics)
  !
  do il=1,ilga
    if (dust_mask(il)==1 .and. c_eff(il)>0.) then
      !
      do kk=1,nclass
        !        unit of dust emission mass flux
        !        fluxtyp: g/cm2/sec; fluxmas: g/m2/sec
        fluxmas(il,kk)=fluxtyp(il,kk)*10000.*slai(il)
        !        mask out dust fluxes, where soil moisture threshold is reached
        if (smfr(il)>w0) fluxmas(il,kk) = 0.
        !        mask out dust fluxes where snow present
        if (fn(il)>0.1)  fluxmas(il,kk)=0.
      end do
      !
      !       convert mass flux to number conc.
      !
      do kk=1,nclass
        dpc(kk)=dpb(kk)+(dpb(kk+1)-dpb(kk))/2.
        fluxnum(il,kk)=1.e12*fluxmas(il,kk)/(rop* &
                          4./3.*pi*dpc(kk)**3)
        !
        !       ! fluxmas: g/m2/sec; fluxnum: 1/m2/sec
        !
        if (isvdust>0) then
          diagmas(il,kk)=fluxmas(il,kk)/log(dpb(kk+1)/dpb(kk))
          diagnum(il,kk)=fluxnum(il,kk)/log(dpb(kk+1)/dpb(kk))
        end if
      end do
      !
      !        ! diagmas: kg/m2/sec; diagnum: 1/m2/sec
      !
      do isf=1,isdust*2
        diagmas(il,isf)=max(diagmas(il,isf)*1.e-3,0.)
        diagnum(il,isf)=max(diagnum(il,isf),0.)
      end do
      !
    end if  ! (dust_mask and c_eff)
  end do   ! il
  !
  !      write to dust emission size distri. (for diagnostics)
  !
  !      setup boundary size of each pla section
  !
  if (isec==1) then
    inb(1)=minai
    ine(1)=maxci
  else if (isec==2) then
    inb(1)=minai
    ine(1)=maxai
    inb(2)=minci
    ine(2)=maxci
  else
    call xit('DUFCC',-1)
  end if
  !
  do il=1,ilga
    !
    do is=1,isec
      do kk=1,nclass
        if (mod(kk,nbin) == 0 .and. kk/nbin == ine(is)) then
          dre(is)=dpb(kk+1)
        end if
        !
        if (kk > (inb(is)-1)*nbin .and. kk <= ine(is)*nbin) then
          flux_num(il,is) = flux_num(il,is) + fluxnum(il,kk)
          drm(is) = drm(is) + fluxnum(il,kk)*dpc(kk)
        end if
      end do  ! kk
    end do    ! is
    !
    drb(1) = dpb(1)
    do is=2,isec
      drb(is)=dre(is-1)
    end do ! is
    !
    !       dust emission size distri. parameters in pla sections
    !
    ino=0
    do is=1,isec
      if (flux_num(il,is) <= 0. ) ino=1
    end do
    if (ino==0) then
      tdmask(il)=1.
      do is=1,isec
        drm(is)=drm(is)/flux_num(il,is)
        tdsize(il,is)=drm(is)*1.e-6
        tdflx(il,is) =flux_num(il,is)/log(dre(is)/drb(is))
      end do    ! is
      if (isec==1) then
        tdvar(il,1)=2.3
      else if (isec==2) then
        tdvar(il,1)=2.5
        tdvar(il,2)=1.5
      end if
    end if
    !
  end do     ! il
  !
  !   --------------------------------------------------------------
  !
  return
  !
end subroutine dufcc
